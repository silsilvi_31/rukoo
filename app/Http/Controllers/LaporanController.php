<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class LaporanController extends Controller
{
    private $transaksi;
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
        $this->transaksi = DB::table('transaksi')->get();
    }

    public function index()
    {
        return view('admin.laporan.index');
    }

    public function lap_jurnal()
    {
        $jurnal_sj = DB::table('jurnal')
                                ->orderBy('tgl', 'ASC')
                                ->orderBy('jenis_jurnal', 'DESC')
                                ->get();
        
        $data['jurnal_sj'] = $jurnal_sj;
        // $data['jurnal_gj'] = $jurnal_gj;
        // $data['jurnal_bl'] = $jurnal_bl;
        // $data['jurnal_sj'] = $jurnal_sj;

        return view('admin.laporan.lap_jurnal')->with($data);
    }

    public function lap_penjualan()
    {
        return view('admin.laporan.lap_penjualan');
    }

    public function table_penjualan(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->tglAwal));
        $tgl_akhir = date('Y-m-d', strtotime($req->tglAkhir));

        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tglAkhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $sj = DB::table('suratjalan')
                            ->whereDate('tgl', '<', $tglAkhir)
                            ->get();

        $id_sj = [];

        foreach ($sj as $value) {
            $id_sj[] = $value->id;
        }
        
        $data = DB::table('suratjalan as sj')
                            ->where('sj.status',NULL)
                            ->whereNotIn('sj.id', $id_sj)
                            ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                            ->leftJoin('pelanggan as pl', 'sj.member', '=', 'pl.id')
                            ->select('sj.id','sj.tgl','sj.pelanggan','pl.nama','sj.member','sj.catatan','sj.total','sj.bayar','sj.tgl','sj.opsi','sj.ongkir','sj.pembayaran',
                                        'sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.is_batal','sj.batal',
                                            'kar.nama as sopir')
                            ->orderBy('sj.tgl', 'DESC')
                            ->orderBy('sj.id', 'DESC')
                            ->whereBetween('sj.tgl', [$tgl_awal, $tgl_akhir])
                            ->get(); 

        $transaksi = $this->transaksi;

        return datatables::of($data)
        ->editColumn('pelanggan', function ($data) {
            $nama = isset($data->pelanggan) ? $data->pelanggan : $data->nama; 
            return $nama;
        })
        ->addColumn('status_bayar', function ($data) {
            $status_bayar = isset($data->pembayaran) ? '<span class="badge badge-success" style="color:white">Lunas</span>' : '<span class="badge badge-secondary" style="color:white">Belum Lunas</span>';
            return $status_bayar;
        })
        ->editColumn('status', function ($data)
        {
            $status = isset($data->is_batal) ? '<span class="badge badge-danger" style="color:white">Batal</span> ('.$data->batal.')' : '' ;
            return $status;
        })
        ->rawColumns(['status_bayar', 'status'])
        ->make(true);
    }

    public function lap_penjualan_pelanggan()
    {
        return view('admin.laporan.lap_penjualan_pelanggan');
    }

    public function get_satuan($id_satuan)
    {
        $satuan = DB::table('satuan')->where('status', null)->where('id', $id_satuan)->first();

        return isset($satuan) ? $satuan->nama : '';
    }

    public function get_member($id_member)
    {
        $pelanggan = DB::table('pelanggan')->where('status', null)->where('id', $id_member)->first();

        return isset($pelanggan) ? $pelanggan->nama : '';
    }

    public function get_penjualan($sj, $member)
    {
        $dty = array_filter($sj->toArray(), function ($value) use ($member) {
            return $value->id_pelanggan == $member;
        });

        return $dty;
    }

    public function table_penjualan_pelanggan(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->_tglAwal));
        $tgl_akhir = date('Y-m-d', strtotime($req->_tglAkhir));

        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();
                            
        $tglAkhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';
        $sjQ = DB::table('suratjalan')
                        ->whereDate('tgl', '<', $tglAkhir)
                        ->get();
                
        $id_sjQ = [];

        foreach ($sjQ as $value) {
            $id_sjQ[] = $value->id;
        }

        $pelanggan = DB::table('pelanggan')->get();

        $sj = DB::table('suratjalan as a')
                                ->leftJoin('suratjalan_detail as b', 'a.id', '=', 'b.id_sj')
                                ->leftJoin('pelanggan as c', 'a.member', '=', 'c.id')
                                ->leftJoin('satuan as d', 'b.satuan', '=', 'd.id')
                                ->whereNotNull('is_cek_nota')
                                ->whereNull('is_batal')
                                ->whereNotNull('member')
                                ->whereBetween('a.tgl', [$tgl_awal, $tgl_akhir])
                                ->whereNotIn('a.id', $id_sjQ)
                                ->select('a.id as id_sj','a.tgl','a.pembayaran','a.total','a.is_cek_nota','a.is_batal','a.member as id_pelanggan',
                                            'b.nama_brg','b.qty','b.harga','b.subtotal',
                                                'c.nama',
                                                    'd.nama as satuan');

        $data_sj = $sj->get();

        $grup_member = $sj->groupBy('member')->whereNotNull('member')->get();

        $dt = [];
        foreach ($grup_member as $value) {
            $dt[] = (object) [
                'id_pelanggan' => $value->id_pelanggan,
                'pelanggan' => $value->nama,
                'penjualan' => $this->get_penjualan($data_sj, $value->id_pelanggan)
            ];
        }

        $dt_sj = [];
        $total = 0;

        foreach ($dt as $x) {
            $dt_sj[] = (object) [
                        'keterangan' => '<span style="color:green">'.$x->pelanggan.'</span>',
                        'id_sj' => '',
                        'nama_brg' => '',
                        'satuan' => '',
                        'qty' => '',
                        'harga' => '',
                        'subtotal' => '',
                        'total' => '',
                        'tg' => '',
                    ];
            $total = 0;
            foreach ($x->penjualan as $y) {
                $total += $y->subtotal;
                $dt_sj[] = (object) [
                    'keterangan' => date('d-m-Y', strtotime($y->tgl)) ,
                    'id_sj' => $y->id_sj,
                    'nama_brg' => $y->nama_brg,
                    'satuan' => $y->satuan,
                    'qty' => $y->qty,
                    'harga' => $y->harga,
                    'subtotal' => $y->subtotal,
                    'total' => $y->total,
                    'tg' => $total
                ];

                // $total += $y->subtotal;
            } 

            $dt_sj[] = (object) [
                'keterangan' => '',
                'id_sj' => '',
                'nama_brg' => '',
                'satuan' => '',
                'qty' => '',
                'harga' => '<strong>Total || '.$x->pelanggan.'</strong>',
                'subtotal' => $total,
                'total' => $total
            ];            
        }
        $dataQ['data'] = $dt_sj;
        // dd($dataQ);
        return response()->json($dataQ);
    }
    
    public function lap_penjualan_barang()
    {
        return view('admin.laporan.lap_penjualan_barang');
    }

    public function table_penjualan_barang(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->_tglAwal));
        $tgl_akhir = date('Y-m-d', strtotime($req->_tglAkhir));

        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderby('created_at', 'DESC')
                                ->first();

        $tglAkhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $sj = DB::table('suratjalan')
                            ->whereDate('tgl', '<', $tglAkhir)
                            ->get();

        $id_sjQ = [];
        foreach ($sj as $value) {
            $id_sjQ[] = $value->id;
        }

        $sj_detail = DB::table('suratjalan_detail as a')
                                        ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                                        ->leftJoin('satuan as c', 'a.satuan', '=', 'c.id')
                                        ->select('a.id_brg', 'a.nama_brg', 'c.nama as nama_satuan','b.id', 'b.tgl',
                                                    DB::raw('sum(qty) as qty_terjual'),
                                                    DB::raw('sum(subtotal) as total_nilai'))
                                        ->whereNull('b.is_batal')
                                        ->whereBetween('b.tgl', [$tgl_awal, $tgl_akhir])
                                        ->whereNotIn('a.id_sj', $id_sjQ)
                                        ->groupBy('id_brg');
                                        // ->get();

        $max_qty_terjual = DB::table('suratjalan_detail as a')
                                ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                                ->leftJoin('satuan as c', 'a.satuan', '=', 'c.id')
                                ->select('a.id_brg', 'a.nama_brg', 'c.nama as nama_satuan', 'b.tgl',
                                            DB::raw('sum(qty) as qty_terjual'),
                                            DB::raw('sum(subtotal) as total_nilai'))
                                ->whereNull('b.is_batal')
                                ->whereBetween('b.tgl', [$tgl_awal, $tgl_akhir])
                                ->whereNotIn('a.id_sj', $id_sjQ)
                                ->groupBy('id_brg')
                                ->get();  
                                
        $dtg = [];                                
        foreach ($sj_detail->get() as $value) {
            $dtg[] = [
                'id_brg' => $value->id_brg,
                'qty' => $value->qty_terjual
            ];
        }
        $cl = collect($dtg)->max('qty');

        $dt = [];
        $no = 1;
        $tanda = '';
        foreach ($sj_detail->get() as $v) {
            $tanda = ($v->qty_terjual == $cl) ? '&nbsp<span class="badge badge-success">terlaris</span>' : '';
            $dt[] = (object) [
                'no' => $no++,
                'tgl' => $v->tgl,
                'id_brg' => $v->id_brg,
                'nama_brg' => $v->nama_brg,
                'satuan' => $v->nama_satuan,
                'qty_terjual' => $v->qty_terjual,
                'total_nilai' => $v->total_nilai,
                'tanda' => $tanda
            ];
        }

        return datatables::of($dt)
        ->editColumn('nama_brg', function ($dt) {
            $tanda = $dt->nama_brg.$dt->tanda;
            return $tanda;
        })
        ->rawColumns(['nama_brg'])
        ->make(true);
    }

    public function lap_piutang_pelanggan()
    {
        return view('admin.laporan.lap_piutang_pelanggan');
    }

    public function get_transaksi_byr($transaksi, $id_sj)
    {
        $dty = array_filter($transaksi->toArray(), function ($value) use ($id_sj) {
            return $value->id_sj == $id_sj;
        });

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v->total;
            }
        }
        return $hasil;
 
    }

    public function table_piutang_pelanggan(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->_tglAwal));
        $tgl_akhir = date('Y-m-d', strtotime($req->_tglAkhir));

        $pelanggan = DB::table('pelanggan')->get();

        $sj = DB::table('suratjalan as a')
                                ->leftJoin('suratjalan_detail as b', 'a.id', '=', 'b.id_sj')
                                ->leftJoin('pelanggan as c', 'a.member', '=', 'c.id')
                                ->leftJoin('satuan as d', 'b.satuan', '=', 'd.id')
                                ->whereNotNull('a.is_cek_nota')
                                ->whereNull('a.is_batal')
                                ->whereNotNull('a.member')
                                // ->whereNull('a.pembayaran')
                                ->whereBetween('a.tgl', [$tgl_awal, $tgl_akhir])
                                ->select('a.id as id_sj','a.tgl','a.pembayaran','a.total','a.bayar','a.is_cek_nota','a.is_batal','a.member as id_pelanggan',
                                            'b.nama_brg','b.qty','b.harga','b.subtotal',
                                                'c.nama',
                                                    'd.nama as satuan');
        $data_sj = $sj->get();

        $grup_member = $sj->groupBy('member')->whereNotNull('member')->get();

        $transaksi = $this->transaksi;
        // dd($transaksi);

        $dt = [];
        foreach ($grup_member as $value) {
            $dt[] = (object) [
                'id_sj' => $value->id_sj,
                'id_pelanggan' => $value->id_pelanggan,
                'pelanggan' => $value->nama,
                'penjualan' => $this->get_penjualan($data_sj, $value->id_pelanggan),
                'bayar' => $value->bayar,
                'transaksi' => $this->get_transaksi_byr($transaksi, $value->id_sj)
            ];
        }

        // dd($dt);
        $dt_sj = [];
        $total = 0;
        $total_byr = 0;
        $total_pelunasan = 0;
        $cek_lunas = 0;

        foreach ($dt as $x) {
            $bayar = 0;
            $transaksi = 0;
            $cek_lunas = $total - ($x->bayar + $x->transaksi);
            // if ($cek_lunas > 0) {
                $dt_sj[] = (object) [
                    'keterangan' => '<span style="color:green">'.$x->pelanggan.'</span>',
                    'id_sj' => '',
                    'nama_brg' => '',
                    'satuan' => '',
                    'qty' => '',
                    'harga' => '',
                    'subtotal' => '',
                    'total' => '',
                ];
             
                $total = 0;
                foreach ($x->penjualan as $y) {
                    $dt_sj[] = (object) [
                        'keterangan' => date('d-m-Y', strtotime($y->tgl)) ,
                        'id_sj' => $y->id_sj,
                        'nama_brg' => $y->nama_brg,
                        'satuan' => $y->satuan,
                        'qty' => $y->qty,
                        'harga' => $y->harga,
                        'subtotal' => $y->subtotal,
                        'total' => $y->total,
                    ];

                    $total += $y->subtotal;
                    $total_byr += $y->total;
                }

                $dt_sj[] = (object) [
                    'keterangan' => '',
                    'id_sj' => '',
                    'nama_brg' => '',
                    'satuan' => '',
                    'qty' => '',
                    'harga' => '<strong>Total || '.$x->pelanggan.'</strong>',
                    'subtotal' => $total,
                    'total' => $total,
                ];

                $dt_sj[] = (object) [
                    'keterangan' => '',
                    'id_sj' => '',
                    'nama_brg' => '',
                    'satuan' => '',
                    'qty' => '',
                    'harga' => '<strong>Bayar || '.$x->pelanggan.'</strong>',
                    'subtotal' => $x->bayar + $x->transaksi,
                    'total' => $x->bayar + $x->transaksi,
                ];

                $dt_sj[] = (object) [
                    'keterangan' => '',
                    'id_sj' => '',
                    'nama_brg' => '',
                    'satuan' => '',
                    'qty' => '',
                    'harga' => '<strong>Piutang || '.$x->pelanggan.'</strong>',
                    'subtotal' => $total - ($x->bayar + $x->transaksi),
                    'total' => $total - ($x->bayar + $x->transaksi),
                ];
            // }

        }
        $dataQ['data'] = $dt_sj;
        // dd($dataQ);
        return response()->json($dataQ);
    }

    public function lap_pembelian_barang()
    {
        return view('admin.laporan.lap_pembelian_barang');
    }

    public function table_pembelian(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->tglAwal));
        $tgl_akhir = date('Y-m-d', strtotime($req->tglAkhir));

        $data = DB::table('beli as a')
                            ->where('a.status',NULL)
                            ->leftJoin('suplier as s', 'a.suplier', '=', 's.id')
                            ->select('a.id', 'a.tgl', 'a.total', 'a.tgl_bayar', 'a.is_cek_beli',
                                        's.nama as nama_suplier')
                            ->orderBy('a.tgl', 'DESC')
                            ->orderBy('a.id', 'DESC')
                            ->whereBetween('a.tgl', [$tgl_awal, $tgl_akhir])
                            ->get();
                            
        return datatables::of($data)
        ->addColumn('status_bayar', function ($data) {
            $status_bayar = isset($data->tgl_bayar) ? '<span class="badge badge-success" style="color:white">Lunas</span>' : '<span class="badge badge-secondary" style="color:white">Belum Lunas</span>';
            return $status_bayar;
        })
        ->editColumn('status', function ($data)
        {
            $status = isset($data->is_batal) ? '<span class="badge badge-danger" style="color:white">Batal</span> ('.$data->batal.')' : '' ;
            return $status;
        })
        ->rawColumns(['status_bayar', 'status'])
        ->make(true);
    }
    
    public function lap_pembelian()
    {
        return view('admin.laporan.lap_pembelian');
    }

    public function table_pembelian_barang(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->_tglAwal));
        $tgl_akhir = date('Y-m-d', strtotime($req->_tglAkhir));

        $sj_detail = DB::table('beli as a')
                                ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'b.id_detail_beli')
                                ->leftJoin('satuan as c', 'b.id_satuan', '=', 'c.id')
                                ->select('a.tgl','b.id_brg', 'b.nama_brg', 'c.nama as nama_satuan', DB::raw('sum(qty) as qty_terjual'), DB::raw('sum(subtotal) as total_nilai'))
                                ->whereBetween('a.tgl', [$tgl_awal, $tgl_akhir])
                                ->groupBy('b.id_brg')
                                ->get();

        $dt = [];
        $no = 1;
        foreach ($sj_detail as $v) {
            $dt[] = [
                'no' => $no++,
                'tgl' => $v->tgl,
                'id_brg' => $v->id_brg,
                'nama_brg' => $v->nama_brg,
                'satuan' => $v->nama_satuan,
                'qty_terjual' => $v->qty_terjual,
                'total_nilai' => $v->total_nilai
            ];
        }

        return datatables::of($dt)
        ->make(true);
    }

    public function lap_pembelian_suplier()
    {
        return view('admin.laporan.lap_pembelian_suplier');
    }

    public function get_pembelian($beli, $id_suplier)
    {
        $dty = array_filter($beli->toArray(), function ($value) use ($id_suplier) {
            return $value->suplier == $id_suplier;
        });

        return $dty;
    }

    public function table_pembelian_suplier(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->_tglAwal));
        $tgl_akhir = date('Y-m-d', strtotime($req->_tglAkhir));

        $suplier = DB::table('suplier')->get();

        $beli = DB::table('beli as a')
                                ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'b.id_detail_beli')
                                ->leftJoin('suplier as c', 'a.suplier', '=', 'c.id')
                                ->leftJoin('satuan as d', 'b.id_satuan', '=', 'd.id')
                                ->whereNotNull('a.is_cek_beli')
                                ->whereBetween('a.tgl', [$tgl_awal, $tgl_akhir])
                                ->select('a.id_beli','a.no_nota_spl','a.tgl','a.pembayaran','a.total','a.is_cek_beli','a.suplier',
                                            'b.nama_brg','b.qty','b.harga','b.subtotal',
                                                'c.nama',
                                                    'd.nama as satuan');
        $data_beli = $beli->get();

        $grup_suplier = $beli->groupBy('suplier')->whereNotNull('suplier')->get();

        $dt = [];
        foreach ($grup_suplier as $value) {
            $dt[] = (object) [
                'id_suplier' => $value->suplier,
                'suplier' => $value->nama,
                'no_nota_spl' => $value->no_nota_spl,
                'pembelian' => $this->get_pembelian($data_beli, $value->suplier)
            ];
        }

        $dt_beli = [];
        $total = 0;

        foreach ($dt as $x) {
            $dt_beli[] = (object) [
                        'keterangan' => '<span style="color:green">'.$x->suplier.'</span>',
                        'id_beli' => '',
                        'no_nota_spl' => '',
                        'nama_brg' => '',
                        'satuan' => '',
                        'qty' => '',
                        'harga' => '',
                        'subtotal' => '',
                        'total' => ''
                    ];
                    
            foreach ($x->pembelian as $y) {
                $dt_beli[] = (object) [
                    'keterangan' => date('d-m-Y', strtotime($y->tgl)) ,
                    'id_beli' => $y->id_beli,
                    'no_nota_spl' => $x->no_nota_spl,
                    'nama_brg' => $y->nama_brg,
                    'satuan' => $y->satuan,
                    'qty' => $y->qty,
                    'harga' => $y->harga,
                    'subtotal' => $y->subtotal,
                    'total' => $y->total
                ];

                $total += $y->subtotal;
            }

            $dt_beli[] = (object) [
                'keterangan' => '',
                'id_beli' => '',
                'no_nota_spl' => '',
                'nama_brg' => '',
                'satuan' => '',
                'qty' => '',
                'harga' => '<strong>Total || '.$x->suplier.'</strong>',
                'subtotal' => $total,
                'total' => $total
            ];            
        }
        $dataQ['data'] = $dt_beli;

        return response()->json($dataQ);
    }

    public function lap_pembelian_hutang()
    {
        return view('admin.laporan.lap_pembelian_hutang');
    }

    public function table_pembelian_hutang(Request $req)
    {
        $tgl_awal = date('Y-m-d', strtotime($req->_tglAwal));
        $tgl_akhir = date('Y-m-d', strtotime($req->_tglAkhir));

        $suplier = DB::table('suplier')->get();

        $beli = DB::table('beli as a')
                                ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'b.id_detail_beli')
                                ->leftJoin('suplier as c', 'a.suplier', '=', 'c.id')
                                ->leftJoin('satuan as d', 'b.id_satuan', '=', 'd.id')
                                ->whereNotNull('a.is_cek_beli')
                                ->whereNull('a.tgl_bayar')
                                ->whereBetween('a.tgl', [$tgl_awal, $tgl_akhir])
                                ->select('a.id_beli','a.no_nota_spl','a.tgl','a.pembayaran','a.total','a.is_cek_beli','a.suplier',
                                            'b.nama_brg','b.qty','b.harga','b.subtotal',
                                                'c.nama',
                                                    'd.nama as satuan');
        $data_beli = $beli->get();

        $grup_suplier = $beli->groupBy('suplier')->whereNotNull('suplier')->get();

        $dt = [];
        foreach ($grup_suplier as $value) {
            $dt[] = (object) [
                'id_suplier' => $value->suplier,
                'suplier' => $value->nama,
                'no_nota_spl' => $value->no_nota_spl,
                'pembelian' => $this->get_pembelian($data_beli, $value->suplier)
            ];
        }

        $dt_beli = [];
        $total = 0;

        foreach ($dt as $x) {
            $dt_beli[] = (object) [
                        'keterangan' => '<span style="color:green">'.$x->suplier.'</span>',
                        'id_beli' => '',
                        'no_nota_spl' => '',
                        'nama_brg' => '',
                        'satuan' => '',
                        'qty' => '',
                        'harga' => '',
                        'subtotal' => '',
                        'total' => ''
                    ];
                    
            foreach ($x->pembelian as $y) {
                $dt_beli[] = (object) [
                    'keterangan' => date('d-m-Y', strtotime($y->tgl)) ,
                    'id_beli' => $y->id_beli,
                    'no_nota_spl' => $x->no_nota_spl,
                    'nama_brg' => $y->nama_brg,
                    'satuan' => $y->satuan,
                    'qty' => $y->qty,
                    'harga' => $y->harga,
                    'subtotal' => $y->subtotal,
                    'total' => $y->total
                ];

                $total += $y->subtotal;
            }

            $dt_beli[] = (object) [
                'keterangan' => '',
                'id_beli' => '',
                'no_nota_spl' => '',
                'nama_brg' => '',
                'satuan' => '',
                'qty' => '',
                'harga' => '<strong>Total || '.$x->suplier.'</strong>',
                'subtotal' => $total,
                'total' => $total
            ];            
        }
        $dataQ['data'] = $dt_beli;

        return response()->json($dataQ);
    }
}