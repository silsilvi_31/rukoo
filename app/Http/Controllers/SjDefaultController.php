<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\SjExport;
use Exception;

class SjDefaultController extends Controller
{
    private $url;
    private $transaksi;

    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
        // $this->url = 'http://adm.wijayaplywoodsindonesia.com/api/ina/stok';
        // $this->url = 'http://192.168.5.9:8080/api/ina/stok';
        // $this->apina = json_decode(file_get_contents($this->url), true);
        $this->transaksi = DB::table('transaksi')->get();
    }

    public function index()
    {
        return view('admin.sjdefault.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return isset($data) ? $data->nama : 'No Name';
    }

    public function get_transaksi($transaksi, $id_sj)
    {
        $dty = array_filter($transaksi->toArray(), function ($value) use ($id_sj) {
            return $value->id_sj == $id_sj;
        });

        $bayar = null;
        $jenis_byr = '';
        foreach ($dty as $value) {
            $bayar += $value->total;
            if ($value->jenis_byr == 'Transfer') {
                $jenis_byr = '<br> Transfer : ';
            }
        }

        $data['bayar'] = $bayar;
        $data['jenis_byr'] = $jenis_byr;
        return $data;
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? $parent_jurnal->tgl_akhir : '';                                

        $sj = DB::table('suratjalan')
                    ->whereDate('tgl', '<', $tgl_akhir)
                    ->get();

        $id_sj = [];

        foreach ($sj as $value) {
            $id_sj[] = $value->id;
        }
                    
        $data = DB::table('suratjalan as sj')
                    ->where('sj.status',NULL)
                    ->whereNotIn('sj.id', $id_sj)
                    ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                    ->leftJoin('pelanggan as pl', 'sj.member', '=', 'pl.id')
                    ->select('sj.id','sj.tgl','sj.pelanggan','pl.nama','sj.member','sj.catatan','sj.total','sj.bayar','sj.tgl','sj.opsi','sj.ongkir','sj.pembayaran',
                                'sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.is_batal','sj.batal',
                                    'kar.nama as sopir')
                    ->orderBy('sj.tgl', 'DESC')
                    ->orderBy('sj.id', 'DESC')
                    ->get(); 

        $transaksi = $this->transaksi;
        // dd($transaksi);
        
        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('id', function ($data) {
            
        })
        ->editColumn('tgl', function ($data) {
            $tanggal = date('d-m-Y', strtotime($data->tgl));
            return $tanggal;
        }) 
        ->editColumn('batal', function ($data) {
            $nama = isset($data->is_batal) ? $data->batal.' ('.$this->get_karyawan($data->is_batal).')' : null; 
            return $nama;
        })
        ->editColumn('cek_sj', function ($data) {
            $is_cek_nota = $data->is_cek_nota;
            $nama = isset($is_cek_nota) ? $this->get_karyawan($data->cek_nota) : '-';
            
            if ($is_cek_nota == 1) {
                return '<center><i class="fa fa-check"></i>'.$nama.'</center>';
            } else if ($is_cek_nota == 2) {
                return '<center><i class="fa fa-close"></i>'.$nama.'</center>';
            } else if ($is_cek_nota == '') {
                return '<center><i class="fa fa-minus"></center>';
            } else {
            
            }
        }) 
        ->addColumn('status_bayar', function ($data) use ($transaksi) {
            $total = $data->total;
            $bayar = $data->bayar;
            $pelunasan = 0;
            $pelunasan = $this->get_transaksi($transaksi, $data->id)['bayar'];

            $total_bayar = $bayar+$pelunasan;
            $ketr = '';
            $sisa = 0;
            if ( $total > $total_bayar ) {
                $sisa = $total-$total_bayar;
                $ketr = '<span class="badge badge-warning">Belum Lunas</span>';
            } else {
                $ketr = '<span class="badge badge-success">Lunas</span>';
            }

            return $ketr;
        })
        ->addColumn('opsi', function ($data) use ($transaksi){
            $tgl = date("d-m-Y", strtotime($data->tgl));
            $pelanggan = isset($data->pelanggan) ? $data->pelanggan : $data->nama;
            $opsi = $data->opsi;
            $total = ($opsi == 2) ? NULL : $data->total;
            $ongkir = $data->ongkir;
            $grandtotal = ($opsi == 2 ) ? NULL : $total + $ongkir;
            $edit = route('sjdefault.form_edit', [base64_encode($data->id)]);
            $nota = route('sjdefault.nota', [base64_encode($data->id)]);
            $sj = route('sjdefault.sj', [base64_encode($data->id)]);
            $link_retur = route('retur.form', [base64_encode($data->id)]);
            $id_sj = "'".base64_encode($data->id)."'";
            $no_nota = $data->id;

            //Awal get Status Pelunasan
            $bayar = $data->bayar;
            $pelunasan = 0;
            $pelunasan = $this->get_transaksi($transaksi, $data->id)['bayar'];
            $total_bayar = $bayar+$pelunasan;
            $ketr = '';
            $sisa = 0;

            if ( $total > $total_bayar ) {
                $sisa = $total-$total_bayar;
                $ketr = 'y';
            } else {
                $ketr = 't';
            }
            //AKhir get status pelunasan
            
            $status = (isset($data->pembayaran) == 1 && !isset($data->is_batal)) ? 'btn-success' : 'btn-secondary disabled';
            $status_edit = ($data->is_cek_nota != 1 ) ? 'btn-primary' : 'btn-secondary disabled';
            $status_hapus = ( (($data->is_cek_nota != 1)) || (isset($data->is_batal)) )? '' : 'none';
            $status_tanda_terima = (!isset($data->is_cek_nota) || isset($data->pembayaran) ) ? 'none' : '';
            $status_retur = (isset($data->is_cek_nota) && isset($data->pembayaran)) ? 'btn-success' : 'btn-secondary disabled';

            return '<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_detail_sj" data-id="'.$no_nota.'" data-tgl="'.$tgl.'" data-pelanggan="'.$pelanggan.'" data-total="'.$total.'" data-ongkir="'.$ongkir.'" data-grandtotal="'.$grandtotal.'"><i class="fa fa-info"></i></button>
                    <button type="button" class="btn btn-sm btn-danger" style="display:'.$status_hapus.'" onclick="delete_suratjalan('.$id_sj.')"><i class="fa fa-trash"></i></button>
                    <a href="#" id="'.$data->id.'" class="print_nota btn btn-sm '.$status.'"><i class="fa fa-print"></i> Nota</a>
                    <button type="button" class="btn btn-sm btn-info" style="display:'.$status_tanda_terima.'" data-toggle="modal" data-target="#modal_tanda_terima" data-id="'.$no_nota.'" data-total="'.$grandtotal.'"><i class="fa fa-money"></i></button>
                    <a href="'.$link_retur.'" class="btn btn-sm '.$status_retur.'">Retur</a>';
        })
        ->rawColumns(['tgl','cek_sj', 'opsi', 'status_bayar'])
        ->make(true);
    }

    public function datatable_sj(Request $req)
    {
        $id = $req->_id;
        $data = DB::table('suratjalan as sj')
                    ->where('a.status', NULL)
                    ->where('a.id_sj', $id)
                    ->leftJoin('suratjalan_detail as a', 'sj.id', '=', 'a.id_sj')
                    ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                    ->select('a.id','a.nama_brg','a.ketr','a.satuan as id_satuan','a.qty','a.harga','a.harga_new','a.potongan','a.subtotal', 'a.ketr_tambahan','a.ketr_ganti_harga',
                                'sj.id','sj.opsi',
                                    'b.nama as satuan')
                    ->get();

        return Datatables::of($data)  
        ->addIndexColumn() 
        ->editColumn('harga', function ($data) {
            $harga = (($data->opsi) == 2) ? NULL : $data->harga;
            return $harga;
        })
        ->editColumn('subtotal', function ($data) {
            $subtotal = (($data->opsi) == 2) ? NULL : $data->subtotal;
            return $subtotal;
        })
        // ->addColumn('subtotal', function ($data) {
        //     $qty = $data->qty;
        //     $harga_item = isset($data->harga_new)$data->harga;
        //     $potongan = $data->potongan;
        //     $subtotal = ($qty * $harga_item) - ($qty * $potongan);
        //     return number_format($subtotal, 0,',', '.'); 
        // }) 
        ->make(true);  
        // dd($data);                                      
    }

    public function form()
    {
        $kendaraan = DB::table('kendaraan')
                            // ->where('status', NULL)
                            ->where('penyusutan', '<>', NULL)
                            ->get();
        $sopir = DB::table('karyawan')
                                ->where('id_jabatan',4)
                                ->get();
        $pelanggan = DB::table('pelanggan')->where('status', NULL)->get();
                                               
        $data['kendaraan'] = $kendaraan;
        $data['sopir'] = $sopir;
        $data['pelanggan'] = $pelanggan;
        
        return view('admin.sjdefault.form')->with($data);
    }

    public function total(Request $req)
    {
        $id = $req->_id;
        $sj = DB::table('suratjalan')->where('id', $id)->first();
        $ongkir = isset($sj) ? $sj->ongkir : NULL;
        $total = isset($sj) ? $sj->total : NULL;
        $grandtotal = $ongkir + $total;
        $data['ongkir'] = number_format($ongkir,0,',','.');
        $data['total'] = number_format($total, 0, ',', '.');
        $data['grandtotal'] = number_format($grandtotal, 0, ',', '.');

        return response()->json($data);
    }

    public function no_urut()
    {
        $id_suratjalan = DB::table('suratjalan')->where('status',NULL)->max('id');
        $no = $id_suratjalan;
        $no++;
        return response()->json($no);
    }

    public function select_pelanggan()
    {
        $data = DB::table('pelanggan')->where('status', NULL)->get();
        
        return response()->json($data);
    }

    public function get_brg_jual($sj_detail, $id_brg, $tgl)
    {
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];
        
        $dty = array_filter($sj_detail, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($sj_detail, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        return $hasil;
    }

    public function get_brg_masuk($apiIna, $id_brg, $tgl)
    {
        $id = $id_brg;
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];

        $dty = array_filter($apiIna, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($apiIna, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        
        return $hasil;
    }

    public function get_brg_masuk_fix($apiina, $nama_brg, $tgl)
    {
        $id = $nama_brg;

        $cari = [
            'id' => $id,
            'tgl_opname' => $tgl['tgl_opname'],
            'tgl_now' => $tgl['tgl_now']
        ];

        $hasil = 0;
        $tmp = [];
        $qty = [];
        $tgl1 = strtotime($cari['tgl_opname']);
        $tgl2 = time();
        $diff = $tgl2 - $tgl1;
        $selisih = floor($diff / (60 * 60 * 24));
        
            for ($i=0; $i < $selisih+1 ; $i++) { 
                $tgl_qu = date("Y-m-d", strtotime("+".$i." days", strtotime($cari['tgl_opname'])));
                $cc = [
                    'id' => $id,
                    'tgl_qu' => $tgl_qu
                ];

                $dty = array_filter($apiina, function ($value) use ($cc) {
                    $df = $value['nama_brg'] == $cc['id'] && $value['tgl'] == $cc['tgl_qu'];
                        return $df;
                });
                
                $tmp[] = $dty;
            }

            if (!empty($tmp)) {
                foreach ($tmp as $key => $e) {
                    foreach ($e as $k => $c) {
                        $hasil += $c['qty'];
                    }
                }
            }

        return $hasil;
    }

    public function get_brg_jual_fix($sj_detail, $id_brg, $tgl)
    {
        $id = (string)$id_brg;
        // dd($sj_detail);
        $cari = [
            'id' => $id,
            'tgl_opname' => $tgl['tgl_opname'],
            'tgl_now' => $tgl['tgl_now']
        ];

        $hasil = 0;
        $tmp = [];
        $qty = [];
        $tgl1 = strtotime($cari['tgl_opname']);
        $tgl2 = time();
        $diff = $tgl2 - $tgl1;
        $selisih = floor($diff / (60 * 60 * 24));

        for ($i=0; $i < $selisih+1 ; $i++) { 
            $tgl_qu = date("Y-m-d", strtotime("+".$i." days", strtotime($cari['tgl_opname'])));
            $cc = [
                'id' => $id,
                'tgl_qu' => $tgl_qu
            ];

            $dty = array_filter($sj_detail->toArray(), function ($value) use ($cc) {
                $df = $value->id_brg== $cc['id'] && $value->tgl == $cc['tgl_qu'];
                    return $df;
            });
            
            $tmp[] = $dty;
        }

        // dd($tmp->toArray());

        if (!empty($tmp)) {
            foreach ($tmp as $key => $e) {
                foreach ($e as $k => $c) {
                    $hasil += $c->qty;
                }
            }
        }
        // dd($hasil);
        return $hasil;
    }

    public function get_qty_beli($data_beli, $id_brg, $tgl)
    {
        $cari = [
                    'id_brg' => (string) $id_brg,
                    'tgl' => (string) $tgl
        ];

        $dty = array_filter($data_beli, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl'] >= $cari['tgl'];
        });

        $tmp = [];
        $hasil = 0;
        foreach ($dty as $k => $v) {
            $hasil += $v['qty'];
        }

        return $hasil;
    }

    public function datatable_brg()
    {
        $tgl_now = date("Y-m-d");
        $data = DB::table('barang as a')
                ->where('a.status',NULL)
                ->where('jenis_brg', 5)
                ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                ->select('a.kode', 'a.nama_brg', 'a.harga', 'a.satuan as id_satuan','b.nama as satuan', 'a.jenis_brg','a.stok','a.tgl_opname')
                ->get();
        
        // $rUrl =  $this->url;
        // $apiIna = json_decode(file_get_contents($rUrl), true);

        $brg_sj = [];
        // $brg_masuk = [];
        $brg_beli = [];

        // foreach ($apiIna as $y => $e) {
        //     $brg_masuk[] = [
        //         'id_sj' => $e['id_sj'],
        //         'tgl' => (string)strtotime($e['tgl']),
        //         'tgl_s' => $e['tgl'],
        //         'id_brg' => $e['id_brg'],
        //         'nama_brg' => $e['nama_brg'],
        //         'qty' => $e['qty'],
        //         'harga' => $e['harga'],
        //         'harga_new' => $e['harga_new']
        //     ];
        // }

        $sj_detail = DB::table('suratjalan_detail as a')
                                ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                                // ->whereNotNull('b.bayar')
                                ->whereNotNull('b.is_cek_nota')
                                ->select('a.id_brg','a.qty','b.tgl')
                                ->get();

        foreach ($sj_detail as $t => $d) {
            $brg_sj[] = [
                'id_brg' => $d->id_brg,
                'qty' => $d->qty,
                'tgl' => strtotime($d->tgl)
            ];
        }

        $beli = DB::table('beli as a')
                            ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'id_detail_beli')
                            ->whereNotNull('a.is_cek_beli')
                            ->get();

        foreach ($beli as $key => $value) {
            $brg_beli[] = [
                'id_brg' => $value->id_brg,
                'qty' => $value->qty,
                'tgl' => strtotime($value->tgl)
            ];
        }

        $dt = [];
        $awal = 0;
        $hasil = 0;
        $hasil_masuk = 0;

        foreach ($data as $v) {
            $opname_time = isset($v->tgl_opname) ? (string)strtotime($v->tgl_opname) : null; 

            $stok_b = $this->get_qty_beli($brg_beli, $v->kode, $opname_time);
            $stok_k = $this->get_brg_jual($brg_sj, $v->kode, $opname_time);
            // $stok_m = $this->get_brg_masuk($brg_masuk, $v->kode, $opname_time) + $stok_b;

            $hasil = $v->stok + $stok_b - $stok_k;

            $dt[] = (object) [
                'kode' => $v->kode,
                'nama_brg' => $v->nama_brg,
                'harga' => $v->harga,
                'id_satuan' => $v->id_satuan,
                'satuan' => $v->satuan,
                'stok_awal' => $awal,
                'stok_masuk' => $v->stok + $stok_b,
                'stok_keluar' => $stok_k,
                'sisa' => $hasil,
                'tgl_opname' => $opname_time,
                'list---------' => $stok_b,
                'list_keluar' => $stok_k
            ];
        }

        // dd($dt);
        return Datatables::of($dt)
        ->addIndexColumn()
        ->addColumn('opsi', function ($dt){
            $kode_barang = $dt->kode;
            $nama_brg = "'".$dt->nama_brg."'";
            $harga = $dt->harga;
            $id_satuan = $dt->id_satuan;
            $satuan = "'".$dt->satuan."'";
            $sisa = $dt->sisa;
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_brg('.$kode_barang.','.$nama_brg.','.$harga.','.$id_satuan.','.$satuan.','.$sisa.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function add_pelanggan(Request $req)
    {
        // inisialisai pelanggan 
        $id_user = session::get('id_user');
        $nama_pl = $req->_namaPl;
        $alamat_pl = $req->_alamatPl;
        $no_telp = $req->_telpPl;
        $email = $req->_emailPl;

        $data_pl = [
            'nama' => $nama_pl,
            'alamat' => $alamat_pl,
            'no_telp' => $no_telp,
            'email' => $email,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ];

        $return_id = DB::table('pelanggan')->insertGetId($data_pl);

        if (isset($return_id)) {
            $res = [
                'code' => 201,
                'msg' => 'Pelanggan berhasil ditambahkan',
                'id' => $return_id,
                'nama_pl' => $nama_pl
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Pelanggan Gagal ditambahkan !',
                'id' => NULL,
                'nama_pl' => NULL
            ];
        }
        return response()->json($res);
    }

    public function remake_akun($id_sj)
    {
        $jurnal = DB::table('jurnal')
                            ->whereIn('no_akun', ['118','119'])
                            ->delete();

        $update_total = DB::table('jurnal')
                                ->where('no_akun', '6111,00')
                                ->where('ref', $id_sj)
                                ->where('status', '=', NULL)
                                ->update([
                                    'harga' => $this->get_hpp($id_sj, 1),
                                    'total' => $this->get_hpp($id_sj, 1)
                                ]);
    }



    // START FUNGSI AKUNTANSI
    public function is_lps($id_brg) 
    {
        $search = 'lps';
        if (is_numeric($id_brg)) {
            $barang = DB::table('barang')->where('kode', $id_brg)->first();
            $namaBrg = $barang->nama_brg;
        } else {
            $namaBrg = $id_brg;
        }
        
        $compare = strstr($namaBrg, $search);
        $hasil = '';
        if ($compare) {
            $hasil = 'true';
        } else {
            $hasil = 'false';
        }
        
        return $hasil;
    }

    public function akun_barang($jenis_akun, $id_brg, $help_b, $keterangan)
    {
        $lower = isset($keterangan) ? strtolower($keterangan) : null;
        $akun_ketemu = '';
        $cari_akun_item = DB::table('set_akun')
                                    ->where('id_set', $id_brg)
                                    ->where('help_a', $jenis_akun)
                                    ->where('help_b', $help_b)
                                    ->where('help_c', $lower)
                                    ->first();
        if (!isset($cari_akun_item) && ($jenis_akun == 4)) {
            $akun_ketemu = '4111';
            if (strtolower($keterangan) == 'a') {
                $cari_akun_item = DB::table('set_akun')
                                    ->where('id_set', $id_brg)
                                    ->where('help_a', $jenis_akun)
                                    ->where('help_b', $help_b)
                                    ->where('help_c', null)
                                    ->first(); 
            }
            $insert = DB::table('error_log')->insert(['nama' => 'akun tidak ditemukan untuk'.$id_brg.'-'.$jenis_akun.'-'.$help_b.'-'.strtolower($keterangan)]);
        } else {
            if (strtolower($keterangan) == 'a') {
                $cari_akun_item = DB::table('set_akun')
                                    ->where('id_set', $id_brg)
                                    ->where('help_a', $jenis_akun)
                                    ->where('help_b', $help_b)
                                    ->where('help_c', null)
                                    ->first(); 
            }
            $akun_ketemu = isset($cari_akun_item) ? $cari_akun_item->kode : '';
        }
        return $akun_ketemu;
    }

    public function get_hpp($id, $hpp)
    {
        $get_harga_pokok = DB::table('jurnal')
                                ->where('ref',$id)
                                ->where('hpp', $hpp)
                                ->where('status', '=', NULL)
                                ->sum('total');

        return $get_harga_pokok;
    }

    public function harga_pokok($id_brg)
    {
        $cari_hpp = DB::table('barang')
                                    ->where('kode', $id_brg)
                                    ->where('status', NULL)
                                    ->first();

        return $cari_hpp->hpp;
    }

    public function jenis_ketr($ketr)
    {
        if (isset($ketr)) {
            if ($ketr == 'AA' || $ketr == 'A' || $ketr == 'a' || $ketr == "aa") {
                $ketr = 'A';
            } else {
                $ketr = null;
            }
        } else {
            $ketr = null;
        }

        return $ketr;
    }

    public function nama_akun($no_akun)
    {
        $cari_nama_akun = DB::table('akun')
                                    ->where('no_akun', $no_akun)
                                    ->where('status', NULL)
                                    ->first();

        return $cari_nama_akun->akun;
    }

    public function update_jurnal($id_sj, $tgl, $member, $ongkir, $catatan)
    {
        $pelanggan = DB::table('pelanggan')->where('status', null)->where('id', $member)->first();
        $update_jurnal = DB::table('jurnal')->where('ref', $id_sj)->update([
            'nama' => $pelanggan->nama
        ]);

        $update_catatan = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', '6111,00')
                                    ->where('status', NULL)
                                    ->update([
                                        'keterangan' => $catatan
                                    ]);  

        $update_ongkir = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', '5200,00')
                                    ->orwhere('no_akun', '1300,07')
                                    ->where('status', NULL)
                                    ->update([
                                        'harga' => $ongkir
                                    ]);    
                                    
        $update_qty = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', '2220,00')
                                    ->where('status', NULL)
                                    ->update([
                                        'keterangan' => $catatan,
                                    ]);  
                     
    }
    // END FUNGSI AKUNTANSI

    //Start set akun
        public function set_akun($aksi, $sj, $sj_detail, $id_detail_sj, $jumlah)
        {
            $data_pelanggan = DB::table('pelanggan')->where('id', $sj['member'])->first();
            $pelanggan = isset($sj['member']) ? $data_pelanggan->nama : $sj['pelanggan'];
            
            if ($aksi == 'insert') {
                // HPP Triplek
                $akun[0]['tgl'] = $sj['tgl'];
                $akun[0]['id_item'] = NULL;
                $akun[0]['ref'] = $sj['id'];
                $akun[0]['hpp'] = NULL;
                $akun[0]['grup'] = 1;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $pelanggan; 
                $akun[0]['no_akun'] = '510';
                $akun[0]['keterangan'] = null;
                $akun[0]['map'] = 'd';
                $akun[0]['hit'] = '';
                $akun[0]['qty'] = NULL;
                $akun[0]['harga'] = 0;
                $akun[0]['total'] = 0;
                
                // Stok ina
                $akun[1]['tgl'] = $sj['tgl'];
                $akun[1]['id_item'] = $id_detail_sj;
                $akun[1]['ref'] =  $sj['id'];
                $akun[1]['hpp'] = 1;
                $akun[1]['grup'] = 1;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $pelanggan;
                $akun[1]['no_akun'] = '140';
                $akun[1]['keterangan'] = $sj_detail['nama_brg'];
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $sj_detail['qty'];
                $akun[1]['harga'] = $this->harga_pokok($sj_detail['id_brg']);
                $akun[1]['total'] = $sj_detail['qty'] * $this->harga_pokok($sj_detail['id_brg']);
                
                // kas 
                $akun[2]['tgl'] = $sj['tgl'];
                $akun[2]['id_item'] = null;
                $akun[2]['ref'] =  $sj['id'];
                $akun[2]['hpp'] = NULL;
                $akun[2]['grup'] = 2;
                $akun[2]['jenis_jurnal'] = 'sj';
                $akun[2]['nama'] = $pelanggan;
                $akun[2]['no_akun'] = '130';
                $akun[2]['keterangan'] = '';
                $akun[2]['map'] = 'd';
                $akun[2]['hit'] = '';
                $akun[2]['qty'] = NULL;
                $akun[2]['harga'] = $jumlah;
                $akun[2]['total'] = $jumlah;
    
                // Pendapatan Penjualan triplek
                $akun[3]['tgl'] = $sj['tgl'];
                $akun[3]['id_item'] = $id_detail_sj;
                $akun[3]['ref'] =  $sj['id'];
                $akun[3]['hpp'] = 2;
                $akun[3]['grup'] = 2;
                $akun[3]['jenis_jurnal'] = 'sj';
                $akun[3]['nama'] = $pelanggan;
                $akun[3]['no_akun'] = '410';
                $akun[3]['keterangan'] = 'Triplek '.$sj_detail['nama_brg'];
                $akun[3]['map'] = 'k';
                $akun[3]['hit'] = 'b';
                $akun[3]['qty'] = $sj_detail['qty'];
                $akun[3]['harga'] = $sj_detail['harga'] - $sj_detail['potongan'];
                $akun[3]['total'] = ($sj_detail['qty'] * $sj_detail['harga']) - ($sj_detail['qty'] * $sj_detail['potongan']);
               
                $simpan_jurnal = DB::table('jurnal')->insert($akun);
                $update_hpp = DB::table('jurnal')
                                        ->where('no_akun', '510')
                                        ->where('ref', $sj['id'])
                                        ->where('status', '=', NULL)
                                        ->update([
                                            'harga' => $this->get_hpp( $sj['id'], 1),
                                            'total' => $this->get_hpp( $sj['id'], 1)
                                        ]);
            } elseif ($aksi == 'add_item') {
                // Stok ina
                $akun[0]['tgl'] = $sj['tgl'];
                $akun[0]['id_item'] = $id_detail_sj;
                $akun[0]['ref'] =  $sj['id'];
                $akun[0]['hpp'] = 1;
                $akun[0]['grup'] = 1;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $pelanggan;
                $akun[0]['no_akun'] = '140';
                $akun[0]['keterangan'] = $sj_detail['nama_brg'];
                $akun[0]['map'] = 'k';
                $akun[0]['hit'] = 'b';
                $akun[0]['qty'] = $sj_detail['qty'];
                $akun[0]['harga'] = $this->harga_pokok($sj_detail['id_brg']);
                $akun[0]['total'] = $sj_detail['qty'] * $this->harga_pokok($sj_detail['id_brg']);
                

                // Pendapatan Penjualan triplek
                $akun[1]['tgl'] = $sj['tgl'];
                $akun[1]['id_item'] = $id_detail_sj;
                $akun[1]['ref'] =  $sj['id'];
                $akun[1]['hpp'] = 2;
                $akun[1]['grup'] = 2;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $pelanggan;
                $akun[1]['no_akun'] = '410';
                $akun[1]['keterangan'] = 'Triplek '.$sj_detail['nama_brg'];
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $sj_detail['qty'];
                $akun[1]['harga'] = $sj_detail['harga'] - $sj_detail['potongan'];
                $akun[1]['total'] = ($sj_detail['qty'] * $sj_detail['harga']) - ($sj_detail['qty'] * $sj_detail['potongan']);

                $simpan_item_jurnal = DB::table('jurnal')->insert($akun);
                $update_akun = DB::table('jurnal')
                                            ->where('ref',  $sj['id'])
                                            ->where('no_akun', '130')
                                            ->where('status', NULL)
                                            ->update([
                                                'harga' => $this->get_hpp( $sj['id'], 2),
                                                'total' => $this->get_hpp( $sj['id'], 2)
                                            ]);   

                $update_hpp = DB::table('jurnal')
                                            ->where('no_akun', '510')
                                            ->where('ref',  $sj['id'])
                                            ->where('status', '=', NULL)
                                            ->update([
                                                'harga' => $this->get_hpp( $sj['id'], 1),
                                                'total' => $this->get_hpp( $sj['id'], 1)
                                            ]);
            }
        }
    //End set akun

    public function save(Request $req)
    {
       //inisialiasi surat jalan
        $id_user = session::get('id_user');
        $no_nota = $req->_noNota;
        $tgl = date("Y-m-d", strtotime($req->_tgl)) ;
        // $pelanggan = $req->_pelanggan;
        $member = $req->_idPelanggan;
        $catatan = $req->_catatan;
        $ongkir = $req->_ongkir;
        $is_cek_nota = NULL;

        //inisialiasi surat jalan detail
        $id_brg = $req->_idBrg;
        $nama_brg = $req->_namaBrg;
        $stok = $req->_stok;
        $satuan = $req->_idSatuan;
        $harga_new = $req->_hargaNew;
        $harga = $req->_harga ;
        $ketr_ganti_harga = $req->_ketrGantiHarga;
        $qty = $req->_qty;
        $potongan = $req->_potongan;
        $ketr = $req->_ketr;
        $ketr_tambahan = $req->_ketrTambahan;
        $subtotal = ($qty * (isset($harga_new) ? $harga_new : $harga)) - ($qty * $potongan);
        $id_item = $req->_idItem;
        
        $data_sj = [
            'id' => $no_nota,
            'tgl' => $tgl,
            // 'pelanggan' => $pelanggan,
            'member' => $member,
            'catatan' => $catatan,
            'ongkir' => $ongkir,
            'is_cek_nota' => $is_cek_nota,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ];

        $data_sj_detail = [
            'id_sj' => $no_nota,
            'id_brg' => $id_brg,
            'nama_brg' => $nama_brg,
            'satuan' => $satuan,
            'harga' => $harga,
            'harga_new' => $harga_new,
            'ketr_ganti_harga' => $ketr_ganti_harga,
            'qty' => $qty,
            'potongan' => $potongan,
            'subtotal' => $subtotal,
            'ketr' => $ketr,
            'ketr_tambahan' => $ketr_tambahan,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ]; 
        // dd($data_sj);

        $cek_sj = DB::table('suratjalan')->where('id',$no_nota)->first();
        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();

        $tgl_int = strtotime($tgl);
        $tgl_akhir_int = isset($parent_jurnal) ? strtotime($parent_jurnal->tgl_akhir) : null;                                        
        
        if (!$tgl || (!$member && !$member) || (!$cek_sj && !$nama_brg)) {
            $msg = [];
            $res = [
                'code' => 400,
                'msg' => 'Data Belum Lengkap'
            ];
        } 
        elseif ($qty > $stok){
            $msg = [];
            $res = [
                'code' => 400,
                'msg' => 'Stok Tidak mencukupi'
            ];
        } elseif ($tgl_int <= $tgl_akhir_int) {
            $res = [
                'code' => 400,
                'msg' => 'Sudah tutup buku'
            ];
        } else {
            if (is_null($cek_sj)) {
                $insert_sj = DB::table('suratjalan')->insert($data_sj);
                $insert_sj_detail = DB::table('suratjalan_detail')->insertGetId($data_sj_detail);
                // $this->set_akun('insert', $data_sj, $data_sj_detail, $insert_sj_detail);

                if ($insert_sj) {
                    $jumlah = DB::table('suratjalan_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                    $update_total_sj = DB::table('suratjalan')->where('id', $no_nota)->update([
                        'total' => $jumlah
                    ]);
                    $this->set_akun('insert', $data_sj, $data_sj_detail, $insert_sj_detail, $jumlah);
                    $res = [
                        'code' => 200,
                        'msg' => 'Berhasil Disimpan',
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Gagal Disimpan'
                    ];
                }
                
            } elseif (!is_null($cek_sj)) {
                if (isset($id_item)) {
                    $sj_detail = DB::table('suratjalan_detail')->where('id', $id_item)->first();

                    $update_item = DB::table('suratjalan_detail')->where('id',$id_item)->update($data_sj_detail);

                    if ($update_item) {
                        $jumlah = DB::table('suratjalan_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                        $update_total_sj = DB::table('suratjalan')->where('id', $no_nota)->update([
                            'total' => $jumlah,
                            'is_cek_nota' => $is_cek_nota
                        ]);
                        $res = [
                            'code' => 200,
                            'msg' => 'Data Barang Berhasil Diupdate',
                        ];
                    } else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Data Barang Gagal Diupdate'
                        ];
                    }
                } else {
                    if (isset($nama_brg)) {
                        $insert_sj_detail = DB::table('suratjalan_detail')->insertGetId($data_sj_detail);
                        if (isset($insert_sj_detail)) {
                            $jumlah = DB::table('suratjalan_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                            $update_total_sj = DB::table('suratjalan')->where('id', $no_nota)->update([
                                'total' => $jumlah,
                                'is_cek_nota' => $is_cek_nota
                            ]);
                            $this->set_akun('add_item', $data_sj, $data_sj_detail, $insert_sj_detail, $jumlah);
                            $res = [
                                'code' => 200,
                                'msg' => 'Data Barang Berhasil Disimpan',
                            ];
                        } else {
                            $res = [
                                'code' => 400,
                                'msg' => 'Data Barang Gagal Disimpan'
                            ];
                        }
                    }

                    $this->update_jurnal($no_nota, $tgl, $member, $ongkir, $catatan);

                    $update_sj =  DB::table('suratjalan')->where('id', $no_nota)->update($data_sj);
                    
                    if ($update_sj) {
                        $res = [
                            'code' => 201,
                            'msg' => 'SJ Berhasil Diupdate',
                        ];
                    }else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Gagal update SJ !'
                        ];
                    }
                }
            }
        }
        return response()->json($res);
    }

    public function update_checkout(Request $req)
    {
        $no_nota = $req->_noNota;
        $bayar = $req->_bayar;
        $kembalian = $req->_kembalian;

        $data_checkout = [
                            'bayar' => $bayar,
                            'kembalian' => $kembalian
                        ];

        $update = DB::table('suratjalan')->where('id', $no_nota)->update($data_checkout);
        
        if ($update) {
            $res = [
                'code' => 201,
                'msg' => 'SJ Berhasil Diupdate',
                'bayar' => number_format($bayar, 0,',', '.'),
                'kembalian' => number_format($kembalian, 0,',', '.'),
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal update SJ !',
                'bayar' => null,
                'kembalian' => null
            ];
        }
        return response()->json($res);
    }

    public function set_akun_tanda_terima($id, $total_bayar, $pelanggan, $dtt)
    {
        // kas/bank
        $akun[0]['tgl'] = $dtt['tgl_bayar'];
        $akun[0]['id_item'] = NULL;
        $akun[0]['ref'] = $id;
        $akun[0]['hpp'] = NULL;
        $akun[0]['grup'] = 3;
        $akun[0]['jenis_jurnal'] = 'sj';
        $akun[0]['nama'] = $pelanggan; 
        $akun[0]['no_akun'] = $dtt['pembayaran'] == 'Transfer' ? 120 : 110;
        $akun[0]['keterangan'] = null;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = '';
        $akun[0]['qty'] = NULL;
        $akun[0]['harga'] = $total_bayar;
        $akun[0]['total'] = $total_bayar;
        
        // piutang
        $akun[1]['tgl'] = $dtt['tgl_bayar'];
        $akun[1]['id_item'] = null;
        $akun[1]['ref'] =  $id;
        $akun[1]['hpp'] = 1;
        $akun[1]['grup'] = 3;
        $akun[1]['jenis_jurnal'] = 'sj';
        $akun[1]['nama'] = $pelanggan;
        $akun[1]['no_akun'] = '130';
        $akun[1]['keterangan'] = '';
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = 'b';
        $akun[1]['qty'] = null;
        $akun[1]['harga'] = $total_bayar;
        $akun[1]['total'] = $total_bayar;

        $simpan_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function simpan_tanda_terima(Request $req)
    {
        $id_user = session::get('id_user');
        $no_nota = $req->_noNota;
        $total_bayar = $req->_totalBayar;
        $tanggal = $req->_tanggal;
        $pembayaran = $req->_pembayaran;
        $uang_sebesar = $req->_uangSebesar;
        $kembalian = $req->_kembalian;

        $dtt = [
            'tgl_bayar' => date("Y-m-d H:i:s", strtotime($tanggal)),
            'pembayaran' => $pembayaran,
            'bayar' => $uang_sebesar,
            'kembalian' => $kembalian,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
        ];

        $sj = DB::table('suratjalan as a')
                            ->leftJoin('pelanggan as b', 'a.member', '=', 'b.id')
                            ->where('a.id', $no_nota)->first();

        $pelanggan = isset($sj->member) ? $sj->nama : $sj->pelanggan;

        $update = DB::table('suratjalan')->where('id', $no_nota)->update($dtt);

        if ($update) {
            $this->set_akun_tanda_terima($no_nota, $uang_sebesar, $pelanggan, $dtt);
            $res = [
                'code' => 201,
                'msg' => 'Tanda Terima Berhasil Disimpan',
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Tanda Terima Gagal Disimpan',
            ];
        }
        return response()->json($res);

    }

    public function datatable_brg_detail(Request $req)
    {
        $id = $req->_idBeli;

        $data = DB::table('suratjalan_detail as a')
                    ->where('a.status', NULL)
                    ->where('a.id_sj', $id)
                    ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                    ->leftJoin('barang as c', 'a.nama_brg', '=', 'c.nama_brg')
                    ->select('a.id','a.nama_brg','a.ketr','a.ketr_tambahan','a.satuan as id_satuan','a.qty','a.harga','a.harga_new','a.potongan','a.ketr_ganti_harga',
                                'b.nama as satuan',
                                'c.kode')
                    ->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('harga', function ($data) {
            $harga = isset($data->harga_new) ? $data->harga_new : $data->harga;
            return $harga;
        })
        ->addColumn('subtotal', function ($data) {
            $qty = $data->qty;
            $harga_item = isset($data->harga_new) ? $data->harga_new : $data->harga ;
            $potongan = $data->potongan;
            $subtotal = ($qty * $harga_item) - ($qty * $potongan);
            return number_format($subtotal, 0,',', '.');
        })
        ->addColumn('opsi', function ($data) {
            $id_item = $data->id;
            $nama_item = "'".$data->nama_brg."'";
            $harga_item = $data->harga;
            $kode_brg = $data->kode;

            $harga_new = isset($data->harga_new) ? $data->harga_new : NULL;
            $ketr_ganti_harga = isset($data->ketr_ganti_harga) ? "'".$data->ketr_ganti_harga."'" : "'"."'";
            $id_satuan = $data->id_satuan;
            $satuan = "'".$data->satuan."'";
            $qty = !is_null($data->qty) ? $data->qty : 0 ;
            $potongan = !is_null($data->potongan) ? $data->potongan : 0;
            $ketr = !is_null($data->ketr) ?  "'".$data->ketr."'" : "'"."'";
            $ketr_tambahan = !is_null($data->ketr_tambahan) ?  "'".$data->ketr_tambahan."'" : "'"."'";
            $subtotal = ($qty * $harga_item) - ($qty * $potongan);
            // return '<button type="button" class="btn btn-sm btn-primary" onclick="select_item('.$id_item.','.$kode_brg.','.$nama_item.','.$id_satuan.','.$satuan.','.$harga_item.','.$qty.','.$potongan.','.$ketr.','.$ketr_tambahan.','.$subtotal.','.$ketr_ganti_harga.','.$harga_new.')"><i class="fa fa-edit"></i></a>
            //         <button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$id_item.')"><i class="fa fa-trash"></i></button>';
                    return '<button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$id_item.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form_edit($id)
    {
        $id = base64_decode($id);
        $suratjalan = DB::table('suratjalan')->where('id', $id)->first();

        $kendaraan = DB::table('kendaraan')->where('penyusutan', '<>', NULL)->get();
        $sopir = DB::table('karyawan')->where('id_jabatan',4)->get();
        $pelanggan = DB::table('pelanggan')->where('status', NULL)->get();
        
        $data['kendaraan'] = $kendaraan;
        $data['sopir'] = $sopir;
        $data['pelanggan'] = $pelanggan;

        $data['id'] = $suratjalan->id;
        $data['tgl'] = $suratjalan->tgl;
        $data['id_pelanggan'] = $suratjalan->id_pelanggan;
        $data['catatan'] = $suratjalan->catatan;
        $data['pembayaran'] = $suratjalan->pembayaran;
        $data['no_rek'] = $suratjalan->no_rek;
        $data['pengiriman'] = $suratjalan->pengiriman;
        $data['id_kendaraan'] = $suratjalan->id_kendaraan;
        $data['id_karyawan'] = $suratjalan->id_karyawan;
        $data['ongkir'] = $suratjalan->ongkir;
        $data['opsi'] = $suratjalan->opsi;
        $data['form'] = 'edit';         
        
        return view('admin.sjdefault.form')->with($data);
    }

    public function insert_log($id_sj, $id_user)
    {
        $sj =DB::table('suratjalan')->where('id', $id_sj)->first();
        $sj_array = (array)$sj;
        $user_del = ['user_del' => $id_user];
        $merge_sj = array_merge($sj_array,$user_del);
        $sj_detail = DB::table('suratjalan_detail')->where('id_sj', $id_sj)->get();

        $data_item = [];
        foreach ($sj_detail as $v) {
            $data_item[] = [
                        'id_sj' => $v->id_sj,
                        'nama_brg' => $v->nama_brg,
                        'satuan' => $v->satuan,
                        'harga' => $v->harga,
                        'harga_new' => $v->harga_new,
                        'ketr_ganti_harga' => $v->ketr_ganti_harga,
                        'qty' => $v->qty,
                        'potongan' => $v->potongan,
                        'subtotal' => $v->subtotal,
                        'ketr' => $v->ketr,
                        'ketr_tambahan' => $v->ketr_tambahan,
                        'created_at' => $v->created_at,
                        'user_add' => $v->user_add,
                        'updated_at' => $v->updated_at,
                        'user_upd' => $v->user_upd,
                        'status' => $v->status
                    ];
        }

        // dd($data_item);
        $inser_sj = DB::table('log_suratjalan')->insert($merge_sj);
        $inser_sj_detail = DB::table('log_suratjalan_detail')->insert($data_item);

        $res = '';
        if ($inser_sj && $inser_sj_detail) {
            $res = 'y';
        }

        return $res;
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id = base64_decode($req->_idSj);

        $res = [];
        
        $cek = $this->insert_log($id, $id_user);

        if ($cek == 'y') {
            $delete = DB::table('suratjalan')->where('id', $id)->delete();
        
            if($delete) {
                $delete_sj = DB::table('suratjalan_detail')->where('id_sj', $id)->delete();
                $delete_jurnal = DB::table('jurnal')->where('ref', $id)->delete();
                $res = [
                    'code' => 300,
                    'msg' => 'Data telah dihapus'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Gagal dihapus'
                ];
            }
           
        } 
        $data['response'] = $res;
        return response()->json($data);
    }

    public function hapus_jurnal_item($id_sj, $id, $ketr, $id_detail_sj)
    {
        $update_kas = DB::table('jurnal')
                            ->where('ref', $id_sj)
                            ->where('id_item', $id_detail_sj)
                            // ->where('no_akun', $item_kas)
                            ->update( [ 'status' => 9 ]);
        $update_pendapatan = DB::table('jurnal')
                            ->where('ref', $id_sj)
                            ->where('id_item', $id_detail_sj)
                            // ->where('no_akun', $item_pendapatan)
                            ->update(['status' => 9 ]);
        $res = [];

        if ($update_kas || $update_pendapatan) {
            $update_hpp = DB::table('jurnal')
                                            ->where('no_akun', '6111,00')
                                            ->where('ref', $id_sj)
                                            ->where('status', '=', NULL)
                                            ->update([
                                                'harga' => $this->get_hpp($id_sj, 1),
                                                'total' => $this->get_hpp($id_sj, 1)
                                            ]);

            $update_piutang = DB::table('jurnal')
                                            ->where('ref', $id_sj)
                                            ->where('no_akun', '1111')
                                            ->where('status', NULL)
                                            ->update([
                                                'harga' => $this->get_hpp($id_sj, 2),
                                                'total' => $this->get_hpp($id_sj, 2)
                                            ]);                                                 
             $res = [
                'code' => 200,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Data gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function delete_item(Request $req) 
    {
        $id_user = session::get('id_user');
        $id = $req->_idItem;

        $sj_detail = DB::table('suratjalan_detail')->where('id', $id)->first();
        // $id_detail_sj = $sj_detail->id;
        $id_sj = $sj_detail->id_sj;
        $nama_barang = $sj_detail->nama_brg;
        $ketr_barang = $sj_detail->ketr;

        $barang = DB::table('barang')->where('nama_brg', $nama_barang)->first();
        $id_barang = $barang->kode;

        $sj = DB::table('suratjalan')->where('id', $id_sj)->first();
        $opsi = $sj->opsi;

        $data_sj_detail = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];

        $delete_sj = DB::table('suratjalan_detail')->where('id', $id)->delete();
        if($delete_sj) {
            $jumlah = DB::table('suratjalan_detail')->where('id_sj',$id_sj)->where('status',NULL)->sum('subtotal');
            $update_total_sj = DB::table('suratjalan')->where('id', $id_sj)->update([
                'total' => $jumlah
            ]);
            $this->hapus_jurnal_item($id_sj, $id_barang, $ketr_barang, $id);
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function nota($id)
    {
        $suratjalan = DB::table('suratjalan as sj')
                            ->where('sj.status', NULL) 
                            ->where('sj.id',$id)
                            ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                            ->leftJoin('pelanggan as pl', 'sj.member', '=', 'pl.id')
                            ->select('sj.id','sj.tgl','sj.catatan','sj.pembayaran','sj.no_rek','sj.bank','sj.pengiriman','sj.id_kendaraan','sj.id_karyawan','sj.total','sj.ongkir','sj.opsi','sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.user_add',
                                        'sj.pelanggan','sj.member','pl.nama','sj.bayar', 'sj.kembalian')
                            ->first();

        $detail_barang = DB::table('suratjalan_detail as a')
                            ->where('a.status', NULL)
                            ->where('a.id_sj', $id)
                            ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                            ->select('a.id','a.nama_brg','a.ketr','a.satuan as id_satuan','a.qty','a.harga','a.harga_new','a.potongan','a.subtotal','b.nama as satuan')
                            // ->orderBy('a.nama_brg', 'ASC')
                            ->get(); 

        $transaksi = DB::table('transaksi')->where('id_sj', $id)->get();

        $data_item = [];    
        $total_qty = 0;  
        $total_pot = 0;  
        $pot = 0;
        $pot_kali = 0;
        $total_harga = 0;
        $totalq = 0;
        foreach ($detail_barang as $item) {
            $pot_kali = $item->qty * $item->potongan;
            $total_harga = $item->qty * $item->harga;
            $data_item[] = (object) [
                            'nama_brg' => $item->nama_brg,
                            'ketr' => $item->ketr,
                            'satuan' => $item->satuan,
                            'qty' => $item->qty,
                            'harga' => isset($item->harga_new) ? $item->harga_new : $item->harga,
                            'potongan' => $item->potongan,
                            'subtotal' => $item->subtotal,
                        ];
            $total_qty += $item->qty;
            $pot += $item->potongan;
            $total_pot += $pot_kali;
            $totalq += $total_harga;
        }
        
        $data_transaksi = [];
        foreach ($transaksi as $v) {
            $data_transaksi[] = (object) [
                'jenis_byr'     => $v->jenis_byr,
                'total'         => $v->total
            ];
        }
        
        $data['detail_barang'] = $data_item;
        $data['transaksi'] = $data_transaksi;
        //inisialisasi data SJ
        $data['id'] = $suratjalan->id;
        $data['tgl'] = date('d M y', strtotime($suratjalan->tgl));
        $data['catatan'] = $suratjalan->catatan;
        $data['pembayaran'] = $suratjalan->pembayaran;
        $data['no_rek'] = $suratjalan->no_rek;
        $data['bank'] = $suratjalan->bank;
        $data['total'] = $suratjalan->total + $suratjalan->ongkir;
        $data['ongkir'] = $suratjalan->ongkir;
        $data['bayar'] = $suratjalan->bayar ;
        $data['kembalian'] = $suratjalan->kembalian;
        $data['total_qty'] = $total_qty;
        $data['pot'] = $pot;
        $data['total_pot'] = $total_pot;
        $data['totalq'] = $totalq;

        //inisialisasi data pelanggan
        $data['nama'] = isset($suratjalan->pelanggan) ? $suratjalan->pelanggan : $suratjalan->nama;
        // $data['alamat'] = $suratjalan->alamat;
        // $data['no_telp'] = $suratjalan->no_telp;
        // $data['email'] = $suratjalan->email;
        $data['mengetahui'] = $this->get_karyawan($suratjalan->user_add);
        $data['cek_nota'] = isset($suratjalan->cek_nota) ? $this->get_karyawan($suratjalan->cek_nota) : '-';
        // dd($data);
        return view('admin.sjdefault.nota')->with($data);
    }

    public function sj($id)
    {
        $id = base64_decode($id);
        $suratjalan = DB::table('suratjalan as sj')
                            ->where('sj.status', NULL) 
                            ->where('sj.id',$id)
                            ->leftJoin('pelanggan as pl', 'sj.id_pelanggan', '=', 'pl.id')
                            ->leftJoin('kendaraan as k', 'sj.id_kendaraan', '=', 'k.id')
                            ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                            ->select('sj.id','sj.tgl','sj.catatan','sj.pembayaran','sj.pengiriman','sj.id_kendaraan','sj.id_karyawan','sj.total','sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.user_add',
                                        'pl.nama','pl.alamat','pl.no_telp','pl.email',
                                            'k.nama as kendaraan','k.no_plat',
                                                'kar.nama as sopir')
                            ->first();
        $detail_barang = DB::table('suratjalan_detail as a')
                            ->where('a.status', NULL)
                            ->where('a.id_sj', $id)
                            ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                            ->select('a.id','a.nama_brg','a.ketr','a.satuan as id_satuan','a.qty','a.harga','a.potongan','b.nama as satuan')
                            ->get();        
                                    
        $jumlah = DB::table('suratjalan_detail')->where('id_sj',$id)->sum('qty');
        
        $data['detail_barang'] = $detail_barang;
                
        //inisialisasi data SJ
        $data['id'] = $suratjalan->id;
        $data['tgl'] = date('d M yy', strtotime($suratjalan->tgl));
        $data['catatan'] = $suratjalan->catatan;
        $data['pembayaran'] = $suratjalan->pembayaran;
        $data['pengiriman'] = $suratjalan->pengiriman;
        $data['sopir'] = $suratjalan->sopir;
        $data['kendaraan'] = $suratjalan->kendaraan;
        $data['no_plat'] = $suratjalan->no_plat;
        $data['total'] = $suratjalan->total;
        $data['jumlah'] = $jumlah;

        //inisialisasi data pelanggan
        $data['nama'] = $suratjalan->nama;
        $data['alamat'] = $suratjalan->alamat;
        $data['no_telp'] = $suratjalan->no_telp;
        $data['email'] = $suratjalan->email;
        $data['mengetahui'] = $this->get_karyawan($suratjalan->user_add);
        $data['cek_nota'] = isset($suratjalan->cek_nota) ? $this->get_karyawan($suratjalan->cek_nota) : '-';
       
        return view('admin.sjdefault.sj')->with($data);
    }

    // START FUNGSI UNTUK REKAP SJ
    public function ketebalan($nama_brg)
    {
        $data['panjang'] = '';
        $data['lebar'] = '';
        $data['tebal'] = '';

        if ($this->is_lps($nama_brg) == 'true') {
            $pecah = explode('x', $nama_brg);
            $data['panjang'] = $pecah[0];
            $data['lebar'] = $pecah[1];
            $data['tebal'] = (int) filter_var($pecah[2],FILTER_SANITIZE_NUMBER_INT);
        } else {
            $data['panjang'] = 244;
            $data['lebar'] = 122;
            $data['tebal'] = (int) filter_var($nama_brg, FILTER_SANITIZE_NUMBER_INT);
        }
        
        return $data;
    }

    public function rekap_piutang($id_pelanggan, $subtotal, $jenis_pembayaran)
    {
        $data['subtotal'] ='';
        $data['piutang'] ='';  
        $data['bank'] ='';

        if ( $id_pelanggan == 10 || $id_pelanggan == 13 || $id_pelanggan == 25 || $id_pelanggan == 29 || $id_pelanggan == 40 || $id_pelanggan == 60 || $id_pelanggan == 61) {
            $data['piutang'] = $subtotal;
        } elseif($jenis_pembayaran == 'Transfer') {
            $data['bank'] = $subtotal;
        } else {
            $data['subtotal'] = $subtotal;
        }
        return $data;
    }

    public function get_jenis_barang($nama_brg)
    {        
        $pecah = explode('mm', $nama_brg);
        $jenis_barang = trim(isset($pecah[1]) ? $pecah[1] : $nama_brg, " ");

        return $jenis_barang;
    }

    public function rekap_ketr($ketr, $ketr_tambahan, $potongan)
    {
        $dt = [];
        $cek_ketr = isset($ketr) ? $dt[0]=$ketr : null;
        $cek_ketr_tambahan = isset($ketr_tambahan) ? $dt[1]=$ketr_tambahan : null;
        $cek_potongan = isset($potongan) ? $dt[2]='potongan '.$potongan : null;
        
        $hasil = implode(', ',$dt);
        return $hasil;
    }
    //END FUNGSI REKAP SJ

    public function excel_sj($tgl) 
    {
        $pecah = explode('&', $tgl);
          $tgl_m = $pecah[0];
            $tgl_a = $pecah[1];  

        $parent_jurnal = DB::table('parent_jurnal')
                                    ->where('status', 'tutup')
                                    ->orderBy('created_at', 'DESC')
                                    ->first();
        
        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';
        $id_sjQ  = [];

        $sj = DB::table('suratjalan')
                            ->whereDate('tgl', '<', $tgl_akhir)
                            ->get();

        foreach ($sj as $v) {
            $id_sjQ[] = $v->id;
        }

        $tgl_m_int = strtotime($tgl_m);
        $tgl_a_int = strtotime($tgl_a);

        if ( $tgl_m_int <= $tgl_akhir ) {
            $tgl_m = NULL;
        }

        if ( $tgl_a_int <= $tgl_akhir ) {
            $tgl_a = NULL;
        }

        $rekap = DB::table('suratjalan as sj')
                                ->leftJoin('suratjalan_detail as sjd', 'sj.id', '=', 'sjd.id_sj')
                                ->leftJoin('pelanggan as pl', 'sj.member', '=', 'pl.id')
                                // ->whereBetween('tgl', [$tgl_m, $tgl_a])
                                ->whereNotIn('sj.id', $id_sjQ)
                                ->select('sj.id', 'sj.tgl', 'sj.catatan', 'sj.pembayaran','sj.total','sj.member','sj.is_batal',
                                        'sjd.id as id_item','sjd.nama_brg', 'sjd.qty', 'sjd.ketr', 'sjd.ketr_tambahan', 'sjd.harga', 'sjd.harga_new','sjd.subtotal', 'sjd.potongan',
                                        'sj.pelanggan as nama_pelanggan', 'pl.nama')
                                ->orderBy('sj.tgl', 'ASC')
                                ->orderBy('sj.id', 'ASC')
                                ->orderBy('id_item', 'ASC');
                                // ->get();  
        
        if (isset($tgl_m) && isset($tgl_a)) {
            $rekapQ = $rekap->whereBetween('tgl', [$tgl_m, $tgl_a])->get();
        } 

        if (isset($tgl_m) && !isset($tgl_a)) {
            $rekapQ = $rekap->where('tgl', $tgl_m)->get();
        } 

        if (!isset($tgl_m) && isset($tgl_a)) {
            $rekapQ = $rekap->where('tgl', $tgl_a)->get();
        }

        if (!isset($tgl_m) && !isset($tgl_a)) {
            $rekapQ = $rekap->where('tgl', '')->get();
        }
        
        $data_rekap = [];
        
        $no = 1;
        $total_sale = 0;
        foreach ($rekapQ as $value) {
            $subtotal = isset($value->is_batal) ? null : $value->subtotal;
            $data_rekap[] = (object) [
                            'no' => $no++,
                            'id' => $value->id,
                            'tgl' => $value->tgl,
                            'nama_pelanggan' => isset($value->member) ? $value->nama : strtolower($value->nama_pelanggan),
                            'total' => $value->total,
                            'nama_brg' => $value->nama_brg,
                            'qty' => $value->qty,
                            'harga' => $value->harga,
                            'subtotal' => isset($value->is_batal) ? 'BATAL' : $subtotal,
                        ];

            $total_sale += $subtotal;
        }

        $dt['rekap'] = $data_rekap;
        $dt['tgl_m'] = $tgl_m;
        $dt['tgl_a'] = $tgl_a;
        $dt['total'] = $total_sale;

        // dd($dt);
        $nama_file = "Rekap SJ ".$tgl_m."-".$tgl_a.".xlsx";
        return Excel::download(new SjExport($dt), $nama_file);                               
    }

    public function print_sj_count(Request $req)
    {
        $id_sj = $req->_idSj;
        $jenis = $req->_jenis;

        $print = [];

        if ( $jenis == 'print_sj') {
           $print = [
               'print_sj' => 1
           ];
        } else {
            $print = [
                'print_nota' => 1
            ];
        }
        $update_print = DB::table('suratjalan')->where('id', $id_sj)->update($print);
        
        $res = [];

        if ($update_print) {
            $res = [
                'kode' => 200,
                'msg' => 'Berhasil Update Data'
            ];
        } else {
            $res = [
                'kode' => 400,
                'msg' => 'Gagal Update Data'
            ];            
        }

        return response()->json($res);
    }

    public function form_retur($id)
    {
        $id_sj = base64_decode($id);
        $sj = DB::table('suratjalan as a')
                        ->leftJoin('suratjalan_detail as b', 'a.id', '=', 'b.id_sj')
                        ->where('a.id', $id_sj)
                        ->select('a.id', 'a.tgl')
                        ->first();

        $data['id']     = $id_sj;
        $data['tgl']    = $sj->tgl;

        return view('admin.sjdefault.retur')->with($data);
    }

    public function datatable_detail_retur(Request $req)
    {
        $id = $req->_id;

        $data = DB::table('suratjalan as a')
                            ->leftJoin('suratjalan_detail as b', 'a.id', '=', 'b.id_sj')
                            ->leftJoin('barang as c', 'b.id_brg', '=', 'c.kode')
                            ->leftJoin('satuan as d', 'b.satuan', '=', 'd.id')
                            ->where('a.id', $id)
                            ->select('a.id', 'b.id as id_item', 'b.id_brg', 'b.nama_brg', 'b.qty', 'b.harga', 'b.potongan', 'b.subtotal',
                                        'c.hpp', 'd.nama as satuan', )
                            ->get();

        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('qty', function ($data) {
            $qty = '<span id="qty'.$data->id_item.'">'.$data->qty.'</span>';
            return $qty;
        })
        ->addColumn('hpp', function ($data) {
            $hpp = '<span id="hpp'.$data->id_item.'" class="hpp">'.$data->hpp.'</span>';
            return $hpp;
        })
        ->addColumn('qty_retur', function ($data) {
            $html = '<input type="text" id="v'.$data->id_item.'" class="qty_retur form-control" style="width:70px" autocomplete="off" value="">';
            return $html;
        })
        ->editColumn('subtotal', function ($data) {
            return '<span id="s'.$data->id_item.'" class="subtotal"></span>';
        })
        ->rawColumns(['qty_retur', 'subtotal', 'hpp', 'qty'])
        ->make(true);
    }

    public function get_ket($id_sj)
    {
        $detail_sj = DB::table('suratjalan_detail')->where('id_sj', $id_sj)->get();

        $dt = [];
        foreach ($detail_sj as $value) {
            $dt[] = $value->nama_brg;
        }

        $hasil = implode(', ', $dt);
        return $hasil;
    }

    public function save_retur(Request $req)
    {
        // dd($req->all());
        $id_sj = $req->_idSj;
        $tgl_retur = date('Y-m-d', strtotime($req->_tglRetur));
        $qty_retur = $req->_dtQty;

        $data_qtyQ = [];
        $qty_returQ = [];


        foreach ($qty_retur as $q) {
            $data_qtyQ[] = $q['id_item'];
            $qty_returQ[] = $q;
        }

        $sj_detail = DB::table('suratjalan_detail as a')
                            ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                            ->leftJoin('pelanggan as c', 'b.member', '=', 'c.id')
                            ->where('a.id_sj', $id_sj)
                            ->select('a.id_sj', 'a.id', 'a.id_brg', 'a.nama_brg', 'c.nama', 'a.harga','a.subtotal')
                            ->get();

        $dt = [];
        $item_akun = [];
        $n = 0;
        $total_qty = 0;
        $total_subtotal = 0;
        foreach ($sj_detail as $key => $v) {
            $id_ = (string)$v->id;

            $qtyQ = array_filter($qty_returQ, function ($value) use ($id_) {
                return $value['id_item'] == $id_;
            });

            if (!empty($qtyQ[$key]['qty_retur'])) {
                $dt[] = [
                    'id_sj'         => $id_sj,
                    'id_detail'     => $id_,
                    'tgl_retur'     => $tgl_retur,
                    'id_brg'        => $v->id_brg,
                    'qty_retur'     => isset($qtyQ[$key]['qty_retur']) ? $qtyQ[$key]['qty_retur'] : null,
                    'hpp'           => isset($qtyQ[$key]['hpp']) ? $qtyQ[$key]['hpp'] : null,
                    'subtotal'      => isset($qtyQ[$key]['subtotal']) ? $qtyQ[$key]['subtotal'] : null,
                ];

                    $n = $key;
                    $qty = isset($qtyQ[$key]['qty_retur']) ? $qtyQ[$key]['qty_retur'] : null;
                    $hpp = isset($qtyQ[$key]['hpp']) ? $qtyQ[$key]['hpp'] : null;
                    $subtotal =  isset($qtyQ[$key]['subtotal']) ? $qtyQ[$key]['subtotal'] : null;
                    $akun[$key]['tgl'] = $tgl_retur;
                    $akun[$key]['id_item'] =$id_;
                    $akun[$key]['no_akun'] = '140';
                    $akun[$key]['jenis_jurnal'] = 'ina-batal';
                    $akun[$key]['ref'] = $id_sj;
                    $akun[$key]['nama'] = $v->nama;
                    $akun[$key]['keterangan'] = $v->nama_brg;
                    $akun[$key]['map'] = 'd';
                    $akun[$key]['hit'] = 'b';
                    $akun[$key]['grup'] = 1;
                    $akun[$key]['qty'] = $qty;
                    $akun[$key]['m3'] = NULL;
                    $akun[$key]['harga'] = $hpp;
                    $akun[$key]['total'] = $subtotal;

                    $total_qty += $qty;
                    $total_subtotal += $subtotal;
            }
        }

        $fase_0 = $n + 1;
        $akun[$fase_0]['tgl'] = $tgl_retur;
        $akun[$fase_0]['id_item'] =$id_;
        $akun[$fase_0]['no_akun'] = '510';
        $akun[$fase_0]['jenis_jurnal'] = 'ina-batal';
        $akun[$fase_0]['ref'] = $id_sj;
        $akun[$fase_0]['nama'] = $v->nama;
        $akun[$fase_0]['keterangan'] = $this->get_ket($id_sj);
        $akun[$fase_0]['map'] = 'k';
        $akun[$fase_0]['hit'] = '';
        $akun[$fase_0]['grup'] = 1;
        $akun[$fase_0]['qty'] = $total_qty;
        $akun[$fase_0]['m3'] = NULL;
        $akun[$fase_0]['harga'] = $total_subtotal;
        $akun[$fase_0]['total'] = $total_subtotal;

        $fase_1 = $fase_0 + 1;
        $nn = 0;
        $total_sub = 0;
        $total_byk = 0;
        foreach ($sj_detail as $k => $value) {
            $nn = $fase_1++;

            $id_ = (string)$value->id;
            $qtyQ = array_filter($qty_returQ, function ($value) use ($id_) {
                return $value['id_item'] == $id_;
            });

            if (!empty($qtyQ[$k]['qty_retur'])) {
                $qtye = isset($qtyQ[$k]['qty_retur']) ? $qtyQ[$k]['qty_retur'] : null;

                $subtotal = $value->harga * $qtye;
                $akun[$nn]['tgl'] = $tgl_retur;
                $akun[$nn]['id_item'] =$id_;
                $akun[$nn]['no_akun'] = '410';
                $akun[$nn]['jenis_jurnal'] = 'ina-batal';
                $akun[$nn]['ref'] = $id_sj;
                $akun[$nn]['nama'] = $value->nama;
                $akun[$nn]['keterangan'] = $value->nama_brg;
                $akun[$nn]['map'] = 'd';
                $akun[$nn]['hit'] = 'b';
                $akun[$nn]['grup'] = 2;
                $akun[$nn]['qty'] = $qtye;
                $akun[$nn]['m3'] = NULL;
                $akun[$nn]['harga'] = $value->harga;
                $akun[$nn]['total'] = $subtotal;

                $total_byk += $qtye;
                $total_sub += $subtotal;
            }
        }

        $fase_2 = $nn + 1;
        $akun[$fase_2]['tgl'] = $tgl_retur;
        $akun[$fase_2]['id_item'] =$id_;
        $akun[$fase_2]['no_akun'] = '110';
        $akun[$fase_2]['jenis_jurnal'] = 'ina-batal';
        $akun[$fase_2]['ref'] = $id_sj;
        $akun[$fase_2]['nama'] = $v->nama;
        $akun[$fase_2]['keterangan'] = $this->get_ket($id_sj);
        $akun[$fase_2]['map'] = 'k';
        $akun[$fase_2]['hit'] = '';
        $akun[$fase_2]['grup'] = 2;
        $akun[$fase_2]['qty'] = $total_byk;
        $akun[$fase_2]['m3'] = NULL;
        $akun[$fase_2]['harga'] = $total_sub;
        $akun[$fase_2]['total'] = $total_sub;

        DB::beginTransaction();

        try {

            if (isset($tgl_retur)) {
                $insert = DB::table('retur_brg')->insert($dt);
                $inser_jurnal = DB::table('jurnal')->insert($akun);
                DB::commit();
    
                $res = [
                    'code'      => 200,
                    'msg'       => 'Berhasil Disimpan'
                ];
            } else {
                $res = [
                    'code'      => 400,
                    'msg'       => 'Gagal Disimpan'
                ];
            }
           
        } catch (Exception $th) {
            DB::rollback();
            $res = [
                'code'      => 200,
                'msg'       => 'Berhasil Disimpan'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function list_retur()
    {
        return view('admin.sjdefault.list_retur');
    }

    public function list_item_retur($id)
    {
        $fitur = DB::table('retur_brg as a')
                    ->leftJoin('suratjalan_detail as b', 'a.id_detail', '=', 'b.id')
                    ->where('a.id_sj', $id)
                    ->select('a.id','a.id_sj', 'a.id_brg', 'b.nama_brg', 'a.qty_retur','a.hpp', 'a.subtotal')
                    ->get();

        $dt = [];
        
        foreach ($fitur as $key => $value) {
            // $is_beli = ($value->is_beli == 1 ) ? 'checked' : ''; 
            // $dt[] = '<li>'.$value->qty.' - '.$value->nama_brg.'</li> 
            //         ';
            $dt[] = '<li>'.$value->qty_retur.' - '.$value->nama_brg.'</li>';
        }
        $dtt = implode('', $dt);

        return $dtt;
    }

    public function datatable_retur()
    {
        $retur = DB::table('retur_brg as a')
                        ->groupBy('id_sj')
                        // ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                        // ->leftJoin('suratjalan_detail as c', 'a.id_detail', '=', 'c.id')
                        // ->leftJoin('pelanggan as d', 'b.member', '=', 'd.id')
                        // ->select('a.id_sj', 'a.tgl_retur', 'a.qty_retur', 'a.hpp', 'a.subtotal', 'd.nama', 'c.nama_brg')
                        ->get();
        
        // $dt = [];
        // $total = 0;
        // foreach ($retur as $value) {
        //     $dt[] = (object) [
        //         'id_sj'     => $value->id_sj,
        //         'tgl_retur' => $value->tgl_retur,
        //         'qty_retur' => $value->qty_retur,
        //         'hpp'       => $value->hpp,
        //         'subtotal'  => $value->subtotal, 
        //         'nama'      => $value->nama,
        //         'nama_brg'  => $value-> nama_brg
        //     ];

        //     $total += $value->subtotal;
        // }

        // $data['dt'] = $dt;
        // $data['total'] = $total;

        // dd($data);
        return Datatables::of($retur)
        ->addIndexColumn()
        ->editColumn('id_sj', function ($retur) {
            return 'SJ '.$retur->id_sj;
        })
        ->editColumn('tgl_retur', function ($retur) {
            return date('d-m-Y', strtotime($retur->tgl_retur));
        })
        ->editColumn('nama_brg', function ($retur) {
            return $this->list_item_retur($retur->id_sj);
        })
        // ->editColumn('tgl_retur', function ($retur) use ($total){
        //     return $total;
        // })
        ->rawColumns(['nama_brg'])
        ->make(true);
    }

    // public function delete_tgl(Request $req)
    // {
    //     $id_user = session::get('id_user');
    //     $tgl_m = $req->_tglM;
    //     $tgl_a = $req->_tglA;

    //     $res = [];

    //     $delete = DB:: table('suratjalan')
    //                     ->whereBetween('tgl', [$tgl_m, $tgl_a])
    //                     ->delete();

    //     if ($delete) {
    //         $delete_sj = DB::table('suratjalan_detail')->where('id_sj', $id)->delete();
    //         $res = [
    //             'code' => 300,
    //             'msg' => 'Data telah dihapus'
    //         ];
    //     } else {
    //         $res = [
    //             'code' => 400,
    //             'msg' => 'Gagal dihapus'
    //         ];
    //     }
    //     $data['response'] = $res;
    //     return response()->json($data);
    // }
}
