<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\StokExport;

class StokController extends Controller
{
    private $apina;
    private $barangQ;
    private $sjQ;
    private $url;
    private $beli;

    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta"); 
        // $this->url = 'http://adm.wijayaplywoodsindonesia.com/api/ina/stok';
        // $this->url = 'http://192.168.5.9:8080/api/ina/stok';
        // $this->apina = json_decode(file_get_contents($this->url), true);
        $this->barangQ = DB::table('barang')->where('status', NULL)->get();
        $this->sjQ = DB::table('suratjalan_detail as a')
                        ->leftJoin('suratjalan as b', 'b.id', '=', 'a.id_sj')
                        // ->whereNotNull('b.bayar')
                        ->whereNotNull('b.is_cek_nota')
                        ->whereNull('b.is_batal')
                        ->select('b.id as id_sj','b.tgl','a.nama_brg','a.qty','a.harga','a.harga_new')
                        ->get();

        $this->beli = DB::table('beli')
                            ->whereNotNull('is_cek_beli')
                            ->get();
    }

    public function index()
    {
        return view('admin.stok.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    // a dipakek
    public function get_brg_jual($sj_detail, $id_brg, $tgl)
    {
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];
        
        $dty = array_filter($sj_detail, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($sj_detail, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        return $hasil;
    }

    public function get_brg_masuk($apiIna, $id_brg, $tgl)
    {
        $id = $id_brg;
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];

        $dty = array_filter($apiIna, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($apiIna, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        
        return $hasil;
    }

    public function get_detail_brg_masuk($apiina, $id_brg)
    {
        $id = $id_brg;

        $dty = array_filter($apiina, function ($value) use ($id) {
            return $value['id_brg'] == $id;
        });

        return $dty;
    }

    public function get_brg_jual_detail($sj_detail, $id_brg, $tgl)
    {
        $id_brg = (string)$id_brg;
        $tanggal = (string) $tgl;
        $cari = [
                    'id_brg' => $id_brg,
                    'tgl' => $tanggal
        ];

        $dty = array_filter($sj_detail->toArray(), function ($value) use ($cari) {
            return $value->id_brg == $cari['id_brg'] && $value->tgl == $cari['tgl'];
        });

        $hasil = 0;
        $tmp = [];
        foreach ($dty as $k => $v) {
            $hasil += $v->qty;
        }

        return $hasil;
    }

    public function get_brg_masuk_detail($apiIna, $id_brg, $tgl)
    {
        $id_brg = (string)$id_brg;
        $tanggal = (string) $tgl;
        $cari = [
                    'id_brg' => $id_brg,
                    'tgl' => $tanggal
        ];

        $dty = array_filter($apiIna, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl'] == $cari['tgl'];
        });

        $hasil = 0;
        $tmp = [];
        foreach ($dty as $k => $v) {
            $hasil += $v['qty'];
        }

        return $hasil;
    }

    public function get_brg_beli_detail($beli, $id_brg, $tgl)
    {
        $id_brg = (string)$id_brg;
        $tanggal = (string) $tgl;
        $cari = [
                    'id_brg' => $id_brg,
                    'tgl' => $tanggal
        ];

        $dty = array_filter($beli->toArray(), function ($value) use ($cari) {
            return $value->id_brg == $cari['id_brg'] && $value->tgl == $cari['tgl'];
        });

        $hasil = 0;
        $tmp = [];
        foreach ($dty as $k => $v) {
            $hasil += $v->qty;
        }

        return $hasil;
    }

    public function get_qty_beli($data_beli, $id_brg, $tgl)
    {
        $cari = [
                    'id_brg' => (string) $id_brg,
                    'tgl' => (string) $tgl
        ];

        $dty = array_filter($data_beli, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl'] >= $cari['tgl'];
        });

        $tmp = [];
        $hasil = 0;
        foreach ($dty as $k => $v) {
            $hasil += $v['qty'];
        }

        return $hasil;
    }
    // s dipakek

    public function datatable()
    {
        // $tgl_now = date("Y-m-d");
        $brg_masuk = [];
        $brg_sj = [];
        $brg_beli = [];

        $data = DB::table('barang')->where('status', NULL)->get();

        // $rUrl = $this->url;
        
        // $apiIna = json_decode(file_get_contents($rUrl), true);

        // foreach ($apiIna as $y => $e) {
        //     $brg_masuk[] = [
        //         'id_sj' => $e['id_sj'],
        //         'tgl' => (string)strtotime($e['tgl']),
        //         'tgl_s' => $e['tgl'],
        //         'nama_brg' => $e['nama_brg'],
        //         'id_brg' => $e['id_brg'],
        //         'qty' => $e['qty'],
        //         'harga' => $e['harga'],
        //         'harga_new' => $e['harga_new']
        //     ];
        // }

        $sj_detail = DB::table('suratjalan_detail as a')
                            ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                            // ->whereNotNull('b.bayar')
                            ->whereNotNull('b.is_cek_nota')
                            ->whereNull('b.is_batal')
                            ->select('a.id_brg','a.qty','b.tgl' )
                            ->get();
        // dd($sj_detail);

        foreach ($sj_detail as $t => $d) {
            $brg_sj[] = [
                'id_brg' => $d->id_brg,
                'qty' => $d->qty,
                'tgl' => strtotime($d->tgl)
            ];
        }

        $beli = DB::table('beli as a')
                            ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'id_detail_beli')
                            ->whereNotNull('a.is_cek_beli')
                            ->get();

        foreach ($beli as $key => $value) {
            $brg_beli[] = [
                'id_brg' => $value->id_brg,
                'qty' => $value->qty,
                'tgl' => strtotime($value->tgl)
            ];
        }

        $dt = [];
        $awal = 0;
        $hasil = 0;
        $hasil_masuk = 0;

        foreach ($data as $v) {
            $opname_time = isset($v->tgl_opname) ? (string)strtotime($v->tgl_opname) : null;               
            
            $stok_b = $this->get_qty_beli($brg_beli, $v->kode, $opname_time);
            $stok_k = $this->get_brg_jual($brg_sj, $v->kode, $opname_time);
            $stok_m = $this->get_brg_masuk($brg_masuk, $v->kode, $opname_time) + $stok_b;

            $hasil = $v->stok + $stok_b - $stok_k;

            $dt[] = (object) [
                                'kode' => $v->kode,
                                'nama_brg' => $v->nama_brg,
                                'stok_awal' => $awal,
                                'stok_masuk' => $v->stok + $stok_b,
                                'stok_beli' => $stok_b,
                                'stok_keluar' => $stok_k,
                                'sisa' => $hasil,
                                'tgl_opname' => $opname_time,
                                'list---------' => $stok_m,
                                'list_keluar' => $stok_k,
                            ];
        }

        // dd($dt);
        return datatables::of($dt)
        ->addIndexColumn()
        ->addColumn('stok_awal', function ($dt){
            return $dt->stok_awal;
        })
        ->addColumn('stok_masuk', function ($dt){
            return $dt->stok_masuk;
        })
        ->addColumn('stok_keluar', function ($dt){
            return $dt->stok_keluar;
        })
        ->addColumn('opsi', function ($dt){
            $link_detail = $sj = route('stok.detail', [base64_encode($dt->kode)]);
            return '<a href="'.$link_detail.'" class="btn btn-sm btn-primary">Detail</a>';
        })
        ->rawColumns(['stok_awal','stok_masuk', 'stok_keluar', 'sisa', 'opsi'])
        ->make(true);
    }

    public function detail($id_brg)
    {
        $kode = base64_decode($id_brg);
        $data['kode'] = $kode;

        $barang = DB::table('barang')->where('kode', $kode)->first();
        $data['nama_brg'] = $barang->nama_brg;
        return view('admin.stok.detail_stok')->with($data);
    }

    public function max_tgl($tgl, $nama_brg)
    {
        $max = DB::table('log_opname_stok')
                            ->where('tgl', $tgl)
                            ->where('nama_brg', $nama_brg)
                            ->select('nama_brg', 'stok_sesudah', DB::Raw('max(id)'))
                            ->groupBy('nama_brg', 'stok_sesudah')
                            ->first();

        return isset($max) ? $max->stok_sesudah : null;
    }

    public function datatable_detail(Request $req)
    {
        $kode = $req->_kodeBrg;
        $data = DB::table('barang')->where('kode', $kode)->first();

        // $rUrl = $this->url;
        // $apiIna = json_decode(file_get_contents($rUrl), true);

        // $detail = $this->get_detail_brg_masuk($apiIna, $data->kode);

        $tgl_set = date('Y-m-d',strtotime('2020-10-26'));
        $tgl_now = date('Y-m-d');
        // $tgl_now = date('Y-m-d', strtotime('2020-11-01'));

        $sj = DB::table('suratjalan_detail as a')
                        ->leftJoin('suratjalan as b', 'b.id', '=', 'a.id_sj')
                        ->where('a.id_brg', $data->kode)
                        ->whereBetween('b.tgl', [$tgl_set, $tgl_now])
                        // ->whereNotNull('b.bayar')
                        ->whereNotNull('b.is_cek_nota')
                        ->whereNull('b.is_batal')
                        ->select('b.id as id_sj','b.tgl','a.id_brg','a.nama_brg','a.qty','a.harga','a.harga_new')
                        ->get();

        $beli = DB::table('beli as a')
                        ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'b.id_detail_beli')
                        ->where('id_brg', $data->kode)
                        ->whereNotNull('a.is_cek_beli')
                        ->get();

        $log = DB::table('log_opname_stok')
                        // ->where('tgl', $tgl_)
                        ->where('id_brg', $data->kode)
                        ->orderBy('created_at', 'DESC')
                        ->select('tgl','nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                        ->groupBy('tgl','nama_brg', 'stok_sesudah')
                        ->get();

        $dt = [];
        $tmp_tgl = '';
        $tmp_tgl_fix = [];

        // $tmp_tgl_api = [];
        $tmp_tgl_jual = [];
        $tmp_tgl_opname = [];
        $tmp_tgl_beli = [];
        
        // foreach ($detail as $x) {
        //     $tmp_tgl_api[] = [
        //         'tgl' =>  $x['tgl']
        //     ];
        // }

        foreach ($sj as $y) {
            $tmp_tgl_jual[] = [
                'tgl' => $y->tgl
            ];
        }

        foreach ($beli as $z) {
           $tmp_tgl_beli[] = [
               'tgl' => $z->tgl
           ];
        }

        foreach ($log as $w) {
            $tmp_tgl_opname[] = [
                'tgl' => $w->tgl
            ];
        }

        // $tmp_tgl = array_merge($tmp_tgl_api, $tmp_tgl_jual);
        // $tmp_tgl_fase = array_merge($tmp_tgl, $tmp_tgl_beli);
        // $tmp_tgl_fase = array_merge($tmp_tgl, $tmp_tgl_opname);
        $tmp_tgl_fase = array_merge($tmp_tgl_jual, $tmp_tgl_opname);

        $tmp_tgl_fase1 = array_merge($tmp_tgl_fase, $tmp_tgl_beli);
        
        foreach ($tmp_tgl_fase1 as $e) {
            $tmp_tgl_fix[$e['tgl']] =  $e['tgl']; 
        }
        $hasil = 0;
        $awal = 0;
        $ketr = '';
        
        foreach (array_sort($tmp_tgl_fix) as $v) {
            $stok_b = $this->get_brg_beli_detail($beli, $data->kode, $v);
            $ketr_beli = ($stok_b > 0) ? $stok_b.' <span class="badge bg-blue">pembelian</span>' : null;

            // $stok_m = $this->get_brg_masuk_detail($detail, $data->kode, $v);
            // $ketr_masuk = ($stok_m > 0) ? $stok_m.' <span class="badge bg-green">pabrik</span>' : null;

            $stok_total_m = $stok_b;
            $stok_k = $this->get_brg_jual_detail($sj, $data->kode, $v);
            
            $log = DB::table('log_opname_stok')
                                ->where('tgl', $v)
                                ->where('nama_brg', $data->nama_brg)
                                ->orderBy('created_at', 'DESC')
                                ->select('tgl','nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                                ->groupBy('tgl','nama_brg', 'stok_sesudah')
                                ->first();

            if ( isset($log) ) {
                $awal = $log->stok_sesudah;
                $ketr = '<span class="badge bg-green">opname</span>';
            } else {
                $ketr = '';
            }

            $hasil = $awal + $stok_total_m - $stok_k;
            $dt[] = (object) [
                        'tgl' => $v,
                        'id_brg' => $data->kode,
                        'nama_brg' => $data->nama_brg,
                        'stok_awal' => $awal,
                        'stok_masuk' => $stok_total_m,
                        'stok_keluar' => $stok_k,
                        'sisa' => $hasil,
                        'ketr' => $ketr,
                        // 'ketr_m' => $ketr_masuk,
                        'ketr_b' => $ketr_beli,
                        'beli' => $stok_b
                    ];
            $awal = $hasil;
        }

        $log_opname = DB::table('log_opname_stok')
                                ->where('nama_brg', $data->nama_brg)
                                ->whereNotIn('tgl', $tmp_tgl_fix)
                                ->select('tgl')
                                ->groupBy('tgl')
                                ->get();

        foreach ($log_opname as $h) {
            $dt[] = (object) [
                'tgl' => $h->tgl,
                'id_brg' => $data->kode,
                'nama_brg' => $data->nama_brg,
                'stok_awal' => $this->max_tgl($h->tgl, $data->nama_brg),
                'stok_masuk' => null,
                'stok_keluar' => null,
                'sisa' => null,
                'ketr' => '<span class="badge bg-green">opname</span>',
                'ketr_m' => null,
                'ketr_b' => null,
                'beli' => null
            ];
        }

        return datatables::of($dt)
        ->addIndexColumn()
        ->addColumn('stok_awal', function ($dt){
            return $dt->stok_awal.' '.$dt->ketr;
        })
        ->addColumn('stok_masuk', function ($dt){
            // $br = isset($ketr_m) ? '<br>' : null;
            return '<table><tr>
                        <td rowspan="2">'.$dt->stok_masuk.'</td>
                        <td>'.$dt->ketr_b.'</td>
                    </tr>
                   </table>';
                //    '<table><tr>
                //         <td rowspan="2">'.$dt->stok_masuk.'</td>
                //         <td>'.$dt->ketr_m.$br.$dt->ketr_b.'</td>
                //     </tr>
                //    </table>';
        })
        ->addColumn('stok_keluar', function ($dt){
            return $dt->stok_keluar;
        })
        ->addColumn('sisa', function ($dt){
            return $dt->sisa;
        })
        ->addColumn('opsi', function ($dt){
            return '<a href="" class="btn btn-sm btn-primary">Detail</a>';
        })
        ->rawColumns(['stok_awal','stok_masuk', 'stok_keluar', 'sisa', 'opsi'])
        ->make(true);
    }

    public function closestnumber($cari, $tgl) {
        $akhir = null;
        foreach ($tgl as $c) {
            if ($c < $cari) {
                $akhir = $c;
            } else if ($c == $cari) {
                return $cari;
            } else if ($c > $cari) {
                return $akhir;
            }
        }
        return $akhir;
    }

    public function list_stok($tgl, $id_brg)
    {
        // $detail = $this->get_detail_brg_masuk($apina, $id_brg);
        $tgl_set = date('Y-m-d',strtotime('2020-10-26'));
        $tgl_now = date('Y-m-d');

        $sj = DB::table('suratjalan_detail as a')
                        ->leftJoin('suratjalan as b', 'b.id', '=', 'a.id_sj')
                        ->where('a.id_brg', $id_brg)
                        ->whereBetween('b.tgl', [$tgl_set, $tgl_now])
                        // ->whereNotNull('b.bayar')
                        ->whereNotNull('b.is_cek_nota')
                        ->whereNull('b.is_batal')
                        ->select('b.id as id_sj','b.tgl','a.id_brg','a.nama_brg','a.qty','a.harga','a.harga_new')
                        ->get();
        
        $tgl_ = date('Y-m-d', $tgl);
        $log = DB::table('log_opname_stok')
                        // ->where('tgl', $tgl_)
                        ->where('id_brg', $id_brg)
                        ->orderBy('created_at', 'DESC')
                        ->select('tgl','nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                        ->groupBy('tgl','nama_brg', 'stok_sesudah')
                        ->get();

        $beli = DB::table('beli as a')
                        ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'id_detail_beli')
                        ->whereNotNull('a.is_cek_beli')
                        ->where('b.id_brg', $id_brg)
                        ->get();

        $dt = [];
        $tmp_tgl = '';
        $tmp_tgl_fix = [];

        // $tmp_tgl_api = [];
        $tmp_tgl_jual = [];
        $tmp_tgl_opname = [];
        $tmp_tgl_beli = [];
        
        // foreach ($detail as $x) {
        //     $tmp_tgl_api[] = [
        //         'tgl' =>  $x['tgl']
        //     ];
        // }

        foreach ($sj as $y) {
            $tmp_tgl_jual[] = [
                'tgl' => $y->tgl
            ];
        }

        foreach ($log as $z) {
            $tmp_tgl_opname[] = [
                'tgl' => $z->tgl
            ];
        }

        foreach ($beli as $v) {
            $tmp_tgl_beli[] = [
                'tgl' => $v->tgl
            ];
        }

        // $tgl_log = isset($log) ? ['tgl' => $log->tgl] : ['tgl' => null];
        // $tmp_tgl = array_merge($tmp_tgl_api, $tmp_tgl_jual);
        // $tmp_tgl_fase2 = array_merge($tmp_tgl, $tmp_tgl_opname);
        $tmp_tgl_fase2 = array_merge($tmp_tgl_jual, $tmp_tgl_opname);
        $tmp_tgl_fase3 = array_merge($tmp_tgl_fase2, $tmp_tgl_beli);
        
        foreach ($tmp_tgl_fase3 as $e) {
            $tmp_tgl_fix[$e['tgl']] =  $e['tgl'];
        }

        $tglQ = [];
        foreach (array_sort($tmp_tgl_fix) as $t) {
            $tglQ[] = strtotime($t);
        }

        $cari_tgl = array_filter($tglQ, function ($v) use ($tgl) {
           return $v == $tgl;
        });

        $saklar = 'off';

        if (empty($cari_tgl)) {
            $saklar = 'on';
        }

        // $mx_tgl = (!empty($tmp_tgl_fix)) ? max( array_values($tglQ) ): null; 
        
        $tgl_dekat = (!empty($tmp_tgl_fix)) ? $this->closestnumber( $tgl, array_values($tglQ) ): null; 
        // $tgl_max = date('d-m-Y', $mx_tgl);

        $hasil = 0;
        $awal = 0;

        foreach (array_sort($tmp_tgl_fix) as $u) {
            $stok_b = $this->get_brg_beli_detail($beli, $id_brg, $u);
            // $stok_m = $this->get_brg_masuk_detail($detail, $id_brg, $u) + $stok_b;
            $stok_k = $this->get_brg_jual_detail($sj, $id_brg, $u);

            $log = DB::table('log_opname_stok')
                                ->where('tgl', $u)
                                ->where('id_brg', $id_brg)
                                ->orderBy('created_at', 'DESC')
                                ->select('tgl','nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                                ->groupBy('tgl','nama_brg', 'stok_sesudah')
                                ->first();

            if ( isset($log) ) {
                $awal = $log->stok_sesudah;
                $ketr = '<span class="badge bg-green">opname</span>';
            } else {
                $ketr = '';
            }

            $hasil = $awal + $stok_b - $stok_k;

            $dt[] = [
                        'tgl' => $u,
                        'tgl_time' => strtotime($u),
                        'stok_awal' => $awal,
                        'stok_masuk' => $stok_b,
                        'stok_keluar' => $stok_k,
                        'sisa' => $hasil
            ];
            $awal = $hasil;
        }

        $dty = array_filter($dt, function ($value) use ($tgl_dekat) {
            return $value['tgl_time'] == $tgl_dekat;  
        });

        // if ($tgl > $mx_tgl ) {
        //     $dty = array_filter($dt, function ($value) use ($mx_tgl) {
        //         return $value['tgl_time'] == $mx_tgl;
        //     });
        // } 

        $sty = [];

        if (!empty($dty)) {
            foreach ($dty as $s) {
               
                $sty = [
                        'stok_a' => $s['stok_awal'],
                        'stok_m' => $s['stok_masuk'],
                        'stok_k' => $s['stok_keluar'],
                        'sisaa' => $s['sisa'],
                        'kk' => $tglQ,
                        'tgl_api' => $tmp_tgl_jual
                ];
                if ($saklar == 'on') {
                    $sty = [
                        'stok_a' => $s['sisa'],
                        'stok_m' => null,
                        'stok_k' => null,
                        'sisaa' => null,
                        'kk' => $tglQ,
                        'tgl_api' => $tmp_tgl_jual
                    ];    
                }
            }
        } else {
                $sty = [
                        'stok_a' => null,
                        'stok_m' => null,
                        'stok_k' => null,
                        'sisaa' => null,
                        'kk' => $tglQ,
                        'tgl_api' => $tmp_tgl_jual
                        // 'mx_date' => $mx_tgl
                ];
        }
        
        return $sty;
    }

    public function excel_sj($tgl) 
    {
        $tgl_time = (string)strtotime($tgl);
        // $brg_masuk = [];
        $brg_sj = [];

        $data = DB::table('barang')->where('jenis_brg', 5)->where('status', NULL)->get();

        // $rUrl =  $this->url;
        
        // $apiIna = json_decode(file_get_contents($rUrl), true);

        $dt = [];
        $awal = 0;
        $hasil = 0;
        $hasil_masuk = 0;
        $hasil_harian = 0;

        $dd = [];

        // foreach ($apiIna as $y => $e) {
        //     $brg_masuk[] = [
        //         'id_sj' => $e['id_sj'],
        //         'tgl_s' => $e['tgl'],
        //         'tgl' => (string)strtotime($e['tgl']),
        //         'id_brg' => $e['id_brg'],
        //         'nama_brg' => $e['nama_brg'],
        //         'qty' => $e['qty'],
        //         'harga' => $e['harga'],
        //         'harga_new' => $e['harga_new']
        //     ];
        // }

        $sj_detail = DB::table('suratjalan_detail as a')
                            ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                            // ->whereNotNull('b.bayar')
                            ->whereNotNull('b.is_cek_nota')
                            ->whereNull('b.is_batal')
                            ->select('a.id_brg','a.qty','b.tgl' )
                            ->get();

        foreach ($sj_detail as $t => $d) {
            $brg_sj[] = [
                'id_brg' => $d->id_brg,
                'qty' => $d->qty,
                'tgl' => strtotime($d->tgl)
            ];
        }

        $beli = DB::table('beli as a')
                            ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'id_detail_beli')
                            ->whereNotNull('a.is_cek_beli')
                            ->get();

        foreach ($beli as $key => $value) {
            $brg_beli[] = [
                'id_brg' => $value->id_brg,
                'qty' => $value->qty,
                'tgl' => strtotime($value->tgl)
            ];
        }
                            

        // dd($brg_sj);
        $no = 1;
        foreach ($data as $v) {
            // $opname_time = isset($v->tgl_opname) ? (string)strtotime($v->tgl_opname) : null;
            // $stok_keluar = $this->get_brg_jual($brg_sj, $v->kode, $opname_time);
            // $stok_masuk = $this->get_brg_masuk($brg_masuk, $v->nama_brg, $opname_time);

            $log = DB::table('log_opname_stok')
                                ->where('tgl', $tgl)
                                ->where('id_brg', $v->kode)
                                ->orderBy('created_at', 'DESC')
                                ->select('tgl','nama_brg', 'stok_sesudah', DB::Raw('max(created_at) as ct'))
                                ->groupBy('tgl','nama_brg', 'stok_sesudah')
                                ->first();
            
            // $dd[] = $log;
            $awal_ = $this->list_stok($tgl_time, $v->kode)['stok_a'];
            $stok_m = $this->list_stok($tgl_time, $v->kode)['stok_m'];
            $stok_k = $this->list_stok($tgl_time, $v->kode)['stok_k']; 
            
            if ( isset($log) ) {
                $awal_ = $log->stok_sesudah;
            } 
            // else {
            //     $hasil_harian = $stok_masuk - $stok_keluar;
            // }

            $hasil = $awal_ + $stok_m - $stok_k;

            $dt[] = (object) [
                                'no' => $no++,
                                'kode' => $v->kode,
                                'nama_brg' => $v->nama_brg,
                                'harga' => $v->harga,
                                'awal' =>  $awal_,
                                'stok_masuk' =>  $stok_m,
                                'stok_keluar' => $stok_k,
                                'total' => ($this->list_stok($tgl_time, $v->kode)['stok_k']) * $v->harga,
                                // 'sisa' =>  $this->list_stok($tgl_time, $v->nama_brg, $this->apina)['sisaa'],
                                'sisa' => $hasil,
                                // 'ssss' => $hasil,
                                'list' => $this->list_stok($tgl_time, $v->kode),
                                'opname' => $log
                            ];
            
        }

        $nama_file = "Rekap SJ ".$tgl.".xlsx";
        return Excel::download(new StokExport($dt), $nama_file);
        // dd($dt);

    }
}
