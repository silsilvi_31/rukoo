<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\SjExport;

class StokOpnameController extends Controller
{
    private $url;
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
        // $this->url = 'http://adm.wijayaplywoodsindonesia.com/api/ina/stok';
        // $this->url = 'http://192.168.5.9:8080/api/ina/stok';
        // $this->apina = json_decode(file_get_contents($this->url), true);
    }

    public function index()
    {
        return view('admin.StokOpname.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function get_brg_jual($sj_detail, $id_brg, $tgl)
    {
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];
        
        $dty = array_filter($sj_detail, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($sj_detail, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        return $hasil;
    }

    public function get_brg_masuk($apiIna, $id_brg, $tgl)
    {
        $id = $id_brg;
        $cari = [
            'id' => $id_brg,
            'tgl_time' => $tgl
        ];

        $dty = array_filter($apiIna, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id'] && $value['tgl'] >= $cari['tgl_time'];
        });

        if (is_null($tgl)) {
            $dty = array_filter($apiIna, function ($value) use ($cari) {
                return $value['id_brg'] == $cari['id'];
            });
        }

        $hasil = 0;
        if (isset($dty)) {
            foreach ($dty as $v) {
                $hasil += $v['qty'];
            }
        }
        
        return $hasil;
    }

    public function get_qty_beli($data_beli, $id_brg, $tgl)
    {
        $cari = [
                    'id_brg' => (string) $id_brg,
                    'tgl' => (string) $tgl
        ];

        $dty = array_filter($data_beli, function ($value) use ($cari) {
            return $value['id_brg'] == $cari['id_brg'] && $value['tgl'] >= $cari['tgl'];
        });

        $tmp = [];
        $hasil = 0;
        foreach ($dty as $k => $v) {
            $hasil += $v['qty'];
        }

        return $hasil;
    }

    public function datatable()
    {// $tgl_now = date("Y-m-d");
        // $brg_masuk = [];
        $brg_sj = [];
        $brg_beli = [];

        $data = DB::table('barang')->where('status', NULL)->get();

        // $rUrl = $this->url;
        
        // $apiIna = json_decode(file_get_contents($rUrl), true);

        // foreach ($apiIna as $y => $e) {
        //     $brg_masuk[] = [
        //         'id_sj' => $e['id_sj'],
        //         'tgl' => (string)strtotime($e['tgl']),
        //         'tgl_s' => $e['tgl'],
        //         'nama_brg' => $e['nama_brg'],
        //         'id_brg' => $e['id_brg'],
        //         'qty' => $e['qty'],
        //         'harga' => $e['harga'],
        //         'harga_new' => $e['harga_new']
        //     ];
        // }

        $sj_detail = DB::table('suratjalan_detail as a')
                            ->leftJoin('suratjalan as b', 'a.id_sj', '=', 'b.id')
                            // ->whereNotNull('b.bayar')
                            ->whereNotNull('b.is_cek_nota')
                            ->whereNull('b.is_batal')
                            ->select('a.id_brg','a.qty','b.tgl' )
                            ->get();
        // dd($sj_detail);

        foreach ($sj_detail as $t => $d) {
            $brg_sj[] = [
                'id_brg' => $d->id_brg,
                'qty' => $d->qty,
                'tgl' => strtotime($d->tgl)
            ];
        }

        $beli = DB::table('beli as a')
                            ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'id_detail_beli')
                            ->whereNotNull('a.is_cek_beli')
                            ->get();

        foreach ($beli as $key => $value) {
            $brg_beli[] = [
                'id_brg' => $value->id_brg,
                'qty' => $value->qty,
                'tgl' => strtotime($value->tgl)
            ];
        }

        $dt = [];
        $awal = 0;
        $hasil = 0;
        $hasil_masuk = 0;

        foreach ($data as $v) {
            $opname_time = isset($v->tgl_opname) ? (string)strtotime($v->tgl_opname) : null;               
            
            $stok_b = $this->get_qty_beli($brg_beli, $v->kode, $opname_time);
            $stok_k = $this->get_brg_jual($brg_sj, $v->kode, $opname_time);
            // $stok_m = $this->get_brg_masuk($brg_masuk, $v->kode, $opname_time) + $stok_b;

            $hasil = $v->stok + $stok_b - $stok_k;

            $dt[] = (object) [
                                'kode' => $v->kode,
                                'nama_brg' => $v->nama_brg,
                                'stok_awal' => $awal,
                                'stok_masuk' => $v->stok + $stok_b,
                                'stok_beli' => $stok_b,
                                'stok_keluar' => $stok_k,
                                'sisa' => $hasil,
                                'tgl_opname' => $opname_time,
                                'list---------' => $stok_b,
                                'list_keluar' => $stok_k,
                            ];
        }

        return datatables::of($dt)
        ->addIndexColumn()
        ->addColumn('stok_awal', function ($dt){
            return $dt->stok_awal;
        })
        ->addColumn('stok_masuk', function ($dt){
            return $dt->stok_masuk;
        })
        ->addColumn('stok_keluar', function ($dt){
            return $dt->stok_keluar;
        })
        ->addColumn('sisa', function ($dt){
            return $dt->sisa;
        })
        ->addColumn('opsi', function ($dt){
            $link_detail = route('stok.detail', [base64_encode($dt->kode)]);
            $tgl = date('d-m-Y');
            return '<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal_opname" data-kode="'.$dt->kode.'" data-namabrg="'.$dt->nama_brg.'" data-sisa="'.$dt->sisa.'" data-tgl="'.$tgl.'">Opname</button>
                    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal_detail_opname" data-kode="'.$dt->kode.'" data-namabrg="'.$dt->nama_brg.'">Detail</button>';
        })
        ->rawColumns(['stok_awal','stok_masuk', 'stok_keluar', 'sisa', 'opsi'])
        ->make(true);
    }

    public function set_akun_opname($data_opname, $id_opname)
    {
        $barang = DB::table('barang')->where('kode', $data_opname['id_brg'])->first();

            $akun_debit = $data_opname['ketr'] == 'kelebihan' ? '140' : '640';
            $akun_kredit = $data_opname['ketr'] == 'kehilangan' ? '140' : '310';

            $akun[0]['tgl'] = $data_opname['tgl'];
            $akun[0]['id_item'] = NULL;
            $akun[0]['ref'] = $id_opname;
            $akun[0]['hpp'] = $barang->hpp;
            $akun[0]['grup'] = 1;
            $akun[0]['jenis_jurnal'] = 'opname';
            $akun[0]['nama'] = 'catatan perubahan kualitas'; 
            $akun[0]['no_akun'] = $akun_debit;
            $akun[0]['keterangan'] = $data_opname['nama_brg'].' // '.$data_opname['ketr'];
            $akun[0]['map'] = 'd';
            $akun[0]['hit'] = '';
            $akun[0]['qty'] = $data_opname['selisih'];
            $akun[0]['harga'] = $data_opname['selisih'] * $barang->hpp;
            $akun[0]['total'] = $data_opname['selisih'] * $barang->hpp;

            $akun[1]['tgl'] = $data_opname['tgl'];
            $akun[1]['id_item'] = NULL;
            $akun[1]['ref'] = $id_opname;
            $akun[1]['hpp'] = $barang->hpp;
            $akun[1]['grup'] = 2;
            $akun[1]['jenis_jurnal'] = 'opname';
            $akun[1]['nama'] = 'catatan perubahan kualitas'; 
            $akun[1]['no_akun'] = $akun_kredit;
            $akun[1]['keterangan'] = $data_opname['nama_brg'].' // '.$data_opname['ketr'];
            $akun[1]['map'] = 'k';
            $akun[1]['hit'] = '';
            $akun[1]['qty'] = $data_opname['selisih'];
            $akun[1]['harga'] = $data_opname['selisih'] * $barang->hpp;
            $akun[1]['total'] = $data_opname['selisih'] * $barang->hpp;

            $insert_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $kode = $req->_kode;
        $barang = DB::table('barang')->where('kode', $kode)->first();
        $nama_brg = $barang->nama_brg;
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $stok_sblm = $req->_stokSblm;
        $stok_sesudah = $req->_stokSesudah;
        $selisih = $req->_selisih;
        $ketr = $req->_ketr;

        $data_opname =  [
                            'id_brg' => $kode,
                            'nama_brg' => $nama_brg,
                            'tgl' => $tgl,
                            'stok_sblm' => $stok_sblm,
                            'stok_sesudah' => $stok_sesudah,
                            'selisih' => $selisih,
                            'ketr' => $ketr,
                            'created_at' => date("Y-m-d H:i:s"),
                            'user_add' => $id_user
                        ];

        $cek_opname = DB::table('log_opname_stok')
                                ->where('id_brg', $kode)
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($cek_opname) ? strtotime($cek_opname->tgl) : '';
        $tgl_int = strtotime($tgl);

        if ($tgl_int <= $tgl_akhir) {
            $res = [
                'code' => 400,
                'msg' => 'Tidak bisa opname ditanggal tersebut',
            ];
        } else {
            $insert_opname = DB::table('log_opname_stok')->insertGetId($data_opname);

            if($insert_opname) {
                // $update_stok = DB::table('barang')
                //                     ->where('kode', $kode)
                //                     ->update([
                //                         'stok'          => $stok_sesudah,
                //                         'tgl_opname'    => $tgl
                //                     ]);
                $this->set_akun_opname($data_opname, $insert_opname);
                
                $res = [
                    'code' => 200,
                    'msg' => 'Berhasil Disimpan',
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Gagal Disimpan'
                ];
            }
        }
        
        return response()->json($res);
    }

    public function datatable_detail(Request $req)
    {
        $kode_brg = $req->_detKodeBrg;

        $dt = [];
        $log_opname = DB::table('log_opname_stok')
                                ->where('id_brg', $kode_brg)
                                // ->where('is_cek_opname',1)
                                ->get();

        return datatables::of($log_opname)
        ->addIndexColumn()
        ->addColumn('opsi', function ($log_opname){
            $btn = '<button type="button" class="btn btn-sm btn-danger" onclick="batal_opname('.$log_opname->id.','.$log_opname->id_brg.')">Batal</button>';
            if (isset($log_opname->is_cek_opname)) {
                $btn = '<span class="badge bg-green">approved</span>';
            }
            
            return $btn;
            // return 'opsi';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function batal_opname(Request $req)
    {
        $id = $req->_id;
        $id_brg = $req->_idBrg;

        $delete_opname = DB::table('log_opname_stok')->where('id', $id)->delete();

        if (isset($delete_opname)) {
            $delete_jurnal = DB::table('jurnal')->where('ref', $id)->delete();
            
            $res = [
                'code' => 200,
                'msg' => 'Opname Berhasil Dibatalkan'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Opname Gagal Dibatalkan'
            ];
        }
        return response()->json($res);
    }

}
