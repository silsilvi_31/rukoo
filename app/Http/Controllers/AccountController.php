<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Schema;
// use Excel;
// use App\User;
// use App\Exports\UserExport;
// use App\Exports\PresensiExport;

class AccountController extends Controller
{
    public function index()
    {
        $permis = (object)[
                            'hide_jurnal', 'hide_rekap_sj', 'hide_rekap_bk', 'hide_print_sj', 'hide_print_bk'
                        ];

        $data['permis'] = $permis;
        return view('admin.account.index')->with($data);
    }

    public function datatable()
    {
        $data = DB::table('users as a')
                ->where('a.status', 1)
                ->leftJoin('karyawan as b', 'a.id', '=', 'b.id_users')
                ->leftJoin('ref_role as c', 'a.role', '=', 'c.role')
                ->leftJoin('akses as d', 'a.akses', '=', 'd.id')
                ->select('a.id', 'a.username', 'a.email', 'a.role as id_role', 'a.created_at', 'a.updated_at', 
                            'b.kode', 'b.nama',
                                'c.keterangan as role',
                                    'd.id as id_akses', 'd.nama as akses')
                ->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('email', function ($data) {
            return !is_null($data->email) ? $data->email : '-';
        })
        ->editColumn('akses', function ($data) {
            $id = $data->id;
            $kd_karyawan = $data->kode;
            $nama = $data->nama;
            $id_akses = $data->id_akses;

            return '<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-form-akses" data-id="'.$id.'" data-kd_karyawan="'.$kd_karyawan.'" data-nama="'.$nama.'" data-id_akses="'.$id_akses.'"><i class="fa fa-fw fa-edit"></i></button>&nbsp'.$data->akses;
        })
        ->addColumn('permis', function ($data) {
            $id = $data->id;
            $kd_karyawan = $data->kode;
            $nama = $data->nama;

            return '<button class="btn btn-sm btn-dark" data-toggle="modal" data-target="#modal-form-permis" data-id_pr="'.$id.'" data-kd_karyawan="'.$kd_karyawan.'" data-nama="'.$nama.'"><i class="fa fa-anchor"></i></button>';
        })
        ->editColumn('created_at', function ($data) {
            $tgl = $data->created_at;
            return !is_null($tgl) ? date("d-m-Y", strtotime($tgl)) : '-';
        })
        ->editColumn('updated_at', function ($data) {
            $tgl = $data->updated_at;
            return !is_null($tgl) ? date("d-m-Y", strtotime($tgl)) : '-';
        })
        ->addColumn('opsi', function ($data) {
            $id = $data->id;
            $kd_karyawan = $data->kode;
            $nama = $data->nama;
            $username = $data->username;
            $idd = "'".$data->id."'";
            return '<button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-form" data-id="'.$id.'" data-kd_karyawan="'.$kd_karyawan.'" data-nama="'.$nama.'" data-username="'.$username.'"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-sm btn-danger" onclick="delete_account('.$idd.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['permis', 'akses', 'opsi'])
        ->make(true);
    }

    public function karyawan_list()
    {
        $data = DB::table('karyawan as a')
                // ->whereIn('a.id_users', [null])
                ->where('a.id_users', '=', NULL)
                ->leftJoin('jabatan as b', 'a.id_jabatan', '=', 'b.id')
                ->select('a.kode', 'a.nama', 'b.jabatan')
                ->get();

        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $kode = $data->kode;
            $nama = "'".$data->nama."'";
            return '<button class="btn btn-sm btn-primary" onclick="select_karyawan('.$kode.', '.$nama.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        return view('admin.account.form');
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $kd_karyawan = $req->kode;
        $nama = $req->nama;
        $username = $req->username;
        $password = $req->password;
        $akses = $req->akses;

        $new_id_users = uniqid();
        
        $cek_account = DB::table('users')->where('username', $username)->first();

        if (!is_null($cek_account)) {
            $res = [
                'code' => 300,
                'msg' => 'Gagal tambah, username sudah digunakan !'
            ];
            $data['response'] = $res;
            return redirect()->back()->with($data);
        }else {
            $data_users = [
                'id' => $new_id_users,
                'username' => $username, 
                'password' => bcrypt($password),
                'role' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'user_add' => $id_user,
                'status' => 1,
                'akses' => $akses
            ];

            $insert_users = DB::table('users')->insert($data_users);
            if ($insert_users) {
                $update_id_user_karyawan = DB::table('karyawan')->where('kode', $kd_karyawan)->update([
                    'id_users' => $new_id_users,
                    'updated_at' => date("Y-m-d H:i:s"),
                    'user_upd' => $id_user
                ]);
                $res = [
                    'code' => 200,
                    'msg' => 'Account login berhasil dibuat'
                ];
            }else {
                $res = [
                    'code' => 400,
                    'msg' => 'Account login gagal dibuat'
                ];
            }
            $data['response'] = $res;
            return redirect()->route('account.index')->with($data);
        }
    }

    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $id = $req->_id;
        $username = $req->_username;
        $password = $req->_password;

        $update = DB::table('users')->where('id', $id)->update([
            'username' => $username,
            'password' =>  bcrypt($password),
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
        ]);

        if ($update) {
            $res = [
                'code' => 300,
                'msg' => 'Account '.$username.' berhasil diupdate'
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Account '.$username.' gagal diupdate !'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function akses_update(Request $req)
    {
        $id_user = session::get('id_user');
        $id = $req->_idUser;
        $id_akses = $req->_akses;

        $update_akses = DB::table('users')->where('id', $id)->update([
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'akses' => $id_akses
        ]);

        if ($update_akses) {
            $res = [
                'code' => 300,
                'msg' => 'Akses berhasil diupdate'
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Akses gagal diupdate !'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function permis_update(Request $req)
    {
        $id_user = $req->_id;
        $jawab = $req->permis;
        $dt = [];
        foreach ($jawab as $v) {
            $dt[$v['_permis']] = $v['_v'];
        }

        $id_usr = [
            'id_user' => $id_user
        ];

        $cek_user_permis = DB::table('permission')->where('id_user', $id_user)->first();

        if (!isset($cek_user_permis)) {
            $insert = DB::table('permission')->insert(array_merge($id_usr, $dt));
        }else {
            $update = DB::table('permission')->where('id_user', $id_user)->update($dt);
        }

        $res = [
            'code' => 300,
            'msg' => 'Permission berhasil diubah'
        ];
        $data['response'] = $res;
        return response()->json($data);
    }

    public function permis_get(Request $req)
    {
        $id_user = $req->_id;
        $permis = DB::table('permission')->where('id_user', $id_user)->first(); 
        
        $permis_name = [];

        if (isset($permis)) {
            foreach ($permis as $key => $value) {
                $permis_name[] = (object)[
                    'name' => $key,
                    'val' => $value
                ];
            }
        }
        
        $data['permis'] = $permis_name;
        return response()->json($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id = $req->_id;
        $delete = DB::table('users')->where('id', $id)->update([
            'status' => 9,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user
        ]);

        if ($delete) {
            $update_karyawan_users = DB::table('karyawan')->where('id_users', $id)->update([
                'id_users' => NULL,
                'updated_at' => date("Y-m-d H:i:s"),
                'user_upd' => $id_user
            ]);
            $res = [
                'code' => 300,
                'msg' => 'Account telah dihapus'
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Account gagal dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

    public function index_pimpinan()
    {
        return view('admin.AccountPimpinan.index');
    }

    public function data_pimpinan()
    {
        $data = DB::table('pimpinan')->get();

        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $edit = route('account_pimpinan.form', [base64_encode($data->kode)]);
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function create_account_pimpinan($kode)
    {
        $kode_pimpinan = base64_decode($kode);
        $pimpinan = DB::table('pimpinan')->where('kode', $kode_pimpinan)->first();

        $data['kode'] = $kode;
        $data['nama'] = $pimpinan->nama;
        $data['alamat'] = $pimpinan->alamat;
        $data['no_telp'] = $pimpinan->no_telp;

        return view('admin.AccountPimpinan.form')->with($data);
    }

    public function save_account_pimpinan(Request $req)
    {
        dd($req->all());
    }

    public function createv_account_pimpinan(Request $req)
    {
        $kode_p = base64_decode($req->kode_q);
        $username = $req->username;
        $password = $req->password;

        $uuid = uniqid();
        $res = [];

        $akun = [
            'id' => $uuid,
            'username' => $username,
            'password' => bcrypt($password),
            'role' => 7,
            'created_at' => date('Y-m-d H:i:s'),
            'user_add' => session::get('id_user')
        ];

        $cek_username = DB::table('users')
                        ->where('username', '=', $username)
                        ->first();
        
        if (isset($cek_username)) {
            $res = [
                'code' => 300,
                'msg' => 'Username sudah dipakai'
            ];
        }else {
            $cek_id_pimpinan = DB::table('pimpinan')->where('kode', $kode_p)->first();

            if (!isset($cek_id_pimpinan->id_users)) {
                $insert_users = DB::table('users')->insert($akun);
                $update_id_users = DB::table('pimpinan')
                                        ->where('kode', $kode_p)
                                        ->update([
                                            'id_users' => $uuid
                                        ]);
                if ($insert_users) {
                    $res = [
                        'code' => 200,
                        'msg' => 'Akun berhasil dibuat'
                    ];
                }
            }else {
                $cek_users = DB::table('users')->where('id', $cek_id_pimpinan->id_users)->first();
                if (isset($cek_users)) {
                    $upd_users = DB::table('users')->where('id', $cek_id_pimpinan->id_users)->update([
                        'username' => $username,
                        'password' => bcrypt($password)
                    ]);

                    if ($upd_users) {
                        $res = [
                            'code' => 201,
                            'msg' => 'Akun berhasil diupdate'
                        ];
                    }
                }else if(!isset($cek_users)){
                    $insert = DB::table('users')->insert($akun);
                    if ($insert) {
                        $res = [
                            'code' => 200,
                            'msg' => 'Akun berhasil dibuat'
                        ];
                    }
                }
            }
        }
        $data['response'] = $res;
        return redirect()->back()->with($data);

    }
}