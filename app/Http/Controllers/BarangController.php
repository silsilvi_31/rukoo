<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class BarangController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");    
    }

    public function index()
    {
        return view('admin.master.barang.index');
    }

    public function datatable()
    {
        $data = DB::table('barang as a')
                ->where('a.status', NULL)
                ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id') 
                ->leftJoin('jenis_barang as c', 'a.jenis_brg', '=', 'c.id')
                ->select('a.kode', 'a.nama_brg', 'a.harga', 'b.nama' ,'c.jenis_brg','a.tgl_opname as tgl', 'a.stok as stok_awal')
                ->get();
                
        // dd ($data);
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $edit = route('barang.form_edit', [base64_encode($data->kode)]);
            $kode_barang = "'".base64_encode($data->kode)."'";
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_barang('.$kode_barang.')"><i class="fa fa-trash"></i></button>';
         })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        $satuan = DB::table('satuan')->get();
        $jenis_barang = DB::table('jenis_barang')->get();

        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_barang;
        
        return view('admin.master.barang.form')->with($data);
    }

    public function save(Request $req)
     {
        $id_user = session::get('id_user');
        $nama_barang = $req->namabarang;
        $hpp = $req->harga_beli;
        $harga = $req->harga;
        // $ukuran = $req->ukuran;
        $satuan = $req->id_satuan;
        $jenis_barang = $req->id_jenis_barang;
        $data_nama_barang = [
                              'nama_brg' => $nama_barang,
                              'hpp' => $hpp,
                              'harga' => $harga,
                            //   'ukuran' => $ukuran,
                              'satuan' => $satuan,
                              'jenis_brg' => $jenis_barang,
                              'created_at' => date("Y-m-d H:i:s"),
                              'user_add' => $id_user
                            ];
        
        $insert_nama_barang = DB::table('barang')->insert($data_nama_barang);
       if ($insert_nama_barang) {
           $res =[
               'code' => 200,
               'msg' => 'Berhasil Disimpan'
           ];
        } else {
            $res = [
            'code' => 400,
            'msg' => 'Gagal disimpan'
            ];
       }
       $data['response'] = $res;
       return redirect()->route('barang.index')->with($data);
    }
    public function form_edit($kode)
    {
        $kode_barang = base64_decode($kode);
        $barang = DB::table('barang')->where('kode',$kode_barang)->first();
        $id_satuan = $barang->satuan;
        $id_jenis_barang=$barang->jenis_brg;

        $satuan = DB::table('satuan')->get();
        $jenis_barang = DB::table('jenis_barang')->get();

        $data['kode_barang'] = $barang->kode;
        $data['barang'] = $barang->nama_brg;
        $data['hpp'] = $barang->hpp;
        $data['harga'] = $barang->harga;
        // $data['ukuran'] = $barang->ukuran;
        $data['id_satuan'] = $id_satuan;
        $data['id_jenis_barang'] = $id_jenis_barang;
        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_barang;

        return view('admin.master.barang.form_edit')->with($data);
    }

    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $kode = $req->kode_barang;
        $nama_barang = $req->barang;
        $harga = $req->harga;
        // $ukuran = $req->ukuran;
        $id_satuan = $req->id_satuan;
        $id_jenis_barang = $req->id_jenis_barang;

        $data_barang = [
                        'nama_brg' => $nama_barang,
                        'harga' => $harga,
                        // 'ukuran' => $ukuran,
                        'satuan' => $id_satuan,
                        'jenis_brg' => $id_jenis_barang
                    ];

        $update = DB::table('barang')->where('kode', $kode)->update($data_barang);
        if ($update) {
            $res =[
                'code' => 201,
                'msg' => 'Berhasil Diupdate'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal Update'
            ];
        }
        $data['response'] = $res;
        return redirect()->route('barang.index')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $kode_barang = base64_decode($req->_kodeBarang);

        $data_barang = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];

        $res = [];
        $update = DB::table('barang')->where('kode', $kode_barang)->update($data_barang);

        // $delete = DB::table('barang')->where('kode', $kode_barang)->delete();
        if ($update){
            $res = [
                'code' => 300,
                'msg' => 'Berhasil dihapus'
            ];
        }else{
            $res = [
                'code' => 400,
                'msg'  => 'Gagal Dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }   

}
