<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class CekOpnameController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.CekOpname.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function datatable()
    {
        $data = DB::table('log_opname_stok')
                        ->select('id', 'tgl', 'is_cek_opname', 'cek_opname', DB::raw('COUNT(is_cek_opname) as total_cek'), DB::raw('COUNT(tgl) as total_opname'))
                        ->groupBy('tgl')
                        ->get();

        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('is_cek_opname', function ($data) {
            $is_cek_opname = $data->is_cek_opname;
            $nama = isset($is_cek_opname) ? $this->get_karyawan($data->cek_opname) : '-';
            
            if ($is_cek_opname == 1) {
                return '<center><i class="fa fa-check"></i>'.$nama.'</center>';
            } else if ($is_cek_opname == 2) {
                return '<center><i class="fa fa-close"></i>'.$nama.'</center>';
            } else if ($is_cek_opname == '') {
                return '<center><i class="fa fa-minus"></center>';
            } else {
            
            }
        }) 
        ->addColumn('opsi', function ($data){
            $edit = route('cekopname.cek_detail_opname', [($data->tgl)]);
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary" >Detail<a/>';
        })
        ->rawColumns(['is_cek_opname', 'opsi'])
        ->make(true);
    }

    public function cek_detail_opname($tgl)
    {
        $data['tgl'] = $tgl;
        return view('admin.CekOpname.detail')->with($data);
    }

    public function datatable_detail_opname(Request $req)
    {
        $tgl = $req->_tgl;
        
        $data = DB::table('log_opname_stok')
                                    ->where('tgl', $tgl)
                                    ->get();

        return Datatables::of($data)  
        ->addIndexColumn() 
        ->addColumn('cekbox', function ($data){
            $cek_opname = isset($data->is_cek_opname) ? 1 : 0;
            $cbx = '<div class="checkbox">
                        <label>
                        <input type="checkbox" value="'.$data->id.'" name="cb_detailOpname[]" class="pilih">
                        </label>
                    </div>';
            if ($cek_opname == 1) {
                $cbx = '<div class="checkbox">
                        <label>
                        <input type="checkbox" disabled="disabled" name="cb_detailOpname[]" class="pilih">
                        </label>
                    </div>';
            }
            return $cbx;
        })
        ->editColumn('is_cek_opname', function ($data) {
            $is_cek_opname = $data->is_cek_opname;
            $nama = isset($is_cek_opname) ? $this->get_karyawan($data->cek_opname) : '-';
            
            if ($is_cek_opname == 1) {
                return '<center><i class="fa fa-check"></i>'.$nama.'</center>';
            } else if ($is_cek_opname == 2) {
                return '<center><i class="fa fa-close"></i>'.$nama.'</center>';
            } else if ($is_cek_opname == '') {
                return '<center><i class="fa fa-minus"></center>';
            } else {
            
            }
        }) 
        ->rawColumns(['cekbox','is_cek_opname'])
        ->make(true);                                        
    }

    public function cek_opname(Request $req)
    {
        $id_user = session::get('id_user');
        $id = $req->_id;

        $data = [];
        $dt = [];
        foreach ($id as $v) {
            $data[] = $v['id'];
        }

        $update = DB::table('log_opname_stok')
                            ->whereIn('id', $data)
                            ->update([
                                'is_cek_opname' => 1,
                                'cek_opname' => $id_user
                            ]);
        
        $res = [];
        if ($update) {
            foreach ($id as $e) {
                $opname = DB::table('log_opname_stok')->where('id', $e['id'])->first();

                $id_brg = $opname->id_brg;
                $tgl = $opname->tgl;
                $stok_sesudah = $opname->stok_sesudah;

                $update_brg = DB::table('barang')
                                ->where('kode', $id_brg)
                                ->update([
                                    'stok' => $stok_sesudah,
                                    'tgl_opname' => $tgl
                                ]);
            }
            $res = [
                'code' => 200,
                'msg' => 'Opname Berhasil Dikonfirmasi'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Opname Gagal Dikonfirmasi'
            ];
        }
        return response()->json($res);
    }

    // public function acc_beli(Request $req)
    // {
    //     $id_users = session::get('id_user');
    //     $id = $req->_id;
    //     $btn = $req->_btn;
    //     $status = $req->_status;
        
    //     $data_acc_beli = [
    //         'is_cek_beli' => 1,
    //         'cek_beli' => $id_users,
    //         'user_upd' => $id_users,
    //         'updated_at' => date("Y-m-d H:i:s")
    //     ];

    //     $data_batal_beli = [
    //         'is_cek_beli' => 2,
    //         'cek_beli' => $id_users,
    //         'user_upd' => $id_users,
    //         'updated_at' => date("Y-m-d H:i:s")
    //     ];
        
    //     if($status != 'acc') {
    //         if ($btn == 'setuju') {
    //             $update_beli = DB::table('beli')
    //                             ->where('id_beli', $id)
    //                             ->update($data_acc_beli);
                                
    //             if ($update_beli) {
    //                 $res = [
    //                     'code' => 201,
    //                     'msg' => 'beli Berhasil Di ACC'
    //                 ];
    //             } else {
    //                 $res = [
    //                     'code' => 400,
    //                     'msg' => 'beli gagal di ACC'
    //                 ];
    //             }
    //         } else if ($btn == 'batal') {
    //             $update_beli = DB::table('beli')
    //                         ->where('id_beli', $id)
    //                         ->update($data_batal_beli);
                            
    //             if ($update_beli) {
    //                 $res = [
    //                     'code' => 300,
    //                     'msg' => 'beli Batal di Acc'
    //                 ];
    //             } else {
    //                 $res = [
    //                     'code' => 400,
    //                     'msg' => 'beli gagal untuk dibatalkan'
    //                 ];
    //             }
    //         }
    //     } else {
    //         $res = [
    //             'code' => 400,
    //             'msg' => 'beli sudah di ACC'
    //         ];
    //     }
    //     return response()->json($res);
    // }
}
