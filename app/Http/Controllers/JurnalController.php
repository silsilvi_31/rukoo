<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\JurnalExport;

class JurnalController extends Controller
{
    private $suratjalan;
    private $cekbatal_sj;
    private $beli;
    private $cek_beli;
    private $gaji;
    private $cek_gaji;
    private $kwitansi;
    private $cek_kwitansi;
    private $jurnal_umum;
    private $cek_ju;

    public function __construct()
    {
        date_default_timezone_set("Asia/jakarta");
        $this->suratjalan = DB::table('suratjalan')
                                ->where('is_cek_nota', 1)
                                ->get();

        $this->cekbatal_sj = DB::table('suratjalan')
                                ->whereNotNull('is_batal')
                                ->get();

        $this->batal_sj = DB::table('suratjalan')
                                ->WhereNotNull('is_batal')
                                ->select('id','batal')
                                ->get();

        $this->beli = DB::table('beli')
                                ->where('is_cek_beli', 1)
                                ->get();

        $this->kwitansi = DB::table('kwitansi')
                                ->where('is_cek_kwi')
                                ->get();

    }

    public function index()
    {
        $jenis_jurnal = DB::table('jurnal')
                                ->select('jenis_jurnal')
                                ->groupBy('jenis_jurnal')
                                ->get();

        $data['jenis_jurnal'] = $jenis_jurnal;
        return view('admin.jurnal.index')->with($data);
    }

    public function total(Request $req)
    {
        $tgl = $req->_tgl;
        $jenis_jurnal = $req->_jenisJurnal;

        $debit = DB::table('jurnal')
                    ->where('status', NULL)
                    ->where('map', 'd')
                    ->sum('total');

        $kredit = DB::table('jurnal')
                    ->where('status', NULL)
                    ->where('map', 'k')
                    ->sum('total');
        
        $data['total_debit'] = $debit;
        $data['total_kredit'] = $kredit;
                    
        return response()->json($data);
    }

    public function cek_sj($suratjalan, $id)
    {
        $suratjalan = DB::table('suratjalan')
                            ->where('is_cek_nota', 1)
                            ->where('id', $id)
                            ->first();

        return isset($suratjalan) ? 'y' : 'n';
    }

    public function cek_batal_sj($cekbatal, $id)
    {
        $dty = array_filter($cekbatal->toArray(), function ($v) use ($id) {
            return $v->id == $id;
        });

        return !empty($dty) ? 'y' : 'n';
    }

    public function batal_sj($batal, $id)
    {
        $dty = array_filter($batal->toArray(), function ($v) use ($id) {
            return $v->id == $id;
        });

        $dt = [];
        if (!empty($dty)) {
            foreach ($dty as $a) {
                $dt = $a->batal;
            }    
        }else {
            $dt = '';
        }
        return $dt;
    }

    public function cek_beli($beli, $id)
    {
        $beli = DB::table('beli')
                        ->where('is_cek_beli', 1)
                        ->where('id_beli', $id)
                        ->first();

        return isset($beli) ? 'y' : 'n';
    }

    public function cek_kwitansi($kwitansi, $id)
    {
        $kwitansi = DB::table('kwitansi')
                            ->where('is_cek_kwi', 1)
                            ->where('no_kwi', $id)
                            ->first();

        return isset($kwitansi) ? 'y' : 'n';
    }



    public function datatable(Request $req)
    {
        $id_users = session::get('id_user');
        $tgl = $req->tgl;
        $jenis_jurnal = $req->jenisJurnal;

        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $jurnalQ = DB::table('jurnal')
                            ->whereDate('tgl', '<=', $tgl_akhir)
                            ->get();

        $id_jurnalQ = [];
        foreach ($jurnalQ as $value) {
            $id_jurnalQ[] = $value->id;
        }

        $data = DB::table('jurnal')
                        ->where('status', NULL)
                        ->whereNotIn('id', $id_jurnalQ)
                        ->latest('tgl')
                        ->orderBy('ref', 'ASC')
                        ->orderBy('bk', 'DESC')
                        ->orderBy('bm', 'DESC')
                        ->orderBy('jenis_jurnal', 'ASC')
                        ->orderBy('grup', 'ASC')
                        ->orderBy('map', 'ASC')
                        ->orderBy('id_item', 'ASC');
            
        if ($id_users == '5eb0b96e9af8e' || $id_users == '5eb20e9126392') {
            $data->where('export', NULL);
        }

        if (isset($tgl) || isset($jenis_jurnal)) {
            $data->where('tgl', 'like', '%'.$tgl.'%');
            $data->where('jenis_jurnal', 'like', '%'.$jenis_jurnal.'%');
        } 

        $dt = [];
        foreach ($data->get() as $key => $v) {
            $is_sj = $this->cek_sj($this->suratjalan, $v->ref);
            $cek_batal_sj = $this->cek_batal_sj($this->cekbatal_sj, $v->ref);
            $batal_sj = $this->batal_sj($this->batal_sj, $v->ref);
            $is_beli = $this->cek_beli($this->beli, $v->bm);
            $is_kwitansi = $this->cek_kwitansi($this->kwitansi, $v->ref);

            if (($v->jenis_jurnal == 'sj') && ($is_sj == 'y')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => 'ina '.$v->ref,
                    'bk' => $v->bk,
                    'nama' => ($cek_batal_sj == 'y') ? 'BATAL' : strtolower($v->nama),
                    'keterangan' => ($cek_batal_sj == 'y') ? $batal_sj : $v->keterangan,
                    'map' => ($cek_batal_sj == 'y') ? '' : $v->map,
                    'hit' => ($cek_batal_sj == 'y') ? '' : $v->hit,
                    'qty' => ($cek_batal_sj == 'y') ? '' : $v->qty,
                    'm3' => ($cek_batal_sj == 'y') ? '' : $v->m3,
                    'harga' => ($cek_batal_sj == 'y') ? '' : $v->harga,
                    'total' => ($cek_batal_sj == 'y') ? '' : $v->total,
                    'status' => ($cek_batal_sj == 'y') ? '' : $v->status,
                    'batal' => $cek_batal_sj
                ];
            } elseif (($v->jenis_jurnal == 'bl') && ($is_beli == 'y'))  {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' =>(isset($v->ref)) ? 'SJ '.$v->ref : $v->jenis_jurnal.' '.$v->bm,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            } elseif (($v->jenis_jurnal == 'gaji')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            }
            elseif (($v->jenis_jurnal == 'kwitansi') && ($is_kwitansi == 'y')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            } elseif (($v->jenis_jurnal == 'ju')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            } elseif (($v->jenis_jurnal == 'gaji-mingguan')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            } elseif (($v->jenis_jurnal == 'opname')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            } elseif (($v->jenis_jurnal == 'awalan')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            } elseif (($v->jenis_jurnal == 'opname')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            } elseif (($v->jenis_jurnal == 'lr')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            } elseif (($v->jenis_jurnal == 'ina-batal')) {
                $dt[] = (object) [
                    'id' => $v->id, 
                    'tgl' => $v->tgl,
                    'jurnal' => $v->jurnal,
                    'id_item' => $v->id_item,
                    'no_akun' => $v->no_akun,
                    'hpp' => $v->hpp,
                    'grup' => $v->grup,
                    'jenis_jurnal' => $v->jenis_jurnal,
                    'ref' => $v->jenis_jurnal.' '.$v->ref,
                    'bk' => $v->bm,
                    'nama' => $v->nama,
                    'keterangan' => strtolower($v->keterangan),
                    'map' => $v->map,
                    'hit' => $v->hit,
                    'qty' => $v->qty,
                    'm3' => $v->m3,
                    'harga' => $v->harga,
                    'total' => $v->total,
                    'status' => $v->status,
                    'batal' => null
                ];
            }
        }

        return datatables::of($dt)
        ->editColumn('tgl', function ($dt) {
            $tanggal = date('d-m-Y', strtotime($dt->tgl));
            return $tanggal;
        })
        ->make(true);
    }

    public function excel_jurnal($dt)
    {
        $id_users = session::get('id_user');
        $pecah = explode('*', $dt);
        
        $tgl = ($pecah[0] != 'kosong') ? $pecah[0] : NULL ;
        $jenis_jurnal = ($pecah[1] != 'kosong') ? $pecah[1] : NULL ;
        
        $jurnal = DB::table('jurnal')
                            // ->where('export', NULL)
                            ->where('tgl', 'like', '%'.$tgl.'%')
                            ->where('jenis_jurnal', 'like', '%'.$jenis_jurnal.'%')
                            ->where('status', NULL)
                            ->latest('tgl')
                            ->orderBy('ref', 'ASC')
                            ->orderBy('bk', 'ASC')
                            ->orderBy('bm', 'ASC')
                            ->orderBy('grup', 'ASC')
                            ->orderBy('map', 'ASC')
                            ->orderBy('id_item', 'ASC')
                            // untuk cek bk
                            // ->where('bk', 'BK 6289-2915')
                            ->get();

        $dta = [];
        foreach ($jurnal as $key => $v) {
            $is_sj = $this->cek_sj($v->ref);
          if (($v->jenis_jurnal == 'sj') && ($is_sj == 'y')) {
                // if ($this->cek_sj($v->id) == 'y') {
                    $dta[] = (object) [
                        'id' => $v->id, 
                        'tgl' => $v->tgl,
                        'jurnal' => $v->jurnal,
                        'id_item' => $v->id_item,
                        'no_akun' => $v->no_akun,
                        'hpp' => $v->hpp,
                        'grup' => $v->grup,
                        'jenis_jurnal' => $v->jenis_jurnal,
                        'ref' => $v->ref,
                        'bk' => $v->bk,
                        'nama' => strtolower($v->nama),
                        'keterangan' => $v->keterangan,
                        'map' => $v->map,
                        'hit' => $v->hit,
                        'qty' => $v->qty,
                        'm3' => $v->m3,
                        'harga' => $v->harga,
                        'total' => $v->total,
                        'status' => $v->status,
                        'batal' => null,
                        'ket_batal' => null
                    ];
                // }
                }   elseif ($v->jenis_jurnal == 'bl') {
                    $dta[] = (object) [
                        'id' => $v->id, 
                        'tgl' => $v->tgl,
                        'jurnal' => $v->jurnal,
                        'id_item' => $v->id_item,
                        'no_akun' => $v->no_akun,
                        'hpp' => $v->hpp,
                        'grup' => $v->grup,
                        'jenis_jurnal' => $v->jenis_jurnal,
                        'ref' => $v->ref,
                        'bk' => $v->bm,
                        'nama' => $v->nama,
                        'keterangan' => strtolower($v->keterangan),
                        'map' => $v->map,
                        'hit' => $v->hit,
                        'qty' => $v->qty,
                        'm3' => $v->m3,
                        'harga' => $v->harga,
                        'total' => $v->total,
                        'status' => $v->status,
                        'batal' => null
                    ];
                }  else {
                    $dta[] = (object) [
                        'id' => $v->id, 
                        'tgl' => $v->tgl,
                        'jurnal' => $v->jurnal,
                        'id_item' => $v->id_item,
                        'no_akun' => $v->no_akun,
                        'hpp' => $v->hpp,
                        'grup' => $v->grup,
                        'jenis_jurnal' => $v->jenis_jurnal,
                        'ref' => $v->ref,
                        'bk' => $v->bm,
                        'nama' => $v->nama,
                        'keterangan' => strtolower($v->keterangan),
                        'map' => $v->map,
                        'hit' => $v->hit,
                        'qty' => $v->qty,
                        'm3' => $v->m3,
                        'harga' => $v->harga,
                        'total' => $v->total,
                        'status' => $v->status,
                        'batal' => null
                    ];
                }
            } 
        // dd($dta);
        if ($id_users == '5eb0b96e9af8e' || $id_users == '5eb20e9126392') {
            $jurnal = DB::table('jurnal')
                            ->where('export', NULL)
                            ->where('tgl', 'like', '%'.$tgl.'%')
                            ->where('jenis_jurnal', 'like', '%'.$jenis_jurnal.'%')
                            ->where('status', NULL)
                            ->latest('tgl')
                            ->orderBy('ref', 'ASC')
                            ->orderBy('bk', 'ASC')
                            ->orderBy('grup', 'ASC')
                            ->orderBy('map', 'ASC')
                            ->orderBy('id_item', 'ASC')
                            ->get();
        }
        // dd($jurnal);
        $nama_file = "Jurnal ".$tgl.".xlsx";
        return Excel::download(new JurnalExport($dta), $nama_file);
    }

    public function export_rmv(Request $req)
    {
        // dd($req->all());
        $id_users = session::get('id_user');
        $tgl = $req->tgl;
        $jenis_jurnal = $req->_jenisJurnal;

        $jurnal = DB::table('jurnal')
                    ->where('jenis_jurnal', 'like', '%'.$jenis_jurnal.'%')
                    ->where('tgl', 'like', '%'.$tgl.'%')
                    ->where('status', NULL)
                    ->update([
                                'export' => 1
                            ]);
        if ($jurnal) {
            $insert = DB::table('tgl_ex_jurnal')->insert([
                                                            'tgl' => $tgl,
                                                            'keterangan' => 'Berhasil export',
                                                            'user_export' => $id_users,
                                                            'date' => date("Y-m-d H:i:s")
                                                        ]);
        }else {
            $insert = DB::table('tgl_ex_jurnal')->insert([
                                                            'tgl' => $tgl,
                                                            'keterangan' => 'Gagal export',
                                                            'user_export' => $id_users,
                                                            'date' => date("Y-m-d H:i:s")
                                                        ]);
        }
    }
}
