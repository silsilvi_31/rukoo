<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\SjExport;

class JurnalUmumController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.jurnalUmum.index');
    }

    public function form()
    {
        $satuan = DB::table('satuan')->where('status', null)->get();
        $jenis_brg = DB::table('jenis_barang')->where('status', null)->get();
        $akun= DB::table('akun')->where('status', null)->get();

        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;
        return view('admin.jurnalUmum.form')->with($data);
    }

    public function no_urut()
    {
        $id_jurnalUmum = DB::table('jurnal')->where('jenis_jurnal', 'ju')->where('status',NULL)->max('ref');
        $no = $id_jurnalUmum;
        $no++;
        return response()->json($no);
    }

    public function set_akun($jurnalUmum)
    {
        $akun[0]['tgl'] = $jurnalUmum['tgl'];
        $akun[0]['id_item'] = null;
        $akun[0]['no_akun'] = $jurnalUmum['akun_debit'];
        $akun[0]['jenis_jurnal'] = 'ju';
        $akun[0]['ref'] = strtolower($jurnalUmum['ref']);
        $akun[0]['nama'] = $jurnalUmum['nama'];
        $akun[0]['keterangan'] = $jurnalUmum['ketr'];
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = null;
        $akun[0]['grup'] = 1;
        $akun[0]['qty'] = $jurnalUmum['qty'];
        $akun[0]['harga'] = $jurnalUmum['harga'];
        $akun[0]['total'] = $jurnalUmum['subtotal'];

        $akun[1]['tgl'] = $jurnalUmum['tgl'];
        $akun[1]['id_item'] = null;
        $akun[1]['no_akun'] = $jurnalUmum['akun_kredit'];
        $akun[1]['jenis_jurnal'] = 'ju';
        $akun[1]['ref'] = strtolower($jurnalUmum['ref']);
        $akun[1]['nama'] = $jurnalUmum['nama'];
        $akun[1]['keterangan'] = $jurnalUmum['ketr'];
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = null;
        $akun[1]['grup'] = 2;
        $akun[1]['qty'] = $jurnalUmum['qty'];
        $akun[1]['harga'] =  $jurnalUmum['harga'];
        $akun[1]['total'] = $jurnalUmum['subtotal'];

        $insert = DB::table('jurnal')->insert($akun);
    }

    public function save(Request $req)
    {
        $no_jurnal_umum = $req->_noJurnalUmum;
        $tgl = date("Y-m-d", strtotime($req->_tgl)) ;
        $nama = $req->_nama;
        $no_akun_debit = $req->_noAkunDebit;

        $nama_brg = $req->_namaBrg;
        $no_akun_kredit = $req->_noAkunKredit;
        $qty = $req->_qty;
        $harga = $req->_harga;
        $subtotal = $req->_subtotal;
        $ketr = $req->_ketr;

        $data_jurnal = [
            'ref' => $no_jurnal_umum,
            'tgl' => $tgl,
            'nama' => $nama,
            'akun_debit' => $no_akun_debit,
            'nama_brg' => $nama_brg,
            'akun_kredit' => $no_akun_kredit,
            'qty' => $qty,
            'harga' => $harga,
            'subtotal' => $subtotal,
            'ketr' => $ketr,
        ];

        if (!$tgl || !$nama || !$no_akun_debit) {
            $res = [
                'code' => 300,
                'msg' => 'Data Belum diisi lengkap'
            ];
        } else {
            $this->set_akun($data_jurnal);
                $res = [
                    'code' => 200,
                    'msg' => 'Data berhasil disimpan'
                ];
        }
        return response()->json($res);
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                    ->where('status', 'tutup')
                                    ->orderBy('created_at', 'DESC')
                                    ->first();
        
        $tgl_akhir = isset($parent_jurnal) ? $parent_jurnal->tgl_akhir : '';

        $jurnal = DB::table('jurnal')
                                ->where('jenis_jurnal', 'ju')
                                ->whereDate('tgl', '<', $tgl_akhir)
                                ->get();
        $id_ju = [];
        
        foreach ($jurnal as $value) {
            $id_ju[] = $value->id;
        }

        $data = DB::table('jurnal')
                        ->where('jenis_jurnal', 'ju')
                        ->where('map', 'd')
                        ->whereNotIn('id', $id_ju)
                        ->get(); 

                            
        return datatables::of($data)
        ->addIndexColumn()
        ->editColumn('tgl', function ($data) {
            $tanggal = date('d-m-Y', strtotime($data->tgl));
            return $tanggal;
        }) 
        ->addColumn('debit', function ($data){
            $debit = ($data->map == 'd') ? $data->harga : null;
            return $debit;
         })
        //  ->addColumn('kredit', function ($data){
        //     $kredit = ($data->map == 'k') ? $data->harga : null;
        //     return $kredit;
        //  })
        ->addColumn('opsi', function ($data){
            $id = $data->ref;
           return '<button type="button" class="btn btn-sm btn-danger" onclick="delete_jurnalUmum('.$id.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['tgl', 'opsi'])
        ->make(true);
    }

    public function datatable_detail(Request $req)
    {
        $no_ju = $req->_noJu;
        $data = DB::table('jurnal')
                        ->where('jenis_jurnal', 'ju')
                        ->where('ref', $no_ju)
                        ->get();

        return datatables::of($data) 
        ->addIndexColumn()
        ->make(true);
    }

    public function datatable_akun_debit()
    {
        $data = DB::table('akun')->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $no_akun = "'".$data->no_akun."'";
            $akun = "'".$data->akun."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_akun('.$no_akun.','.$akun.')">Pilih</button>';
         })
         ->rawColumns(['opsi'])
         ->make(true);
    }

    public function datatable_akun_kredit()
    {
        $data = DB::table('akun')->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $no_akun = "'".$data->no_akun."'";
            $akun = "'".$data->akun."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_akun_kredit('.$no_akun.','.$akun.')">Pilih</button>';
         })
         ->rawColumns(['opsi'])
         ->make(true);
    }

    public function datatable_brg()
    {
        $data = DB::table('barang as a')
                ->where('a.status',NULL)
                ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                ->select('a.kode', 'a.nama_brg', 'a.harga', 'a.satuan as id_satuan','b.nama as satuan', 'a.jenis_brg','a.stok','a.tgl_opname','a.no_akun')
                ->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $kode_barang = $data->kode;
            $nama_brg = "'".$data->nama_brg."'";
            $harga = $data->harga;
            $id_satuan = $data->id_satuan;
            $satuan = "'".$data->satuan."'";
            $no_akun = "'".$data->no_akun."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_brg('.$kode_barang.','.$nama_brg.','.$harga.','.$id_satuan.','.$satuan.','.$no_akun.')">Pilih</button>';
            return 'opsi';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function add_brg(Request $req)
    {
        $id_user = session::get('id_user');

        $nama_brg = $req->_namaBrg;
        $harga = $req->_harga;
        $jenis_brg = $req->_jenisBrg;
        $id_satuan = $req->_idSatuan;
        $no_akun = $req->_noAkun;

        $data_barang = [
            'nama_brg' => $nama_brg,
            'harga' => $harga,
            'jenis_brg' => $jenis_brg,
            'satuan' => $id_satuan,
            'no_akun' => $no_akun
        ];

        $return_id = DB::table('barang')->insertGetId($data_barang);

        $help_a = substr($no_akun, 0, 1);
        $help_b = substr($no_akun, 3, 1);

        $data_setAkun = [
            'kode' => $no_akun,
            'id_set' => $return_id,
            'help_a' => $help_a,
            'help_b' => $help_b,
            'help_c' => 'bl'
        ];

        if(isset($return_id)){
            // $insert_set_akun = DB::table('set_akun')->insert($data_setAkun);
            $res = [
                'code' => 200,
                'msg' => 'Barang berhasil ditambahkan',
                'kode' => $return_id,
                'nama_brg' => $nama_brg,
                'harga' => $harga,
                'jenis_brg' => $jenis_brg,
                'id_satuan' => $id_satuan,
                'no_akun' => $no_akun,
                'created_at' => date("Y-m-d H:i:s"),
                'user_add' => $id_user

            ];
        }else{
            $res = [
                'code' => 400,
                'msg' => 'Kepada Gagal ditambahkan !',
                'kode' => NULL,
                'nama_brg' => NULL,
                'harga' => NULL,      
                'jenis_brg' => NULL,
                'id_satuan' => NULL,
                'no_akun' => NULL,
                'created_at' => NULL,
                'user_add' => NULL
            ];
        }
        return response()->json($res);
    }

    public function delete(Request $req)
    {
        $id = $req->_id;
        
        $delete = DB::table('jurnal')
                        ->where('jenis_jurnal', 'ju')       
                        ->where('ref', $id)
                        ->delete();

       if ($delete) {
           $res = [
                'code'  => '300',
                'msg'   => 'Data berhasil dihapus'
           ];
        } else {
           $res = [
                'code'  => '400',
                'msg'   => 'Data Gagal dihapus'
           ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }

}
