<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class CekBeliController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.CekBeli.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? $parent_jurnal->tgl_akhir : '';                                

        $beli = DB::table('beli')
                            ->whereDate('tgl', '<', $tgl_akhir)
                            ->get();
        
        $id_beli = [];

        foreach ($beli as $value) {
            $id_beli[] = $value->id_beli;
        }

        $data = DB::table('beli as a')
                            ->leftJoin('suplier as b', 'a.suplier', '=', 'b.id')
                            ->whereNotIn('id_beli', $id_beli)
                            ->get();

        return Datatables::of($data)
        ->editColumn('is_cek_beli', function ($data) {
            $is_cek_beli = $data->is_cek_beli;
            $nama = isset($is_cek_beli) ? $this->get_karyawan($data->cek_beli) : '-';
            
            if ($is_cek_beli == 1) {
                return '<center><i class="fa fa-check"></i>'.$nama.'</center>';
            } else if ($is_cek_beli == 2) {
                return '<center><i class="fa fa-close"></i>'.$nama.'</center>';
            } else if ($is_cek_beli == '') {
                return '<center><i class="fa fa-minus"></center>';
            } else {
            
            }
        }) 
        ->addColumn('opsi', function ($data){
            $id_beli = $data->id_beli;
            $tgl = date("d-m-Y", strtotime($data->tgl));
            $suplier = $data->nama;
            $form = $data->form;
            $total = $data->total;
            $cek_beli = ($data->is_cek_beli == 1) ? 'acc' : 'tidak acc';

            return '<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal_cekbeli" data-form="cek_beli" data-id="'.$id_beli.'" data-tgl="'.$tgl.'" data-suplier="'.$suplier.'" data-total="'.$total.'" data-cek="'.$cek_beli.'">Cek Beli</button>';
        })
        ->rawColumns(['is_cek_beli', 'opsi'])
        ->make(true);
    }

    public function datatable_beli(Request $req)
    {
        $id = $req->_id;
        $data = DB::table('beli as a')
                    ->where('a.status', NULL)
                    ->where('a.id_beli', $id)
                    ->leftJoin('beli_detail as b', 'a.id_beli', '=', 'b.id_detail_beli')
                    ->leftJoin('satuan as c', 'b.id_satuan', '=', 'c.id')
                    ->select('a.id','a.id_beli','b.nama_brg','b.ketr','b.qty','b.harga','b.subtotal',
                                    'c.nama as satuan')
                    ->get();

        return Datatables::of($data)  
        ->addIndexColumn() 
        ->make(true);                                        
    }

    public function acc_beli(Request $req)
    {
        $id_users = session::get('id_user');
        $id = $req->_id;
        $btn = $req->_btn;
        $status = $req->_status;
        
        $data_acc_beli = [
            'is_cek_beli' => 1,
            'cek_beli' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $data_batal_beli = [
            'is_cek_beli' => 2,
            'cek_beli' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];
        
        if($status != 'acc') {
            if ($btn == 'setuju') {
                $update_beli = DB::table('beli')
                                ->where('id_beli', $id)
                                ->update($data_acc_beli);
                                
                if ($update_beli) {
                    $res = [
                        'code' => 201,
                        'msg' => 'beli Berhasil Di ACC'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'beli gagal di ACC'
                    ];
                }
            } else if ($btn == 'batal') {
                $update_beli = DB::table('beli')
                            ->where('id_beli', $id)
                            ->update($data_batal_beli);
                            
                if ($update_beli) {
                    $res = [
                        'code' => 300,
                        'msg' => 'beli Batal di Acc'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'beli gagal untuk dibatalkan'
                    ];
                }
            }
        } else {
            $res = [
                'code' => 400,
                'msg' => 'beli sudah di ACC'
            ];
        }
        return response()->json($res);
    }
}
