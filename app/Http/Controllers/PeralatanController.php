<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
// use Excel;
// use App\User;
// use App\Exports\UserExport;
// use App\Exports\PresensiExport;

class PeralatanController extends Controller
{
    public function index()
    {
        return view('admin.master.peralatan.index');
    }

    public function datatable()
    {
        $data = DB::table('barang as a')
                        ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                        ->where('a.status', NULL)
                        ->where('a.jenis_brg', 23)
                        ->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data) {
            $edit = route('peralatan.form_edit', [base64_encode($data->kode)]);
            return '<a href="'.$edit.'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-sm btn-danger" onclick="delete_peralatan('.$data->kode.')"><i class="fa fa-trash-o"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form()
    {
        $satuan = DB::table('satuan')->get();
        $jenis_barang = DB::table('jenis_barang')->where('id', 23)->get();

        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_barang;
        
        return view('admin.master.peralatan.form')->with($data);
    }

    public function save(Request $req)
    {
        $id_user = session::get('id_user');
        $nama_brg = $req->namabarang;
        $harga = $req->harga;
        $stok_awal = $req->stok_awal;
        $id_satuan = $req->id_satuan;
        $jenis_brg = $req->id_jenis_barang;
        
        $peralatan = [
            'nama_brg' => $nama_brg,
            'stok' => $stok_awal,
            'harga' => $harga,
            'satuan' => $id_satuan,
            'jenis_brg' => $jenis_brg
        ];

        $insert_peralatan = DB::table('barang')->insert($peralatan);
        if ($insert_peralatan) {
            $data = [
                'code' => 200,
                'msg' => 'Berhasil disimpan'
            ];
        }else {
            $data = [
                'code' => 400,
                'msg' => 'Gagal disimpan'
            ];
        }
        return redirect()->route('peralatan.index')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id= $req->_idBrg;

        $data_peralatan = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];
        $update_peralatan = DB::table('barang')->where('kode',$id)->update($data_peralatan);
        if ($update_peralatan) {
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function form_edit($id)
    {
        $id_brg = base64_decode($id);
        $barang = DB::table('barang')->where('kode',$id_brg)->first();
        $satuan = DB::table('satuan')->get();
        $jenis_brg = DB::table('jenis_barang')->where('id', 23)->get();

        $data['id_brg'] = base64_encode($barang->kode);
        $data['nama'] = $barang->nama_brg;
        $data['harga'] = $barang->harga;
        $data['id_satuan'] = $barang->satuan;
        $data['stok'] = $barang->stok;
        $data['satuan'] = $satuan;
        $data['id_jenis_barang'] = $barang->jenis_brg;
        $data['jenis_barang'] = $jenis_brg;

        return view('admin.master.peralatan.form_edit')->with($data);
    }

    public function update(Request $req)
    {
        $id_user = session::get('id_user');
        $kode_brg = base64_decode($req->kode_barang);
        $nama_brg = $req->barang;
        $harga = $req->harga;
        $id_satuan = $req->id_satuan;
        $jenis_brg = $req->id_jenis_barang;
       
        $data_peralatan = [
            'nama_brg' => $nama_brg,
            'harga' => $harga,
            'satuan' => $id_satuan,
            'jenis_brg' => $jenis_brg,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user
        ];

        $update = DB::table('barang')->where('kode', $kode_brg)->update($data_peralatan);
        if ($update) {
            $res = [
                'code' => 201,
                'msg' => 'Data telah diupdate'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal diupdate'
            ];
        }
        $data['response'] = $res;
        return redirect()->route('peralatan.index')->with($data);
        // return "update";
    }
}