<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class claimAbsenController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/jakarta");
    }

    public function index()
    {
        return view('admin.claimabsen.index');
    }

    public function total_claim($tgl, $kodep)
    {
        $total_absen = DB::table('absen')->where('tgl', $tgl)->count('id');
        $total_claim = DB::table('claimabsen')->where('tgl', $tgl)->count('id');

        $dt = [
            'total_absen' => $total_absen,
            'total_claim' => $total_claim
        ];

        return $dt;
    }

    public function datatable()
    {
        $data = DB::table('absen')
                        ->select('kodep', 'tgl', DB::raw('count(*) as total'))
                        ->groupBy('tgl')
                        ->get();


        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('total_claim', function ($data)
        {
            $kodep = $data->kodep;
            $tgl = $data->tgl;
            return $this->total_claim($data->tgl,$data->kodep)['total_absen'].' / '.$this->total_claim($data->tgl,$data->kodep)['total_claim'];
        })
        ->addColumn('opsi', function ($data) {
            $edit = route('claimabsen.form_edit', [($data->tgl)]);
            $tgl = "'".$data->tgl."'";
            $total_absen = $this->total_claim($data->tgl,$data->kodep)['total_absen'];
            $total_claim = $this->total_claim($data->tgl,$data->kodep)['total_claim'];
            $status_edit = ($total_absen == $total_claim) ? 'btn-secondary disabled' : 'btn-primary';
            $status_hapus = ($total_absen == $total_claim) ? 'btn-secondary disabled' : 'btn-danger';

            return '<a href="'.$edit.'" class="btn btn-sm '.$status_edit.'"><i class="fa fa-edit"></i><a/>
                    <button type="button" class="btn btn-sm '.$status_hapus.'" onclick="delete_absen('.$tgl.')"><i class="fa fa-trash"></i></button>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal_claim" data-tgl="'.$data->tgl.'">Claim</button>';
            // return 'opsi';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function get_nama(Request $req)
    {
        $kodep = $req->_kodep;
        $data_kar = DB::table('karyawan')
                        ->where('kode',  $kodep)
                        ->first();
            
        $nama = isset($data_kar) ? $data_kar->nama : null;
        $data['nama'] = $nama;
        return response()->json($data);
    }

    public function form()
    {
        return view('admin.claimabsen.form');
    }

    public function jamkerja($jamhadir, $jampulang)
    {
        $jamkerja=floor(abs(strtotime($jampulang)-strtotime($jamhadir)) / (60 * 60));
		return $jamkerja;
    }

    function lembur($jamkerja) {
        if ($jamkerja>9) { 
            $lembur = $jamkerja - 9;
        } else {
            $lembur = null;
        }
		
		return $lembur;
    }

    public function add_absen(Request $req)
    {
        $id_user = session::get('id_user');
        $tgl = date("Y-m-d", strtotime($req->_tgl));
        $kodep = $req->_kodep;
        $jam_hadir = $req->_jamHadir;
        $jam_pulang = $req->_jamPulang;

        $izin = $req->_izin;
        $potongan = $req->_potongan;
        $ketr = $req->_ketr;

        $jamkerja = $this->jamkerja($jam_hadir, $jam_pulang);
        $lembur = $this->lembur($jamkerja);

        $data_absen = [
                        "tgl" => $tgl,
                        "kodep" => $kodep,
                        "jamhadir" => $jam_hadir,
                        "jampulang" => $jam_pulang,
                        "izin" => $izin,
                        "potongan" => $potongan,
                        "lembur" => $lembur,
                        "ketr" => $ketr,
                        "user_add" => $id_user,
                        "created_at" => date("Y-m-d H:i:s")
                    ];

        $cek_absen = DB::table('absen')
                                ->where('kodep', $kodep)
                                ->where('tgl', $tgl)
                                ->first();

        if (isset($cek_absen)) {
            $res = [
                'code' => 400,
                'msg' => 'Kode Karyawan Telah Terpakai'
            ];
        } else {
            $insert_absen = DB::table('absen')->insert($data_absen);

            if ($insert_absen) {
                $res = [
                        'code' => 300,
                        'msg' => 'Data Berhasil Disimpan'
                ];
            } else {
                $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal Disimpan'
                ];
            }
        }

        return response()->json($res);
    }

    public function tampil_absen(Request $req)
    {
        $tgl = isset($req->_tgl) ? date('Y-m-d', strtotime($req->_tgl)) : null;
        $absen = DB::table('absen as a')
                        ->leftJoin('karyawan as b', 'a.kodep', '=', 'b.kode')
                        ->where('tgl', $tgl)
                        ->select('a.id','a.kodep', 'b.nama', 'a.tgl', 'a.jamhadir', 'a.jampulang')
                        ->get();

        return response()->json($absen);
    }

    public function form_edit($tgl)
    {
        $data['tgl'] = date('d-m-Y', strtotime($tgl));
        return view('admin.claimabsen.form')->with($data);
    }

    public function get_absen(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $dt = [];

        $absen = DB::table('absen as a')
                                ->leftJoin('karyawan as b', 'a.kodep', '=', 'b.kode')
                                ->where('a.tgl', $tgl)
                                ->get();

        foreach ($absen as $key => $v) {
            $dt[] = [
                        'id' => $v->id,
                        'tgl' => $v->tgl,
                        'kodep' => $v->kodep,
                        'nama' => $v->nama,
                        'jamhadir' => $v->jamhadir,
                        'jampulang' => $v->jampulang
                    ];
        }
        // dd($dt);
        return response()->json($dt);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $tgl = date('Y-m-d', strtotime($req->_tgl));

        $res = [];
        $delete = DB::table('absen')->where('tgl', $tgl)->delete();

        if ($delete) {
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function delete_item_absen(Request $req)
    {
        $id = $req->_id;

        $delete = DB::table('absen')->where('id', $id)->delete();

        if($delete) {
            $res = [
                'code' => 300,
                'msg' => 'Berhasil Hapus Item Absen'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal Hapus Item Absen'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }
            
    public function cek_bandingClaim($kodep, $tgl)
    {
        $cek = 't';
        $claim = DB::table('claimabsen')
                            ->where('kodep', $kodep)
                            ->where('tgl', $tgl)
                            ->first();
        if (isset($claim)) {
            $cek = 'y';
        }
        return $cek;
    }

    public function datatable_detail_absen(Request $req)
    {
        $tgl = $req->_tgl;
        $data = DB::table('absen')
                    ->where('tgl', $tgl)
                    ->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('cekbox', function ($data) {
            $kodep = $data->kodep;
            $tgl = $data->tgl;
            $cb = '<td><input type="checkbox" name="cekbok[]" value="'.$kodep.'"></td>';
            if ($this->cek_bandingClaim($kodep, $tgl) == 'y') {
                $cb = '<td><input type="checkbox" name="cekbok[]" disabled="disabled" value="'.$kodep.'"></td>';
            }
            return $cb;
        })
        ->rawColumns(['cekbox'])
        ->make(true);
    }

    public function cari_absen($data_absen, $kodep, $tgl)
    {
        $kodep = (string)$kodep;
        $tanggal = (string) $tgl;
        $cari = [
                    'kodep' => $kodep,
                    'tgl' => $tanggal
        ];

        $dty = array_filter($data_absen->toArray(), function ($value) use ($cari) {
            return $value->kodep == $cari['kodep'] && $value->tgl == $cari['tgl'];
        });

        // $hasil = 0;
        $tmp = [];
        foreach ($dty as $k => $v) {
            $tmp = [
              'tgl' =>  $v->tgl,
              'kodep' => $v->kodep,
              'jamhadir' => $v->jamhadir,
              'jampulang' => $v->jampulang,
              'izin' => $v->izin,
              'potongan' => $v->potongan
             ];
        }

        return $tmp;
    }

    function hitung_gapok($kodep,$jamkerja) {
        $karyawan = DB::table('karyawan')->where('kode', $kodep)->first();
        
        if ($jamkerja>=8) { 
			$gapok = 6 * $karyawan->gapok;
		} elseif ($jamkerja>=1 && $jamkerja<=7) {
			$gapok = ($jamkerja-1) * $karyawan->gapok;
		} else {
			$gapok = 0;
        }
        
        return $gapok;
    }

    function hitung_gajilembur($kodep,$jamkerja) {
        $karyawan = DB::table('karyawan')->where('kode', $kodep)->first();

        if ($jamkerja>=9) { 
            $gajilembur = 2 * $karyawan->gaji_lembur;
        } elseif ($jamkerja>7 && $jamkerja<=9) {
            $selisih_jamkerja = $jamkerja-7;
            $gajilembur = $selisih_jamkerja * $karyawan->gaji_lembur;
        } else {
            $gajilembur = 0;
        }
        
        return $gajilembur;
    }

    function hitung_gajilembur2($kodep,$jamkerja) {
        $karyawan = DB::table('karyawan')->where('kode', $kodep)->first();

        if ($jamkerja>9) {
            $selisih_jamlembur = $jamkerja-9;
            $gajilembur2 = $selisih_jamlembur * $karyawan->gaji_lembur2;
        } else {
            $gajilembur2 = 0;
        }
        
        return $gajilembur2;
    }

    function pot_izin($izin) {
        if (isset($izin)) {
            if ($izin == 'l' OR $izin == 'L') {
                $pot_izin = 5000;
            } else {
                $pot_izin = 2000;
            }
        } else {
            $pot_izin = null;
        }
        
        return $pot_izin;
    }

    public function set_akun($tgl)
    {
        $data = DB::table('claimabsen')
                        ->select(DB::raw('sum(total_gaji) as total_gj'), 
                                    DB::raw('sum(potongan) as pot'),
                                    DB::raw('sum(lembur) as lembur_gj'))
                        ->where('tgl', $tgl)
                        ->first();

        $max_id = DB::table('jurnal')->where('jenis_jurnal', 'gaji')->max('ref');
        $id = is_null($max_id) ? 1 : $max_id+1;

        $total_gj_pegawai = $data->total_gj + $data->lembur_gj - $data->pot;
        $tgll = date('d F Y', strtotime($tgl));
        
        $akun[0]['tgl'] = $tgl;
        $akun[0]['id_item'] = null;
        $akun[0]['no_akun'] = '620';
        $akun[0]['jenis_jurnal'] = 'gaji';
        $akun[0]['ref'] = $id;
        $akun[0]['nama'] = 'ina';
        $akun[0]['keterangan'] = 'gaji '.$tgll;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = null;
        $akun[0]['grup'] = 1;
        $akun[0]['qty'] = null;
        $akun[0]['harga'] = $total_gj_pegawai;
        $akun[0]['total'] = $total_gj_pegawai;

        $akun[1]['tgl'] = $tgl;
        $akun[1]['id_item'] = null;
        $akun[1]['no_akun'] ='220';
        $akun[1]['jenis_jurnal'] = 'gaji';
        $akun[1]['ref'] = $id;
        $akun[1]['nama'] = 'ina';
        $akun[1]['keterangan'] = 'gaji '.$tgll;
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = null;
        $akun[1]['grup'] = 2;
        $akun[1]['qty'] = null;
        $akun[1]['harga'] =  $total_gj_pegawai;
        $akun[1]['total'] = $total_gj_pegawai;

        $insert_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function simpan_claim(Request $req)
    {
        $dtt = $req->_kodep;
        $tgl = $req->_tgl;

        $data = [];
        $dt = [];
        $dataQ = [];

        $data_absen = DB::table('absen')->get();
        
        if (!isset($dtt)) {
            $res = [
                'code' => 300,
                'msg' => 'Tidak ada data dipilih'
            ];
        } else {
            foreach ($dtt as $v) {
                $tgl = $this->cari_absen($data_absen, $v['id'], $tgl)['tgl'];
                $kodep = $this->cari_absen($data_absen, $v['id'], $tgl)['kodep'];
                $jam_hadir = $this->cari_absen($data_absen, $v['id'], $tgl)['jamhadir'];
                $jam_pulang = $this->cari_absen($data_absen, $v['id'], $tgl)['jampulang'];
                $izin = $this->cari_absen($data_absen, $v['id'], $tgl)['izin'];
                $jamkerja = $this->jamkerja($jam_hadir, $jam_pulang);
                $lembur = $this->lembur($jamkerja);
                $hitung_gapok = $this->hitung_gapok($kodep, $jamkerja);
                $hitung_gajilembur = $this->hitung_gajilembur($kodep, $jamkerja);
                $hitung_gajilembur2 = $this->hitung_gajilembur2($kodep, $jamkerja);
                $hitung_total_gaji = $hitung_gapok + $hitung_gajilembur;
                $potongan_izin = $this->pot_izin($izin);
                $potongan = $this->cari_absen($data_absen, $v['id'], $tgl)['potongan'];
                $total_pot = $potongan_izin + $potongan;
                $data[] = [
                        'tgl' => $tgl,
                        'kodep' => $kodep,
                        'jamhadir' => $jam_hadir,
                        'jampulang' => $jam_pulang,
                        'jamkerja' => $jamkerja,
                        'jamlembur' => $lembur,
                        'lembur' => $hitung_gajilembur2,
                        'total_gaji' => $hitung_total_gaji,
                        'izin' => $izin,
                        'potongan' => $total_pot
                    ];

                $update_absen = DB::table('absen')->where('kodep', $kodep)->where('tgl', $tgl)->update(['status' => 1]);
            }

            $insert = DB::table('claimabsen')->insert($data);
            
            if ($insert) {
                $this->set_akun($tgl);
                $res = [
                    'code' => 200,
                    'msg' => 'Absen Berhasil Di Klaim'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Absen Gagal Di Klaim'
                ];
            }
        }
        return response()->json($res);
       
    }
}
