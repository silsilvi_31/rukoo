<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class PembelianController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/jakarta");
    }

    public function no_urut()
    {
        $id_suratjalan = DB::table('beli')->where('status',NULL)->max('id_beli');
        $no = $id_suratjalan;
        $no++;
        return response()->json($no);
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function index()
    {
        return view('admin.pembelian.index');
    }

    public function datatable()
    {
        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? $parent_jurnal->tgl_akhir : '';                                

        $beli = DB::table('beli')
                            ->whereDate('tgl', '<', $tgl_akhir)
                            ->get();
        
        $id_beli = [];

        foreach ($beli as $value) {
            $id_beli[] = $value->id_beli;
        }

        $data = DB::table('beli as a')
                            ->leftJoin('suplier as b', 'a.suplier', '=', 'b.id')
                            ->where('a.status', NULL)
                            ->whereNotIn('id_beli', $id_beli)
                            ->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->editColumn('is_cek_beli', function ($data) {
            $is_cek_beli = $data->is_cek_beli;
            $nama = isset($is_cek_beli) ? $this->get_karyawan($data->cek_beli) : '-';
            
            if ($is_cek_beli == 1) {
                return '<center><i class="fa fa-check"></i>'.$nama.'</center>';
            } else if ($is_cek_beli == 2) {
                return '<center><i class="fa fa-close"></i>'.$nama.'</center>';
            } else if ($is_cek_beli == '') {
                return '<center><i class="fa fa-minus"></center>';
            } else {
            
            }
        }) 
        ->addColumn('opsi', function ($data) {
            $edit = route('pembelian.form_edit', [base64_encode($data->id_beli)]);
            $id_beli = "'".base64_encode($data->id_beli)."'";
            $status = ($data->is_cek_beli == 1 ) ? 'btn-success' : 'btn-secondary disabled';
            $status_edit = ($data->is_cek_beli != 1 ) ? 'btn-primary' : 'btn-secondary disabled';
            $status_hapus = ($data->is_cek_beli != 1) ? 'btn-danger' : 'btn-secondary disabled';
            $status_bayar = (!isset($data->is_cek_beli) || isset($data->pembayaran) ) ? 'none' : '';
            return '<a href="'.$edit.'" class="btn btn-sm '.$status_edit.'"><i class="fa fa-edit"></i><a/>
                    <button type="button" class="btn btn-sm '.$status_hapus.'" onclick="delete_beli('.$id_beli.')"><i class="fa fa-trash"></i></button>
                    <button type="button" class="btn btn-sm btn-info" style="display:'.$status_bayar.'" data-toggle="modal" data-target="#modal_bayar" data-id="'.$data->id_beli.'" data-total="'.$data->total.'">Bayar</button>';
        })
        ->rawColumns(['opsi', 'is_cek_beli'])
        ->make(true);
    }

    public function form()
    {
        $satuan = DB::table('satuan')->get();
        $jenis_brg = DB::table('jenis_barang')->get();
        $akun = DB::table('akun')->get();
        $suplier = DB::table('suplier')->get();

        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;
        $data['suplier'] = $suplier;
        return view('admin.pembelian.form')->with($data);
    }

    public function datatable_brg()
    {
        $data = DB::table('barang as a')
                ->where('a.status',NULL)
                ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                ->select('a.kode', 'a.nama_brg','a.hpp', 'a.harga', 'a.satuan as id_satuan','b.nama as satuan', 'a.jenis_brg','a.stok','a.tgl_opname','a.no_akun')
                ->get();
        
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $kode_barang = $data->kode;
            $nama_brg = "'".$data->nama_brg."'";
            $hpp = $data->hpp;
            $harga = $data->harga;
            $id_satuan = $data->id_satuan;
            $satuan = "'".$data->satuan."'";
            $no_akun = "'".$data->no_akun."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_brg('.$kode_barang.','.$nama_brg.','.$harga.','.$id_satuan.','.$satuan.','.$no_akun.','.$hpp.')">Pilih</button>';
            return 'opsi';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function datatable_detail(Request $req)
    {
        $id_beli = $req->_idBeli;

        $data = DB::table('beli_detail as a')
                                ->where('a.status', NULL)
                                ->where('a.id_detail_beli', $id_beli)
                                ->leftJoin('satuan as b', 'a.id_satuan', '=', 'b.id')
                                ->leftJoin('barang as c', 'a.id_brg', '=', 'c.kode')
                                ->select('a.id as id_item','a.nama_brg','a.ketr','a.id_satuan as id_satuan','a.qty','a.harga','a.subtotal',
                                            'b.nama as satuan',
                                            'c.kode')
                                ->get();

        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('harga', function ($data) {
            return number_format($data->harga, 0,',', '.');
        })
        ->addColumn('subtotal', function ($data) {
            return number_format($data->subtotal, 0,',', '.');
        })
        ->addColumn('opsi', function ($data) {
            $id_item = $data->id_item;
            return '<button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$id_item.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function get_ket($id_beli)
    {
        $detail_beli = DB::table('beli_detail as a')
                                ->leftJoin('satuan as b', 'b.id', '=', 'a.id_satuan')
                                ->where('id_detail_beli', $id_beli)->get();

        $dt = [];
        foreach ($detail_beli as $value) {
            $dt[] = $value->nama_brg.' @'.$value->nama.' '.$value->ketr;
        }

        $hasil = implode(', ', $dt);
        return $hasil;
    }

    public function get_total($id_beli)
    {
        $total_d = DB::table('jurnal')->where('bm',$id_beli)
                            ->where('map', 'd')
                            ->where('jenis_jurnal', 'bl')
                            ->sum('total');
        
        return $total_d;
    }

    public function set_akun($tipe, $beli, $beli_detail, $id_beli_detail)
    {
        $suplier =  DB::table('suplier')->where('id', $beli['suplier'])->first();
        $ketr = isset($beli_detail['ketr']) ? ' // '.$beli_detail['ketr'] : '';
        $ref = ($beli['opsi'] == 1) ? $beli['no_nota_spl'] : null;
        if ($tipe == 'insert') {
            $akun[0]['tgl'] = $beli['tgl'];
            $akun[0]['id_item'] = null;
            $akun[0]['no_akun'] = $beli_detail['no_akun_brg'];
            $akun[0]['jenis_jurnal'] = 'bl';
            $akun[0]['ref'] = $ref;
            $akun[0]['bm'] = strtolower($beli['id_beli']);
            $akun[0]['nama'] = $suplier->nama;
            $akun[0]['keterangan'] = $beli_detail['nama_brg'].$ketr;
            $akun[0]['map'] = 'd';
            $akun[0]['hit'] = 'b';
            $akun[0]['grup'] = 1;
            $akun[0]['qty'] = $beli_detail['qty'];
            $akun[0]['harga'] = $beli_detail['harga'];
            $akun[0]['total'] = $beli_detail['qty'] * $beli_detail['harga'];

            $akun[1]['tgl'] = $beli['tgl'];
            $akun[1]['id_item'] = null;
            $akun[1]['no_akun'] = $beli['no_akun'];
            $akun[1]['jenis_jurnal'] = 'bl';
            $akun[1]['ref'] = $ref;
            $akun[1]['bm'] = strtolower($beli['id_beli']);
            $akun[1]['nama'] = $suplier->nama;
            $akun[1]['keterangan'] = $beli_detail['nama_brg'].$ketr;
            $akun[1]['map'] = 'k';
            $akun[1]['hit'] = 'b';
            $akun[1]['grup'] = 2;
            $akun[1]['qty'] = $beli_detail['qty'];
            $akun[1]['harga'] =  $beli_detail['harga'];
            $akun[1]['total'] = $beli_detail['qty'] * $beli_detail['harga'];

            $insert_jurnal = DB::table('jurnal')->insert($akun);
        } elseif ($tipe == 'add_item') { 
            $cek_jurnal = DB::table('jurnal')->where('bm', $beli['id_beli'])->where('jenis_jurnal', 'bl')->first();

            if ( !isset ($cek_jurnal) ) {
                $akun[0]['tgl'] = $beli['tgl'];
                $akun[0]['id_item'] = null;
                $akun[0]['no_akun'] = $beli['no_akun'];
                $akun[0]['jenis_jurnal'] = 'bl';
                $akun[0]['ref'] = $ref;
                $akun[0]['bm'] = strtolower($beli['id_beli']);
                $akun[0]['nama'] = $suplier->nama;
                $akun[0]['keterangan'] = $beli_detail['nama_brg'].$ketr;
                $akun[0]['map'] = 'k';
                $akun[0]['hit'] = 'b';
                $akun[0]['grup'] = 2;
                $akun[0]['qty'] = $beli_detail['qty'];
                $akun[0]['harga'] = $beli_detail['harga'];
                $akun[0]['total'] = $beli_detail['qty'] * $beli_detail['harga'];
            }

            $akun[1]['tgl'] = $beli['tgl'];
            $akun[1]['id_item'] = $id_beli_detail;
            $akun[1]['no_akun'] = $beli_detail['no_akun_brg'];
            $akun[1]['jenis_jurnal'] = 'bl';
            $akun[1]['ref'] = $ref;
            $akun[1]['bm'] = strtolower($beli['id_beli']);
            $akun[1]['nama'] = $suplier->nama;
            $akun[1]['keterangan'] = $beli_detail['nama_brg'].$ketr;
            $akun[1]['map'] = 'd';
            $akun[1]['hit'] = 'b';
            $akun[1]['grup'] = 1;
            $akun[1]['qty'] = $beli_detail['qty'];
            $akun[1]['harga'] =  $beli_detail['harga'];
            $akun[1]['total'] = $beli_detail['qty'] * $beli_detail['harga'];

            $insert_jurnal = DB::table('jurnal')->insert($akun);
            $upd_jurnal = DB::table('jurnal')
                                        ->where('bm', $beli['id_beli'])
                                        ->where('jenis_jurnal', 'bl')
                                        ->where('map','k')
                                        ->update([
                                            'keterangan' => $this->get_ket($beli['id_beli']),
                                            'harga' => $this->get_total($beli['id_beli']),
                                            'total' => $this->get_total($beli['id_beli']),
                                        ]);
        }
    }

    public function update_jurnal($beli)
    {
        $suplier =  DB::table('suplier')->where('id', $beli['suplier'])->first();
        $update_jurnal = DB::table('jurnal')
                                ->where('bm', $beli['id_beli'])
                                ->update([
                                    'tgl' => date('Y-m-d', strtotime($beli['tgl'])),
                                    'nama' => $suplier->nama
                                ]);
        
        $update_jurnal = DB::table('jurnal')
                                ->where('bm', $beli['id_beli'])
                                ->where('map', 'k')
                                ->update([
                                    'no_akun' => $beli['no_akun'],
                                ]);
    }

    public function save(Request $req)
    {
        // dd($req->all());
        $id_user = session::get('id_user');
        $id_beli = $req->_idBeli;
        $no_nota_spl = $req->_noNotaSpl;
        $tgl = $req->_tgl;
        $nama = $req->_nama;
        $no_akun_k = $req->_noAkunK;
        $opsi = $req->_opsi;

        $id_item = $req->_idItem;
        $id_brg = $req->_idBrg;
        $nama_brg = $req->_namaBrg;
        $no_akun_d = $req->_noAkunD;
        $id_satuan = $req->_idSatuan; 
        $qty = $req->_qty;
        $hpp = $req->_hpp;
        $harga = $req->_harga;
        $subtotal = $req->_subtotal;
        $ketr = $req->_ketr;
        
        $beli = [
                        'id_beli' => $id_beli,
                        'tgl' => date('Y-m-d', strtotime($tgl)),
                        'suplier' => $nama,
                        'no_nota_spl' => $no_nota_spl,
                        'no_akun' => $no_akun_k,
                        'opsi' => $opsi,
                        "created_at" => date("Y-m-d H:i:s"),
                        "user_add" => $id_user
                        ];

        $detail_beli = [
                            'id_detail_beli' => $id_beli,
                            'id_brg' => $id_brg,
                            'nama_brg' => $nama_brg,
                            'no_akun_brg' => $no_akun_d,
                            'ketr' => $ketr,
                            'id_satuan' => $id_satuan,
                            'qty' => $qty,
                            'harga' => $hpp,
                            'subtotal' => $subtotal
                        ];
        $cek_beli = DB::table('beli')->where('id_beli',$id_beli)->first();
        
        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();

        $tgl_int = strtotime($tgl);
        $tgl_akhir_int = isset($parent_jurnal) ? strtotime($parent_jurnal->tgl_akhir) : null;
        // dd($tgl_int);
        if (!$tgl || !$nama || !$no_nota_spl) {
            $res = [
                'code' => 400,
                'msg' => 'Data Belum Lengkap'
            ];
        } elseif ($tgl_int <= $tgl_akhir_int) {
            $res = [
                'code' => 400,
                'msg' => 'Tgl tidak sesuai'
            ];
        } else {
            if (is_null($cek_beli)) {
                 $insert_beli = DB::table('beli')->insert($beli);
                if($insert_beli) {
                    $insert_detail_beli = DB::table('beli_detail')->insertGetId($detail_beli);
                    $this->set_akun('insert', $beli, $detail_beli, $insert_detail_beli);
                    $jumlah = DB::table('beli_detail')->where('id_detail_beli',$id_beli)->where('status',NULL)->sum('subtotal');
                    $update_total_beli = DB::table('beli')->where('id_beli', $id_beli)->update([
                        'total' => $jumlah
                    ]);
                    $res = [
                        'code' => 300,
                        'msg' => 'Data Berhasil disimpan'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Data Gagal disimpan'
                    ];
                }
            } elseif (!is_null($cek_beli)) {
                if (isset($id_item)) {
                    $beli_detail = DB::table('beli_detail')->where('id', $id_item)->first();

                    $update_item = DB::table('beli_detail')->where('id',$id_item)->update($detail_beli);

                    if ($update_item) {
                        $jumlah = DB::table('beli_detail')->where('id_sj',$id_beli)->where('status',NULL)->sum('subtotal');
                        $update_total_sj = DB::table('suratjalan')->where('id_beli', $id_beli)->update([
                            'total' => $jumlah,
                        ]);
                        $res = [
                            'code' => 200,
                            'msg' => 'Data Barang Berhasil Diupdate',
                        ];
                    } else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Data Barang Gagal Diupdate'
                        ];
                    }
                } else {
                    if (isset($nama_brg)) {
                        $insert_beli_detail = DB::table('beli_detail')->insertGetId($detail_beli);
                        if (isset($insert_beli_detail)) {
                            $this->set_akun('add_item', $beli, $detail_beli, $insert_beli_detail);
                            $jumlah = DB::table('beli_detail')->where('id_detail_beli',$id_beli)->where('status',NULL)->sum('subtotal');
                            $update_total_sj = DB::table('beli')->where('id_beli', $id_beli)->update([
                                'total' => $jumlah,
                            ]);
                            $res = [
                                'code' => 200,
                                'msg' => 'Data Barang Berhasil Disimpan',
                            ];
                        } else {
                            $res = [
                                'code' => 400,
                                'msg' => 'Data Barang Gagal Disimpan'
                            ];
                        }
                    }
                    $update_beli =  DB::table('beli')->where('id_beli', $id_beli)->update($beli);
                    $this->update_jurnal($beli);
                    
                    if ($update_beli) {
                        $res = [
                            'code' => 201,
                            'msg' => 'SJ Berhasil Diupdate',
                        ];
                    }else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Gagal update SJ !'
                        ];
                    }
                }
            }
        }
        
        return response()->json($res);
    }

    public function simpan_bayar(Request $req)
    {
        $id_user = session::get('id_user');
        $id_beli = $req->_idBeli;
        $total_bayar = $req->_totalBayar;
        $tanggal = $req->_tanggal;
        $pembayaran = $req->_pembayaran;

        $dtt = [
            'tgl_bayar' => date("Y-m-d H:i:s", strtotime($tanggal)),
            'pembayaran' => $pembayaran,
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
        ];

        $beli = DB::table('beli as a')
                            ->leftJoin('suplier as b', 'a.suplier', '=', 'b.id')
                            ->where('a.id_beli', $id_beli)
                            ->first();

        $suplier = $beli->nama;

        $update = DB::table('beli')->where('id_beli', $id_beli)->update($dtt);

        if ($update) {
            $this->set_akun_bayar($id_beli, $total_bayar, $suplier, $dtt);
            $res = [
                'code' => 201,
                'msg' => 'Tanda Terima Berhasil Disimpan',
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Tanda Terima Gagal Disimpan',
            ];
        }
        return response()->json($res);

    }

    public function set_akun_bayar($id, $total_bayar, $suplier, $dtt)
    {
        // kas/bank
        $akun[0]['tgl'] = $dtt['tgl_bayar'];
        $akun[0]['id_item'] = NULL;
        $akun[0]['bm'] = $id;
        $akun[0]['hpp'] = NULL;
        $akun[0]['grup'] = 3;
        $akun[0]['jenis_jurnal'] = 'bl';
        $akun[0]['nama'] = $suplier; 
        $akun[0]['no_akun'] = '210';
        $akun[0]['keterangan'] = null;
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = '';
        $akun[0]['qty'] = NULL;
        $akun[0]['harga'] = $total_bayar;
        $akun[0]['total'] = $total_bayar;
        
        // piutang
        $akun[1]['tgl'] = $dtt['tgl_bayar'];
        $akun[1]['id_item'] = null;
        $akun[1]['bm'] =  $id;
        $akun[1]['hpp'] = 1;
        $akun[1]['grup'] = 3;
        $akun[1]['jenis_jurnal'] = 'bl';
        $akun[1]['nama'] = $suplier;
        $akun[1]['no_akun'] = $dtt['pembayaran'] == 'Transfer' ? 120 : 110;
        $akun[1]['keterangan'] = '';
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = 'b';
        $akun[1]['qty'] = null;
        $akun[1]['harga'] = $total_bayar;
        $akun[1]['total'] = $total_bayar;

        $simpan_jurnal = DB::table('jurnal')->insert($akun);
    }

    public function hapus_jurnal_item($id_beli)
    {
        $update_jurnal = DB::table('jurnal')
                                ->where('bm', $id_beli)
                                ->where('jenis_jurnal', 'bl')
                                ->where('map', 'k')
                                ->where('status', NULL)
                                ->update([
                                    'keterangan' => $this->get_ket($id_beli),
                                    'harga' => $this->get_total($id_beli),
                                    'total' => $this->get_total($id_beli)
                                ]);
    }

    public function delete_item(Request $req) 
    {
        $id_user = session::get('id_user');
        $id = $req->_idItem;

        $beli_detail = DB::table('beli_detail')->where('id', $id)->first();
        $id_beli = $beli_detail->id_detail_beli;

        $data_beli_detail = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];

        $delete_beli_detail = DB::table('beli_detail')->where('id', $id)->delete();
        if($delete_beli_detail) {
            $delete_item_jurnal = DB::table('jurnal')->where('id_item', $id)->delete();
            $jumlah = DB::table('beli_detail')->where('id_detail_beli',$id_beli)->where('status',NULL)->sum('subtotal');
            $update_total_detail_beli = DB::table('beli')->where('id_beli', $id_beli)->update([
                'total' => $jumlah
            ]);
            $this->hapus_jurnal_item($id_beli);
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function datatable_akun()
    {
        $data = DB::table('akun')->get();

        return datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $no_akun = "'".$data->no_akun."'";
            $akun = "'".$data->akun."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_akun('.$no_akun.','.$akun.')">Pilih</button>';
         })
         ->rawColumns(['opsi'])
         ->make(true);
    }

    public function add_brg(Request $req)
    {
        $id_user = session::get('id_user');
        $nama_brg = $req->_namaBrg;
        $hpp = $req->_hpp;
        $harga = $req->_harga;
        $jenis_brg = $req->_jenisBrg;
        $id_satuan = $req->_idSatuan;
        $no_akun = $req->_noAkun;

        $data_barang = [
            'nama_brg' => $nama_brg,
            'hpp' => $hpp,
            'harga' => $harga,
            'jenis_brg' => $jenis_brg,
            'satuan' => $id_satuan,
            'no_akun' => $no_akun
        ];

        $return_id = DB::table('barang')->insertGetId($data_barang);

        $help_a = substr($no_akun, 0, 1);
        $help_b = substr($no_akun, 3, 1);

        $data_setAkun = [
            'kode' => $no_akun,
            'id_set' => $return_id,
            'help_a' => $help_a,
            'help_b' => $help_b,
            'help_c' => 'bl'
        ];

        if(isset($return_id)){
            // $insert_set_akun = DB::table('set_akun')->insert($data_setAkun);
            $res = [
                'code' => 200,
                'msg' => 'Barang berhasil ditambahkan',
                'kode' => $return_id,
                'nama_brg' => $nama_brg,
                'hpp' => $hpp,
                'harga' => $harga,
                'jenis_brg' => $jenis_brg,
                'id_satuan' => $id_satuan,
                'no_akun' => $no_akun,
                'created_at' => date("Y-m-d H:i:s"),
                'user_add' => $id_user

            ];
        }else{
            $res = [
                'code' => 400,
                'msg' => 'Kepada Gagal ditambahkan !',
                'kode' => NULL,
                'nama_brg' => NULL,
                'hpp' => NULL,
                'harga' => NULL,      
                'jenis_brg' => NULL,
                'id_satuan' => NULL,
                'no_akun' => NULL,
                'created_at' => NULL,
                'user_add' => NULL
            ];
        }
        return response()->json($res);
    }

    public function form_edit($id_beli)
    {
        $id_beli = base64_decode($id_beli);
        $beli = DB::table('beli as a')
                        ->leftJoin('akun as b', 'a.no_akun','=', 'b.no_akun')
                        ->where('a.id_beli', $id_beli)
                        ->first();
        // dd($beli);

        $satuan = DB::table('satuan')->get();
        $jenis_brg = DB::table('jenis_barang')->get();
        $akun = DB::table('akun')->get();

        $suplier = DB::table('suplier')->get();

        $data['id_beli'] = $beli->id_beli;
        $data['tgl'] = date('d-m-Y', strtotime($beli->tgl));
        $data['no_nota_spl'] = $beli->no_nota_spl;
        $data['id_suplier'] = $beli->suplier;
        $data['no_akun'] = $beli->no_akun;
        $data['nama_akun'] = $beli->akun;
        $data['satuan'] = $satuan;
        $data['jenis_barang'] = $jenis_brg;
        $data['akun'] = $akun;
        $data['suplier'] = $suplier;
        $data['form'] = 'edit';
        // dd($data);
        return view('admin.pembelian.form')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id_beli = base64_decode($req->_idBeli);

        $res = [];

        $delete = DB::table('beli')->where('id_beli', $id_beli)->delete();

        if($delete) {
            $delete_beli_detail = DB::table('beli_detail')->where('id_detail_beli', $id_beli)->delete();
            $delete_jurnal = DB::table('jurnal')
                                ->where('bm', $id_beli)
                                ->where('jenis_jurnal','bl')
                                ->delete();
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }

        $data['response'] = $res;
        return response()->json($data);
    }
}
