<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\NeracaExport;

class NeracaController extends Controller
{
    public function __construct ()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.neraca.index');
    }

    public function total($data)
    {
        $total = 0;
        foreach ($data as $value) {
            $total += $value->total;            
        }
        return $total;
    }

    public function datatable_aktiva(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $tgl_dua = date('Y-m-d', strtotime($req->_tglDua));

        $aktiva = DB::table('akun')
                            ->where('parent_id', 1)
                            ->orWhereNull('parent_id')
                            ->where('kel', 1)
                            ->orderBy('no_akun')
                            ->get();
        
        $parent_jurnal = DB::table('parent_jurnal')
                            ->where('status', 'tutup')
                            ->orderBy('created_at', 'DESC')
                            ->first();
    
        // dd($parent_jurnal);
        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? strtotime($parent_jurnal->tgl_akhir) : NULL;
        $tgl_awal_int = strtotime($tgl);
        $tgl_dua_int = strtotime($tgl_dua);

        if ( $tgl_awal_int <= $tgl_akhir ) {
            $tgl = NULL;
        }

        if ( $tgl_dua_int <= $tgl_akhir ) {
           $tgl_dua = NULL;
        }

        // dd([$tgl, $tgl_dua]);

        $dt = [];
        $total_d = 0;
        $total_k = 0;
        $total_all = 0;

        $fer = 0;
                                
        foreach ($aktiva as $v) {
            $sql_data_debit = DB::table('jurnal')
                                ->where('no_akun', $v->no_akun)
                                ->where('map', 'd');

            $sql_data_kredit = DB::table('jurnal')
                                ->where('no_akun', $v->no_akun)
                                ->where('map', 'k');
                                

            if (isset($tgl) && isset($tgl_dua)) {
                $data_debit = $sql_data_debit->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit = $sql_data_kredit->whereBetween('tgl', [$tgl, $tgl_dua])->get();
            } 

            if (isset($tgl) && !isset($tgl_dua)) {
                $data_debit = $sql_data_debit->where('tgl', $tgl)->get();
                $data_kredit = $sql_data_kredit->where('tgl', $tgl)->get();
            }

            if (isset($tgl_dua) && !isset($tgl)) {
                $data_debit = $sql_data_debit->where('tgl', $tgl_dua)->get();
                $data_kredit = $sql_data_kredit->where('tgl', $tgl_dua)->get();
            }

            if ( !isset($tgl) && !isset($tgl_dua) ) {
                $data_debit = $sql_data_debit->where('tgl', '')->get();
                $data_kredit = $sql_data_kredit->where('tgl', '')->get();
            }
             
            $debit =  $this->total($data_debit);
            $kredit = $this->total($data_kredit);

            $total = $debit - $kredit;

            $total_fix = ($total) < 0 ? '('.str_replace('-', '', number_format($total, 0, ',', '.')).')' : number_format($total, 0, ',', '.');
           
            $dt[] = [
                        'no_akun' => $v->no_akun,
                        'akun' => $v->akun,
                        'parent_id' => $v->parent_id,
                        'total_debit' => number_format($debit, 0, ',', '.'),
                        'total_kredit' => number_format($kredit, 0, ',', '.'),
                        'total' => $total_fix,
            ];

            $total_d += $debit;
            $total_k += $kredit;
        }

        $total_all = $total_d - ($total_k);
        $total_all_fix = ($total_all) < 0 ? '('.str_replace('-', '', number_format($total_all, 0, ',', '.')).')' : number_format($total_all, 0, ',', '.');
        $dataQ['tt_debit'] = number_format($total_d, 0, ',', '.');
        $dataQ['tt_kredit'] = number_format($total_k, 0, ',', '.');
        $dataQ['total'] = $total_all_fix;
        $dataQ['data'] = $dt;
        // dd($dataQ);
        return response()->json($dataQ);
    }

    public function datatable_pasiva(Request $req)
    {
        $tgl = date('Y-m-d', strtotime($req->_tgl));
        $tgl_dua = date('Y-m-d', strtotime($req->_tglDua));

        $pasiva = DB::table('akun')
                            ->whereIn('parent_id', [2, 3])
                            ->orWhereNull('parent_id')
                            ->whereIn('kel', [2,3])
                            ->orderBy('no_akun')
                            ->get();

        $parent_jurnal = DB::table('parent_jurnal')
                            ->where('status', 'tutup')
                            ->orderBy('created_at', 'DESC')
                            ->first();
                            
    
        // dd($parent_jurnal);
        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? strtotime($parent_jurnal->tgl_akhir) : NULL;
        $tgl_awal_int = strtotime($tgl);
        $tgl_dua_int = strtotime($tgl_dua);

        if ( $tgl_awal_int <= $tgl_akhir ) {
            $tgl = NULL;
        }

        if ( $tgl_dua_int <= $tgl_akhir ) {
           $tgl_dua = NULL;
        }

        $dt = [];
        $total_d = 0;
        $total_k = 0;
        $total_all = 0;
        foreach ($pasiva as $v) {
            $sql_data_debit = DB::table('jurnal')
                                ->where('no_akun', $v->no_akun)
                                ->where('map', 'd');
                                
            $sql_data_kredit = DB::table('jurnal')
                                ->where('no_akun', $v->no_akun)
                                ->where('map', 'k');

            if (isset($tgl) && isset($tgl_dua)) {
                $data_debit = $sql_data_debit->whereBetween('tgl', [$tgl, $tgl_dua])->get();
                $data_kredit = $sql_data_kredit->whereBetween('tgl', [$tgl, $tgl_dua])->get();
            } 

            if (isset($tgl) && !isset($tgl_dua)) {
                $data_debit = $sql_data_debit->where('tgl', $tgl)->get();
                $data_kredit = $sql_data_kredit->where('tgl', $tgl)->get();
            }

            if (isset($tgl_dua) && !isset($tgl_dua)) {
                $data_debit = $sql_data_debit->where('tgl', $tgl_dua)->get();
                $data_kredit = $sql_data_kredit->where('tgl', $tgl_dua)->get();
            }

            if ( !isset($tgl) && !isset($tgl_dua) ) {
                $data_debit = $sql_data_debit->where('tgl', '')->get();
                $data_kredit = $sql_data_kredit->where('tgl', '')->get();
            }
             
            $debit =  $this->total($data_debit);
            $kredit = $this->total($data_kredit);

            $total = $debit - $kredit;

            $total_fix = ($total) < 0 ? '('.str_replace('-', '', number_format($total, 0, ',', '.')).')' : number_format($total, 0, ',', '.');
           
            $dt[] = [
                        'no_akun' => $v->no_akun,
                        'akun' => $v->akun,
                        'parent_id' => $v->parent_id,
                        'total_debit' => number_format($debit, 0, ',', '.'),
                        'total_kredit' => number_format($kredit, 0, ',', '.'),
                        'total' => $total_fix,
                        // 'opsi' => 'opsi'
            ];

            $total_d += $debit;
            $total_k += $kredit;
        }

        $total_all = $total_d - ($total_k);
        $total_all_fix = ($total_all) < 0 ? '('.str_replace('-', '', number_format($total_all, 0, ',', '.')).')' : number_format($total_all, 0, ',', '.');
        $dataQ['tt_debit'] = number_format($total_d, 0, ',', '.');
        $dataQ['tt_kredit'] = number_format($total_k, 0, ',', '.');
        $dataQ['total'] = $total_all_fix;
        $dataQ['data'] = $dt;
        // dd($dataQ);
        return response()->json($dataQ);
    }

    public function excel_neraca($tgl)
    {
        $pecah = explode('&', $tgl);
          $tgl_m = $pecah[0];
            $tgl_a = $pecah[1];  

            $tgl_m_format = date('Y-m-d', strtotime($tgl_m));
            $tgl_a_format = date('Y-m-d', strtotime($tgl_a));

            // AWAL AKTIVA
            $aktiva = DB::table('akun')
                            ->where('parent_id', 1)
                            ->orWhereNull('parent_id')
                            ->where('kel', 1)
                            ->orderBy('no_akun')
                            ->get();

            $dt_aktiva = [];
            $total_d_aktiva = 0;
            $total_k_aktiva = 0;
            $total_all_aktiva = 0;
            foreach ($aktiva as $v) {
                $data_debit_aktiva = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_aktiva = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_aktiva =  $this->total($data_debit_aktiva);
                $kredit_aktiva = $this->total($data_kredit_aktiva);
    
                $total_aktiva = $debit_aktiva - $kredit_aktiva;
    
                $total_fix_aktiva = ($total_aktiva) < 0 ? '('.str_replace('-', '', $total_aktiva).')' : $total_aktiva;
                
                $dt_aktiva[] = (object) [
                            'no_akun_aktiva' => $v->no_akun,
                            'akun_aktiva' => $v->akun,
                            'parent_id_aktiva' => $v->parent_id,
                            'debit_aktiva' => $debit_aktiva,
                            'kredit_aktiva' => $kredit_aktiva,
                            'total_aktiva' => $total_fix_aktiva,
                ];
    
                $total_d_aktiva += $debit_aktiva;
                $total_k_aktiva += $kredit_aktiva;
            }
            $total_all_aktiva = $total_d_aktiva - ($total_k_aktiva);
            $total_all_fix = ($total_all_aktiva) < 0 ? '('.str_replace('-', '', $total_all_aktiva).')' : $total_all_aktiva;
            // AKHIR AKTIVA

            // AWAL PASIVA
            $pasiva = DB::table('akun')
                            ->whereIn('parent_id', [2, 3])
                            ->orWhereNull('parent_id')
                            ->whereIn('kel', [2,3])
                            ->orderBy('no_akun')
                            ->get();

            $dt_pasiva = [];
            $total_d_pasiva = 0;
            $total_k_pasiva = 0;
            $total_all_pasiva = 0;
            foreach ($pasiva as $v) {
                $data_debit_pasiva = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'd')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
    
                $data_kredit_pasiva = DB::table('jurnal')
                                    ->where('no_akun', $v->no_akun)
                                    ->where('map', 'k')
                                    ->whereBetween('tgl', [$tgl_m_format, $tgl_a_format])
                                    ->get();
                    
                $debit_pasiva =  $this->total($data_debit_pasiva);
                $kredit_pasiva = $this->total($data_kredit_pasiva);
    
                $total_pasiva = $debit_pasiva - $kredit_pasiva;
    
                $total_fix_pasiva = ($total_pasiva) < 0 ? '('.str_replace('-', '', $total_pasiva).')' : $total_pasiva;
                
                $dt_pasiva[] = (object) [
                            'no_akun_pasiva' => $v->no_akun,
                            'akun_pasiva' => $v->akun,
                            'parent_id_pasiva' => $v->parent_id,
                            'debit_pasiva' => $debit_pasiva,
                            'kredit_pasiva' => $kredit_pasiva,
                            'total_pasiva' => $total_fix_pasiva,
                ];
    
                $total_d_pasiva += $debit_pasiva;
                $total_k_pasiva += $kredit_pasiva;
            }
            $total_all_pasiva = $total_d_pasiva - ($total_k_pasiva);
            $total_all_fix_pasiva = ($total_all_pasiva) < 0 ? '('.str_replace('-', '', $total_all_pasiva).')' : $total_all_pasiva;
            // AKHIR PASIVA

            $dt['rekap'] = $dt_aktiva;
            $dt['tt_debit_aktiva'] = $total_d_aktiva;
            $dt['tt_kredit_aktiva'] = $total_k_aktiva;
            $dt['tt_fix_aktiva'] = $total_all_fix;
            $dt['rekap_pasiva'] = $dt_pasiva;
            $dt['tt_debit_pasiva'] = $total_d_pasiva;
            $dt['tt_kredit_pasiva'] = $total_k_pasiva;
            $dt['tt_fix_pasiva'] = $total_all_fix_pasiva;
            $dt['tgl_m'] = $tgl_m;
            $dt['tgl_a'] = $tgl_a;

        $nama_file = "Rekap Neraca ".$tgl_m."-".$tgl_a.".xlsx";
        return Excel::download(new NeracaExport($dt), $nama_file); 
    }

    public function cek_tgl(Request $req)
    {
        $tgl = strtotime($req->_tgl);

        $parent_jurnal = DB::table('parent_jurnal')
                                        ->where('status', 'tutup')
                                        ->orderBy('created_at', 'DESC')
                                        ->first();
                
        // dd($parent_jurnal);
        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? strtotime($parent_jurnal->tgl_akhir) : NULL;

        if (!is_null($tgl_akhir)) {
            if ($tgl > $tgl_akhir) {
                $res = [
                        'tgl' => $req->_tgl
                ];
            } else {
                $res = [
                    'tgl' => null
                ];
            }
        } else {
            $res = [
                'tgl' => $req->_tgl
            ];
        }
        return response()->json($res);
    }

}