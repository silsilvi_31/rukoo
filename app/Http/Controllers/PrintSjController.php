<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;
use File;
use Excel;
use App\Exports\SjExport;

class PrintSjController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        return view('admin.PrintSj.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return $data->nama;
    }

    public function datatable()
    {
        $data = DB::table('suratjalan as sj')
                    ->where('sj.status',NULL)
                    ->where('sj.print_sj', '=', NULL)
                    ->orWhere('sj.print_nota', '=', NULL)
                    ->leftJoin('pelanggan as pl', 'sj.id_pelanggan', '=', 'pl.id')
                    ->leftJoin('kendaraan as k', 'sj.id_kendaraan', '=', 'k.id')
                    ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                    ->select('sj.id','sj.tgl','pl.nama','sj.catatan','sj.pembayaran','sj.pengiriman','sj.id_kendaraan','sj.id_karyawan','sj.total','sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj',
                                'k.nama as kendaraan', 
                                    'kar.nama as sopir')
                    ->orderBy('sj.tgl', 'DESC')
                    ->orderBy('sj.id', 'DESC')
                    ->get(); 
        
        return datatables::of($data)
        ->addIndexColumn()
        ->editColumn('is_cek_nota', function ($data) {
            $is_cek_nota = $data->is_cek_nota;

            $nama = isset($is_cek_nota) ? $this->get_karyawan($data->cek_nota) : '-';
            
            if ($is_cek_nota == 1) {
                return '<center><i class="fa fa-check"></i>'.$nama.'</center>';
            } else if ($is_cek_nota == 2) {
                return '<center><i class="fa fa-close"></i>'.$nama.'</center>';
            } else if ($is_cek_nota == '') {
                return '<center><i class="fa fa-minus"></i></center>';
            } else {

            }
        })
        ->editColumn('tgl', function ($data) {
            $tanggal = date('d-m-Y', strtotime($data->tgl));
            return $tanggal;
        }) 
        ->addColumn('opsi', function ($data){
            $edit = route('PrintSj.form_edit', [base64_encode($data->id)]);
            $nota = route('PrintSj.nota', [base64_encode($data->id)]);
            $sj = route('PrintSj.sj', [base64_encode($data->id)]);
            $id_sj = "'".base64_encode($data->id)."'";
            $status = ($data->is_cek_nota == 1) ? 'btn-success' : 'btn-secondary disabled';
            $status_edit = ($data->is_cek_nota != 1 ) ? 'btn-primary' : 'btn-secondary disabled';
            $status_hapus = ($data->is_cek_nota != 1) ? 'btn-danger' : 'btn-secondary disabled';
            return '<a href="'.$edit.'" class="btn btn-sm '.$status_edit.'"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-sm '.$status_hapus.'" onclick="delete_suratjalan('.$id_sj.')"><i class="fa fa-trash"></i></button>
                    <a href="#" id="'.$data->id.'" class="print_nota btn btn-sm '.$status.'"><i class="fa fa-print"></i> Nota</a>
                    <a href="#" id="'.$data->id.'" class="print_sj btn btn-sm '.$status.'"><i class="fa fa-print"></i> SJ</a>';
        })
        ->rawColumns(['tgl','is_cek_nota', 'opsi'])
        ->make(true);
    }

    public function print_sj_count(Request $req)
    {
        $id_sj = $req->_idSj;
        $jenis = $req->_jenis;

        $print = [];

        if ( $jenis == 'print_sj') {
           $print = [
               'print_sj' => 1
           ];
        } else {
            $print = [
                'print_nota' => 1
            ];
        }

        $update_print = DB::table('suratjalan')->where('id', $id_sj)->update($print);
        
        $res = [];

        if ($update_print) {
            $res = [
                'kode' => 200,
                'msg' => 'Berhasil Update Data'
            ];
        } else {
            $res = [
                'kode' => 400,
                'msg' => 'Gagal Update Data'
            ];            
        }

        return response()->json($res);
    }

    public function form()
    {
        $kendaraan = DB::table('kendaraan')
                            // ->where('status', NULL)
                            ->where('penyusutan', '<>', NULL)
                            ->get();
        $sopir = DB::table('karyawan')
                                ->where('id_jabatan',4)
                                ->get();
        $pelanggan = DB::table('pelanggan')->where('status', NULL)->get();
                                               
        $data['kendaraan'] = $kendaraan;
        $data['sopir'] = $sopir;
        $data['pelanggan'] = $pelanggan;
        
        return view('admin.PrintSj.form')->with($data);
    }

    public function total(Request $req)
    {
        $id = $req->_id;
        $sj = DB::table('suratjalan')->where('id', $id)->first();
        $ongkir = isset($sj) ? $sj->ongkir : NULL;
        $total = isset($sj) ? $sj->total : NULL;
        $grandtotal = $ongkir + $total;
        $data['ongkir'] = number_format($ongkir,0,',','.');
        $data['total'] = number_format($total, 0, ',', '.');
        $data['grandtotal'] = number_format($grandtotal, 0, ',', '.');

        return response()->json($data);
    }

    public function no_urut()
    {
        $id_suratjalan = DB::table('suratjalan')->where('status',NULL)->max('id');
        $no = $id_suratjalan;
        $no++;
        return response()->json($no);
    }

    public function select_pelanggan()
    {
        $data = DB::table('pelanggan')->where('status', NULL)->get();
        
        return response()->json($data);
    }

    public function datatable_brg()
    {
        $data = DB::table('barang as a')
                ->where('a.status',NULL)
                ->where('jenis_brg', 5)
                ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                ->select('a.kode', 'a.nama_brg', 'a.harga', 'a.satuan as id_satuan','b.nama as satuan', 'a.jenis_brg')
                ->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->addColumn('opsi', function ($data){
            $kode_barang = $data->kode;
            $nama_brg = "'".$data->nama_brg."'";
            $harga = $data->harga;
            $id_satuan = $data->id_satuan;
            $satuan = "'".$data->satuan."'";
            return '<button id="btn_pilih" type="button" class="btn btn-sm btn-primary" onclick="select_brg('.$kode_barang.','.$nama_brg.','.$harga.','.$id_satuan.','.$satuan.')">Pilih</button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);

    }

    //Start AKUNTANSI
    public function akun_barang($jenis_akun, $id_brg, $help_b, $keterangan)
    {
        $lower = isset($keterangan) ? strtolower($keterangan) : null;
        $akun_ketemu = '';
        $cari_akun_item = DB::table('set_akun')
                                    ->where('id_set', $id_brg)
                                    ->where('help_a', $jenis_akun)
                                    ->where('help_b', $help_b)
                                    ->where('help_c', $lower)
                                    ->first();
        if (!isset($cari_akun_item) && ($jenis_akun == 4)) {
            $akun_ketemu = '4111';
            if (strtolower($keterangan) == 'a') {
                $cari_akun_item = DB::table('set_akun')
                                    ->where('id_set', $id_brg)
                                    ->where('help_a', $jenis_akun)
                                    ->where('help_b', $help_b)
                                    ->where('help_c', null)
                                    ->first(); 
            }
            $insert = DB::table('error_log')->insert(['nama' => 'akun tidak ditemukan untuk'.$id_brg.'-'.$jenis_akun.'-'.$help_b.'-'.strtolower($keterangan)]);
        } else {
            if (strtolower($keterangan) == 'a') {
                $cari_akun_item = DB::table('set_akun')
                                    ->where('id_set', $id_brg)
                                    ->where('help_a', $jenis_akun)
                                    ->where('help_b', $help_b)
                                    ->where('help_c', null)
                                    ->first(); 
            }
            $akun_ketemu = isset($cari_akun_item) ? $cari_akun_item->kode : '';
        }
        return $akun_ketemu;
    }

    public function akun_kendaraan($id_kendaraan, $keterangan)
    {
        $id_ken = ($id_kendaraan == 2) ? 11 : $id_kendaraan;
        $cari_akun_kendaraan = DB::table('set_akun')
                                    ->where('id_set', $id_ken)
                                    ->where('help_c', $keterangan)
                                    ->first();
        return $cari_akun_kendaraan->kode;       

    }

    public function nama_pelanggan($id_pelanggan)
    {
        $cari_nama_pelanggan = DB::table('pelanggan')
                                    ->where('id', $id_pelanggan)
                                    ->where('status', NULL)
                                    ->first();
                                
        return $cari_nama_pelanggan->nama;
    }

    public function nama_akun($no_akun)
    {
        $cari_nama_akun = DB::table('akun')
                                    ->where('no_akun', $no_akun)
                                    ->where('status', NULL)
                                    ->first();

        return isset($cari_nama_akun) ? $cari_nama_akun->akun : '';
    }

    public function penyusutan_kendaraan($id_kendaraan)
    {
        $cari_penyusutan_kendaraan = DB::table('kendaraan')
                                    ->where('id', $id_kendaraan)
                                    // ->where('status', NULL)
                                    ->first();
        if(!isset($cari_penyusutan_kendaraan)){
            $insert = DB::table('error_log')->insert([
                'nama' => 'tidak ditemukan akun kendaraan'
            ]);
        }
        return $cari_penyusutan_kendaraan->penyusutan;
    }

    public function harga_pokok($id_brg)
    {
        $cari_hpp = DB::table('barang')
                                    ->where('kode', $id_brg)
                                    ->where('status', NULL)
                                    ->first();

        return $cari_hpp->hpp;
    }

    public function get_hpp($id, $hpp)
    {
        $get_harga_pokok = DB::table('jurnal')->where('ref',$id)->where('hpp', $hpp)->where('status', '=', NULL)->sum('total');

        return $get_harga_pokok;
    }

    public function total_qty_nonik($id, $hit)
    {
        $qty_nonik = DB::table('jurnal')
                                ->where('ref', $id)
                                ->where('hit', $hit)
                                ->where('status', NULL)
                                ->sum('qty');

        return $qty_nonik;
    }

    public function akun_pelanggan($jenis_akun, $id_pelanggan, $help_b, $keterangan)
    {

        if ( $id_pelanggan == 10 || $id_pelanggan == 13 || $id_pelanggan == 25 || $id_pelanggan == 29 || $id_pelanggan == 40 || $id_pelanggan == 60 || $id_pelanggan == 61) {
            $cari_akun_pelanggan = DB::table('set_akun')
                                        ->where('id_set', $id_pelanggan)
                                        ->where('help_a', $jenis_akun)
                                        ->where('help_b', $help_b)
                                        ->where('help_c', $keterangan)
                                        ->first();
            $no_akun_pelanggan = $cari_akun_pelanggan->kode;
        } else {
            $no_akun_pelanggan = '1300,19';
        }

        return $no_akun_pelanggan;

    }

    public function jenis_ketr($ketr)
    {
        if (isset($ketr)) {
            if ($ketr == 'AA' || $ketr == 'A' || $ketr == 'a' || $ketr == "aa") {
                $ketr = 'A';
            } else {
                $ketr = null;
            }
        } else {
            $ketr = null;
        }

        return $ketr;
    }
    //tes

    public function is_lps($id_brg) 
    {
        $search = 'lps';
        if (is_numeric($id_brg)) {
            $barang = DB::table('barang')->where('kode', $id_brg)->first();
            $namaBrg = $barang->nama_brg;
        } else {
            $namaBrg = $id_brg;
        }
        
        $compare = strstr($namaBrg, $search);
        $hasil = '';
        if ($compare) {
            $hasil = 'true';
        } else {
            $hasil = 'false';
        }
        
        return $hasil;
    }

    public function set_akun($aksi, $tanggal, $id, $kepada, $opsi, $keterangan, $id_brg, $id_kendaraan, $qty, $harga_akun, $potongan, $jumlah, $ongkir, $catatan, $ketr_tambahan, $id_detail_sj, $pengiriman)
    {
        if ($opsi == NULL) {
            if ($aksi == 'insert') {
                // HPP Triplek
                $akun[0]['tgl'] = $tanggal;
                $akun[0]['id_item'] = null;
                $akun[0]['ref'] = $id;
                $akun[0]['hpp'] = NULL;
                $akun[0]['grup'] = 1;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $this->nama_pelanggan($kepada); 
                $akun[0]['no_akun'] = '6111,00';
                $akun[0]['keterangan'] = $catatan;
                $akun[0]['map'] = 'd';
                $akun[0]['hit'] = '';
                $akun[0]['qty'] = NULL;
                $akun[0]['harga'] = 0;
                $akun[0]['total'] = 0;
                
                // Stok gudang
                $akun[1]['tgl'] = $tanggal;
                $akun[1]['id_item'] = $id_detail_sj;
                $akun[1]['ref'] = $id;
                $akun[1]['hpp'] = 1;
                $akun[1]['grup'] = 1;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $this->nama_pelanggan($kepada);
                $akun[1]['no_akun'] = $this->akun_barang(1, $id_brg, 1, $this->jenis_ketr($keterangan));
                $akun[1]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 1, $this->jenis_ketr($keterangan))));
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $qty;
                $akun[1]['harga'] = $this->harga_pokok($id_brg);
                $akun[1]['total'] = $qty * $this->harga_pokok($id_brg);;
                
                // Akm. penyusutan kendaraan
                // $akun[2]['tgl'] = $tanggal;
                // $akun[2]['id_item'] = null;
                // $akun[2]['ref'] = $id;
                // $akun[2]['hpp'] = 1;
                // $akun[2]['grup'] = 2;
                // $akun[2]['jenis_jurnal'] = 'sj';
                // $akun[2]['nama'] = $this->nama_pelanggan($kepada);
                // $akun[2]['no_akun'] = ($pengiriman == 'Ambil Sendiri') ? '118' : $this->akun_kendaraan($id_kendaraan, 'akm');
                // $akun[2]['keterangan'] = '';
                // $akun[2]['map'] = 'k';
                // $akun[2]['hit'] = '';
                // $akun[2]['qty'] = NULL;
                // $akun[2]['harga'] = ($pengiriman == 'Ambil Sendiri') ? 0 : $this->penyusutan_kendaraan($id_kendaraan);
                // $akun[2]['total'] = ($pengiriman == 'Ambil Sendiri') ? 0 : $this->penyusutan_kendaraan($id_kendaraan);
    
                // hutang gaji
                $akun[3]['tgl'] = $tanggal;
                $akun[3]['id_item'] = null;
                $akun[3]['ref'] = $id;
                $akun[3]['hpp'] = 1;
                $akun[3]['grup'] = 2;
                $akun[3]['jenis_jurnal'] = 'sj';
                $akun[3]['nama'] = $this->nama_pelanggan($kepada);
                $akun[3]['keterangan'] = '';
                $akun[3]['no_akun'] = ($pengiriman == 'Ambil Sendiri') ? '119' : '2231,00';
                $akun[3]['map'] = 'k';
                $akun[3]['hit'] = 'b';
                $akun[3]['qty'] = 2;
                $akun[3]['harga'] = ($pengiriman == 'Ambil Sendiri') ? 0 : 150000;
                $akun[3]['total'] = ($pengiriman == 'Ambil Sendiri') ? 0 : 2 * 150000;
    
                // piutang tunai 
                $akun[4]['tgl'] = $tanggal;
                $akun[4]['id_item'] = null;
                $akun[4]['ref'] = $id;
                $akun[4]['hpp'] = NULL;
                $akun[4]['grup'] = 3;
                $akun[4]['jenis_jurnal'] = 'sj';
                $akun[4]['nama'] = $this->nama_pelanggan($kepada);
                $akun[4]['keterangan'] = '';
                $akun[4]['no_akun'] = $this->akun_pelanggan(1, $kepada, 0, 'pl');
                $akun[4]['map'] = 'd';
                $akun[4]['hit'] = '';
                $akun[4]['qty'] = NULL;
                $akun[4]['harga'] = $jumlah;
                $akun[4]['total'] = $jumlah;
    
                $keterangan_tambahan = isset($ketr_tambahan) ? ' // '.$ketr_tambahan : NULL;
                // Pendapatan Penjualan triplek
                $akun[5]['tgl'] = $tanggal;
                $akun[5]['id_item'] = $id_detail_sj;
                $akun[5]['ref'] = $id;
                $akun[5]['hpp'] = 2;
                $akun[5]['grup'] = 3;
                $akun[5]['jenis_jurnal'] = 'sj';
                $akun[5]['nama'] = $this->nama_pelanggan($kepada);
                $akun[5]['no_akun'] = ($this->is_lps($id_brg) == 'true') ? '4111,00' : 
                                        $this->akun_barang(4, $id_brg, 1, $this->jenis_ketr($keterangan));
                $akun[5]['keterangan'] = ($this->is_lps($id_brg) == 'true') ? 'Pendapatan' : 
                                            $this->nama_akun($this->akun_barang(4, $id_brg, 1, $this->jenis_ketr($keterangan))).$keterangan_tambahan;
                $akun[5]['map'] = 'k';
                $akun[5]['hit'] = 'b';
                $akun[5]['qty'] = $qty;
                $akun[5]['harga'] = $harga_akun - $potongan;
                $akun[5]['total'] = ($qty * $harga_akun) - ($qty * $potongan);
                
                $simpan_jurnal = DB::table('jurnal')->insert($akun);
                $update_hpp = DB::table('jurnal')
                                        ->where('no_akun', '6111,00')
                                        ->where('ref', $id)
                                        ->where('status', '=', NULL)
                                        ->update([
                                            'harga' => $this->get_hpp($id, 1),
                                            'total' => $this->get_hpp($id, 1)
                                        ]);

            } elseif ($aksi == 'add_item') {
                // Stok gudang
                $akun[0]['tgl'] = $tanggal;
                $akun[0]['id_item'] = $id_detail_sj;
                $akun[0]['ref'] = $id;
                $akun[0]['hpp'] = 1;
                $akun[0]['grup'] = 1;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $this->nama_pelanggan($kepada);
                $akun[0]['no_akun'] = $this->akun_barang(1, $id_brg, 1, $this->jenis_ketr($keterangan));
                $akun[0]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 1, $this->jenis_ketr($keterangan))));
                $akun[0]['map'] = 'k';
                $akun[0]['hit'] = 'b';
                $akun[0]['qty'] = $qty;
                $akun[0]['harga'] = $this->harga_pokok($id_brg);
                $akun[0]['total'] = $qty * $this->harga_pokok($id_brg);
    
                $keterangan_tambahan = isset($ketr_tambahan) ? ' // '.$ketr_tambahan : NULL;
                // Pendapatan Penjualan triplek
                $akun[1]['tgl'] = $tanggal;
                $akun[1]['id_item'] = $id_detail_sj;
                $akun[1]['ref'] = $id;
                $akun[1]['hpp'] = 2;
                $akun[1]['grup'] = 3;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $this->nama_pelanggan($kepada);
                $akun[1]['no_akun'] = ($this->is_lps($id_brg) == 'true') ? '4111,00' : 
                                        $this->akun_barang(4, $id_brg, 1, $this->jenis_ketr($keterangan));
                $akun[1]['keterangan'] = ($this->is_lps($id_brg) == 'true') ? 'Pendapatan' : 
                                            $this->nama_akun($this->akun_barang(4, $id_brg, 1, $this->jenis_ketr($keterangan))).$keterangan_tambahan;
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $qty;
                $akun[1]['harga'] = $harga_akun - $potongan;
                $akun[1]['total'] = ($qty * $harga_akun) - ($qty * $potongan);
    
                // piutang tunai      
                $pelangganQ = $this->akun_pelanggan(1, $kepada, 0, 'pl');            
                $simpan_item_jurnal = DB::table('jurnal')->insert($akun);
                $update_akun = DB::table('jurnal')
                                            ->where('ref', $id)
                                            ->where('no_akun', $pelangganQ)
                                            ->where('status', NULL)
                                            ->update([
                                                'harga' => $this->get_hpp($id, 2),
                                                'total' => $this->get_hpp($id, 2)
                                            ]);   

                $update_hpp = DB::table('jurnal')
                                            ->where('no_akun', '6111,00')
                                            ->where('ref', $id)
                                            ->where('status', '=', NULL)
                                            ->update([
                                                'harga' => $this->get_hpp($id, 1),
                                                'total' => $this->get_hpp($id, 1)
                                            ]);
                
            } 
        } elseif ($opsi == 1) {
            if ($aksi == 'insert' || $aksi == 'add_item') {
                // Stok Ina
                $akun[0]['tgl'] = $tanggal;
                $akun[0]['id_item'] = $id_detail_sj;
                $akun[0]['ref'] = $id;
                $akun[0]['grup'] = 4;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $this->nama_pelanggan($kepada);
                $akun[0]['no_akun'] = $this->akun_barang(1, $id_brg, 2, $keterangan=null);
                $akun[0]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 2, $keterangan=null)));
                $akun[0]['map'] = 'd';
                $akun[0]['hit'] = 'b';
                $akun[0]['qty'] = $qty;
                $akun[0]['harga'] = $this->harga_pokok($id_brg);

                // Stok gudang
                $akun[1]['tgl'] = $tanggal;
                $akun[1]['id_item'] = $id_detail_sj;
                $akun[1]['ref'] = $id;
                $akun[1]['grup'] = 5;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $this->nama_pelanggan($kepada);
                $akun[1]['no_akun'] = $this->akun_barang(1, $id_brg, 1, $keterangan);
                $akun[1]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 1, $keterangan)));
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $qty;
                $akun[1]['harga'] = $this->harga_pokok($id_brg);

                $simpan_jurnal = DB::table('jurnal')->insert($akun);
            }
        } elseif ($opsi == 2) {
            if ($aksi == 'insert') {
                // Hutang Multijaya Forestindo atau Nonik
                $akun[0]['tgl'] = $tanggal;
                $akun[0]['ref'] = $id;
                $akun[0]['grup'] = 6;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $this->nama_pelanggan($kepada); 
                $akun[0]['no_akun'] = '2220,00';
                $akun[0]['keterangan'] = $catatan;
                $akun[0]['map'] = 'd';
                $akun[0]['hit'] = '';
                $akun[0]['qty'] = $qty;
                $akun[0]['harga'] = NULL;

                // Stok Nonik
                $akun[1]['tgl'] = $tanggal;
                $akun[1]['ref'] = $id;
                $akun[1]['grup'] = 7;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $this->nama_pelanggan($kepada);
                $akun[1]['no_akun'] = $this->akun_barang(1, $id_brg, 4, $keterangan);
                $akun[1]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 4, $keterangan)));
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $qty;
                $akun[1]['harga'] = NULL;

                $simpan_jurnal = DB::table('jurnal')->insert($akun);
            } elseif ($aksi == 'add_item') {
                // Stok Nonik
                $akun[0]['tgl'] = $tanggal;
                $akun[0]['ref'] = $id;
                $akun[0]['grup'] = 7;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $this->nama_pelanggan($kepada);
                $akun[0]['no_akun'] = $this->akun_barang(1, $id_brg, 4, $keterangan);
                $akun[0]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 4, $keterangan)));
                $akun[0]['map'] = 'k';
                $akun[0]['hit'] = 'b';
                $akun[0]['qty'] = $qty;
                $akun[0]['harga'] = NULL;

                $simpan_jurnal = DB::table('jurnal')->insert($akun);
                $update_qty = DB::table('jurnal')
                                            ->where('ref', $id)
                                            ->where('no_akun', '2220,00')
                                            ->where('status', NULL)
                                            ->update([
                                                'qty' => $this->total_qty_nonik($id, 'b')
                                            ]);
            }
        } elseif ($opsi == 3) {
            if ($aksi == 'insert') {
                // Hutang Multijaya Forestindo atau Nonik
                $akun[0]['tgl'] = $tanggal;
                $akun[0]['ref'] = $id;
                $akun[0]['grup'] = 8;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $this->nama_pelanggan($kepada); 
                $akun[0]['no_akun'] = '2220,00';
                $akun[0]['keterangan'] = $catatan;
                $akun[0]['map'] = 'd';
                $akun[0]['hit'] = '';
                $akun[0]['qty'] = $qty;
                $akun[0]['harga'] = NULL;

                // Stok Nonik
                $akun[1]['tgl'] = $tanggal;
                $akun[1]['ref'] = $id;
                $akun[1]['grup'] = 8;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $this->nama_pelanggan($kepada);
                $akun[1]['no_akun'] = $this->akun_barang(1, $id_brg, 4, $keterangan);
                $akun[1]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 4, $keterangan)));
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $qty;
                $akun[1]['harga'] = NULL;

                // Multijaya Forestindo
                $akun[2]['tgl'] = $tanggal;
                $akun[2]['ref'] = $id;
                $akun[2]['grup'] = 9;
                $akun[2]['jenis_jurnal'] = 'sj';
                $akun[2]['nama'] = $this->nama_pelanggan($kepada); 
                $akun[2]['no_akun'] = '1300,07';
                $akun[2]['keterangan'] = '';
                $akun[2]['map'] = 'd';
                $akun[2]['hit'] = '';
                $akun[2]['qty'] = NULL;
                $akun[2]['harga'] = $ongkir;

                // Biaya Perjalanan
                $akun[3]['tgl'] = $tanggal;
                $akun[3]['ref'] = $id;
                $akun[3]['grup'] = 9;
                $akun[3]['jenis_jurnal'] = 'sj';
                $akun[3]['nama'] = $this->nama_pelanggan($kepada);
                $akun[3]['no_akun'] = '5200,00';
                $akun[3]['keterangan'] = 'Ongkos Kirim';
                $akun[3]['map'] = 'k';
                $akun[3]['hit'] = '';
                $akun[3]['qty'] = NULL;
                $akun[3]['harga'] = $ongkir;

                $simpan_jurnal = DB::table('jurnal')->insert($akun);
            } elseif ($aksi == 'add_item') {
                // Stok Nonik
                $akun[1]['tgl'] = $tanggal;
                $akun[1]['ref'] = $id;
                $akun[1]['grup'] = 8;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $this->nama_pelanggan($kepada);
                $akun[1]['no_akun'] = $this->akun_barang(1, $id_brg, 4, $keterangan);
                $akun[1]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 4, $keterangan)));
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $qty;
                $akun[1]['harga'] = NULL;

                $simpan_jurnal = DB::table('jurnal')->insert($akun);
                $update_qty = DB::table('jurnal')
                                            ->where('ref', $id)
                                            ->where('no_akun', '2220,00')
                                            ->where('status', NULL)
                                            ->update([
                                                'qty' => $this->total_qty_nonik($id, 'b')
                                            ]);
            }
        } elseif ($opsi == 5) {
            if ($aksi == 'insert' || $aksi == 'add_item') {
                // Stok Dyson
                $akun[0]['tgl'] = $tanggal;
                $akun[0]['id_item'] = $id_detail_sj;
                $akun[0]['ref'] = $id;
                $akun[0]['grup'] = 4;
                $akun[0]['jenis_jurnal'] = 'sj';
                $akun[0]['nama'] = $this->nama_pelanggan($kepada);
                $akun[0]['no_akun'] = $this->akun_barang(1, $id_brg, 7, $keterangan=null);
                $akun[0]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 7, $keterangan=null)));
                $akun[0]['map'] = 'd';
                $akun[0]['hit'] = 'b';
                $akun[0]['qty'] = $qty;
                $akun[0]['harga'] = $this->harga_pokok($id_brg);

                // Stok gudang
                $akun[1]['tgl'] = $tanggal;
                $akun[1]['id_item'] = $id_detail_sj;
                $akun[1]['ref'] = $id;
                $akun[1]['grup'] = 5;
                $akun[1]['jenis_jurnal'] = 'sj';
                $akun[1]['nama'] = $this->nama_pelanggan($kepada);
                $akun[1]['no_akun'] = $this->akun_barang(1, $id_brg, 1, $keterangan);
                $akun[1]['keterangan'] = $this->nama_akun(($this->akun_barang(1, $id_brg, 1, $keterangan)));
                $akun[1]['map'] = 'k';
                $akun[1]['hit'] = 'b';
                $akun[1]['qty'] = $qty;
                $akun[1]['harga'] = $this->harga_pokok($id_brg);

                $simpan_jurnal = DB::table('jurnal')->insert($akun);
            }
        } else {
            // dd('aaaaa');
        }
    }

    public function update_jurnal_item($id_detail_sj, $kode_barang, $ketr_barang, $ketr_brg, $id_brg, $qty_brg, $harga_brg, $potongan, $ketr_tambahan)
    {
        // $item_kas = $this->akun_barang(1, $kode_barang, 1, $this->jenis_ketr($ketr_barang));
        // $item_pendapatan = ($this->is_lps($id_brg) == 'true') ? '4111,00' : $this->akun_barang(4, $kode_barang, 1, $ketr_barang=null); 

        $detail_sj = DB::table('suratjalan_detail')->where('id', $id_detail_sj)->first();
        $sj = DB::table('suratjalan')->where('id', $detail_sj->id_sj)->first();

        $keterangan_tambahan = isset($ketr_tambahan) ? ' // '.$ketr_tambahan : NULL;
        $update_kas = DB::table('jurnal')
                            ->where('id_item', $id_detail_sj)
                            ->where('grup', 1)
                            ->where('status', '=', NULL)
                            ->update([
                                'no_akun' => $this->akun_barang(1, $id_brg, 1, $this->jenis_ketr($ketr_brg)),
                                'keterangan' => $this->nama_akun(($this->akun_barang(1, $id_brg, 1,  $this->jenis_ketr($ketr_brg)))),
                                'qty' => $qty_brg,
                                'harga' => $this->harga_pokok($id_brg),
                                'total' => $qty_brg * $this->harga_pokok($id_brg),
                            ]); 

        //                     dd($update_kas);

        $update_hpp = DB::table('jurnal')
                            ->where('ref', $detail_sj->id_sj)
                            ->where('no_akun', '6111,00')
                            ->where('status', '=', NULL)
                            ->update([
                                'harga' => $this->get_hpp($detail_sj->id_sj, 1),
                                'total' => $this->get_hpp($detail_sj->id_sj, 1)
                            ]);  
                            
        $update_pendapatan = DB::table('jurnal')
                            ->where('id_item', $id_detail_sj)
                            ->where('grup', 3)
                            ->update([
                                'no_akun' => ($this->is_lps($id_brg) == 'true') ? '4111,00' : $this->akun_barang(4, $id_brg, 1, $this->jenis_ketr($ketr_brg)),
                                'keterangan' => ($this->is_lps($id_brg) == 'true') ? 'Pendapatan' : $this->nama_akun(($this->akun_barang(4, $id_brg, 1, $this->jenis_ketr($ketr_brg)))).$keterangan_tambahan,
                                'qty' => $qty_brg,
                                'harga' => $harga_brg - $potongan,
                                'total' => ($qty_brg * $harga_brg) - ($qty_brg * $potongan)
                            ]);

        $pelangganQ = $this->akun_pelanggan(1, $sj->id_pelanggan, 0, 'pl');
        $update_akun = DB::table('jurnal')
                            ->where('ref', $detail_sj->id_sj)
                            ->where('no_akun', $pelangganQ)
                            ->where('status', '=', NULL)
                            ->update([
                                'harga' => $this->get_hpp($detail_sj->id_sj, 2),
                                'total' => $this->get_hpp($detail_sj->id_sj, 2)
                            ]);                                 
       
        $res = [];

        if ($update_kas) {
             $res = [
                'code' => 200,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Data gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function update_kendaraan_jurnal($id_sj, $id_kendaraan, $id_kendaraan_jurnal, $pengiriman)
    {
        $no_akun_kendaraan = ($pengiriman == 'Ambil Sendiri') ? '118' : $this->akun_kendaraan($id_kendaraan_jurnal, 'akm');
        
        $update_kendaraan = DB::table('jurnal')
                                ->where('ref', $id_sj)
                                ->where('no_akun', $no_akun_kendaraan)
                                // ->where('status', '=', NULL)
                                ->update([
                                    'no_akun' => ($pengiriman == 'Ambil Sendiri') ? '118' : $this->akun_kendaraan($id_kendaraan, 'akm'),
                                    'harga' => ($pengiriman == 'Ambil Sendiri') ? 0 :$this->penyusutan_kendaraan($id_kendaraan),
                                    'total' => ($pengiriman == 'Ambil Sendiri') ? 0 :$this->penyusutan_kendaraan($id_kendaraan)
                                ]); 
        
        $update_total = DB::table('jurnal')
                            ->where('no_akun', '6111,00')
                            ->where('ref', $id_sj)
                            ->where('status', '=', NULL)
                            ->update([
                                'harga' => $this->get_hpp($id_sj, 1),
                                'total' => $this->get_hpp($id_sj, 1)
                            ]);
                                        
                                        
    }

    public function update_jurnal_ina($id_sj, $kode_barang, $ketr_barang, $ketr_brg, $id_brg, $qty_brg, $harga_brg)
    {
        $item_kas = $this->akun_barang(1, $kode_barang, 1, $ketr_barang);
        $item_ina = $this->akun_barang(1, $kode_barang, 2, $ketr_barang=null);

        $update_ina = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', $item_ina)
                                    ->where('status', NULL)
                                    ->update([
                                        'no_akun' => $this->akun_barang(1, $id_brg, 2, $ketr_barang=null),
                                        'keterangan' => $this->nama_akun(($this->akun_barang(1, $id_brg, 2, $ketr_barang=null))),
                                        'qty' => $qty_brg,
                                        'harga' => $this->harga_pokok($id_brg)
                                    ]); 
        
        $update_kas = DB::table('jurnal')     
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', $item_kas)
                                    ->where('status', NULL)
                                    ->update([
                                        'no_akun' => $this->akun_barang(1, $id_brg, 1, $ketr_brg),
                                        'keterangan' => $this->nama_akun(($this->akun_barang(1, $id_brg, 1, $ketr_brg))),
                                        'qty' => $qty_brg,
                                        'harga' => $this->harga_pokok($id_brg)
                                    ]);                                        

        $res = [];

        if ($update_kas) {
                $res = [
                'code' => 200,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Data gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function update_jurnal_nonik($id_sj, $kode_barang, $ketr_barang, $id_brg, $qty_brg, $ongkir, $catatan)
    {
        $item_nonik = $this->akun_barang(1, $kode_barang, 4, $ketr_barang=null);

        $update_nonik = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', $item_nonik)
                                    ->where('status', NULL)
                                    ->update([
                                        'no_akun' => $this->akun_barang(1, $id_brg, 4, $ketr_barang=null),
                                        'keterangan' => $this->nama_akun(($this->akun_barang(1, $id_brg, 4, $ketr_barang=null))),
                                        'qty' => $qty_brg
                                    ]);   

        $update_qty = DB::table('jurnal')
                                ->where('ref', $id_sj)
                                ->where('no_akun', '2220,00')
                                ->where('status', NULL)
                                ->update([
                                    'keterangan' => $catatan,
                                    'qty' => $this->total_qty_nonik($id_sj, 'b')
                                ]); 

        $res = [];

        if ($update_nonik) {
                $res = [
                'code' => 200,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Data gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);                                            
    }

    public function update_jurnal($id_sj, $tgl, $id_pelanggan, $id_pelanggan_jurnal, $ongkir, $catatan)
    {
        $update_jurnal = DB::table('jurnal')->where('ref', $id_sj)->update([
            'tgl' => $tgl,
            'nama' => $this->nama_pelanggan($id_pelanggan)
        ]);

        $update_catatan = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', '6111,00')
                                    ->where('status', NULL)
                                    ->update([
                                        'keterangan' => $catatan
                                    ]);  

        $update_ongkir = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', '5200,00')
                                    ->orwhere('no_akun', '1300,07')
                                    ->where('status', NULL)
                                    ->update([
                                        'harga' => $ongkir
                                    ]);    
                                    
        $update_qty = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', '2220,00')
                                    ->where('status', NULL)
                                    ->update([
                                        'keterangan' => $catatan,
                                    ]);  
                     
        $update_piutang_pelanggan = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', $this->akun_pelanggan(1, $id_pelanggan_jurnal, 0, 'pl'))
                                    ->update([
                                        'no_akun' => $this->akun_pelanggan(1, $id_pelanggan, 0, 'pl')
                                    ]);
    }

    public function update_jurnal_dyson($id_sj, $kode_barang, $ketr_barang, $ketr_brg, $id_brg, $qty_brg, $harga_brg)
    {
        $item_kas = $this->akun_barang(1, $kode_barang, 1, $ketr_barang);
        $item_dyson = $this->akun_barang(1, $kode_barang, 7, $ketr_barang=null);

        $update_dyson = DB::table('jurnal')
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', $item_dyson)
                                    ->where('status', NULL)
                                    ->update([
                                        'no_akun' => $this->akun_barang(1, $id_brg, 7, $ketr_barang=null),
                                        'keterangan' => $this->nama_akun(($this->akun_barang(1, $id_brg, 2, $ketr_barang=null))),
                                        'qty' => $qty_brg,
                                        'harga' => $this->harga_pokok($id_brg)
                                    ]); 
        
        $update_kas = DB::table('jurnal')     
                                    ->where('ref', $id_sj)
                                    ->where('no_akun', $item_kas)
                                    ->where('status', NULL)
                                    ->update([
                                        'no_akun' => $this->akun_barang(1, $id_brg, 1, $ketr_brg),
                                        'keterangan' => $this->nama_akun(($this->akun_barang(1, $id_brg, 1, $ketr_brg))),
                                        'qty' => $qty_brg,
                                        'harga' => $this->harga_pokok($id_brg)
                                    ]);                                        

        $res = [];

        if ($update_kas) {
                $res = [
                'code' => 200,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Data gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }
    //end AKUNTANSI

    public function add_pelanggan(Request $req)
    {
        // inisialisai pelanggan 
        $id_user = session::get('id_user');
        $nama_pl = $req->_namaPl;
        $alamat_pl = $req->_alamatPl;
        $no_telp = $req->_telpPl;
        $email = $req->_emailPl;

        $data_pl = [
            'nama' => $nama_pl,
            'alamat' => $alamat_pl,
            'no_telp' => $no_telp,
            'email' => $email,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ];

        $return_id = DB::table('pelanggan')->insertGetId($data_pl);

        if (isset($return_id)) {
            $res = [
                'code' => 201,
                'msg' => 'Pelanggan berhasil ditambahkan',
                'id' => $return_id,
                'nama_pl' => $nama_pl
            ];
        }else {
            $res = [
                'code' => 400,
                'msg' => 'Pelanggan Gagal ditambahkan !',
                'id' => NULL,
                'nama_pl' => NULL
            ];
        }
        return response()->json($res);
    }

    public function remake_akun($id_sj)
    {
        $jurnal = DB::table('jurnal')
                            ->whereIn('no_akun', ['118','119'])
                            ->delete();

        $update_total = DB::table('jurnal')
                                ->where('no_akun', '6111,00')
                                ->where('ref', $id_sj)
                                ->where('status', '=', NULL)
                                ->update([
                                    'harga' => $this->get_hpp($id_sj, 1),
                                    'total' => $this->get_hpp($id_sj, 1)
                                ]);
    }

    public function save(Request $req)
    {
       //inisialiasi surat jalan
        $id_user = session::get('id_user');
        $no_nota = $req->_noNota;
        $tgl = date("Y-m-d", strtotime($req->_tgl)) ;
        $id_pelanggan = $req->_idPelanggan;
        $catatan = $req->_catatan;
        $pembayaran = $req->_pembayaran;
        $pengiriman = $req->_pengiriman;
        $id_kendaraan = $req->_idKendaraan;
        $id_karyawan = $req->_idKaryawan;
        $id_bank = $req->_idBank;
        $opsi = $req->_opsi;
        $ongkir = $req->_ongkir;
        $is_cek_nota = NULL;

        //inisialiasi surat jalan detail
        $id_brg = $req->_idBrg;
        $nama_brg = $req->_namaBrg;
        $satuan = $req->_idSatuan;
        $harga_new = $req->_hargaNew;
        $harga = $req->_harga ;
        $ketr_ganti_harga = $req->_ketrGantiHarga;
        $qty = $req->_qty;
        $potongan = $req->_potongan;
        $ketr = $req->_ketr;
        $ketr_tambahan = $req->_ketrTambahan;
        $subtotal = ($qty * (isset($harga_new) ? $harga_new : $harga)) - ($qty * $potongan);
        $id_item = $req->_idItem;
        
        $bank = DB::table('bank')->where('id',$id_bank)->first();
        
        $data_sj = [
            'id' => $no_nota,
            'tgl' => $tgl,
            'id_pelanggan' => $id_pelanggan,
            'catatan' => $catatan,
            'pembayaran' => $pembayaran,
            'bank' => isset($bank) ? $bank->nama : '-',
            'no_rek' => isset($bank) ? $bank->no_rek : '',
            'pengiriman' => $pengiriman,
            'id_kendaraan' => $id_kendaraan,
            'id_karyawan' => $id_karyawan,
            'opsi' => $opsi,
            'ongkir' => $ongkir,
            'is_cek_nota' => $is_cek_nota,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ];

        $data_sj_detail = [
            'id_sj' => $no_nota,
            'nama_brg' => $nama_brg,
            'satuan' => $satuan,
            'harga' => $harga,
            'harga_new' => $harga_new,
            'ketr_ganti_harga' => $ketr_ganti_harga,
            'qty' => $qty,
            'potongan' => $potongan,
            'subtotal' => $subtotal,
            'ketr' => $ketr,
            'ketr_tambahan' => $ketr_tambahan,
            'created_at' => date("Y-m-d H:i:s"),
            'user_add' => $id_user
        ]; 

        $cek_sj = DB::table('suratjalan')->where('id',$no_nota)->first();
        
        if (!$tgl || !$id_pelanggan || !$pembayaran || !$pengiriman || (!$cek_sj && !$nama_brg)) {
            $msg = [];
            $res = [
                'code' => 400,
                'msg' => 'Data Belum Lengkap'
            ];
        } else {
            $harga_akun = isset($harga_new) ? $harga_new : $harga;
            $potongan_akun = isset($potongan) ? $potongan : NULL;
            $ketr_tambahan_akun = isset($ketr_tambahan) ? $ketr_tambahan : NULL;

            if (is_null($cek_sj)) {
                $insert_sj = DB::table('suratjalan')->insert($data_sj);
                $insert_sj_detail = DB::table('suratjalan_detail')->insertGetId($data_sj_detail);
                if ($insert_sj) {
                    $jumlah = DB::table('suratjalan_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                    $this->set_akun('insert', $tgl, $no_nota, $id_pelanggan, $opsi, $ketr, $id_brg, $id_kendaraan, $qty, $harga_akun, 
                                                $potongan_akun, $jumlah, $ongkir, $catatan, $ketr_tambahan, $insert_sj_detail,$pengiriman);
                    $update_total_sj = DB::table('suratjalan')->where('id', $no_nota)->update([
                        'total' => $jumlah
                    ]);
                    $this->remake_akun($no_nota);
                    $res = [
                        'code' => 200,
                        'msg' => 'Berhasil Disimpan',
                        'id_bank' => isset($bank) ? $bank->id : ''
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Gagal Disimpan'
                    ];
                }
                
            } elseif (!is_null($cek_sj)) {
                if (isset($id_item)) {
                    $sj_detail = DB::table('suratjalan_detail')->where('id', $id_item)->first();
                    $id_suratjalan = $sj_detail->id_sj;
                    $nama_barang = $sj_detail->nama_brg;
                    $ketr_barang = $sj_detail->ketr;
                    $opsi = $cek_sj->opsi;

                    $barang = DB::table('barang')->where('nama_brg', $nama_barang)->first();
                    $kode_barang = $barang->kode;

                    if ( $opsi == NULL) {
                        $this->update_jurnal_item($id_item, $kode_barang, $ketr_barang, $ketr, $id_brg, $qty, $harga_akun, $potongan_akun, $ketr_tambahan_akun);
                    } elseif( $opsi == 1) {
                        $this->update_jurnal_ina($id_suratjalan, $kode_barang, $ketr_barang, $ketr, $id_brg, $qty, $harga_akun);
                    } elseif( $opsi == 2 || $opsi == 3 ) {
                        update_jurnal_nonik($id_suratjalan, $kode_barang, $ketr_barang, $id_brg, $qty_brg, $ongkir, $catatan);
                    } elseif ( $opsi == 5) {
                        $this->update_jurnal_dyson($id_suratjalan, $kode_barang, $ketr_barang, $ketr, $id_brg, $qty, $harga_akun);
                    }
                    
                    $update_item = DB::table('suratjalan_detail')->where('id',$id_item)->update($data_sj_detail);

                    if ($update_item) {
                        $jumlah = DB::table('suratjalan_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                        $update_total_sj = DB::table('suratjalan')->where('id', $no_nota)->update([
                            'total' => $jumlah,
                            'is_cek_nota' => $is_cek_nota
                        ]);
                        
                        $res = [
                            'code' => 200,
                            'msg' => 'Data Barang Berhasil Diupdate',
                            'id_bank' => isset($bank) ? $bank->id : ''
                        ];
                    } else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Data Barang Gagal Diupdate'
                        ];
                    }
                } else {
                    if (isset($nama_brg)) {
                        $insert_sj_detail = DB::table('suratjalan_detail')->insertGetId($data_sj_detail);
                        if (isset($insert_sj_detail)) {
                            $jumlah = DB::table('suratjalan_detail')->where('id_sj',$no_nota)->where('status',NULL)->sum('subtotal');
                            $update_total_sj = DB::table('suratjalan')->where('id', $no_nota)->update([
                                'total' => $jumlah,
                                'is_cek_nota' => $is_cek_nota
                            ]);
                            $this->set_akun('add_item', $tgl, $no_nota, $id_pelanggan, $opsi, $ketr, $id_brg, $id_kendaraan, $qty, $harga_akun, 
                                                        $potongan_akun, $jumlah, $ongkir, $catatan, $ketr_tambahan, $insert_sj_detail,$pengiriman);
                            
                            $this->remake_akun($no_nota);
                            $res = [
                                'code' => 200,
                                'msg' => 'Data Barang Berhasil Disimpan',
                                'id_bank' => isset($bank) ? $bank->id : ''
                            ];
                        } else {
                            $res = [
                                'code' => 400,
                                'msg' => 'Data Barang Gagal Disimpan'
                            ];
                        }
                    }

                    $id_kendaraan_jurnal = $cek_sj->id_kendaraan;
                    $id_pelanggan_jurnal = $cek_sj->id_pelanggan;
                    // $this->update_kendaraan_jurnal($no_nota, $id_kendaraan, $id_kendaraan_jurnal, $pengiriman);
                    $this->update_jurnal($no_nota, $tgl, $id_pelanggan, $id_pelanggan_jurnal, $ongkir, $catatan);

                    $update_sj =  DB::table('suratjalan')->where('id', $no_nota)->update($data_sj);
                    
                    if ($update_sj) {
                        $res = [
                            'code' => 201,
                            'msg' => 'SJ Berhasil Diupdate',
                            'id_bank' => isset($bank) ? $bank->id : ''
                        ];
                    }else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Gagal update SJ !'
                        ];
                    }
                }
                    
            }
        }
        return response()->json($res);
    }

    public function datatable_brg_detail(Request $req)
    {
        $id = $req->_id;

        $data = DB::table('suratjalan_detail as a')
                    ->where('a.status', NULL)
                    ->where('a.id_sj', $id)
                    ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                    ->leftJoin('barang as c', 'a.nama_brg', '=', 'c.nama_brg')
                    ->select('a.id','a.nama_brg','a.ketr','a.ketr_tambahan','a.satuan as id_satuan','a.qty','a.harga','a.harga_new','a.potongan','a.ketr_ganti_harga',
                                'b.nama as satuan',
                                'c.kode')
                    ->get();
        return Datatables::of($data)
        ->addIndexColumn()
        ->editColumn('harga', function ($data) {
            $harga = isset($data->harga_new) ? $data->harga_new : $data->harga;
            return $harga;
        })
        ->addColumn('subtotal', function ($data) {
            $qty = $data->qty;
            $harga_item = isset($data->harga_new) ? $data->harga_new : $data->harga ;
            $potongan = $data->potongan;
            $subtotal = ($qty * $harga_item) - ($qty * $potongan);
            return number_format($subtotal, 0,',', '.');
        })
        ->addColumn('opsi', function ($data) {
            $id_item = $data->id;
            $nama_item = "'".$data->nama_brg."'";
            $harga_item = $data->harga;
            $kode_brg = $data->kode;

            $harga_new = isset($data->harga_new) ? $data->harga_new : NULL;
            $ketr_ganti_harga = isset($data->ketr_ganti_harga) ? "'".$data->ketr_ganti_harga."'" : "'"."'";
            $id_satuan = $data->id_satuan;
            $satuan = "'".$data->satuan."'";
            $qty = !is_null($data->qty) ? $data->qty : 0 ;
            $potongan = !is_null($data->potongan) ? $data->potongan : 0;
            $ketr = !is_null($data->ketr) ?  "'".$data->ketr."'" : "'"."'";
            $ketr_tambahan = !is_null($data->ketr_tambahan) ?  "'".$data->ketr_tambahan."'" : "'"."'";
            $subtotal = ($qty * $harga_item) - ($qty * $potongan);
            return '<button type="button" class="btn btn-sm btn-primary" onclick="select_item('.$id_item.','.$kode_brg.','.$nama_item.','.$id_satuan.','.$satuan.','.$harga_item.','.$qty.','.$potongan.','.$ketr.','.$ketr_tambahan.','.$subtotal.','.$ketr_ganti_harga.','.$harga_new.')"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-sm btn-danger" onclick="delete_item('.$id_item.')"><i class="fa fa-trash"></i></button>';
        })
        ->rawColumns(['opsi'])
        ->make(true);
    }

    public function form_edit($id)
    {
        $id = base64_decode($id);
        $suratjalan = DB::table('suratjalan')->where('id', $id)->first();

        $kendaraan = DB::table('kendaraan')->where('penyusutan', '<>', NULL)->get();
        $sopir = DB::table('karyawan')->where('id_jabatan',4)->get();
        $pelanggan = DB::table('pelanggan')->where('status', NULL)->get();
        
        $data['kendaraan'] = $kendaraan;
        $data['sopir'] = $sopir;
        $data['pelanggan'] = $pelanggan;

        $data['id'] = $suratjalan->id;
        $data['tgl'] = $suratjalan->tgl;
        $data['id_pelanggan'] = $suratjalan->id_pelanggan;
        $data['catatan'] = $suratjalan->catatan;
        $data['pembayaran'] = $suratjalan->pembayaran;
        $data['no_rek'] = $suratjalan->no_rek;
        $data['pengiriman'] = $suratjalan->pengiriman;
        $data['id_kendaraan'] = $suratjalan->id_kendaraan;
        $data['id_karyawan'] = $suratjalan->id_karyawan;
        $data['ongkir'] = $suratjalan->ongkir;
        $data['opsi'] = $suratjalan->opsi;
        $data['form'] = 'edit';         
        
        return view('admin.PrintSj.form')->with($data);
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id = base64_decode($req->_idSj);

        $res = [];
        
        $delete = DB::table('suratjalan')->where('id', $id)->delete();
        
        if($delete) {
            $delete_sj = DB::table('suratjalan_detail')->where('id_sj', $id)->delete();
            $delete_jurnal = DB::table('jurnal')->where('ref', $id)->delete();
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function hapus_jurnal_item($id_sj, $id, $ketr, $id_detail_sj, $id_pelanggan)
    {
        // $item_kas = $this->akun_barang(1, $id, 1, $ketr);
        // $item_pendapatan =  $this->akun_barang(4, $id, 1, $ketr=null);

        $update_kas = DB::table('jurnal')
                            ->where('ref', $id_sj)
                            ->where('id_item', $id_detail_sj)
                            // ->where('no_akun', $item_kas)
                            ->update( [ 'status' => 9 ]);
        $update_pendapatan = DB::table('jurnal')
                            ->where('ref', $id_sj)
                            ->where('id_item', $id_detail_sj)
                            // ->where('no_akun', $item_pendapatan)
                            ->update(['status' => 9 ]);
        $res = [];

        if ($update_kas || $update_pendapatan) {
            $update_hpp = DB::table('jurnal')
                                            ->where('no_akun', '6111,00')
                                            ->where('ref', $id_sj)
                                            ->where('status', '=', NULL)
                                            ->update([
                                                'harga' => $this->get_hpp($id_sj, 1),
                                                'total' => $this->get_hpp($id_sj, 1)
                                            ]);

            $pelangganQ = $this->akun_pelanggan(1, $id_pelanggan, 0, 'pl');                                                    
            $update_piutang = DB::table('jurnal')
                                            ->where('ref', $id_sj)
                                            ->where('no_akun', $pelangganQ)
                                            ->where('status', NULL)
                                            ->update([
                                                'harga' => $this->get_hpp($id_sj, 2),
                                                'total' => $this->get_hpp($id_sj, 2)
                                            ]);                                                 
             $res = [
                'code' => 200,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Data gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function hapus_jurnal_ina($id_sj, $id, $ketr)
    {
        $item_ina = $this->akun_barang(1, $id, 2, $ketr=null);
        $item_kas =  $this->akun_barang(1, $id, 1, $ketr);

        $update_ina = DB::table('jurnal')->where('ref', $id_sj)->where('no_akun', $item_ina)->update( [ 'status' => 9 ]);
        $update_kas = DB::table('jurnal')->where('ref', $id_sj)->where('no_akun', $item_kas)->update( [ 'status' => 9 ]);

        $res = [];

        if ($update_ina || $update_kas) {                      
             $res = [
                'code' => 200,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Data gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function hapus_jurnal_nonik($id_sj, $id)
    {
        $item_nonik = $this->akun_barang(1, $id, 4, $ketr=null);

        $update_nonik = DB::table('jurnal')->where('ref', $id_sj)->where('no_akun', $item_nonik)->update( [ 'status' => 9 ]);
        $res = [];

        if ($update_nonik) {    
            $update_qty = DB::table('jurnal')
                                            ->where('ref', $id_sj)
                                            ->where('no_akun', '2220,00')
                                            ->where('status', NULL)
                                            ->update([
                                                'qty' => $this->total_qty_nonik($id_sj, 'b')
                                            ]);                  
             $res = [
                'code' => 200,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Data gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function delete_item(Request $req) 
    {
        $id_user = session::get('id_user');
        $id = $req->_idItem;

        $sj_detail = DB::table('suratjalan_detail')->where('id', $id)->first();
        // $id_detail_sj = $sj_detail->id;
        $id_sj = $sj_detail->id_sj;
        $nama_barang = $sj_detail->nama_brg;
        $ketr_barang = $sj_detail->ketr;

        $barang = DB::table('barang')->where('nama_brg', $nama_barang)->first();
        $id_barang = $barang->kode;

        $sj = DB::table('suratjalan')->where('id', $id_sj)->first();
        $opsi = $sj->opsi;
        $id_pelanggan = $sj->id_pelanggan;

        $data_sj_detail = [
            'updated_at' => date("Y-m-d H:i:s"),
            'user_upd' => $id_user,
            'status' => 9
        ];
        $res = [];

        $delete_sj = DB::table('suratjalan_detail')->where('id', $id)->delete();
        if($delete_sj) {
            $jumlah = DB::table('suratjalan_detail')->where('id_sj',$id_sj)->where('status',NULL)->sum('subtotal');
            $update_total_sj = DB::table('suratjalan')->where('id', $id_sj)->update([
                'total' => $jumlah
            ]);

            if ($opsi == NULL) {
                $this->hapus_jurnal_item($id_sj, $id_barang, $ketr_barang, $id, $id_pelanggan);
            } elseif ($opsi == 1) {
                $this->hapus_jurnal_ina($id_sj, $id_barang, $ketr_barang);
            } elseif ($opsi == 2 || $opsi == 3) {
                $this->hapus_jurnal_nonik($id_sj, $id_barang);
            }
            
            $res = [
                'code' => 300,
                'msg' => 'Data telah dihapus'
            ];
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Gagal dihapus'
            ];
        }
        $data['response'] = $res;
        return response()->json($data);
    }

    public function nota($id)
    {
        // $id = base64_decode($id);
        $suratjalan = DB::table('suratjalan as sj')
                            ->where('sj.status', NULL) 
                            ->where('sj.id',$id)
                            ->leftJoin('pelanggan as pl', 'sj.id_pelanggan', '=', 'pl.id')
                            ->leftJoin('kendaraan as k', 'sj.id_kendaraan', '=', 'k.id')
                            ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                            ->select('sj.id','sj.tgl','sj.catatan','sj.pembayaran','sj.no_rek','sj.bank','sj.pengiriman','sj.id_kendaraan','sj.id_karyawan','sj.total','sj.ongkir','sj.opsi','sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.user_add',
                                        'pl.nama','pl.alamat','pl.no_telp','pl.email',
                                            'k.nama as kendaraan', 
                                                'kar.nama as sopir')
                            ->first();

        $detail_barang = DB::table('suratjalan_detail as a')
                            ->where('a.status', NULL)
                            ->where('a.id_sj', $id)
                            ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                            ->select('a.id','a.nama_brg','a.ketr','a.satuan as id_satuan','a.qty','a.harga','a.harga_new','a.potongan','a.subtotal','b.nama as satuan')
                            // ->orderBy('a.nama_brg', 'ASC')
                            ->get(); 

        $data_item = [];        
        foreach ($detail_barang as $item) {
            $data_item[] = (object) [
                            'nama_brg' => $item->nama_brg,
                            'ketr' => $item->ketr,
                            'satuan' => $item->satuan,
                            'qty' => $item->qty,
                            'harga' => isset($item->harga_new) ? $item->harga_new : $item->harga,
                            'potongan' => $item->potongan,
                            'subtotal' => $item->subtotal
                        ];
        }                        
        
        $data['detail_barang'] = $data_item;
               
        //inisialisasi data SJ
        $data['id'] = $suratjalan->id;
        $data['tgl'] = date('d M y', strtotime($suratjalan->tgl));
        $data['catatan'] = $suratjalan->catatan;
        $data['pembayaran'] = $suratjalan->pembayaran;
        $data['no_rek'] = $suratjalan->no_rek;
        $data['bank'] = $suratjalan->bank;
        $data['total'] = $suratjalan->total;
        $data['ongkir'] = $suratjalan->ongkir;

        //inisialisasi data pelanggan
        $data['nama'] = $suratjalan->nama;
        $data['alamat'] = $suratjalan->alamat;
        $data['no_telp'] = $suratjalan->no_telp;
        $data['email'] = $suratjalan->email;
        $data['mengetahui'] = $this->get_karyawan($suratjalan->user_add);
        $data['cek_nota'] = isset($suratjalan->cek_nota) ? $this->get_karyawan($suratjalan->cek_nota) : '-';

        return view('admin.PrintSj.nota')->with($data);
    }

    public function sj($id)
    {
        // $id = base64_decode($id);
        $suratjalan = DB::table('suratjalan as sj')
                            ->where('sj.status', NULL) 
                            ->where('sj.id',$id)
                            ->leftJoin('pelanggan as pl', 'sj.id_pelanggan', '=', 'pl.id')
                            ->leftJoin('kendaraan as k', 'sj.id_kendaraan', '=', 'k.id')
                            ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                            ->select('sj.id','sj.tgl','sj.catatan','sj.pembayaran','sj.pengiriman','sj.id_kendaraan','sj.id_karyawan','sj.total','sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.user_add',
                                        'pl.nama','pl.alamat','pl.no_telp','pl.email',
                                            'k.nama as kendaraan','k.no_plat',
                                                'kar.nama as sopir')
                            ->first();
        $detail_barang = DB::table('suratjalan_detail as a')
                            ->where('a.status', NULL)
                            ->where('a.id_sj', $id)
                            ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                            ->select('a.id','a.nama_brg','a.ketr','a.satuan as id_satuan','a.qty','a.harga','a.potongan','b.nama as satuan')
                            ->get();        
                                    
        $jumlah = DB::table('suratjalan_detail')->where('id_sj',$id)->sum('qty');
        
        $data['detail_barang'] = $detail_barang;
                
        //inisialisasi data SJ
        $data['id'] = $suratjalan->id;
        $data['tgl'] = date('d M Y', strtotime($suratjalan->tgl));
        $data['catatan'] = $suratjalan->catatan;
        $data['pembayaran'] = $suratjalan->pembayaran;
        $data['pengiriman'] = $suratjalan->pengiriman;
        $data['sopir'] = $suratjalan->sopir;
        $data['kendaraan'] = $suratjalan->kendaraan;
        $data['no_plat'] = $suratjalan->no_plat;
        $data['total'] = $suratjalan->total;
        $data['jumlah'] = $jumlah;

        //inisialisasi data pelanggan
        $data['nama'] = $suratjalan->nama;
        $data['alamat'] = $suratjalan->alamat;
        $data['no_telp'] = $suratjalan->no_telp;
        $data['email'] = $suratjalan->email;
        $data['mengetahui'] = $this->get_karyawan($suratjalan->user_add);
        $data['cek_nota'] = isset($suratjalan->cek_nota) ? $this->get_karyawan($suratjalan->cek_nota) : '-';
       
        return view('admin.PrintSj.sj')->with($data);
    }

    // START FUNGSI UNTUK REKAP SJ
    public function ketebalan($nama_brg)
    {
        $data['panjang'] = '';
        $data['lebar'] = '';
        $data['tebal'] = '';

        if ($this->is_lps($nama_brg) == 'true') {
            $pecah = explode('x', $nama_brg);
            $data['panjang'] = $pecah[0];
            $data['lebar'] = $pecah[1];
            $data['tebal'] = (int) filter_var($pecah[2],FILTER_SANITIZE_NUMBER_INT);
        } else {
            $data['panjang'] = 244;
            $data['lebar'] = 122;
            $data['tebal'] = (int) filter_var($nama_brg, FILTER_SANITIZE_NUMBER_INT);
        }
        
        return $data;
    }

    public function rekap_piutang($id_pelanggan, $subtotal, $jenis_pembayaran)
    {
        $data['subtotal'] ='';
        $data['piutang'] ='';  
        $data['bank'] ='';

        if ( $id_pelanggan == 10 || $id_pelanggan == 13 || $id_pelanggan == 25 || $id_pelanggan == 29 || $id_pelanggan == 40 || $id_pelanggan == 60 || $id_pelanggan == 61) {
            $data['piutang'] = $subtotal;
        } elseif($jenis_pembayaran == 'Transfer') {
            $data['bank'] = $subtotal;
        } else {
            $data['subtotal'] = $subtotal;
        }
        return $data;
    }

    public function get_jenis_barang($nama_brg)
    {        
        $pecah = explode('mm', $nama_brg);
        $jenis_barang = trim(isset($pecah[1]) ? $pecah[1] : $nama_brg, " ");

        return $jenis_barang;
    }

    public function rekap_ketr($ketr, $ketr_tambahan, $potongan)
    {
        $dt = [];
        $cek_ketr = isset($ketr) ? $dt[0]=$ketr : null;
        $cek_ketr_tambahan = isset($ketr_tambahan) ? $dt[1]=$ketr_tambahan : null;
        $cek_potongan = isset($potongan) ? $dt[2]='potongan '.$potongan : null;
        
        $hasil = implode(', ',$dt);
        return $hasil;
    }
    //END FUNGSI REKAP SJ

    public function excel_sj($tgl) 
    {
        $pecah = explode('&', $tgl);
          $tgl_m = $pecah[0];
            $tgl_a = $pecah[1];  
        // $tgl_m = $req->_tglM;
        // $tgl_a = $req->_tglA;
        $rekap = DB::table('suratjalan as sj')
                                ->leftJoin('suratjalan_detail as sjd', 'sj.id', '=', 'sjd.id_sj')
                                ->leftJoin('pelanggan as pl', 'sj.id_pelanggan', '=', 'pl.id')
                                ->whereBetween('tgl', [$tgl_m, $tgl_a])
                                ->select('sj.id', 'sj.tgl', 'sj.catatan', 'sj.id_pelanggan', 'sj.pembayaran',
                                        'sjd.id as id_item','sjd.nama_brg', 'sjd.qty', 'sjd.ketr', 'sjd.ketr_tambahan', 'sjd.harga', 'sjd.harga_new','sjd.subtotal', 'sjd.potongan',
                                        'pl.nama as nama_pelanggan')
                                ->orderBy('sj.tgl', 'ASC')
                                ->orderBy('sj.id', 'ASC')
                                ->orderBy('id_item', 'ASC')
                                ->get();    
        
        $data_rekap = [];
        
        foreach ($rekap as $value) {
            $data_rekap[] = (object) [
                            'id' => $value->id,
                            'tgl' => $value->tgl,
                            'id_pelanggan' => $value->id_pelanggan,
                            'potongan' => $value->potongan,
                            'nama_pelanggan' => strtolower($value->nama_pelanggan),
                            'ketebalan' => $this->ketebalan($value->nama_brg)['tebal'],
                            'qty' => $value->qty,
                            'jenis' => ($this->is_lps($value->nama_brg) == 'true') ? '' : $this->get_jenis_barang($value->nama_brg),
                            'ketr' => $this->rekap_ketr($value->ketr,$value->ketr_tambahan,$value->potongan),
                            'harga' => isset($value->harga_new) ? $value->harga_new : $value->harga,
                            'catatan' => $value->catatan,
                            'subtotal' => $this->rekap_piutang($value->id_pelanggan, $value->subtotal, $value->pembayaran)['subtotal'],
                            'piutang' => $this->rekap_piutang($value->id_pelanggan, $value->subtotal, $value->pembayaran)['piutang'],
                            'tgl_lunas' => NULL,
                            'bank' => $this->rekap_piutang($value->id_pelanggan, $value->subtotal, $value->pembayaran)['bank'],
                            'lebar' => $this->ketebalan($value->nama_brg)['lebar'],
                            'panjang' => $this->ketebalan($value->nama_brg)['panjang'],
                            'm3' => ($this->ketebalan($value->nama_brg)['tebal']*$value->qty*122*244)/10000000,
                        ];
        }
        
        $nama_file = "Rekap SJ ".$tgl_m."-".$tgl_a.".xlsx";
        return Excel::download(new SjExport($data_rekap), $nama_file);                               
    }

    // public function delete_tgl(Request $req)
    // {
    //     $id_user = session::get('id_user');
    //     $tgl_m = $req->_tglM;
    //     $tgl_a = $req->_tglA;

    //     $res = [];

    //     $delete = DB:: table('suratjalan')
    //                     ->whereBetween('tgl', [$tgl_m, $tgl_a])
    //                     ->delete();

    //     if ($delete) {
    //         $delete_sj = DB::table('suratjalan_detail')->where('id_sj', $id)->delete();
    //         $res = [
    //             'code' => 300,
    //             'msg' => 'Data telah dihapus'
    //         ];
    //     } else {
    //         $res = [
    //             'code' => 400,
    //             'msg' => 'Gagal dihapus'
    //         ];
    //     }
    //     $data['response'] = $res;
    //     return response()->json($data);
    // }
}
