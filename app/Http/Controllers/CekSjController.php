<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Session;
use DB;
use Redirect;
use Validator;

class CekSjController extends Controller
{
    private $transaksi;
    public function __construct()
    {
        date_default_timezone_set("Asia/Jakarta");
        $this->transaksi = DB::table('transaksi')->get();
    }

    public function index()
    {
        return view('admin.CekSj.index');
    }

    public function get_karyawan($id_user)
    {
        $data = DB::table('karyawan')->where('id_users', $id_user)->first();
        return isset($data) ? $data->nama : 'No Name';
    }

    public function get_transaksi($transaksi, $id_sj)
    {
        $dty = array_filter($transaksi->toArray(), function ($value) use ($id_sj) {
            return $value->id_sj == $id_sj;
        });

        $bayar = null;
        $jenis_byr = '';
        foreach ($dty as $value) {
            $bayar += $value->total;
            if ($value->jenis_byr == 'Transfer') {
                $jenis_byr = '<br> Transfer : ';
            }
        }

        $data['bayar'] = $bayar;
        $data['jenis_byr'] = $jenis_byr;
        return $data;
    }

    public function datatable()
    {

        $parent_jurnal = DB::table('parent_jurnal')
                                ->where('status', 'tutup')
                                ->orderBy('created_at', 'DESC')
                                ->first();

        $tgl_akhir = isset($parent_jurnal->tgl_akhir) ? $parent_jurnal->tgl_akhir : '';                                

        $sj = DB::table('suratjalan')
                    ->whereDate('tgl', '<', $tgl_akhir)
                    ->get();

        $id_sj = [];

        foreach ($sj as $value) {
            $id_sj[] = $value->id;
        }

        $data = DB::table('suratjalan as sj')
                    ->where('sj.status',NULL)
                    ->whereNotIn('sj.id', $id_sj)   
                    ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                    ->leftJoin('pelanggan as pl', 'sj.member', '=', 'pl.id')
                    ->select('sj.id','sj.tgl','sj.pelanggan','pl.nama as pelanggan','sj.member','sj.catatan','sj.total','sj.bayar','sj.tgl','sj.opsi','sj.ongkir','sj.pembayaran','sj.pengiriman','sj.bank','sj.user_add',
                                'sj.print_nota','sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.is_batal','sj.batal',
                                    'kar.nama as sopir')
                    ->orderBy('sj.tgl', 'DESC')
                    ->orderBy('sj.id', 'DESC')
                    ->get();

        $transaksi = $this->transaksi;

        return Datatables::of($data)
        
        ->editColumn('is_cek_nota', function ($data) {
            $user_add = $data->user_add;
            $is_cek_nota = $data->is_cek_nota;
            $user_add = isset($user_add) ? $this->get_karyawan($user_add) : NULL;
            $cek_nota = isset($is_cek_nota) ? $this->get_karyawan($data->cek_nota) : NULL;
            $is_batal = isset($data->is_batal) ? $this->get_karyawan($data->is_batal).' ('.$data->batal.')' : NULL;

            $status_user_add = isset($user_add) ? NULL : 'hidden';
            $status_cek_nota = isset($cek_nota) ? NULL : 'hidden';
            $status_batal = isset($is_batal) ? NULL : 'hidden';

            $cek = 'Kasir : <span class="badge badge-primary" '.$status_user_add.'> '.$user_add.' </span> </br>
                    Cek : <span class="badge badge-success" '.$status_cek_nota.'> '.$cek_nota.' </span> </br>
                    Batal : <span class="badge badge-danger" '.$status_batal.'> '.$is_batal.' </span> </br>';
            return $cek;
            // if ($is_cek_nota == 1) {
            //     return '<center><i class="fa fa-check"></i>'.$cek_nota.'</center>';
            // } else if ($is_cek_nota == 2) {
            //     return '<center><i class="fa fa-close"></i>'.$cek_nota.'</center>';
            // } else if ($is_cek_nota == '') {
            //     return '<center><i class="fa fa-minus"></center>';
            // } 
        }) 
        // ->editColumn('batal', function ($data) {
        //     $nama = isset($data->is_batal) ? $data->batal.' ('.$this->get_karyawan($data->is_batal).')' : null; 
        //     return $nama;
        // })
        ->editColumn('pembayaran', function ($data) use ($transaksi) {
            $jenis_byr = $this->get_transaksi($transaksi, $data->id)['jenis_byr'];
            $pembayaran = isset($data->pembayaran) ? $data->pembayaran.' : ' : null; 
            $bayar = $data->bayar;
            $pelunasan = $this->get_transaksi($transaksi, $data->id)['bayar'];
            $hasil = ($pembayaran != 'Transfer') ? $pembayaran.$bayar.$jenis_byr.$pelunasan : 'Transfer';
            return $hasil;
        }) 
        // ->addColumn('status_bayar', function ($data) use ($transaksi) {
        //     $total = $data->total;
        //     $bayar = $data->bayar;
        //     $pelunasan = 0;
        //     $pelunasan = $this->get_transaksi($transaksi, $data->id)['bayar'];

        //     $total_bayar = $bayar+$pelunasan;
        //     $ketr = '';
        //     $sisa = 0;
        //     if ( $total > $total_bayar ) {
        //         $sisa = $total-$total_bayar;
        //         $ketr = '<span class="badge badge-secondary">Unpaid</span>';
        //     } else {
        //         $ketr = '<span class="badge badge-success">Paid</span>';
        //     }

        //     return $ketr;
        // })
        ->editColumn('total', function ($data)  use ($transaksi) {
            $total_format = number_format($data->total, 0,',', '.'); 
            $total = $data->total;
            $bayar = $data->bayar;
            $pelunasan = 0;
            $pelunasan = $this->get_transaksi($transaksi, $data->id)['bayar'];

            $total_bayar = $bayar+$pelunasan;
            $ketr = '';
            $sisa = 0;
            if ( $total > $total_bayar ) {
                $sisa = $total-$total_bayar;
                $ketr = '<br><span class="badge badge-warning">Belum Lunas</span>';
            } else {
                $ketr = '<br><span class="badge badge-success">Lunas</span>';
            }

            return $total_format.$ketr;
        })
        ->addColumn('opsi', function ($data){
            $id_sj = $data->id;
            $tgl = date("d-m-Y", strtotime($data->tgl));
            $pelanggan = isset($data->pelanggan) ? $data->pelanggan : $data->nama; 
            $catatan = $data->catatan;
            $pembayaran = $data->pembayaran;
            $pengiriman = $data->pengiriman;
            // $id_kendaraan = $data->kendaraan;
            // $id_karyawan = $data->sopir;
            $opsi = $data->opsi;
            $total = ($opsi == 2) ? NULL : $data->total;
            $ongkir = $data->ongkir;
            $grandtotal = ($opsi == 2 ) ? NULL : $total + $ongkir;
            $bank = $data->bank;
            $cek_nota = ($data->is_cek_nota == '1') ? 'acc' : 'tidak acc';
            $nota = route('ceksj.nota', [base64_encode($data->id)]);
            $sj = route('ceksj.sj', [base64_encode($data->id)]);
            $status = ($data->is_cek_nota == 1 && !isset($data->is_batal)) ? 'btn-success' : 'btn-secondary disabled';
            $status_hapus = ($data->is_cek_nota != 1) ? 'btn-danger' : 'btn-secondary disabled';
            $status_batal = ($data->print_nota != 1) ? null : 'none' ;
            $batal = $data->batal;
            $status_cek = ( (isset($data->is_batal)) || ($data->is_cek_nota == 1) ) ? 'none' : null;
            return '<button type="button" class="btn btn-sm btn-primary" style="display:'.$status_cek.'" data-toggle="modal" data-target="#modal_ceksj" data-form="cek_sj" data-id="'.$id_sj.'" data-tgl="'.$tgl.'" data-pelanggan="'.$pelanggan.'" data-total="'.$total.'" data-ongkir="'.$ongkir.'" data-grandtotal="'.$grandtotal.'" data-cek="'.$cek_nota.'">Cek Nota</button>
                    <a href="#" id="'.$data->id.'" class="print_nota btn btn-sm '.$status.'"><i class="fa fa-print"></i> Nota</a>
                    <button type="button" class="btn btn-sm '.$status_hapus.'" onclick="delete_suratjalan('.$id_sj.')"><i class="fa fa-trash"></i></button>
                    <button type="button" class="btn btn-sm btn-danger" style="display:'.$status_batal.'" data-toggle="modal" data-target="#modal_btl" data-id="'.$id_sj.'" data-ket="'.$batal.'">Batal</button>';
        })
        ->rawColumns(['is_cek_nota', 'opsi', 'pembayaran', 'total'])
        ->make(true);
    }

    public function datatable_sj(Request $req)
    {
        $id = $req->_id;
        $data = DB::table('suratjalan as sj')
                    ->where('a.status', NULL)
                    ->where('a.id_sj', $id)
                    ->leftJoin('suratjalan_detail as a', 'sj.id', '=', 'a.id_sj')
                    ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                    ->select('a.id','a.nama_brg','a.ketr','a.satuan as id_satuan','a.qty','a.harga','a.harga_new','a.potongan','a.subtotal', 'a.ketr_tambahan','a.ketr_ganti_harga',
                                'sj.id','sj.opsi',
                                    'b.nama as satuan')
                    ->get();

        return Datatables::of($data)  
        ->addIndexColumn() 
        ->editColumn('harga', function ($data) {
            $harga = (($data->opsi) == 2) ? NULL : $data->harga;
            return $harga;
        })
        ->editColumn('subtotal', function ($data) {
            $subtotal = (($data->opsi) == 2) ? NULL : $data->subtotal;
            return $subtotal;
        })
        // ->addColumn('subtotal', function ($data) {
        //     $qty = $data->qty;
        //     $harga_item = isset($data->harga_new)$data->harga;
        //     $potongan = $data->potongan;
        //     $subtotal = ($qty * $harga_item) - ($qty * $potongan);
        //     return number_format($subtotal, 0,',', '.'); 
        // }) 
        ->make(true);                                        
    }

    public function acc_sj(Request $req)
    {
        $id_users = session::get('id_user');
        $id = $req->_id;
        $btn = $req->_btn;
        $status = $req->_status;
        
        $data_acc_sj = [
            'is_cek_nota' => 1,
            'cek_nota' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $data_batal_sj = [
            'is_cek_nota' => 2,
            'cek_nota' => $id_users,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $cek_bayar = DB::table('suratjalan')
                            ->where('id', $id)
                            ->first();
        
        
        if($status != 'acc') {
            if ($btn == 'setuju') {
                // if (isset($cek_bayar->bayar)) {
                    $update_sj = DB::table('suratjalan')
                                    ->where('id', $id)
                                    ->update($data_acc_sj);
                    if ($update_sj) {
                        $res = [
                            'code' => 201,
                            'msg' => 'SJ Berhasil Di ACC'
                        ];
                    } else {
                        $res = [
                            'code' => 400,
                            'msg' => 'Sj gagal di ACC'
                        ];
                    }
                // } else {
                    // $res = [
                    //     'code' => 300,
                    //     'msg' => 'Gagal Konfirmasi, Belum Dibayar !'
                    // ];
                // }
                
            } else if ($btn == 'batal') {
                $update_sj = DB::table('suratjalan')
                            ->where('id', $id)
                            ->update($data_batal_sj);
                if ($update_sj) {
                    $res = [
                        'code' => 300,
                        'msg' => 'SJ Batal di Acc'
                    ];
                } else {
                    $res = [
                        'code' => 400,
                        'msg' => 'Sj gagal untuk dibatalkan'
                    ];
                }
            }
        } else {
            $res = [
                'code' => 400,
                'msg' => 'Sj sudah di ACC'
            ];
        }
        return response()->json($res);
    }

    public function nota($id)
    {
        $suratjalan = DB::table('suratjalan as sj')
                            ->where('sj.status', NULL) 
                            ->where('sj.id',$id)
                            ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                            ->select('sj.id','sj.tgl','sj.catatan','sj.pembayaran','sj.no_rek','sj.bank','sj.pengiriman','sj.id_kendaraan','sj.id_karyawan','sj.total','sj.ongkir','sj.opsi','sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.user_add',
                                        'sj.pelanggan as nama', 'sj.bayar', 'sj.kembalian')
                            ->first();

        $detail_barang = DB::table('suratjalan_detail as a')
                            ->where('a.status', NULL)
                            ->where('a.id_sj', $id)
                            ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                            ->select('a.id','a.nama_brg','a.ketr','a.satuan as id_satuan','a.qty','a.harga','a.harga_new','a.potongan','a.subtotal','b.nama as satuan')
                            // ->orderBy('a.nama_brg', 'ASC')
                            ->get(); 

        $transaksi = DB::table('transaksi')->where('id_sj', $id)->get();

        $data_item = [];    
        $total_qty = 0;  
        $total_pot = 0;  
        $pot = 0;
        $pot_kali = 0;
        $total_harga = 0;
        $totalq = 0;
        foreach ($detail_barang as $item) {
            $pot_kali = $item->qty * $item->potongan;
            $total_harga = $item->qty * $item->harga;
            $data_item[] = (object) [
                            'nama_brg' => $item->nama_brg,
                            'ketr' => $item->ketr,
                            'satuan' => $item->satuan,
                            'qty' => $item->qty,
                            'harga' => isset($item->harga_new) ? $item->harga_new : $item->harga,
                            'potongan' => $item->potongan,
                            'subtotal' => $item->subtotal,
                        ];
            $total_qty += $item->qty;
            $pot += $item->potongan;
            $total_pot += $pot_kali;
            $totalq += $total_harga;
        }  
        
        $data_transaksi = [];
        foreach ($transaksi as $v) {
            $data_transaksi[] = (object) [
                'jenis_byr'     => $v->jenis_byr,
                'total'         => $v->total
            ];
        }
        
        $data['detail_barang'] = $data_item;
        $data['transaksi'] = $data_transaksi;
               
        //inisialisasi data SJ
        $data['id'] = $suratjalan->id;
        $data['tgl'] = date('d M y', strtotime($suratjalan->tgl));
        $data['catatan'] = $suratjalan->catatan;
        $data['pembayaran'] = $suratjalan->pembayaran;
        $data['no_rek'] = $suratjalan->no_rek;
        $data['bank'] = $suratjalan->bank;
        $data['total'] = $suratjalan->total;
        $data['ongkir'] = $suratjalan->ongkir;
        $data['bayar'] = $suratjalan->bayar;
        $data['kembalian'] = $suratjalan->kembalian;
        $data['total_qty'] = $total_qty;
        $data['pot'] = $pot;
        $data['total_pot'] = $total_pot;
        $data['totalq'] = $totalq;

        //inisialisasi data pelanggan
        $data['nama'] = $suratjalan->nama;
        // $data['alamat'] = $suratjalan->alamat;
        // $data['no_telp'] = $suratjalan->no_telp;
        // $data['email'] = $suratjalan->email;
        $data['mengetahui'] = $this->get_karyawan($suratjalan->user_add);
        $data['cek_nota'] = isset($suratjalan->cek_nota) ? $this->get_karyawan($suratjalan->cek_nota) : '-';
        // dd($data);
        return view('admin.sjdefault.nota')->with($data);
    }

    public function sj($id)
    {
        $id = base64_decode($id);
        $suratjalan = DB::table('suratjalan as sj')
                            ->where('sj.status', NULL) 
                            ->where('sj.id',$id)
                            ->leftJoin('pelanggan as pl', 'sj.id_pelanggan', '=', 'pl.id')
                            ->leftJoin('kendaraan as k', 'sj.id_kendaraan', '=', 'k.id')
                            ->leftJoin('karyawan as kar', 'sj.id_karyawan', '=', 'kar.kode')
                            ->select('sj.id','sj.tgl','sj.catatan','sj.pembayaran','sj.pengiriman','sj.id_kendaraan','sj.id_karyawan','sj.total','sj.is_cek_nota', 'sj.cek_nota','sj.is_cek_sj','sj.user_add',
                                        'pl.nama','pl.alamat','pl.no_telp','pl.email',
                                            'k.nama as kendaraan','k.no_plat',
                                                'kar.nama as sopir')
                            ->first();
        $detail_barang = DB::table('suratjalan_detail as a')
                        ->where('a.status', NULL)
                        ->where('a.id_sj', $id)
                        ->leftJoin('satuan as b', 'a.satuan', '=', 'b.id')
                        ->select('a.id','a.nama_brg','a.ketr','a.satuan as id_satuan','a.qty','a.harga','a.potongan','b.nama as satuan')
                        ->get();                        
        
        $jumlah = DB::table('suratjalan_detail')->where('id_sj',$id)->sum('qty');
        
        $data['detail_barang'] = $detail_barang;
                
        //inisialisasi data SJ
        $data['id'] = $suratjalan->id;
        $data['tgl'] = date('d M Y', strtotime($suratjalan->tgl));
        $data['catatan'] = $suratjalan->catatan;
        $data['pembayaran'] = $suratjalan->pembayaran;
        $data['pengiriman'] = $suratjalan->pengiriman;
        $data['sopir'] = $suratjalan->sopir;
        $data['kendaraan'] = $suratjalan->kendaraan;
        $data['no_plat'] = $suratjalan->no_plat;
        $data['total'] = $suratjalan->total;
        $data['jumlah'] = $jumlah;

        //inisialisasi data pelanggan
        $data['nama'] = $suratjalan->nama;
        $data['alamat'] = $suratjalan->alamat;
        $data['no_telp'] = $suratjalan->no_telp;
        $data['email'] = $suratjalan->email;
        $data['mengetahui'] = $this->get_karyawan($suratjalan->user_add);
        $data['cek_nota'] = isset($suratjalan->cek_nota) ? $this->get_karyawan($suratjalan->cek_nota) : '-';
       
        return view('admin.CekSj.sj')->with($data);
    }

    public function insert_log($id_sj, $id_user)
    {
        $sj =DB::table('suratjalan')->where('id', $id_sj)->first();
        $sj_array = (array)$sj;
        $user_del = ['user_del' => $id_user];
        $merge_sj = array_merge($sj_array,$user_del);
        $sj_detail = DB::table('suratjalan_detail')->where('id_sj', $id_sj)->get();
        $data_item = [];

        // dd($sj);
        foreach ($sj_detail as $v) {
            $data_item[] = [
                        'id_sj' => $v->id_sj,
                        'nama_brg' => $v->nama_brg,
                        'satuan' => $v->satuan,
                        'harga' => $v->harga,
                        'harga_new' => $v->harga_new,
                        'ketr_ganti_harga' => $v->ketr_ganti_harga,
                        'qty' => $v->qty,
                        'potongan' => $v->potongan,
                        'subtotal' => $v->subtotal,
                        'ketr' => $v->ketr,
                        'ketr_tambahan' => $v->ketr_tambahan,
                        'created_at' => $v->created_at,
                        'user_add' => $v->user_add,
                        'updated_at' => $v->updated_at,
                        'user_upd' => $v->user_upd,
                        'status' => $v->status
                    ];
        }

        // dd($data_item);
        $inser_sj = DB::table('log_suratjalan')->insert($merge_sj);
        $inser_sj_detail = DB::table('log_suratjalan_detail')->insert($data_item);

        $res = '';
        if ($inser_sj && $inser_sj_detail) {
            $res = 'y';
        }

        return $res;
    }

    public function delete(Request $req)
    {
        $id_user = session::get('id_user');
        $id = $req->_idSj;
        $res = [];
        
        $cek = $this->insert_log($id, $id_user);

        if ($cek == 'y') {
            $delete = DB::table('suratjalan')->where('id', $id)->delete();
        
            if($delete) {
                $delete_sj = DB::table('suratjalan_detail')->where('id_sj', $id)->delete();
                $delete_jurnal = DB::table('jurnal')->where('ref', $id)->delete();

                $res = [
                    'code' => 300,
                    'msg' => 'Data telah dihapus'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Gagal dihapus'
                ];
            }
           
        } 
        $data['response'] = $res;
        return response()->json($data);
    }

    public function get_ket($id_sj)
    {
        $detail_sj = DB::table('suratjalan_detail')->where('id_sj', $id_sj)->get();

        $dt = [];
        foreach ($detail_sj as $value) {
            $dt[] = $value->nama_brg;
        }

        $hasil = implode(', ', $dt);
        return $hasil;
    }

    public function set_akun_batal($dt_sj, $dt)
    {
        $set_total_hpp = 0;
        $set_total_harga = 0;
        $set_qty = 0;

        foreach ($dt as $v) {
            $set_total_hpp += $v['hpp'] * $v['qty'];
            $set_total_harga += $v['harga'] * $v['qty'];
            $set_qty += $v['qty'];
        }

        $akun[0]['tgl'] = $dt_sj['tgl'];
        $akun[0]['id_item'] = NULL;
        $akun[0]['no_akun'] = '140';
        $akun[0]['jenis_jurnal'] = 'sj-batal';
        $akun[0]['ref'] = $dt_sj['id_sj'];
        $akun[0]['nama'] = $dt_sj['member'];
        $akun[0]['keterangan'] = $this->get_ket($dt_sj['id_sj']);
        $akun[0]['map'] = 'd';
        $akun[0]['hit'] = NULL;
        $akun[0]['grup'] = 1;
        $akun[0]['qty'] = $set_qty;
        $akun[0]['m3'] = NULL;
        $akun[0]['harga'] = $set_total_hpp;
        $akun[0]['total'] = $set_total_hpp;

        $akun[1]['tgl'] = $dt_sj['tgl'];
        $akun[1]['id_item'] = NULL;
        $akun[1]['no_akun'] = '510';
        $akun[1]['jenis_jurnal'] = 'sj-batal';
        $akun[1]['ref'] = $dt_sj['id_sj'];
        $akun[1]['nama'] = $dt_sj['member'];
        $akun[1]['keterangan'] = NUll;
        $akun[1]['map'] = 'k';
        $akun[1]['hit'] = NULL;
        $akun[1]['grup'] = 1;
        $akun[1]['qty'] = $set_qty;
        $akun[1]['m3'] = NULL;
        $akun[1]['harga'] = $set_total_hpp;
        $akun[1]['total'] = $set_total_hpp;

        $akun[2]['tgl'] = $dt_sj['tgl'];
        $akun[2]['id_item'] = NULL;
        $akun[2]['no_akun'] = '410';
        $akun[2]['jenis_jurnal'] = 'sj-batal';
        $akun[2]['ref'] = $dt_sj['id_sj'];
        $akun[2]['nama'] = $dt_sj['member'];
        $akun[2]['keterangan'] = $this->get_ket($dt_sj['id_sj']);
        $akun[2]['map'] = 'd';
        $akun[2]['hit'] = NULL;
        $akun[2]['grup'] = 1;
        $akun[2]['qty'] = $set_qty;
        $akun[2]['m3'] = NULL;
        $akun[2]['harga'] = $set_total_harga;
        $akun[2]['total'] = $set_total_harga;

        $akun[3]['tgl'] = $dt_sj['tgl'];
        $akun[3]['id_item'] = NULL;
        $akun[3]['no_akun'] = '130';
        $akun[3]['jenis_jurnal'] = 'sj-batal';
        $akun[3]['ref'] = $dt_sj['id_sj'];
        $akun[3]['nama'] = $dt_sj['member'];
        $akun[3]['keterangan'] = NUll;
        $akun[3]['map'] = 'k';
        $akun[3]['hit'] = NULL;
        $akun[3]['grup'] = 1;
        $akun[3]['qty'] = $set_qty;
        $akun[3]['m3'] = NULL;
        $akun[3]['harga'] = $set_total_harga;
        $akun[3]['total'] = $set_total_harga;

        $insert = DB::table('jurnal')->insert($akun);
    }

    public function batal_suratjalan(Request $req)
    {
        $id_users = session::get('id_user');
        $id = $req->_id;
        $ket = $req->_ket;
        
        $data_btl_sj = [
            'is_batal' => $id_users,
            'batal' => $ket,
            'user_upd' => $id_users,
            'updated_at' => date("Y-m-d H:i:s")
        ];

        $cek_batal = DB::table('suratjalan')->where('id', $id)->whereNotNull('is_batal')->first();


        if (isset($cek_batal)) {
            $res = [
                'code' => 400,
                'msg' => 'Data sudah pernah dibatalkan'
            ];
        } elseif (!isset($ket)){
            $res = [
                'code' => 400,
                'msg' => 'Isi Keterangan Pembatalan'
            ];
        } else {
            //awal jurnal balik SJ
            
            $sj = DB::table('suratjalan as a')
                        ->leftJoin('pelanggan as b', 'a.member', '=', 'b.id')
                        ->select('a.id as id_sj', 'a.tgl', 'b.nama')
                        ->where('a.id', $id)
                        ->first();
            
            $sj_detail = DB::table('suratjalan_detail as a')
                        ->leftJoin('barang as b', 'a.id_brg', '=', 'b.kode')
                        ->select('a.id as id_detail', 'a.id_sj', 'a.id_brg', 'a.nama_brg', 'a.harga', 'a.qty', 'b.hpp')
                        ->where('a.id_sj', $id)
                        ->get();

            $dt_sj = [
                'id_sj'        => $sj->id_sj,
                'tgl'       => $sj->tgl,
                'member'    => $sj->nama    
            ];

            $dt = [];
            foreach ($sj_detail as $key => $v) {
                $dt[] = [
                    'id_detail' => $v->id_detail,
                    'id_sj'     => $v->id_sj,
                    'id_brg'    => $v->id_brg,
                    'nama_brg'  => $v->nama_brg,
                    'harga'     => $v->harga,
                    'hpp'       => $v->hpp,
                    'qty'       => $v->qty
                ];
            }
            
            //akhir jurnal balik SJ

            $update = DB::table('suratjalan')->where('id', $id)->update($data_btl_sj);

            if ($update) {
            $this->set_akun_batal($dt_sj, $dt);

                $res = [
                    'code' => 200,
                    'msg' => 'Data berhasil dibatalkan'
                ];
            } else {
                $res = [
                    'code' => 400,
                    'msg' => 'Data gagal dibatalkan'
                ];
            }
        }
        return response()->json($res);
    }

    public function print_sj_count(Request $req)
    {
        $id_sj = $req->_idSj;
        $jenis = $req->_jenis;

        $print = [];

        if ( $jenis == 'print_sj') {
           $print = [
               'print_sj' => 1
           ];
        } else {
            $print = [
                'print_nota' => 1
            ];
        }
        $update_print = DB::table('suratjalan')->where('id', $id_sj)->update($print);
        
        $res = [];

        if ($update_print) {
            $res = [
                'kode' => 200,
                'msg' => 'Berhasil Update Data'
            ];
        } else {
            $res = [
                'kode' => 400,
                'msg' => 'Gagal Update Data'
            ];            
        }

        return response()->json($res);
    }

}
