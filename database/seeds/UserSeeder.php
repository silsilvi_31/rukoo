<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users')->get()->count() == 0) {
            DB::table('users')->insert([
                [
                    'name' => 'admin',
                    'email' => 'admin@gmail.com',
                    'password' => bcrypt('admin'),
                    'created_at'=>\Carbon\Carbon::now('Asia/Jakarta')
                ],[
                    'name' => 'admin1',
                    'email' => 'admin1@gmail.com',
                    'password' => bcrypt('admin1'),
                    'created_at'=>\Carbon\Carbon::now('Asia/Jakarta')

                ]
            ]);
        }
    }
}
