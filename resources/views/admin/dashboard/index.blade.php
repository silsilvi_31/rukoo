@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class=" top_tiles" style="margin: 10px 0;">
          <div class="col-md-3 col-sm-3  tile">
            <span>Total Pelanggan</span>
            <h2>{{ $total_pelanggan }}</h2>
            <span class="sparkline_one" style="height: 160px;">
                  <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
              </span>
          </div>
          <div class="col-md-3 col-sm-3  tile">
            <span>Total Penjualan</span>
            <h2>Rp. {{ $total_penjualan }}</h2>
            <span class="sparkline_one" style="height: 160px;">
                  <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
              </span>
          </div>
          <div class="col-md-3 col-sm-3  tile">
            <span>Total Pembelian</span>
            <h2>Rp. {{ $total_pembelian}}</h2>
            <span class="sparkline_one" style="height: 160px;">
                  <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 125px;"></canvas>
              </span> 
          </div>
          {{-- <div class="col-md-3 col-sm-3  tile">
            <span>Total Sessions</span>
            <h2>231,809</h2>
            <span class="sparkline_one" style="height: 160px;">
                  <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
              </span>
          </div> --}}
        </div>
        <br/>

        <div class="x_panel">
            <div class="x_title">
              <h2>Penjualan <small>Sessions</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <input type="text" id="Tgl" name="tgl" class="form-control" style="width:100px">
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <canvas id="cbx"></canvas>
            </div>
          </div>
    </div>
  </div>    
  @push('js')
  <script type="text/javascript">
  $('#Tgl').datetimepicker({
        format: 'YYYY'
    });
    grafik();

    $("#Tgl").on("dp.change", function (e) {
        grafik();
    });

    function grafik() {
      var tgl = $('#Tgl').val();
      $.ajax({
              type : 'POST',
              url : '{{ route('dashboard.grafik') }}',
              data :  {
                    '_tgl' : tgl
              },
              success: function(data) {
                  console.log(data);
                  var label = [];
                  var value = [];
                  for (var i in data) {
                      label.push(data[i].bulan);
                      value.push(data[i].total);
                  }
                  var ctx = document.getElementById("cbx");
                  var mybarChart = new Chart(ctx, {
                      type: 'bar',
                      data: {
                          labels: label,
                          datasets: [{
                              label: '# of Votes',
                              backgroundColor: "#26B99A",
                              data: value
                          }]
                      },

                      options: {
                          scales: {
                              yAxes: [{
                                  ticks: {
                                      beginAtZero: true
                                  }
                              }]
                          }
                      }
                  });
              }
      });
    }

  </script>
@endpush
@endsection