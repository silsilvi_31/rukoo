@extends('master.ella')
    
@section('content')

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Neraca</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <label class="col-form-label col-md-4 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2">
                  <input type="text" id="Tgl" name="tgl" class="form-control" autocomplete="off">
                </div>
                <div class="col-md-2">
                  <input type="text" id="tglDua" name="tgl_dua" class="form-control" autocomplete="off">
                </div>
                <button type="button" class="btn btn-info" onclick="cari_neraca()">Cari</button>
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                  <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle"
                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Export
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                      <button class="dropdown-item" onclick="excel_neraca()">Excel</button>
                      <a class="dropdown-item" href="#">Pdf</a>
                      <a class="dropdown-item" href="#">Csv</a>
                      <a class="dropdown-item" href="#">Print</a>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="card-box table-responsive">
                  <br>
                  Aktiva
                  <table id="tabel_aktiva" class="table table-sm table-striped jambo_table bulk_action" style="width:100%">
                    <thead>
                      <tr>
                        <th class="no">No Akun</th>
                        <th class="akun">Keterangan</th>
                        {{-- <th class="angka text-right">Debit</th>
                        <th class="angka text-right">Kredit</th> --}}
                        <th class="angka text-right">Total</th>
                      </tr>
                    </thead>
                    <tbody id="body_aktiva">
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2" ><strong>Total</strong></td> 
                        {{-- <td class="text-right" id="total_debit_aktiva"></td>
                        <td class="text-right" id="total_kredit_aktiva"></td>    --}}
                        <td class="text-right" id="total_aktiva" style="background:#8395a7;"></td>  
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
                
              <div class="col-md-6">
                <div class="card-box table-responsive">
                  <br>
                  Pasiva
                  <table id="tb_pasiva" class="table table-sm table-striped jambo_table bulk_action" style="width:100%">
                    <thead>
                      <tr>
                        <th class="no">No Akun</th>
                        <th class="akun">Keterangan</th>
                        {{-- <th class="angka text-right">Debit</th>
                        <th class="angka text-right">Kredit</th> --}}
                        <th class="angka text-right">Total</th>
                      </tr>
                    </thead>
                    <tbody id="body_pasiva">
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2" ><strong>Total</strong></td> 
                        {{-- <td class="text-right" id="total_debit_pasiva"></td>
                        <td class="text-right" id="total_kredit_pasiva"></td>    --}}
                        <td class="text-right" id="total_pasiva" style="background:#8395a7;" ></td>  
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
        
            </div>
  


            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
  
  var tt_pendapatan;
    var tt_hpp;
    var tt_beban;
    var hasil;

    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('#tglDua').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    // $("#Tgl").on("dp.change", function (e) {
    //     var tgl = $(this).val();
    //     var tgl_dua = $('#tglDua').val();
    //     tampil_aktiva(tgl, tgl_dua);
    //     tampil_pasiva(tgl, tgl_dua);
    //     console.log(tgl+' '+tgl_dua);
    //     cek_tgl('tgl_awal');
    // });

    // $("#tglDua").on("dp.change", function (e) {
    //     var tgl = $('#Tgl').val();
    //     var tgl_dua = $(this).val();
    //     tampil_aktiva(tgl, tgl_dua);
    //     tampil_pasiva(tgl, tgl_dua);
    //     console.log(tgl+' '+tgl_dua);
    //     cek_tgl('tgl_dua');
    // });

    function cari_neraca() {
      var tgl = $('#Tgl').val();
      var tgl_dua = $('#tglDua').val();
      // cek_tgl('tgl_awal');
      // cek_tgl('tgl_dua');

      // if (!tgl || !$tgl_dua) {

      // } else {
        tampil_aktiva(tgl, tgl_dua);
        tampil_pasiva(tgl, tgl_dua);
      // }

      
    }

    tampil_aktiva();
    tampil_pasiva();

    function tampil_aktiva(tgl, tglDua) {
      $.ajax({
        type : 'POST',
        url : '{{ route('neraca.datatable_aktiva') }}',
        data: { 
                '_tgl' : tgl,
                '_tglDua' : tglDua,
               },
        success: function (e) {
          $('#body_aktiva').empty();    

          $.each(e.data, function (key, value) { 
                tr = $('<tr/>');    
                if (value.parent_id != null) {
                  tr.append("<td>&nbsp&nbsp&nbsp" + value.no_akun + "</td>"); 
                  tr.append("<td>" + value.akun + "</td>");    
                  // tr.append("<td class='text-right'>" + value.total_debit + "</td>");    
                  // tr.append("<td class='text-right'>" + value.total_kredit + "</td>");    
                  tr.append("<td class='text-right'>" + value.total + "</td>");    
                } else {
                  tr.append("<td><strong>" + value.no_akun + "</strong></td>");    
                  tr.append("<td><strong>" + value.akun + "</strong></td>");    
                  // tr.append("<td class='text-right'><strong>" + value.total_debit + "</strong></td>");    
                  // tr.append("<td class='text-right'><strong>" + value.total_kredit + "</strong></td>");    
                  tr.append("<td class='text-right'><strong>" + value.total + "</strong></td>"); 
                }
                
                $('#body_aktiva').append(tr);    
          });    
          $("#total_debit_aktiva").text(e.tt_debit); 
          $("#total_kredit_aktiva").text(e.tt_kredit); 
          $("#total_aktiva").text(e.total);  
        }
      });
    }

    function tampil_pasiva(tgl, tgl_dua) {
      $.ajax({
        type : 'POST',
        url : '{{ route('neraca.datatable_pasiva') }}',
        data: { 
                '_tgl' : tgl,
                '_tglDua' : tgl_dua,
               },
        success: function (e) {
          $('#body_pasiva').empty();
          $.each(e.data, function (key, value) {    
            tr = $('<tr/>');    
                if (value.parent_id != null) {
                  tr.append("<td>&nbsp&nbsp&nbsp" + value.no_akun + "</td>"); 
                  tr.append("<td>" + value.akun + "</td>");    
                  // tr.append("<td class='text-right'>" + value.total_debit + "</td>");    
                  // tr.append("<td class='text-right'>" + value.total_kredit + "</td>");    
                  tr.append("<td class='text-right'>" + value.total + "</td>");    
                } else {
                  tr.append("<td><strong>" + value.no_akun + "</strong></td>");    
                  tr.append("<td><strong>" + value.akun + "</strong></td>");    
                  // tr.append("<td class='text-right'><strong>" + value.total_debit + "</strong></td>");    
                  // tr.append("<td class='text-right'><strong>" + value.total_kredit + "</strong></td>");    
                  tr.append("<td class='text-right'><strong>" + value.total + "</strong></td>"); 
                }   
                
                $('#body_pasiva').append(tr);    
            });    
            // $("#total_debit_pasiva").text(e.tt_debit); 
            // $("#total_kredit_pasiva").text(e.tt_kredit); 
            $("#total_pasiva").text(e.total);  
        }
      });
    }

    function numberformat(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }  

    function excel_neraca() {
      var tgl_m = $('#Tgl').val();
      var tgl_a = $('#tglDua').val();

      var tgl_gabung = tgl_m+'&'+tgl_a;

      var url = '{{ route('neraca.excel_neraca', ['.p']) }}';
      var url_fix = url.replaceAll('.p',tgl_gabung);
      // console.log(url_fix);
      window.location.href = url_fix;
    }

    function cek_tgl(jenis) {
      var tgl = $('#Tgl').val();

      $.ajax({
        type    : 'POST',
        url     : '{{ route('neraca.cek_tgl') }}',
        data    : {
                    '_tgl' : tgl,
                  },
        success: function (e) {
          if (jenis == 'tgl_awal') {
            $('#Tgl').val(e.tgl);
          } else {
            $('#tglDua').val(e.tgl);
          }

          if (!e.tgl) {
            notif(400, 'Tgl Tidak Sesuai')
          }
        }
      });
    }

  </script>
@endpush