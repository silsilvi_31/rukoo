@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    {{-- Modal Cek Nota --}}
    <div id="modal_ceksj" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cek SJ</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                </label>
                <div class="col-md-7 ">
                  <input type="hidden" id="formSj" name="" class="form-control" readonly>
                  <input type="text" id="" name="id" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                </label>
                <div class="col-md-7">
                  <input type="text" id="tgl" name="tgl" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kepada
                </label>
                <div class="col-md-7">
                  <input type="text" id="Pelanggan" name="pelanggan" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Catatan Khusus
                </label>
                <div class="col-md-7">
                  <input type="text" id="" name="catatan" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Status
                </label>
                <div class="col-md-7">
                  <input type="text" id="" name="status" class="form-control" readonly>
                </div>
              </div>
            </div>

            </form>
            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            <table id="tb_ceksj_detail" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <!-- <th>Ketr</th> -->
                      <th>Satuan</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Harga Baru</th>
                      <th>pot</th>
                      <th>Ketr Tambahan</th>
                      {{-- <th>Ketr Ganti Harga</th> --}}
                      <th>Subtotal</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          <div class="clearfix"></div>
              <div class="ln_solid"></div>
              <div class="col-md-6 col-sm-6"></div>
              <div class="col-md-6 col-sm-6">
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="_total" name="total" class="form-control" readonly>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ongkir
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="_ongkir" name="ongkir" class="form-control" readonly>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Grand Total
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="_grandtotal" name="grandtotal" class="form-control" readonly>
                  </div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btn-setuju" onclick="acc_sj()" >Setuju</button>
            <button type="button" class="btn btn-danger " id="btn-batal" onclick="batal_sj()" >Batal</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    {{-- Modal Cek Nota --}}
    <div id="modal_btl" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cek SJ</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-8 col-sm-8">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                </label>
                <div class="col-md-7 ">
                  <input type="text" id="" name="id_md" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Keterangan
                </label>
                <div class="col-md-7">
                  <input type="text" id="" name="ket_md" class="form-control">
                </div>
              </div>
            </div>
            </form>
          </div>
          <div class="modal-footer">
            <div class="offset-md-3">
              <button type="button" class="btn btn-success" id="btn-setuju" onclick="batal_suratjalan()" >Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Cek SJ</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tb_ceksj" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Tanggal</th>
                        <th>Kepada</th>
                        {{-- <th>Pembayaran</th>
                        <th>Pengiriman</th> --}}
                        <th>Tagihan</th>
                        <th style="width:150px;">Pembayaran</th>
                        {{-- <th>Status Bayar</th> --}}
                        <th style="width:150px;">Cek Nota</th>
                        {{-- <th>Cek Batal</th> --}}
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    var datatableCekSj = $('#tb_ceksj').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: {
            type: 'GET',
            url: '{{ route('ceksj.datatable') }}', 
        },
        columns: [
          
          { data: 'id', name: 'id' },
          { data: 'tgl', name: 'tgl' },
          { data: 'pelanggan', name: 'pelanggan' },
          // { data: 'pembayaran', name: 'pembayaran' },
          // { data: 'pengiriman', name: 'pengiriman' },
          { data: 'total', name: 'total' },
          { data: 'pembayaran', name: 'pembayaran'},
          // { data: 'status_bayar', name: 'status_bayar' },
          { data: 'is_cek_nota', name: 'is_cek_nota' },
          // { data: 'batal', name: 'batal' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_suratjalan(id) {
      $.ajax({
        type    : 'GET',
        url     : '{{ route('ceksj.delete') }}',
        data    : { '_idSj' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableCekSj.draw(); 
          
        }
      });
    }

    var datatable_ceksj_detail = $('#tb_ceksj_detail').DataTable({
      processing : true,
      serverSide : true,
      scrollY: true,
      ajax : {
        type : 'POST',
        url : '{{ route('ceksj.datatable_sj') }}',
        data : function (e) {
          e._id = $('input[name=id]').val();
        }
      },
      columns : [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'nama_brg', name:'nama_brg' },
            // { data: 'ketr', name:'ketr' },
            { data: 'satuan', name:'satuan' },
            { data: 'qty', name:'qty' },
            { data: 'harga', name:'harga' },
            { data: 'harga_new', name:'harga_new' },
            { data: 'potongan', name:'potongan' },
            { data: 'ketr_tambahan', name:'ketr_tambahan' },
            // { data: 'ketr_ganti_harga', name:'ketr_ganti_harga' },
            { data: 'subtotal', name:'subtotal' },
      ]
    }); 

    $('#modal_btl').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var id = $button.data('id');
      var ket = $button.data('ket');
      $('input[name=id_md]').val(id);
      $('input[name=ket_md]').val(ket);
    });

   $('#modal_ceksj').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var form = $button.data('form');
     var id = $button.data('id');
     var tgl = $button.data('tgl');
     var pelanggan = $button.data('pelanggan');
     var catatan = $button.data('catatan');
     var pembayaran = $button.data('pmb');
     var pengiriman =  $button.data('png');
     var id_kendaraan = $button.data('id_kend');
     var id_karyawan = $button.data('id_kar');
     var total = $button.data('total');
     var ongkir = $button.data('ongkir');
     var grandtotal = $button.data('grandtotal');
     var bank = $button.data('bank');
     var cek_nota = $button.data('cek');
     
     console.log(form);
     if(form == 'cek_sj') {
      $('input[name=id]').val(id);
      $('input[name=tgl]').val(tgl);
      $('input[name=pelanggan]').val(pelanggan);
      $('input[name=catatan]').val(catatan);
      $('input[name=pembayaran]').val(pembayaran);
      $('input[name=pengiriman]').val(pengiriman);
      $('input[name=id_kendaraan]').val(id_kendaraan);
      $('input[name=id_karyawan]').val(id_karyawan);
      $('input[name=total]').val(total);
      $('input[name=ongkir]').val(ongkir);
      $('input[name=grandtotal]').val(grandtotal);  
      $('input[name=bank]').val(bank);    
      $('input[name=status]').val(cek_nota);     

      if(cek_nota == 'acc') {
        $('#btn-setuju').attr('hidden', true);
        $('#btn-batal').attr('hidden', true);
      } else {
        $('#btn-setuju').removeAttr('hidden');
        $('#btn-batal').removeAttr('hidden',);
      }

      datatable_ceksj_detail.draw();

     } else if (form == 'cek_nota') {
      // $('input[name=id]').val(id);
      // $('input[name=tgl]').val(tgl);
     }
     
   });

   function acc_sj() {
    var id = $('input[name=id]').val();
    var status = $('input[name=status]').val();
    var btn = 'setuju';
    console.log(btn);
    
    $.ajax({
      type : 'POST',
      url : '{{ route('ceksj.acc_sj') }}',
      data : {
                '_id' : id,
                '_btn' : btn,
                '_status' : status
              },
      success : function (e) {
            datatableCekSj.draw();
            $('#modal_ceksj').modal('hide');
            notif(e.code, e.msg);
      }
    });
   }

   function batal_sj() {
    var id = $('input[name=id]').val();
    var batal = 'batal';
    console.log(batal);
    
    $.ajax({
      type : 'POST',
      url : '{{ route('ceksj.acc_sj') }}',
      data : {
                '_id' : id,
                '_btn' : batal
              },
      success : function (e) {
        datatableCekSj.draw();
            $('#modal_ceksj').modal('hide');
            notif(e.code, e.msg); 
      }
    });
   }

   function batal_suratjalan(id) {
     var id = $('input[name=id_md]').val();
     var ket = $('input[name=ket_md]').val();
     $.ajax({
       type : 'POST',
       url: '{{ route('ceksj.batal_suratjalan') }}',
       data : {
                '_id' :  id,
                '_ket' : ket  
      },
      success: function (e) {
        $('#modal_btl').modal('hide');
        datatableCekSj.draw();
        notif(e.code, e.msg);
      }
     });
   }

   $(document).on('click', '.print_nota', function() {
      var id_sj = $(this).attr('id');
      var jenis = 'print_nota';
      $.ajax({
        type    : 'POST',
        url     : '{{ route('ceksj.print_sj_count') }}',
        data    : { '_idSj' : id_sj,
                    '_jenis' : jenis },
        success : function (e) {
          notif(e.code, e.msg);
          datatableCekSj.ajax.reload(); 
            window.location.href = 'cek-sj/nota/'+id_sj;
        }
      });
    });
  </script>
@endpush