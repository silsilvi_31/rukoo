@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    {{-- Modal Opname --}}
    <div id="modal_opname" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Opname Stok</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Kode Barang <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="Kode" name="kode" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Nama Barang <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="namaBrg" name="nama_brg" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Sisa
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="Sisa" name="sisa" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Tanggal
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="Tgl" name="tgl" class="form-control">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Stok Penyesuaian
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="stokPenyesuaian" name="stok_penyesuaian" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Selisih
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="Selisih" name="selisih" class="form-control" autocomplete="off">
                  {{-- <div id="sp"></div> --}}
                  <span id="sp" class=""></span>
                </div>
              </div>
            </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="simpan_opname()">Simpan</button>
          </div>

        </div>
      </div>
    </div>

        {{-- Modal Detail Opname --}}
        <div id="modal_detail_opname" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
    
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Detail Opname Stok</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Barang <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="detKodeBrg" name="det_kode_brg" class="form-control" readonly>
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="detNamaBrg" name="det_nama_brg" class="form-control" readonly>
                    </div>
                  </div>
                </div>
                </form>
                <div class="card-box table-responsive">
                  <table id="tbDetOpname" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Tgl</th>
                        <th>Stok Sblm</th>
                        <th>Stok Sesudah</th>
                        <th>Selisih</th>
                        <th>Ketr</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
    
            </div>
          </div>
        </div>
        {{-- Akhir modal Detail Opname --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Stok Opname</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
                <div class="card-box table-responsive">
                  <table id="tbStokOpname" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No. Nota</th>
                        <th>Nama Barang</th>
                        {{-- <th>Stok Awal</th> --}}
                        {{-- <th>Stok Masuk</th>
                        <th>Stok Keluar</th> --}}
                        <th>Stok</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
  $('#Tgl').datetimepicker({
    format: 'DD-MM-YYYY'
  });

  var datatableSj = $('#tbStokOpname').DataTable({
      processing: true,
      serverSide: true,
      order: false,
      ajax: {
          type: 'GET',
          url: '{{ route('stokOpname.datatable') }}', 
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'nama_brg', name: 'nama_brg' },
        // { data: 'stok_awal', name: 'stok_awal' },
        // { data: 'stok_masuk', name: 'stok_masuk' },
        // { data: 'stok_keluar', name: 'stok_keluar' },
        { data: 'sisa', name: 'sisa' },
        { data: 'opsi', name: 'opsi' }
      ]
  });

  function simpan_opname() {
    var kode = $('#Kode').val();
    var tgl = $('#Tgl').val();
    var stok_sblm = $('#Sisa').val();
    var stok_sesudah = $('#stokPenyesuaian').val();
    var selisih = $('#Selisih').val();
    var ketr = $('#sp').text();

    $.ajax({
      type : 'POST',
      url   : '{{route('stokOpname.save')}}',
      data : {
              '_kode' : kode,
              '_tgl' : tgl,
              '_stokSblm' : stok_sblm,
              '_stokSesudah' : stok_sesudah,
              '_selisih' : selisih,
              '_ketr' : ketr
            },
      success : function (e){
        notif(e.code, e.msg);
        $('#modal_opname').modal('hide');
        datatableSj.draw();
        $('#stokPenyesuaian').val('');
        $('#Selisih').val('');
        $('#sp').text('');
      }
    }); 
 }

    function delete_suratjalan(id) {
      $.ajax({
        type    : 'GET',
        url     : '{{ route('sjdefault.delete') }}',
        data    : { '_idSj' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableSj.draw(); 
        }
      });
    }

    $('#modal_opname').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var kode = $button.data('kode');
     var nama_brg = $button.data('namabrg');
     var sisa = $button.data('sisa');
     var tgl = $button.data('tgl');
     
     console.log(tgl);
     $('input[name=kode]').val(kode);
      $('input[name=sisa]').val(sisa);
      $('input[name=tgl]').val(tgl);
      $('input[name=nama_brg]').val(nama_brg);

   });

   var tb_detial_opname = $('#tbDetOpname').DataTable({
      processing : true,
      serverSide : true,
      scrollY: true,
      ajax : {
        type : 'POST',
        url : '{{ route('stokOpname.datatable_detail') }}',
        data : function (e) {
          e._detKodeBrg = $('#detKodeBrg').val();
        }
      },
      columns : [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'tgl', name: 'tgl' },
            { data: 'stok_sblm', name: 'stok_sblm' },
            { data: 'stok_sesudah', name: 'stok_sesudah' },
            { data: 'selisih', name: 'selisih' },            
            { data: 'ketr', name: 'ketr' },
            { data: 'opsi', name: 'opsi' },
      ]
    }); 

   $('#modal_detail_opname').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var kode = $button.data('kode');
     var nama_brg = $button.data('namabrg');
     
     $('#detKodeBrg').val(kode);
     $('#detNamaBrg').val(nama_brg);
     tb_detial_opname.draw();
   });

    function export_excel() {
      var tgl_m = $('#tglExport_m').val();
      var tgl_a = $('#tglExport_a').val();
      window.location.href = '/dashboard/sjdefault/excel_sj/'+tgl_m+'&'+tgl_a;
    }

    function delete_tgl() {
      var tgl_m = $('#tglExport_m').val();
      var tgl_a = $('#tglExport_a').val();
      $.ajax({

        
        type    : 'POST',
        url     : '{{ route('sjdefault.delete_tgl') }}',
        data    : { '_tglM' : tgl_m,
                    '_tglA' : tgl_a,
                  },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableSj.draw(); 
        }
      })
    }

    $(document).on('keyup', '#stokPenyesuaian', function(){ 
      var sisa = $('#Sisa').val();
      var stok_penyesuaian = $(this).val();

      console.log(sisa+' '+stok_penyesuaian);
      var hasil = 0;
      var ketr = '';
      var sisaa = parseInt(sisa);
      var stok_penyesuaiann = parseInt(stok_penyesuaian);
      var sp = '';

      if (sisaa > stok_penyesuaiann) {
        hasil = sisaa - stok_penyesuaiann;
        ketr = 'kehilangan';
        $('#sp').addClass('badge badge-warning');
        $('#sp').text(ketr);
      } else {
        hasil = stok_penyesuaiann - sisaa;
        ketr = 'kelebihan';
        $('#sp').addClass('badge badge-info');
        $('#sp').text(ketr);
      }

      if (isNaN(hasil)) {
        hasil = '';
      }

      $('#sp').append(sp);
      console.log(sp);

      $('#Selisih').val(hasil);
    });

    function batal_opname(id, id_brg) {
      $.ajax({
       type : 'POST',
       url: '{{ route('stokOpname.batal_opname') }}',
       data : {
                '_id' :  id,
                '_idBrg' : id_brg
      },
      success: function (e) {
        $('#modal_detail_opname').modal('hide');
        datatableSj.draw();
        tb_detial_opname.draw();
        notif(e.code, e.msg);
      }
     });
    }

    
  </script>
@endpush