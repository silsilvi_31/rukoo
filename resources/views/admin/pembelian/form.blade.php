@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- Awal Modal Akun -->
    <div class="modal fade bs-example-modal-lg" id="modal_akun" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Akun</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tb_akun" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>No. Akun</th>
                              <th>Nama</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Akun -->

    <!-- Awal Modal Barang -->
    <div class="modal fade bs-example-modal-lg" id="modal_brg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tabel_brg" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Nama</th>
                              <th>Hpp</th>
                              <th>Harga Jual</th>
                              <th>Satuan</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Awal Modal barang -->

    {{-- modal tambah barang --}}
    <div class="modal fade bs-example-modal-lg" id="modal_add_barang" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="namaBrgMd" name="nama_brg_md" class="form-control">
                    </div>
                  </div>
                  {{-- <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> p x l x t
                    </label>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="panjangmd" name="panjang_md" class="form-control">
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="lebarmd" name="lebar_md" class="form-control">
                    </div>
                    <div class="col-md-2 col-sm-6 ">
                      <input type="text" id="tebalmd" name="tebal_md" class="form-control">
                    </div>
                  </div> --}}
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Hpp <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="hppMd" name="hpp_md" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga Jual <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="hargaMd" name="harga_md" class="form-control">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="idSatuanMd"  name="id_satuan_md" class="form-control">
                        <option value="">-</option>
                        @foreach ($satuan as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Jenis Barang
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="jenisBrgMd" name="jenis_brg_md" class="form-control">
                        <option value="">-</option>
                        @foreach ($jenis_barang as $item)
                          <option value="{{ $item->id }}">{{ $item->jenis_brg }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun
                    </label>
                    <div class="col-md-6 ">
                      <div class="col-md-6 col-sm-6 ">
                        <select id="noAkunMd" name="no_akun_md" class="form-control">
                          <option value="">-</option>
                          @foreach ($akun as $item)
                            <option value="{{ $item->no_akun }}">{{ $item->no_akun }} - {{ $item->akun }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                  <div class="ln_solid"></div>
              
                <div class="form-group">
                  <div class="offset-md-3">
                    <button type="button" class="btn btn-sm btn-primary">Cancel</button>
                    <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                    <button type="button" class="btn btn-sm btn-success" onclick="add_barang()"> Submit</button>
                  </div>
                </div>
                </div>
          </div>
      </div>
    </div>
    {{-- //end modal tambah barang --}}

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Pembelian</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('pembelian.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('pembelian.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">ID Beli <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 ">
                      <input type="text" id="Form" name="form" required="required" class="form-control" readonly value="{{ !empty($form) ? $form : '' }}">
                      <input type="text" id="idBeli" name="id_beli" required="required" class="form-control" readonly value="{{ !empty($id_beli) ? $id_beli : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : '' }}">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Dari <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <select class="form-control" name="id_pelanggan" id="idPelanggan" autocomplete="off" required='required'>
                        <option value="" id="pelanggan_kosong" >-</option>
                        @php
                            $id_plg = !empty($id_suplier) ? $id_suplier : '';
                            $pmb = !empty($pembayaran) ? $pembayaran : '';
                            $png = !empty($pengiriman) ? $pengiriman : '';
                            $knd = !empty($id_kendaraan) ? $id_kendaraan : ''; 
                            $kry = !empty($id_karyawan) ? $id_karyawan : '';
                            $no_rek = !empty($no_rek) ? $no_rek : '';
                            $opsi =  !empty($opsi) ? $opsi : '';
                        @endphp
                        @foreach ($suplier as $item)
                          <option value="{{ $item->id }}" @if($item->id == $id_plg) selected @endif>{{ $item->nama }}</option>                              
                        @endforeach
                      </select>
                    </div>
                    
                    {{-- <div class="col-md-1">
                      <button type="button" class="btn btn-info pelangganQ"><i class="fa fa-plus"></i></button>
                    </div> --}}
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Opsi <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <select class="form-control" name="opsi" id="Opsi" autocomplete="off" required='required'>
                        <option value="" id="opsiKosong" >-</option>  
                        <option value="1">Triplek</option>                              
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="noNotaSpl" name="no_nota_spl" required="required" class="form-control" value="{{ !empty($no_nota_spl) ? $no_nota_spl : ''}}" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No Akun Kredit <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="text" id="noAkunKredit" name="no_akun_kredit" required='required' class="form-control noAkunQ" value="{{ !empty($no_akun) ? $no_akun : '' }}" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="namaAkunKredit" name="nama_akun_kredit" class="form-control" readonly value="{{ !empty($nama_akun) ? $nama_akun : '' }}">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                {{-- kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang
                    </label>
                    <div class="col-md-5 ">
                      <input type="text" id="idItem" name="id_item" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="idBrg" name="id_brg" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="namaBrg" name="nama_brg" class="form-control brgQ" autocomplete="off">
                    </div>
                    <div class="col-md-1 ">
                      <button type="button" class="btn btn-sm btn-info barangQ"><i class="fa fa-plus"></i></button>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="noAkunDebit" name="no_akun_debit" class="form-control" autocomplete="off" readonly>
                    </div>
                    {{-- <div class="col-md-1 ">
                      <button type="button" class="btn btn-sm btn-info" onclick="new_barang()">New</button>
                    </div> --}}
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="idSatuan"  name="id_satuan" class="form-control" autocomplete="off">
                        <option value="">-</option>
                        @foreach ($satuan as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>  

                {{-- kanan --}}
                <div class="col-md-6 col-sm-6">
                  {{-- <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">m3
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="M3" name="m3" class="form-control">
                    </div>
                  </div> --}}
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Qty
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Qty" name="qty" class="form-control qtyQ" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Harga Jual
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Harga" name="harga" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Hpp
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Hpp" name="hpp" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Subtotal" name="subtotal" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Ketr" name="ketr" class="form-control" autocomplete="off">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="save_beli()">Submit</button>
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <table class="table table-striped table-bordered" id="tb_detail" style="width:100%">
                  <thead>
                      <tr>
                          <th>No.</th>
                          <th>Nama Barang</th>
                          <th>Harga</th>
                          <th>Satuan</th>
                          <th>Qty</th>
                          <th>Subtotal</th>
                          <th>Keterangan</th>
                          <th>Opsi</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>

              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    var form = $('#Form').val();
    if (form == 'edit') {

    } else {
      no_urut();
    }

    function no_urut() {
      $.ajax({
              type : 'GET',
              url : '{{ route('no_urut_beli') }}',
              success : function (e) {
                console.log(e);
                $('#idBeli').val(e);
              }
      });
    }

    $('#Opsi').change(function () {
      var id_p = $(this).val();

      if (id_p == 1) {
        $('#noAkunKredit').val(210)
        $('#noAkunKredit').attr('disabled', true)
      } else {
        $('#noAkunKredit').val('')
        $('#noAkunKredit').attr('disabled', false)
      // console.log(id_p);
      }
    });

    function save_beli() {
      var id_beli = $('#idBeli').val();
      var no_nota_spl = $('#noNotaSpl').val();
      var tgl = $('#Tgl').val();
      var nama = $('#idPelanggan').val();
      var no_akun_k = $('#noAkunKredit').val();
      var opsi = $('#Opsi').val();
      
      var id_item = $('#idItem').val();
      var id_brg = $('#idBrg').val();
      var nama_brg = $('#namaBrg').val();
      var no_akun_d = $('#noAkunDebit').val();
      var id_satuan = $('#idSatuan').val();
      var qty = $('#Qty').val();
      var hpp = $('#Hpp').val();
      var harga = $('#Harga').val();
      var subtotal = $('#Subtotal').val();
      var ketr = $('#Ketr').val();

      $.ajax({
        type  : 'POST',
        url   : '{{ route('pembelian.save') }}',
        data  : {
                  '_idBeli' : id_beli,
                  '_noNotaSpl' : no_nota_spl,
                  '_tgl' : tgl,
                  '_nama' : nama,
                  '_opsi' : opsi,
                  '_noAkunK' : no_akun_k,
                  '_idItem' : id_item,
                  '_idBrg' : id_brg,
                  '_namaBrg' : nama_brg,
                  '_noAkunD' : no_akun_d,
                  '_idSatuan' : id_satuan,
                  '_qty' : qty,
                  '_hpp' : hpp,
                  '_harga' : harga,
                  '_subtotal' : subtotal,
                  '_ketr' : ketr
                },
        success  : function (e) {
          notif(e.code,e.msg);
          clear_akun();
          $('#noAkunDebit').val('');
          tb_detail.draw();

        }
      });
    }

    function clear_akun() {
      $('#idBrg').val('');
      $('#namaBrg').val('');
      $('#idSatuan').val('');
      $('#Qty').val('');
      $('#Hpp').val('');
      $('#Harga').val('');
      $('#Subtotal').val('');
      $('#Ketr').val('');
    }

    $(document).on('click', '.noAkunQ', function(){  
      $('#modal_akun').modal('show');  
    });

    $(document).on('click', '.brgQ', function(){  
      $('#modal_brg').modal('show');  
    });

    var tb_akun = '';

    tb_akun = $('#tb_akun').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
            type: 'GET',
            url : '{{ route('pembelian.datatable_akun') }}', 
      },
      columns:[
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'no_akun', data: 'no_akun' },
        { data: 'akun', data: 'akun'},
        { data: 'opsi', data: 'opsi'}
      ]
    });

    function select_akun(no_akun, akun) {
      $('#modal_akun').modal('hide');
      $('#noAkunKredit').val(no_akun);
      $('#namaAkunKredit').val(akun);
    }

    function select_brg(kode_brg,nama_brg,harga, id_satuan, satuan, no_akun, hpp) {
      $('#modal_brg').modal('hide');
      $('#idBrg').val(kode_brg);
      $('#namaBrg').val(nama_brg);
      $('#Hpp').val(hpp)
      $('#Harga').val(harga);
      $('#idSatuan').val(id_satuan);
      $('#Satuan').val(satuan);
      $('#noAkunDebit').val(no_akun)
      clear_barang2();
    }   

    function clear_barang2() {
      $('#Qty').val('');
      $('#Ketr').val('');
      $('#Subtotal').val('');
      $('#idSatuan').val('');
    }

    var tb_barang = $('#tabel_brg').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('pembelian.datatable_brg') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'hpp', name: 'hpp' },
          { data: 'harga', name: 'harga' },
          { data: 'satuan', name: 'satuan' },
          { data: 'opsi', name: 'opsi' },
        ]
      });

      function hitung_subtotal(harga, qty) {
        var subtotal = ( harga * qty );
        $('#Subtotal').val(subtotal);
      }

      $(document).on('keyup', '.qtyQ', function(){
        var qty = $('#Qty').val();
        var harga = $('#Hpp').val();
        hitung_subtotal(harga, qty);
      });

      $(document).on('click', '.barangQ', function(){  
        $('#modal_add_barang').modal('show');  
      }); 

      var tb_detail = '';
      tb_detail = $('#tb_detail').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'POST',
            url: '{{ route('pembelian.datatable_detail') }}', 
            data : function (e) {
              e._idBeli = $('#idBeli').val();
            }
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'harga', name: 'harga' },
          { data: 'satuan', name: 'satuan' },
          { data: 'qty', name: 'qty' },
          { data: 'subtotal', name: 'subtotal' },
          { data: 'ketr', name: 'ketr' },
          { data: 'opsi', name: 'opsi' },
        ]
      });

      function delete_item(id) {
      $.ajax({
        type : 'POST',
        url : '{{ route('pembelian.delete_item') }}',
        data : { '_idItem' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_detail.draw();
          }
        });
      }

      function add_barang() {
        var nama_brgMd = $('#namaBrgMd').val();
        var hpp_brgMd = $('#hppMd').val();
        var harga_brgMd = $('#hargaMd').val();
        var id_satuanMd = $('#idSatuanMd').val();
        var jenis_brgMd = $('#jenisBrgMd').val();
        var no_akunMd = $('#noAkunMd').val();

        $.ajax({
          type  : 'POST',
          url   : '{{ route('add_brg.save') }}',
          data  : {
                    '_namaBrg' : nama_brgMd,
                    '_hpp' : hpp_brgMd,
                    '_harga' : harga_brgMd,
                    '_idSatuan' : id_satuanMd,
                    '_jenisBrg' : jenis_brgMd,
                    '_noAkun' : no_akunMd
                  },
          success  : function (e) {
            console.log(e);
            notif(e.code, e.msg);
            $('#modal_add_barang').modal('hide');
            tb_barang.draw();
            $('#idBrg').val(e.kode);
            $('#namaBrg').val(e.nama_brg);
            $('#Hpp').val(e.hpp);
            $('#Harga').val(e.harga);
            $('#idSatuan').val(e.id_satuan);
            $('#noAkunDebit').val(e.no_akun);
            $('#namaBrgMd').val('');
            $('#hppMd').val('');
            $('#hargaMd').val('');
            $('#idSatuanMd').val('');
            $('#jenisBrgMd').val('');
            $('#noAkunMd').val('');
          }
        });
      }
  </script>
@endpush