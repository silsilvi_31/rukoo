@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <!-- Awal Modal tanda Bayar -->
    <div class="modal fade bs-example-modal-lg" id="modal_bayar" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tanda Bayar</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">ID Beli <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="idBeli" name="id_beli" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total Bayar <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="totalBayar" name="total_bayar" required="required" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tanggal" name="tanggal" required="required" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  @php
                      $id_plg = !empty($id_pelanggan) ? $id_pelanggan : '';
                      $pmb = !empty($pembayaran) ? $pembayaran : '';
                      $png = !empty($pengiriman) ? $pengiriman : '';
                      $knd = !empty($id_kendaraan) ? $id_kendaraan : ''; 
                      $kry = !empty($id_karyawan) ? $id_karyawan : '';
                      $no_rek = !empty($no_rek) ? $no_rek : '';
                      $opsi =  !empty($opsi) ? $opsi : '';
                  @endphp
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pembayaran <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select class="form-control" id="jenisPembayaran" name="pembayaran" autocomplete="off" required='required'>
                        <option></option>
                        <option>Tunai</option>
                        <option>Transfer</option>
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Uang Sebesar <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="uang_sebesar" name="uang_sebesar" class="form-control" autocomplete="off" >
                    </div>
                  </div>
                 
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btn-setuju" onclick="simpan_bayar()" >OK</button>
                  </div>
                </form>

              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Tanda Bayar -->

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Pembelian</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('pembelian.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                  <table id="tb_pembelian" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Total</th>
                        <th>Cek Beli</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript"> 
  $('#Tanggal').datetimepicker({
      format: 'DD-MM-YYYY'
    }); 
    var tb_pembelian = '';

    tb_pembelian = $('#tb_pembelian').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
          type : 'GET',
          url : '{{ route('pembelian.datatable') }}',

      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'tgl', name: 'tgl' },
        { data: 'nama', name: 'nama' },
        { data: 'total', name: 'total' },
        { data: 'is_cek_beli', name: 'is_cek_beli' },
        { data: 'opsi', name: 'opsi' }
      ]
    });

    function delete_beli(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('pembelian.delete') }}',
        data    : { '_idBeli' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_pembelian.draw(); 
        }
      });
    }

    $('#modal_bayar').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var id = $button.data('id');
      var total = $button.data('total');
      $('input[name=id_beli]').val(id);
      $('input[name=total_bayar]').val(total);
    });

    function simpan_bayar() {
      var pembayaran = $('#jenisPembayaran').val();
      var id_beli = $('#idBeli').val();
      var tanggal = $('#Tanggal').val();
      var total_bayar = $('#totalBayar').val();

      $.ajax({
          type : 'POST',
          url : '{{ route('pembelian.simpan_bayar') }}',
          data : {
                  '_idBeli' : id_beli,
                  '_tanggal' : tanggal,
                  '_totalBayar' : total_bayar,
                  '_pembayaran' : pembayaran,
                },
          success: function (e) {
            notif(e.code, e.msg);
            $('#modal_bayar').modal('hide');
            tb_pembelian.draw();
            // window.location.href = '{{ route('sjdefault.index') }}';
            console.log(e);
          }
        });
    }

  </script>
@endpush