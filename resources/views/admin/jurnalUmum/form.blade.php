@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- Awal Modal Akun -->
    <div class="modal fade bs-example-modal-lg" id="modal_akun_debit" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Akun</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tb_akun_debit" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>No. Akun</th>
                              <th>Nama</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Akun -->

    <!-- Awal Modal Akun -->
    <div class="modal fade bs-example-modal-lg" id="modal_akun_kredit" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Akun</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tb_akun_kredit" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>No. Akun</th>
                              <th>Nama</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Akun -->

    <!-- Awal Modal Barang -->
    <div class="modal fade bs-example-modal-lg" id="modal_brg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tabel_brg" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Nama</th>
                              <th>Harga</th>
                              <th>Satuan</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Awal Modal barang -->

    {{-- modal tambah barang --}}
    <div class="modal fade bs-example-modal-lg" id="modal_add_barang" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="namaBrgMd" name="nama_brg_md" class="form-control">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="hargaMd" name="harga_md" class="form-control">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="idSatuanMd"  name="id_satuan_md" class="form-control">
                        <option value="">-</option>
                        @foreach ($satuan as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Jenis Barang
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select id="jenisBrgMd" name="jenis_brg_md" class="form-control">
                        <option value="">-</option>
                        @foreach ($jenis_barang as $item)
                          <option value="{{ $item->id }}">{{ $item->jenis_brg }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun
                    </label>
                    <div class="col-md-6 ">
                      <div class="col-md-6 col-sm-6 ">
                        <select id="noAkunMd" name="no_akun_md" class="form-control">
                          <option value="">-</option>
                          @foreach ($akun as $item)
                            <option value="{{ $item->no_akun }}">{{ $item->no_akun }} - {{ $item->akun }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                  <div class="ln_solid"></div>
              
                <div class="form-group">
                  <div class="offset-md-3">
                    <button type="button" class="btn btn-sm btn-primary">Cancel</button>
                    <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                    <button type="button" class="btn btn-sm btn-success" onclick="add_barang()"> Submit</button>
                  </div>
                </div>
                </div>
          </div>
      </div>
    </div>
    {{-- //end modal tambah barang --}}

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Jurnal Umum</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('jurnalUmum.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('jurnalUmum.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 ">
                      <input type="text" id="Form" name="form" required="required" class="form-control" readonly>
                      <input type="text" id="noJurnalUmum" name="no_jurnalUmum" required="required" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tgl" name="tgl" required='required' class="form-control">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <input type="text" id="Nama" name="nama" required='required' class="form-control" value="">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="noAkunDebit" name="no_akun_debit" class="form-control noAkunD">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="namaAkunDebit" name="nama_akun_debit" class="form-control" readonly>
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                {{-- kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="idItem" name="id_item" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="idBrg" name="id_brg" class="form-control" autocomplete="off" readonly>
                      <input type="text" id="namaBrg" name="nama_brg" class="form-control" autocomplete="off">
                    </div>
                    {{-- <div class="col-md-1 ">
                      <button type="button" class="btn btn-sm btn-info barangQ"><i class="fa fa-plus"></i></button>
                    </div> --}}
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="noAkunKredit" name="no_akun_kredit" class="form-control noAkunK">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Akun
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="namaAkunKredit" name="nama_akun_kredit" class="form-control" readonly>
                    </div>
                  </div>
                </div>  

                {{-- kanan --}}
                <div class="col-md-6 col-sm-6">
                  {{-- <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">m3
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="M3" name="m3" class="form-control">
                    </div>
                  </div> --}}
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Qty
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Qty" name="qty" class="form-control qtyQ">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Harga
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Harga" name="harga" class="form-control hargaQ">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Subtotal" name="subtotal" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr Akun
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="ketrAkun" name="ketr_akun" class="form-control">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="save_jurnal()">Submit</button>
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <table class="table table-striped table-bordered" id="tbJurnalUmum" style="width:100%">
                  <thead>
                      <tr>
                          <th>No. </th>
                          <th>Tgl.</th>
                          <th>No. Akun</th>
                          <th>Ref</th>
                          <th>Nama</th>
                          <th>Keterangan</th>
                          <th>Map</th>
                          <th>Hit</th>
                          <th>m3</th>
                          <th>Harga</th>
                          {{-- <th>Opsi</th> --}}
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>

              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    $('#Tgl').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    var form = $('#Form').val();
    if (form == 'edit') {

    } else {
      no_urut();
    }

    function no_urut() {
      $.ajax({
              type : 'GET',
              url : '{{ route('no_urut_jurnalUmum') }}',
              success : function (e) {
                console.log(e);
                $('#noJurnalUmum').val(e);
              }
      });
    }

    function hitung_subtotal(harga, qty) {
        var subtotal = ( harga * qty );
        $('#Subtotal').val(subtotal);
      }

      $(document).on('keyup', '.qtyQ', function(){
        var qty = $('#Qty').val();
        var harga = $('#Harga').val();
        hitung_subtotal(harga, qty);
      });

      $(document).on('keyup', '.hargaQ', function(){
        var qty = $('#Qty').val();
        var harga = $('#Harga').val();
        hitung_subtotal(harga, qty);
      });

    var tbJurnalUmum = '';

    tbJurnalUmum = $('#tbJurnalUmum').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
            type: 'POST',
            url : '{{ route('jurnalUmum.datatable_detail') }}', 
            data : function (e) {
              e._noJu = $('#noJurnalUmum').val();
            }
      },
      columns:[
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'tgl',        name: 'tgl' },
        { data: 'no_akun',    name: 'no_akun'},
        { data: 'ref',        name: 'ref'},
        { data: 'nama',       name: 'nama' },
        { data: 'keterangan', name: 'keterangan' },
        { data: 'map',        name: 'map' },
        { data: 'hit',        name: 'hit' },
        { data: 'm3',         name: 'm3' },
        { data: 'harga',      name: 'harga' },
        // { data: 'opsi',       name: 'opsi' }
      ]
    });

    function save_jurnal() {
      var no_jurnal_umum = $('#noJurnalUmum').val();
      var tgl = $('#Tgl').val();
      var nama = $('#Nama').val();
      var no_akun_debit = $('#noAkunDebit').val();
      
      var nama_brg = $('#namaBrg').val();
      var no_akun_kredit = $('#noAkunKredit').val();
      var qty = $('#Qty').val();
      var harga = $('#Harga').val();
      var subtotal = $('#Subtotal').val();
      var ket = $('#ketrAkun').val();

      $.ajax({
        type  : 'POST',
        url   : '{{ route('jurnalUmum.save') }}',
        data  : {
                  '_noJurnalUmum' : no_jurnal_umum,
                  '_tgl' : tgl,
                  '_nama' : nama,
                  '_noAkunDebit' : no_akun_debit,
                  '_namaBrg' : nama_brg,
                  '_noAkunKredit' : no_akun_kredit,
                  '_qty' : qty,
                  '_harga' : harga,
                  '_subtotal' : subtotal,
                  '_ketr' : ket
                },
        success  : function (e) {
          notif(e.code,e.msg);
          clear_akun();
          tbJurnalUmum.draw()
        }
      });
    }

    function clear_akun() {
      $('#noAkunKredit').val('');
      $('#namaAkunKredit').val('');
      $('#namaBrg').val('');
      $('#Qty').val('');
      $('#Harga').val('');
      $('#Subtotal').val('');
      $('#ketrAkun').val('');
    }

    $(document).on('click', '.noAkunD', function(){  
      $('#modal_akun_debit').modal('show');  
    });

    $(document).on('click', '.noAkunK', function(){  
      $('#modal_akun_kredit').modal('show');  
    });

    var tb_akun_debit = '';

    tb_akun_debit = $('#tb_akun_debit').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
            type: 'GET',
            url : '{{ route('jurnalUmum.datatable_akun_debit') }}', 
      },
      columns:[
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'no_akun', data: 'no_akun' },
        { data: 'akun', data: 'akun'},
        { data: 'opsi', data: 'opsi'}
      ]
    });

    var tb_akun_kredit = '';

  tb_akun_kredit = $('#tb_akun_kredit').DataTable({
    processing    : true,
    serverSide    : true,
    ajax  : {
          type: 'GET',
          url : '{{ route('jurnalUmum.datatable_akun_kredit') }}', 
    },
    columns:[
      { data: 'DT_RowIndex', name: 'DT_RowIndex' },
      { data: 'no_akun', data: 'no_akun' },
      { data: 'akun', data: 'akun'},
      { data: 'opsi', data: 'opsi'}
    ]
  });

    function select_akun(no_akun, akun) {
      $('#modal_akun_debit').modal('hide');
      $('#noAkunDebit').val(no_akun);
      $('#namaAkunDebit').val(akun);
    }

    function select_akun_kredit(no_akun, akun) {
      $('#modal_akun_kredit').modal('hide');
      $('#noAkunKredit').val(no_akun);
      $('#namaAkunKredit').val(akun);
    }

    $(document).on('click', '.brgQ', function(){  
      $('#modal_brg').modal('show');  
    });

    var tb_barang = $('#tabel_brg').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('pembelian.datatable_brg') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'harga', name: 'harga' },
          { data: 'satuan', name: 'satuan' },
          { data: 'opsi', name: 'opsi' },
        ]
      });

      function select_brg(kode_brg,nama_brg,harga, id_satuan, satuan, no_akun) {
      $('#modal_brg').modal('hide');
      $('#idBrg').val(kode_brg);
      $('#namaBrg').val(nama_brg);
      $('#Harga').val(harga);
      $('#idSatuan').val(id_satuan);
      $('#Satuan').val(satuan);
      $('#noAkunDebit').val(no_akun)
      clear_barang2();
    }   

    function clear_barang2() {
      $('#Qty').val('');
      $('#Ketr').val('');
      $('#Subtotal').val('');
      $('#idSatuan').val('');
    }

    $(document).on('click', '.barangQ', function(){  
        $('#modal_add_barang').modal('show');  
      }); 
      
      function add_barang() {
        var nama_brgMd = $('#namaBrgMd').val();
        var harga_brgMd = $('#hargaMd').val();
        var id_satuanMd = $('#idSatuanMd').val();
        var jenis_brgMd = $('#jenisBrgMd').val();
        var no_akunMd = $('#noAkunMd').val();

        $.ajax({
        type  : 'POST',
        url   : '{{ route('add_brg.save') }}',
        data  : {
                  '_namaBrg' : nama_brgMd,
                  '_harga' : harga_brgMd,
                  '_idSatuan' : id_satuanMd,
                  '_jenisBrg' : jenis_brgMd,
                  '_noAkun' : no_akunMd
                },
        success  : function (e) {
          console.log(e);
          notif(e.code, e.msg);
          $('#modal_add_barang').modal('hide');
          tb_barang.draw();
          $('#idBrg').val(e.kode);
          $('#namaBrg').val(e.nama_brg);
          $('#Harga').val(e.harga);
          $('#idSatuan').val(e.id_satuan);
          $('#noAkunDebit').val(e.no_akun);
          $('#namaBrgMd').val('');
          $('#hargaMd').val('');
          $('#idSatuanMd').val('');
          $('#jenisBrgMd').val('');
          $('#noAkunMd').val('');
        }
      });
      }
  </script>
@endpush