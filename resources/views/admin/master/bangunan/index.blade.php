@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Peralatan</h2>
            <ul class="nav navbar-right panel_toolbox">
              
              <a href=" {{ route('bangunan.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbBangunan" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Satuan</th>
                        <th>Stok</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableBangunan = $('#tbBangunan').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('bangunan.datatable') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'harga', name: 'harga' },
          { data: 'nama', name: 'nama' },
          { data: 'stok', name: 'stok' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_bangunan(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('bangunan.delete') }}',
        data    : { '_idBrg' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableBangunan.draw(); 
        }
      });
    }

  </script>
@endpush