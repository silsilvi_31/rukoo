@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Akun</h2>
            <ul class="nav navbar-right panel_toolbox">
              
              <a href=" {{ route('akun.form') }} " class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
              <a href=" {{ route('akun.formQ') }} " class="btn btn-sm btn-success">Tambah <i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tbAkun" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No. Akun</th>
                        <th>Nama</th>
                        <th>Kelompok</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    // console.log('uyeee');
    var datatableAkun = $('#tbAkun').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('akun.datatable') }}', 
        },
        columns: [
          // { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'no_akun', name: 'no_akun' },
          { data: 'akun', name: 'akun' },
          { data: 'kelompok', name: 'kelompok' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_jabatan(id) {
      $.ajax({
        type    : 'POST',
        url     : '{{ route('jabatan.delete') }}',
        data    : { '_idJabatan' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableJabatan.draw();
        }
      });
    }

  </script>
@endpush