@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Edit Barang</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="{{ route('inventaris.update') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Nama Barang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="hidden" id="" name="kode_barang" required="required" readonly class="form-control" value="{{ $id_brg }}">
                    <input type="text" id="" name="barang" required="required" class="form-control" value="{{ $nama }}">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="harga" required="required" class="form-control" value="{{ $harga }}">
                  </div>
                </div>
                {{-- <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Stok <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="stok" required="required" class="form-control" value="{{ $stok }}">
                  </div>
                </div> --}}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="id_satuan" id="" class="form-control">
                      <option value="">-</option>
                      @foreach ($satuan as $item)
                        <option value="{{ $item->id }}" @if($item->id == $id_satuan) selected @endif>{{ $item->nama }}</option> 
                      @endforeach
                    
                    </select>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Jenis Barang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="id_jenis_barang" id="" class="form-control">
                      <option value="">-</option>
                      @foreach ($jenis_barang as $item)
                        <option value="{{ $item->id }}" @if($item->id == $id_jenis_barang) selected @endif>{{ $item->jenis_brg }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6 offset-md-3">
                    <a href="{{ route('inventaris.index') }}" class="btn btn-warning" type="button">Cancel</a>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">

  </script>
@endpush