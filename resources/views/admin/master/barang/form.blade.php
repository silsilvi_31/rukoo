@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Form Barang</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <br />
              <form id="" action="{{ route('barang.save') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                {{ csrf_field() }}
                
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Tgl <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="Tgl" name="tgl" class="form-control" value="" autocomplete="off">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Nama Barang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="namabarang" required="required" class="form-control " autocomplete="off">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga Beli <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="harga_beli" required="required" class="form-control " autocomplete="off">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Harga Jual<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="harga" required="required" class="form-control " autocomplete="off">
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Stok Awal <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="stokAwal" name="stok_awal" required="required" class="form-control " autocomplete="off">
                  </div>
                </div>
                {{-- <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Ukuran <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="" name="ukuran" required="required" class="form-control " autocomplete="off">
                  </div>
                </div> --}}
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Satuan <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    {{-- <input type="text" id="" name="satuan" required="required" class="form-control " autocomplete="off"> --}}
                    <select name="id_satuan" id="" class="form-control ">
                      <option value="">-</option>
                      @foreach ($satuan as $item)
                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Jenis Barang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <select name="id_jenis_barang" id="" class="form-control">
                      <option value="">-</option>
                      @foreach ($jenis_barang as $item)
                        <option value="{{$item->id}}">{{$item->jenis_brg}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                
                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-6 col-sm-6 offset-md-3">
                    <a href="{{ route('barang.index') }}" class="btn btn-warning" type="button">Cancel</a>
                    <button class="btn btn-danger" type="reset">Reset</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
    console.log('form');
    $('#Tgl').datetimepicker({
      format: 'DD-MM-YYYY'
    });

  </script>
@endpush