@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Cek Opname</h2>
            &nbsp; <button class="btn btn-sm btn-success" onclick="cek_opname()">Konfirmasi</button>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <input type="text" id="Tgl" name="tgl" class="form-control"  value="{{ empty($tgl) ? null : $tgl}}" readonly hidden>
                <div class="card-box table-responsive">
                  <table id="tb_detailopname" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th><input type="checkbox" name="cb_detailOpname[]" class="selectAll" autocomplete="off"></th>
                        {{-- <th>No</th> --}}
                        <th>tgl</th>
                        <th>Nama Barang</th>
                        <th>Stok Sebelum</th>
                        <th>Stok Sesudah</th>
                        <th>Keterangan</th>
                        <th>Selisih</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    var tb_detailopname = $('#tb_detailopname').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: {
            type: 'POST',
            url: '{{ route('cekopname.datatable_detail_opname') }}', 
            data : function (e) {
              e._tgl = $('input[name=tgl]').val();
            }
        },
        columns: [
          { data: 'cekbox', name: 'cekbox' },
          // { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'tgl', name: 'tgl' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'stok_sblm', name: 'stok_sblm' },
          { data: 'stok_sesudah', name: 'stok_sesudah' },
          { data: 'ketr', name: 'ketr' },
          { data: 'selisih', name: 'selisih' },
          { data: 'is_cek_opname', name: 'is_cek_opname' },
        ]
    });

    $(document).ready(function(){
    $('.selectAll').on('click', function(){
      if(this.checked) {
        $('.pilih').each(function() {
          this.checked = true;
        })
      } else {
        $('.pilih').each(function() {
          this.checked = false;
        })
      }
    });

    $('.pilih').on('click', function() {
     if($('.pilih:checked').length == $('.pilih').length) {
       $('.selectAll').prop('checked', true)
     }else{
       $('.selectAll').prop('checked', false)
     }
   })
    
  })

  function cek_opname() {
    dtt = [];
    $('input:checkbox').each(function () {
        var $this = $(this);
        id = $this.val();

        if ($(this).prop('checked')) {
          inp = {
            'id' : id,
          }
          dtt.push(inp);
        }
      });
      console.log(dtt);
      $.ajax({
        type: 'POST',
        url: '{{ route('cekopname.cek_opname')}}',
        data : { 
                  '_id' : dtt,
                },
        success: function (e) {
          notif(e.code, e.msg);
          tb_detailopname.draw();
        }
      })
  }


  </script>
@endpush