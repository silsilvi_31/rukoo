@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Detail Gaji</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <input type="hidden" id="Tgl" name="tgl" value="{{ $tgl }}" readonly>
                  <table id="tb_detailGaji" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>kodep</th>
                        <th>nama</th>
                        <th>Gaji</th>
                        <th>Lembur</th>
                        <th>Potongan</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">  
    var tb_detailGaji = '';

    tb_detailGaji = $('#tb_detailGaji').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
          type : 'POST',
          url : '{{ route('gaji.datatable_detail_gaji') }}',
          data: function (e) {
                e._tgl = $('#Tgl').val();
              } 
      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'kodep', name: 'kodep' },
        { data: 'nama', name: 'nama' },
        { data: 'total_gaji', name: 'total_gaji' },
        { data: 'lembur', data: 'lembur' },
        { data: 'potongan', data : 'potongan' },
        { data: 'opsi', name: 'opsi' }
      ]
    });

  </script>
@endpush