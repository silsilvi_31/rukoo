@extends('master.ella')
    
@section('content')

<style type="text/css">
  body {
    /* border: 1px solid #000 !important; */
  }
  .table-bordered, .table th {
    /* border-bottom: 1px solid #000 !important; */
    border: none !important;
    padding: .3rem;
  }
  .x_panel {
    border : none !important;
  }
  .table td {
    border-bottom: none !important;
    padding: .3rem;
  }
  .nominal {
    float: right;
  }
  tr .total_gf {
    border: 1px solid #000 !important;
  }

  .no {
    width : 10%;
  }

  .akun {
    width: 45%;
  }

  .angka {
    width: 15%; 
  }
</style>

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Laporan Pembelian</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tgl <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 ">
                      <input type="text" id="tglAwal" name="tgl_awal" class="form-control">
                    </div>
                    <div class="col-md-2 col-sm-2 ">
                      <input type="text" id="tglAkhir" name="tgl_akhir" class="form-control">
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                      <button type="button" class="btn btn-info" onclick="cari_pembelian()">Cari</button>
                      <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                        <div class="btn-group" role="group">
                          <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Export
                          </button>
                          <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <button class="dropdown-item" onclick="excel_labarugi()">Excel</button>
                            <a class="dropdown-item" href="#">Pdf</a>
                            <a class="dropdown-item" href="#">Csv</a>
                            <a class="dropdown-item" href="#">Print</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <table id="tb_pembelian" class="table table-striped jambo_table bulk_action" style="width:100%">
                    <thead>
                      <tr>
                        <th>Tgl</th>
                        <th>No</th>
                        <th>Suplier</th>
                        <th>Total</th>
                        <th>Status Bayar</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                    <tfoot>
                      <tr>
                          <th colspan="3" style="text-align:right">Total:</th>
                          <th></th>
                      </tr>
                  </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    $('#tglAwal').datetimepicker({
      format: 'DD-MM-YYYY'
    });

    $('#tglAkhir').datetimepicker({
      format: 'DD-MM-YYYY'
    });

   var datatable_pembelian = '';

   var datatable_pembelian = $('#tb_pembelian').DataTable({
     processing: true,
     serverSide: true,
     order : false,
     ajax : {
       type : 'POST',
       url : '{{ route('laporan.table_pembelian') }}',
       data : function (e) {
         e.tglAwal = $('#tglAwal').val();
         e.tglAkhir = $('#tglAkhir').val();
       }
     },
     columns: [
          { data: 'tgl', name: 'tgl' },
          { data: 'id', name: 'id' },
          { data: 'nama_suplier', name: 'nama_suplier' },
          { data: 'total', name: 'total' },
          { data: 'status_bayar', name: 'status_bayar'},
        ],
        "footerCallback": function( tfoot, data, start, end, display ) {
          var api = this.api();
          var intVal = function ( i ) {
 
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
                    
          };
          $( api.column( 3 ).footer() ).html(
              api.column( 3 ).data().reduce( function ( a, b ) {
                  return intVal(a) + intVal(b);
              }, 0 )
          );
        }
   });

   function cari_pembelian() {
      datatable_pembelian.draw();
    }
    function export_excel() {
      var tgl = $('#tgl').val();
      var jenisJurnal = $('#jenisJurnal').val();
      var param_a,param_b,dt;

      param_a = tgl;
      param_b = jenisJurnal;

      if (!tgl) {
        param_a = 'kosong';
      }

      if (!jenisJurnal) {
        param_b = 'kosong';
      }

      dt = param_a + '*' + param_b
      // console.log(dt);
      
      var url = '{{ route('jurnal.excel', ['.p']) }}';
      var url_fix = url.replaceAll('.p',dt);
      window.location.href = url_fix;
      
      // if (tgl) {
        // window.location.href = '/ruko/public/dashboard/jurnal/excel/'+dt;
      // }
      exportTgl();
    }

    function exportTgl() {
      var tgl = $('#tgl').val();
      var jenis_jurnal = $('#jenisJurnal').val();
      
      $.ajax({
        type    : 'POST',
        url     : '{{ route('jurnal_export.rmv') }}',
        data    : { 
                    '_tgl' : tgl,
                    '_jenisJurnal' : jenis_jurnal
                  },
        success : function (e) {
          console.log(e);
        } 
      });
    }
   
  </script>
@endpush