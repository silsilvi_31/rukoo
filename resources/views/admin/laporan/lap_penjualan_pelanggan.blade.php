@extends('master.ella')
    
@section('content')

<style type="text/css">
  body {
    /* border: 1px solid #000 !important; */
  }
  .table-bordered, .table th {
    /* border-bottom: 1px solid #000 !important; */
    border: none !important;
    padding: .3rem;
  }
  .x_panel {
    border : none !important;
  }
  .table td {
    border-bottom: none !important;
    padding: .3rem;
  }
  .nominal {
    float: right;
  }
  tr .total_gf {
    border: 1px solid #000 !important;
  }

  .no {
    width : 10%;
  }

  .akun {
    width: 45%;
  }

  .angka {
    width: 15%; 
  }
</style>

<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Laporan Penjualan per Pelanggan</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tgl <span class="required">*</span>
                  </label>
                  <div class="col-md-2 col-sm-2 ">
                    <input type="text" id="tglAwal" name="tgl_awal" class="form-control" autocomplete="off">
                  </div>
                  <div class="col-md-2 col-sm-2 ">
                    <input type="text" id="tglAkhir" name="tgl_akhir" class="form-control" autocomplete="off">
                  </div>
                  <div class="col-md-4 col-sm-4 ">
                    {{-- <button type="button" class="btn btn-info" onclick="cari_penjualan_pelanggan()">Cari</button> --}}
                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                      <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Export
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                          <button class="dropdown-item" onclick="excel_labarugi()">Excel</button>
                          <a class="dropdown-item" href="#">Pdf</a>
                          <a class="dropdown-item" href="#">Csv</a>
                          <a class="dropdown-item" href="#">Print</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-box table-responsive">

                  <br>
                  <div class="card-box table-responsive">
                    <table id="tb_penjualan_pelanggan" class="table table-striped jambo_table bulk_action" style="width:100%">
                      <thead>
                        <tr>
                          <th>Tgl/Pelanggan</th>
                          <th>No</th>
                          <th>Nama Barang</th>
                          <th>Satuan</th>
                          <th>Qty</th>
                          <th class="text-right">Harga</th>
                          <th class="text-right">Subtotal</th>
                        </tr>
                      </thead>
                      <tbody id="body_penjualan_pelanggan">
                        
                      </tbody>
                      {{-- <tfoot>
                        <tr>
                          <td colspan="2" ><strong>Total</strong></td> 
                          <td class="text-right" id="total_debit_aktiva"></td>
                          <td class="text-right" id="total_kredit_aktiva"></td>   
                          <td class="text-right" id="total_aktiva" style="background:#8395a7;"></td>  
                        </tr>
                      </tfoot> --}}
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    $('#tglAwal').datetimepicker({
      format: 'DD-MM-YYYY'
    });

    $('#tglAkhir').datetimepicker({
      format: 'DD-MM-YYYY'
    });


    $("#tglAwal").on("dp.change", function (e) {
        var tgl = $(this).val();
        var tgl_dua = $('#tglAkhir').val();
        tampil_penjualan_pelanggan(tgl, tgl_dua);
    });

    $("#tglAkhir").on("dp.change", function (e) {
        var tgl = $("#tglAwal").val();
        var tgl_dua = $(this).val();
        tampil_penjualan_pelanggan(tgl, tgl_dua);
    });

    tampil_penjualan_pelanggan();


    function tampil_penjualan_pelanggan(tgl, tgl_dua) {
      $.ajax({
        type : 'POST',
        url : '{{ route('laporan.table_penjualan_pelanggan') }}',
        data: { 
                '_tglAwal' : tgl,
                '_tglAkhir' : tgl_dua,
               },
        success: function (e) {
          console.log(e);
          $('#body_penjualan_pelanggan').empty();    

          $.each(e.data, function (key, value) { 
                tr = $('<tr/>');    
                tr.append("<td>" + value.keterangan + "</td>");
                tr.append("<td>" + value.id_sj + "</td>");   
                tr.append("<td>" + value.nama_brg + "</td>");   
                tr.append("<td>" + value.qty + "</td>");   
                tr.append("<td>" + value.satuan + "</td>");   
                tr.append("<td class='text-right'>" + value.harga + "</td>");   
                tr.append("<td class='text-right'>" + value.subtotal + "</td>");   

                $('#body_penjualan_pelanggan').append(tr);    
          });     
        }
      });
    }

  </script>
@endpush