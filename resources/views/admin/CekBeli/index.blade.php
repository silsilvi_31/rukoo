@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    {{-- Modal Cek Nota --}}
    <div id="modal_cekbeli" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cek Beli</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                </label>
                <div class="col-md-7 ">
                  <input type="text" id="idBeli" name="id_beli" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                </label>
                <div class="col-md-7">
                  <input type="text" id="Tgl" name="tgl" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kepada
                </label>
                <div class="col-md-7">
                  <input type="text" id="Suplier" name="suplier" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Status
                </label>
                <div class="col-md-7">
                  <input type="text" id="Status" name="status" class="form-control" readonly>
                </div>
              </div>
            </div>

            </form>
            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            <table id="tb_detail_beli" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Satuan</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Keterangan</th>
                      <th>Subtotal</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          <div class="clearfix"></div>
              <div class="ln_solid"></div>
              <div class="col-md-6 col-sm-6"></div>
              <div class="col-md-6 col-sm-6">
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="_total" name="total" class="form-control" readonly>
                  </div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btn-setuju" onclick="acc_beli()" >Setuju</button>
            <button type="button" class="btn btn-danger " id="btn-batal" onclick="batal_beli()" >Batal</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Cek SJ</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <table id="tb_cekbeli" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>tgl</th>
                        <th>Kepada</th>
                        <th>Total</th>
                        <th>Cek Beli</th>
                        {{-- <th>Cek Batal</th> --}}
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
  // var datatableCekBeli = '';
    var datatableCekBeli = $('#tb_cekbeli').DataTable({
        processing: true,
        serverSide: true,
        ordering: false,
        ajax: {
            type: 'GET',
            url: '{{ route('cekbeli.datatable') }}', 
        },
        columns: [
          
          { data: 'id_beli', name: 'id_beli' },
          { data: 'tgl', name: 'tgl' },
          { data: 'nama', name: 'nama' },
          { data: 'total', name: 'total' },
          { data: 'is_cek_beli', name: 'is_cek_beli' },
          // { data: 'batal', name: 'batal' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_suratjalan(id) {
      $.ajax({
        type    : 'GET',
        url     : '{{ route('ceksj.delete') }}',
        data    : { '_idSj' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableCekbeli.draw(); 
          
        }
      });
    }

    var datatable_cekbeli_detail = $('#tb_detail_beli').DataTable({
      processing : true,
      serverSide : true,
      scrollY: true,
      ajax : {
        type : 'POST',
        url : '{{ route('cekbeli.datatable_beli') }}',
        data : function (e) {
          e._id = $('input[name=id_beli]').val();
        }
      },
      columns : [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'nama_brg', name:'nama_brg' },
            { data: 'satuan', name:'satuan' },
            { data: 'qty', name:'qty' },
            { data: 'harga', name:'harga' },
            { data: 'ketr', name:'ketr_tambahan' },
            { data: 'subtotal', name:'subtotal' },
      ]
    }); 

    $('#modal_btl').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var id = $button.data('id');
      var ket = $button.data('ket');
      $('input[name=id_md]').val(id);
      $('input[name=ket_md]').val(ket);
    });

   $('#modal_cekbeli').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var form = $button.data('form');
     var id = $button.data('id');
     var tgl = $button.data('tgl');
     var suplier = $button.data('suplier');
     var total = $button.data('total');
     var cek_beli = $button.data('cek');
     
     if(form == 'cek_beli') {
      $('input[name=id_beli]').val(id);
      $('input[name=tgl]').val(tgl);
      $('input[name=suplier]').val(suplier);
      $('input[name=total]').val(total);
      $('input[name=status]').val(cek_beli);

      if(cek_beli == 'acc') {
        $('#btn-setuju').attr('hidden', true);
        $('#btn-batal').attr('hidden', true);
      } else {
        $('#btn-setuju').removeAttr('hidden');
        $('#btn-batal').removeAttr('hidden',);
      }

      datatable_cekbeli_detail.draw();

     } else if (form == 'cek_nota') {
      // $('input[name=id]').val(id);
      // $('input[name=tgl]').val(tgl);
     }
     
   });

   function acc_beli() {
    var id = $('input[name=id_beli]').val();
    var status = $('input[name=status]').val();
    var btn = 'setuju';
    console.log(btn);
    
    $.ajax({
      type : 'POST',
      url : '{{ route('cekbeli.acc_beli') }}',
      data : {
                '_id' : id,
                '_btn' : btn,
                '_status' : status
              },
      success : function (e) {
        datatableCekBeli.draw();
        $('#modal_cekbeli').modal('hide');
        notif(e.code, e.msg);
      }
    });
   }

   function batal_beli() {
    var id = $('input[name=id_beli]').val();
    var batal = 'batal';
    console.log(batal);
    
    $.ajax({
      type : 'POST',
      url : '{{ route('cekbeli.acc_beli') }}',
      data : {
                '_id' : id,
                '_btn' : batal
              },
      success : function (e) {
        datatableCekBeli.draw();
        $('#modal_cekbeli').modal('hide');
        notif(e.code, e.msg);
      }
    });
   }

   function batal_suratjalan(id) {
     var id = $('input[name=id_md]').val();
     var ket = $('input[name=ket_md]').val();
     $.ajax({
       type : 'POST',
       url: '{{ route('ceksj.batal_suratjalan') }}',
       data : {
                '_id' :  id,
                '_ket' : ket  
      },
      success: function (e) {
        $('#modal_btl').modal('hide');
        datatableCekSj.draw();
        notif(e.code, e.msg);
      }
     });
   }

   $(document).on('click', '.print_nota', function() {
      var id_sj = $(this).attr('id');
      var jenis = 'print_nota';
      $.ajax({
        type    : 'POST',
        url     : '{{ route('ceksj.print_sj_count') }}',
        data    : { '_idSj' : id_sj,
                    '_jenis' : jenis },
        success : function (e) {
          notif(e.code, e.msg);
          datatableSj.ajax.reload(); 
            window.location.href = 'cek-sj/nota/'+id_sj;
        }
      });
    });
  </script>
@endpush