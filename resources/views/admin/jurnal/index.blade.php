@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Jurnal</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <div class="card-box table-responsive">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pencarian <span class="required">*</span>
                    </label>
                    <div class="col-md-3 col-sm-3 ">
                      <select id="jenisJurnal" class="form-control">
                        <option value="">-</option>
                        @foreach ($jenis_jurnal as $item)
                        <option>{{ $item->jenis_jurnal }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                      <input type="text" id="tgl" name="tanggal" class="form-control" autocomplete="off">
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                      <button type="button" class="btn btn-info" onclick="cari_jurnal()">Cari</button>
                      <button type="button" onclick="export_excel()" class="btn btn-primary">Excel</button>
                    </div>
                    <div class="col-md-2 col-sm-2 ">
                      {{-- <a href="{{ route('jurnal.excel') }}" class="btn btn-primary" >Export</a> --}}
                    </div>
                  </div>
{{-- 
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Map
                    </label>
                    <div class="col-md-3 col-sm-3 ">
                      <input type="text" id="totalDebit" name="total_debit" class="form-control" readonly>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                      <input type="text" id="totalKredit" name="total_kredit" class="form-control" readonly>
                    </div>
                  </div> --}}

                  <table id="tb_jurnal" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        {{-- <th>No.</th> --}}
                        <th>Tgl</th>
                        <th>Jurnal</th>
                        <th>No Akun</th>
                        <th>Ref</th>
                        {{-- <th>Grup</th> --}}
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th>Map</th>
                        <th>hit</th>
                        <th>Qty</th>
                        <th>M3</th>
                        <th>harga</th>
                        <th>Total</th>
                        {{-- <th>Opsi</th> --}}
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                    <tfoot>
                      <td colspan="10" style="text-align:right"> <strong>Total:</strong> </td>
                      <td></td>
                      <td></td>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
    $('#tgl').datetimepicker({
      format: 'YYYY-MM-DD'
    });

    hitung_total();

    function hitung_total() {
      var tgl = $('#tgl').val();
      var jenis_jurnal = $('#jenisJurnal').val();


      $.ajax({
        type: 'POST',
        url: '{{ route('jurnal.total') }}',
        data: { 
                '_tgl' : tgl,
                '_jenisJurnal' : jenis_jurnal 
              },
        success : function (e) {
         console.log(e);
         $('#totalDebit').val(e.total_debit);
         $('#totalKredit').val(e.total_kredit);
        }
      })
    }

   var datatable_jurnal = '';

   var datatable_jurnal = $('#tb_jurnal').DataTable({
     processing: true,
     serverSide: true,
     order : false,
     ajax : {
       type : 'POST',
       url : '{{ route('jurnal.datatable') }}',
       data : function (e) {
         e.tgl = $('#tgl').val();
         e.jenisJurnal = $('#jenisJurnal').val();
       }
     },
     columns: [
          // { data: 'id', name: 'id' },
          { data: 'tgl', name: 'tgl' },
          { data: 'jurnal', name: 'jurnal' },
          { data: 'no_akun', name: 'no_akun' },
          { data: 'ref', name: 'ref' },
          // { data: 'grup', name: 'grup' },
          { data: 'nama', name: 'nama' },
          { data: 'keterangan', name: 'keterangan' },
          { data: 'map', name: 'map' },
          { data: 'hit', name: 'hit' },
          { data: 'qty', name: 'qty' },
          { data: 'm3', name: 'm3' },
          { data: 'harga', name: 'harga' },
          { data: 'total', name: 'total' },
          // { data: 'opsi', name: 'opsi' }
        ],
        
        "footerCallback": function( tfoot, data, start, end, display ) {
          var api = this.api();
          var intVal = function ( i ) {
 
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
                    
          };
          var total_harga = api
                .column( 10 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
          
          var total = api
                .column( 11 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

          $( api.column( 10 ).footer() ).html(total_harga);
          $( api.column( 11 ).footer() ).html(total);
        }
   });

    function cari_jurnal() {
      datatable_jurnal.draw();
    }

    function export_excel() {
      var tgl = $('#tgl').val();
      var jenisJurnal = $('#jenisJurnal').val();
      var param_a,param_b,dt;

      param_a = tgl;
      param_b = jenisJurnal;

      if (!tgl) {
        param_a = 'kosong';
      }

      if (!jenisJurnal) {
        param_b = 'kosong';
      }

      dt = param_a + '*' + param_b
      
      var url = '{{ route('jurnal.excel', ['.p']) }}';
      var url_fix = url.replaceAll('.p',dt);
      window.location.href = url_fix;
      
      exportTgl();
    }
  </script>
@endpush