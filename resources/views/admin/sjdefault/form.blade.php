@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <!-- Awal Modal Checkout -->
    <div class="modal fade bs-example-modal-lg" id="modal_checkout" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Checkout</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total Bayar <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="TotalBayar" name="total_bayar" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Dibayar <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Dibayar" name="dibayar" required="required" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kembalian <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Kembalian" name="kembalian" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btn-setuju" onclick="simpan_checkout()" >OK</button>
                  </div>
                </form>

              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Checkout -->

     <!-- Awal Modal Barang/Triplek -->
     <div class="modal fade bs-example-modal-lg" id="modal_brg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data Barang</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <table class="table table-striped table-bordered" id="tabel_brg" style="width:100%">
                      <thead>
                          <tr>
                              <th>No.</th>
                              <th>Nama</th>
                              <th>Harga</th>
                              <th>Satuan</th>
                              <th>Sisa</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- Awal Modal barang/triplek -->

    <!-- Awal Modal Tambah Pelanggan -->
    <div class="modal fade bs-example-modal-lg" id="add_pelanggan" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Tambah Data Pelanggan</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- {{ csrf_field() }} --}}
            {{-- Kiri --}}
              {{-- <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">ID Pelanggan <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="idPl" name="id_pl" required="required" class="form-control" readonly>
                </div>
              </div> --}}
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="namaPl" name="nama_pl" required="required" class="form-control">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Alamat <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                    <textarea id="alamatPl" name="alamat_pl" class="form-control"></textarea>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. telp
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="telpPl" name="telp_pl" class="form-control ">
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Email
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="emailPl" name="email_pl" class="form-control ">
                </div>
              </div>
            {{-- </div> --}}

            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            
            <div class="form-group">
              <div class="offset-md-3">
                <button type="button" class="btn btn-sm btn-primary">Cancel</button>
                <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                <button type="button" class="btn btn-sm btn-success" onclick="add_pl()">Submit</button>
              </div>
            </div>
          </div>                                   
          </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Awal Modal Tambah Pelanggan -->

    <!-- ============================================================== -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Surat Jalan Default</h2>
              <ul class="nav navbar-right panel_toolbox">
                <a href=" {{ route('sjdefault.form') }} " class="btn btn-sm btn-success">New</a>
                <a href=" {{  route('sjdefault.index') }} " class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i> Back </a>
              </ul>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <br/>
              <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                {{-- {{ csrf_field() }} --}}
                {{-- Kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                    </label>
                    <div class="col-md-2 col-sm-2 ">
                      <input type="hidden" id="formSj" name="" class="form-control" readonly value="{{ !empty($form) ? $form : '' }}">
                      <input type="text" id="idBeli" name="id_beli" required="required" class="form-control" readonly value="{{ !empty($id) ? $id : '' }}">
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                      <input type="text" id="tgl" name="tgl" required='required' class="form-control" value="{{ !empty($tgl) ? $tgl : date('d-m-Y') }}">
                    </div>
                  </div>
                  {{-- <div class="item form-group form_pelanggan">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kepada <span class="required">*</span>
                    </label>
                    <div class="col-md-5">
                      <input type="text" id="Pelanggan" name="pelanggan" class="form-control" value="">
                        @php
                            $id_plg = !empty($id_pelanggan) ? $id_pelanggan : '';
                            $pmb = !empty($pembayaran) ? $pembayaran : '';
                            $png = !empty($pengiriman) ? $pengiriman : '';
                            $knd = !empty($id_kendaraan) ? $id_kendaraan : ''; 
                            $kry = !empty($id_karyawan) ? $id_karyawan : '';
                            $no_rek = !empty($no_rek) ? $no_rek : '';
                            $opsi =  !empty($opsi) ? $opsi : '';
                        @endphp
                    </div>
                     <div class="col-md-1">
                      <button type="button" id="btn_pelanggan" class="btn btn-md btn-info" onclick="pilih_member()"><i class="fa fa-user"></i></button>
                    </div>
                  </div> --}}

                  <div class="item form-group form_member">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Member <span class="required">*</span>
                    </label>
                    <div class="col-md-6">
                      <select class="form-control" name="id_pelanggan" id="idPelanggan" autocomplete="off" >
                        <option value="" id="pelanggan_kosong" ></option>
                        @php
                            $id_plg = !empty($id_pelanggan) ? $id_pelanggan : '';
                            $pmb = !empty($pembayaran) ? $pembayaran : '';
                            $png = !empty($pengiriman) ? $pengiriman : '';
                            $knd = !empty($id_kendaraan) ? $id_kendaraan : ''; 
                            $kry = !empty($id_karyawan) ? $id_karyawan : '';
                            $no_rek = !empty($no_rek) ? $no_rek : '';
                            $opsi =  !empty($opsi) ? $opsi : '';
                        @endphp
                        @foreach ($pelanggan as $item)
                          <option value="{{ $item->id }}" @if($item->id == $id_plg) selected @endif>{{ $item->nama }}</option>                              
                        @endforeach
                      </select>
                    </div>
                    {{-- <div class="col-md-1">
                      <button type="button" class="btn btn-info pelangganQ"><i class="fa fa-plus"></i></button>
                    </div> --}}
                  </div>

                  <div class="item form-group" hidden>
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Catatan Khusus
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="" name="catatan" class="form-control " value="{{ !empty($catatan) ? $catatan : '' }}">
                    </div>
                  </div>

                  {{-- <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pengiriman <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select class="form-control" name="pengiriman" id="JenisPengiriman" autocomplete="off" required='required'>
                        <option></option>
                        <option @if($png == "Kirim") selected @endif>Kirim</option>
                        <option @if($png == "Ambil Sendiri") selected @endif>Ambil Sendiri</option>
                      </select>
                    </div>
                  </div> --}}

                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ongkir
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="" name="ongkir" class="form-control ongkirQ" value="{{ !empty($ongkir) ? $ongkir : '' }}">
                    </div>
                  </div>
                </div>

                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="_subtotal" name="" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ongkir
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="_ongkir" name="Ongkir" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Grand Total
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="_grandtotal" name="" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Cash
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="_cash" name="" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kembalian
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="_kembalian" name="" class="form-control" readonly>
                    </div>
                  </div>
                </div>
{{-- 
                <div class="col-md-4 col-sm-3">
                  <div class="pricing">
                    <div class="title">
                        <h2>Total Bayar</h2>
                        <h1><span id="subtotalQ"></span></h1>
                    </div>
                  </div>
                </div> --}}

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                {{-- kiri --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Barang
                    </label>
                    <div class="col-md-4 ">
                      <input type="hidden" id="" name="idQ" class="form-control" readonly> 
                      <input type="hidden" id="" name="id_brg" class="form-control" readonly>
                      <input type="text" id="" name="nama_brg" class="form-control brgQ">
                    </div>
                    <div class="col-md-2 ">
                      <button type="button" class="btn btn-md btn-info" onclick="new_barang()">New</button>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Harga
                    </label>
                    <div class="col-md-6 ">
                      <input type="text" id="" name="harga" class="form-control" readonly>
                    </div>
                    {{-- <div class="col-md-2">
                      <button type="button" id="btn_harga" class="btn btn-md btn-info" onclick="tambah_harga()"><i id="icon_harga" class="fa fa-plus"></i></button>
                    </div> --}}
                  </div>
                  <div class="item form-group harga_new" hidden>
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Harga Baru
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="hargaNew" name="harga_new" class="form-control harga_newQ">
                    </div>
                  </div>
                  <div class="item form-group ketr_ganti_harga" hidden>
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr Ganti Harga
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="KetrGantiHarga" name="ketr_ganti_harga" class="form-control ketr_ganti_hargaQ">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Satuan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="hidden" id="" name="id_satuan" class="form-control" readonly>
                      <input type="text" id="" name="satuan" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="" name="subtotal" class="form-control" readonly>
                    </div>
                  </div>
                </div>  

                {{-- kanan --}}
                <div class="col-md-6 col-sm-6">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Stok
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Stok" name="stok" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Qty
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="" name="qty" class="form-control qtyQ">
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Diskon
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="" name="potongan" class="form-control potonganQ">
                    </div>
                  </div>
                  {{-- <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="" name="ketr" class="form-control ketrQ">
                    </div>
                  </div> --}}
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ketr Tambahan
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="" name="ketr_tambahan" class="form-control" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <div class="col-md-6 col-sm-6">
                  <div class="form-group">
                    <div class="offset-md-3">
                      {{-- <button type="button" class="btn btn-primary">Cancel</button> --}}
                      <button class="btn btn-sm btn-primary" type="reset">Reset</button>
                      <button type="button" class="btn btn-sm btn-success" onclick="simpan_sj()">Submit</button>
                      {{-- <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modal_checkout">Checkout</button> --}}
                      {{-- <button type="button" class="btn btn-info" onclick="print_sj()">Print</button> --}}
                    </div>
                  </div>
                </div>
              </div>                                   
              </form>

              <div class="clearfix"></div>
                <div class="ln_solid"></div>
                <table class="table table-striped table-bordered" id="tbSj" style="width:100%">
                  <thead>
                      <tr>
                          <th>No.</th>
                          <th>Nama</th>
                          <th>Ketr</th>
                          <th>Ketr Tambahan</th>
                          <th>Satuan</th>
                          <th>Qty</th>
                          <th>Harga</th>
                          <th>Potongan</th>
                          <th>Subtotal</th>
                          <th>Opsi</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>

              <div class="clearfix"></div>
              <div class="ln_solid"></div>
              </div>
          </div>
        </div>
      </div>
  </div>
</div>        
@endsection

@push('js')
  <script type="text/javascript">
  $('#tgl').datetimepicker({
      format: 'DD-MM-YYYY'
    });
  var form = $('#formSj').val();
  
  if (form == 'edit') {
  } else {
    no_urut();
  }
  
  hitung_total();
  // select_pelanggan();

  var html = '';

  function select_pelanggan() {
    $.ajax({
      type : "POST",
      url : '{{ route('sjdefault.select_pelanggan') }}',

      success : function (e) {
        
        // for (var i in e){
        //   html += '<option value='+e[i].id+'>'+e[i].nama+'</option>';
        // }
        // html += '<option value='+e[i].id+'>'+e[i].nama+'</option>';
        // $('#idPelanggan').append(html);
      }
    });
  }

  function hitung_subtotal(harga, qty, potongan) {
    var subtotal = ( harga * qty ) - ( potongan * qty );
    $('input[name=subtotal]').val(subtotal);
  }

    //subtotal
    $(document).on('keyup', '.qtyQ', function(){
      var harga = 0; 
     
      var qty = $('input[name=qty]').val();
      var potongan = $('input[name=potongan]').val(); 
      
      var cek = $('input[name=harga_new]').val(); 
      harga = $('input[name=harga_new]').val();
      
      if (!cek) {
          harga =  $('input[name=harga]').val();
      }

      hitung_subtotal(harga, qty, potongan);
    });

    $(document).on('keyup', '.potonganQ', function(){ 
      var harga = 0; 
     
      var qty = $('input[name=qty]').val();
      var potongan = $('input[name=potongan]').val();
      
      var cek = $('input[name=harga_new]').val(); 
      harga = $('input[name=harga_new]').val();
      
      if (!cek) {
          harga =  $('input[name=harga]').val();
      }
      
      hitung_subtotal(harga, qty, potongan);
    });

    $(document).on('keyup', '.ketrQ', function(){ 
      var harga = 0;
      harga_a = 0; 
     
      var qty = $('input[name=qty]').val();
      var potongan = $('input[name=potongan]').val();
      
      var cek = $('input[name=harga_new]').val(); 
      harga = $('input[name=harga_new]').val();
      
      if (!cek) {
          harga =  $('input[name=harga]').val();
      }
      
      hitung_subtotal(harga, qty, potongan);
    });

    $(document).on('keyup', '.harga_newQ', function(){ 
      var harga = $('input[name=harga_new]').val();
      var qty = $('input[name=qty]').val();
      var potongan = $('input[name=potongan]').val();
      
      hitung_subtotal(harga,qty,potongan);
    });

      var tb_barang = $('#tabel_brg').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '{{ route('sjdefault.datatable_brg') }}', 
        },
        columns: [
          { data: 'DT_RowIndex', name: 'DT_RowIndex' },
          { data: 'nama_brg', name: 'nama_brg' },
          { data: 'harga', name: 'harga' },
          { data: 'satuan', name: 'satuan' },
          { data: 'sisa', name: 'sisa' },
          { data: 'opsi', name: 'opsi' },
        ]
      });

      var tb_barang_detail = $('#tbSj').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type : 'POST',
            url : '{{ route('sjdefault.datatable_brg_detail') }}',
            data : function (e) {
              e._idBeli = $('#idBeli').val();
            }
        },
        columns : [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'nama_brg', name:'nama_brg' },
            { data: 'ketr', name:'ketr' },
            { data: 'ketr_tambahan', name:'ketr_tambahan' },
            { data: 'satuan', name:'satuan' },
            { data: 'qty', name:'qty' },
            { data: 'harga', name:'harga' },
            { data: 'potongan', name:'potongan' },
            { data: 'subtotal', name:'subtotal' },
            { data: 'opsi', name:'opsi' }
        ]
      });

    var on = 0;

    function tambah_harga() {
      if (on == 1) {
        on = 0;
        var harga = 0;
        $('.harga_new').attr('hidden',true);
        $('.ketr_ganti_harga').attr('hidden',true);
        $('#icon_harga').removeClass('fa fa-remove');
        $('#icon_harga').addClass('fa fa-plus');
        $('#btn_harga').removeClass('btn-danger');
        $('#btn_harga').addClass('btn-info');
        $('input[name=harga_new]').val('');
        $('input[name=ketr_ganti_harga]').val('');
        var qty = $('input[name=qty]').val();
        var potongan = $('input[name=potongan]').val(); 
        
        var cek = $('input[name=harga_new]').val(); 
        harga = $('input[name=harga_new]').val();
        
        if (!cek) {
            harga =  $('input[name=harga]').val();
        }
        
        hitung_subtotal(harga, qty, potongan);
      } else {
        on = 1;
        $('.harga_new').removeAttr('hidden');
        $('.ketr_ganti_harga').removeAttr('hidden');
        $('#icon_harga').removeClass('fa fa-plus');
        $('#icon_harga').addClass('fa fa-remove');
        $('#btn_harga').removeClass('btn-info');
        $('#btn_harga').addClass('btn-danger');
      }
    }

    function no_urut() {
      $.ajax({
              type : 'GET',
              url : '{{ route('no_urut_sj') }}',
              success : function (e) {
                console.log(e);
                $('#idBeli').val(e);
              }
      });
    }

    function add_pl() {
      var nama_pl = $('#namaPl').val();
      var alamat_pl = $('#alamatPl').val();
      var no_telp = $('#telpPl').val();
      var email = $('#emailPl').val();

      $.ajax({
        type : 'POST',
        url : '{{ route('sjdefault.add_pelanggan') }}',
        data : {
                '_namaPl' : nama_pl,
                '_alamatPl' : alamat_pl,
                '_telpPl' : no_telp,
                '_email' : email
              },
        success: function (e) {
          console.log(e);
          notif(e.code, e.msg);
          $('#add_pelanggan').modal('hide');
          html = '<option value='+e.id+'>'+e.nama_pl+'</option>';
          $('#idPelanggan').prepend(html);
          $("#idPelanggan option:first-child").attr("selected", true);
          $('#pelanggan_kosong').attr('hidden', true);
          // $("#idPelanggan option:first").after("<option>sdsadas</option>");
        }
      });
    }

    function simpan_sj() {
      var no_nota = $('input[name=id_beli]').val();
      var tgl = $('input[name=tgl]').val();
      // var pelanggan = $('input[name=pelanggan]').val();
      var id_pelanggan = $('#idPelanggan').val();
      var catatan = $('input[name=catatan]').val();
      var ongkir = $('input[name=ongkir]').val();

      var id_brg = $('input[name=id_brg]').val();
      var nama_brg = $('input[name=nama_brg]').val();
      var stok = $('input[name=stok]').val();
      var harga = $('input[name=harga]').val();
      var harga_new = $('input[name=harga_new]').val();
      var ketr_ganti_harga = $('input[name=ketr_ganti_harga]').val();
      var id_satuan = $('input[name=id_satuan]').val();
      var subtotal = $('input[name=subtotal]').val();
      var qty = $('input[name=qty]').val();
      var potongan = $('input[name=potongan]').val();
      var ketr = $('input[name=ketr]').val();
      var ketr_tambahan = $('input[name=ketr_tambahan]').val();

      var id_item = $('input[name=idQ]').val();

        $.ajax({
          type : 'POST',
          url : '{{ route('sjdefault.save') }}',
          data : {
                  '_noNota' : no_nota,
                  '_tgl' : tgl,
                  '_idPelanggan' : id_pelanggan,
                  // '_pelanggan' : pelanggan,
                  '_catatan' : catatan,
                  '_ketrTambahan' : ketr_tambahan,
                  '_ongkir' : ongkir,
                  '_idBrg' : id_brg,
                  '_namaBrg' : nama_brg,
                  '_stok' : stok,
                  '_harga' : harga,
                  '_hargaNew' : harga_new,
                  '_ketrGantiHarga' : ketr_ganti_harga,
                  '_idSatuan' : id_satuan,
                  '_subtotal' : subtotal,
                  '_qty' : qty,
                  '_potongan' : potongan,
                  '_ketr' : ketr,
                  '_idItem' : id_item
                },
          success: function (e) {
            clear_barang();
            notif(e.code, e.msg);
            tb_barang_detail.draw();
            tb_barang.draw();
            $('input[name=idQ]').val('');
            hitung_total();
          }
        });
    }

    function hitung_total() {
      var id = $('input[name=id_beli]').val();

      $.ajax({
        type: 'POST',
        url: '{{ route('sjdefault.total') }}',
        data: { '_id' : id },
        success : function (e) {
         console.log(e);
         
          $('#_ongkir').val(e.ongkir);
          $('#_subtotal').val(e.total);
          $('#_grandtotal').val(e.grandtotal);
          
        }
      })
    }

    $(document).on('click', '.brgQ', function(){  
      $('#modal_brg').modal('show');  
    });

    function select_brg(kode_brg,nama_brg,harga, id_satuan, satuan, sisa) {
      $('#modal_brg').modal('hide');
      $('input[name=id_brg]').val(kode_brg);
      $('input[name=nama_brg]').val(nama_brg);
      $('input[name=harga]').val(harga);
      $('input[name=id_satuan]').val(id_satuan);
      $('input[name=satuan]').val(satuan);
      $('input[name=stok]').val(sisa);
      clear_barang2();

      $('.harga_new').attr('hidden',true);
      $('.ketr_ganti_harga').attr('hidden',true);
      $('#icon_harga').removeClass('fa fa-remove');
      $('#icon_harga').addClass('fa fa-plus');
      $('#btn_harga').removeClass('btn-danger');
      $('#btn_harga').addClass('btn-info');
      $('input[name=harga_new]').val('');
      $('input[name=ketr_ganti_harga]').val('');

    }   

    function clear_barang() {
      $('input[name=no]').val('');
      $('input[name=id_brg]').val('');
      $('input[name=nama_brg]').val('');
      $('input[name=harga]').val('');
      $('input[name=harga_new]').val('');
      $('input[name=ketr_ganti_harga]').val('');
      $('input[name=id_satuan]').val('');
      $('input[name=satuan]').val('');
      $('input[name=stok]').val('');
      clear_barang2();
    }

    function new_barang() {
      $('input[name=id_brg]').val('');
      $('input[name=idQ]').val('');
      clear_barang();
    }
    
    function clear_barang2() {
      $('input[name=qty]').val('');
      $('input[name=ketr]').val('');
      $('input[name=ketr_tambahan]').val('');
      $('input[name=potongan]').val('');
      $('input[name=subtotal]').val('');
    }

    function delete_item(id) {
      $.ajax({
        type : 'POST',
        url : '{{ route('sjdefault.delete_item') }}',
        data : { '_idItem' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          tb_barang_detail.draw();
          tb_barang.draw();
          hitung_total();
        }
      });
    }

    $('#modal_checkout').on('shown.bs.modal', function (event) {
      var grandtotal = $('#_grandtotal').val();
      console.log(grandtotal);

      $('#TotalBayar').val(grandtotal);
    });

    $(document).on('keyup', '#Dibayar', function(){ 
      var grandtotal = $('#_grandtotal').val();
      var bayar = $(this).val();

      var grandtotal_ = grandtotal.replaceAll('.','');
      var kembalian = bayar - grandtotal_;
      
      $('#Kembalian').val(kembalian)
      console.log(kembalian); 
    });

    function select_item(id,kode_brg,nama,id_satuan,satuan,harga,qty,potongan,ketr,ketr_tambahan,subtotal,ketr_ganti_harga,harga_new) {
        $('input[name=idQ]').val(id);
        $('input[name=id_brg]').val(kode_brg);
        $('input[name=nama_brg]').val(nama);
        $('input[name=id_satuan]').val(id_satuan);
        $('input[name=satuan]').val(satuan);
        $('input[name=harga]').val(harga);
        $('input[name=qty]').val(qty);
        $('input[name=potongan]').val(potongan);
        $('input[name=ketr]').val(ketr);
        $('input[name=ketr_tambahan]').val(ketr_tambahan);
        $('input[name=subtotal]').val(subtotal);

        if (!harga_new) {
          $('.harga_new').attr('hidden',true);
          $('.ketr_ganti_harga').attr('hidden',true);
          $('input[name=harga_new]').val('')
          $('input[name=ketr_ganti_harga]').val('')
          $('#icon_harga').removeClass('fa fa-remove');
          $('#icon_harga').addClass('fa fa-plus');
          $('#btn_harga').removeClass('btn-danger');
          $('#btn_harga').addClass('btn-info');
        } else {
          $('.harga_new').removeAttr('hidden');
          $('.ketr_ganti_harga').removeAttr('hidden');
          $('#icon_harga').removeClass('fa fa-plus');
          $('#icon_harga').addClass('fa fa-remove');
          $('#btn_harga').removeClass('btn-info');
          $('#btn_harga').addClass('btn-danger');
          $('input[name=harga_new]').val(harga_new)
          $('input[name=ketr_ganti_harga]').val(ketr_ganti_harga)
        }
      }

      function simpan_checkout() {
        var no_nota = $('input[name=id_beli]').val();
        var bayar = $('input[name=dibayar]').val();
        var kembalian =$('input[name=kembalian]').val();

        $.ajax({
          type : 'POST',
          url : '{{ route('sjdefault.update_checkout') }}',
          data : {
                  '_noNota' : no_nota,
                  '_bayar' : bayar,
                  '_kembalian' : kembalian
                },
          success: function (e) {
            notif(e.code, e.msg);
            $('#modal_checkout').modal('hide');
            $('#_cash').val(e.bayar);
            $('#_kembalian').val(e.kembalian);
            console.log(e.bayar);
            window.location.href = '{{ route('sjdefault.index') }}';
          }
        });
      }
      // function print_sj() {
      //   var id =  $('input[name=id]').val();
      //   window.location.href = '/ruko/public/dashboard/sjdefault/nota/'+id;
      // }

        var on = 0;
        function pilih_member() {
          if (on == 1) {
            on = 0;
            $('.form_member').attr('hidden',true);
            $('#idPelanggan').val('');
            $("#Pelanggan").prop('disabled', false);
            // $('.form_pelanggan').removeAttr('hidden');
            console.log(on);
          } else {
            on = 1;
            $('.form_member').removeAttr('hidden');
            $('#Pelanggan').val('');
            $("#Pelanggan").prop('disabled', true);

            // $('.form_pelanggan').attr('hidden',true);
            console.log(on);
          }
        }

      $('#JenisPengiriman').change(function() {
      var jenisPengiriman = $(this).val();

      if ( jenisPengiriman == 'Kirim' ) {
        $('.Jpengiriman').attr('hidden',true);
      } else {
        $('#idKendaraan').val('');
        $('#idKaryawan').val('');
        $('.Jpengiriman').removeAttr('hidden');
      }
    });
      

  </script>
@endpush