@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

    {{-- Modal Cek Nota --}}
    <div id="modal_detail_sj" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cek SJ</h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
            {{-- Kiri --}}
            <div class="col-md-6 col-sm-6">
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                </label>
                <div class="col-md-7 ">
                  <input type="hidden" id="formSj" name="" class="form-control" readonly>
                  <input type="text" id="" name="id" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal
                </label>
                <div class="col-md-7">
                  <input type="text" id="tgl" name="tgl" class="form-control" readonly>
                </div>
              </div>
              <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kepada
                </label>
                <div class="col-md-7">
                  <input type="text" id="Pelanggan" name="pelanggan" class="form-control" readonly>
                </div>
              </div>
              {{-- <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Catatan Khusus
                </label>
                <div class="col-md-7">
                  <input type="text" id="" name="catatan" class="form-control" readonly>
                </div>
              </div> --}}
              <!-- <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Catatan Khusus
                </label>
                <div class="col-md-7">
                  <input type="text" id="" name="status" class="form-control" readonly>
                </div>
              </div> -->
            </div>

            </form>
            <div class="clearfix"></div>
            <div class="ln_solid"></div>
            <table id="tb_detail_sj" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <!-- <th>Ketr</th> -->
                      <th>Satuan</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Harga Baru</th>
                      <th>pot</th>
                      <th>Ketr Tambahan</th>
                      {{-- <th>Ketr Ganti Harga</th> --}}
                      <th>Subtotal</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
          <div class="clearfix"></div>
              <div class="ln_solid"></div>
              <div class="col-md-6 col-sm-6"></div>
              <div class="col-md-6 col-sm-6">
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Subtotal
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="_total" name="total" class="form-control" readonly>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Ongkir
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="_ongkir" name="ongkir" class="form-control" readonly>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Grand Total
                  </label>
                  <div class="col-md-6 col-sm-6 ">
                    <input type="text" id="_grandtotal" name="grandtotal" class="form-control" readonly>
                  </div>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            {{-- <button type="button" class="btn btn-primary" id="btn-setuju" onclick="acc_sj()" >Setuju</button> --}}
            {{-- <button type="button" class="btn btn-danger " id="btn-batal" onclick="batal_sj()" >Batal</button> --}}
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    {{-- Akhir modal cek nota --}}

    <!-- Awal Modal tanda Terima -->
    <div class="modal fade bs-example-modal-lg" id="modal_tanda_terima" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tanda Terima</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No. Nota <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="noNota" name="no_nota" class="form-control" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total Bayar <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="totalBayar" name="total_bayar" required="required" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Tanggal <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Tanggal" name="tanggal" required="required" class="form-control" autocomplete="off">
                    </div>
                  </div>
                  @php
                      $id_plg = !empty($id_pelanggan) ? $id_pelanggan : '';
                      $pmb = !empty($pembayaran) ? $pembayaran : '';
                      $png = !empty($pengiriman) ? $pengiriman : '';
                      $knd = !empty($id_kendaraan) ? $id_kendaraan : ''; 
                      $kry = !empty($id_karyawan) ? $id_karyawan : '';
                      $no_rek = !empty($no_rek) ? $no_rek : '';
                      $opsi =  !empty($opsi) ? $opsi : '';
                  @endphp
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pembayaran <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <select class="form-control" id="jenisPembayaran" name="pembayaran" autocomplete="off" required='required'>
                        <option></option>
                        <option>Tunai</option>
                        <option>Transfer</option>
                      </select>
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Uang Sebesar <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="uangSebesar" name="uang_sebesar" class="form-control" autocomplete="off" >
                    </div>
                  </div>
                  <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kembalian <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                      <input type="text" id="Kembalian" name="kembalian" class="form-control" autocomplete="off" readonly>
                    </div>
                  </div>
                 
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="btn-setuju" onclick="simpan_tanda_terima()" >OK</button>
                  </div>
                </form>

              </div>
          </div>
      </div>
    </div>
    <!-- Akhir Modal Tanda Terima -->

    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Surat Jalan Default</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('sjdefault.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
              <a href="{{ route('transaksi.index') }}" class="btn btn-sm btn-primary">Transaksi</a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                <label class="col-form-label col-md-3 label-align" for="first-name">Tanggal</label>
                <div class="col-md-2">
                  <input type="text" id="tglExport_m" name="tanggal_export" class="form-control" autocomplete="off">
                </div>
                <div class="col-md-2">
                  <input type="text" id="tglExport_a" name="tanggal_export" class="form-control" autocomplete="off">
                </div>
                  <button type="button" onclick="export_excel()" class="btn btn-primary">Excel</button>
                  {{-- <button type="button" onclick="delete_tgl()" class="btn btn-danger">Hapus</button> --}}
                </div>
                
                <div class="card-box table-responsive">
                  <table id="tbSuratJalan" class="table table-sm table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th style="width:10px;">No. </th>
                        <th>tgl</th>
                        <th>Kepada</th>
                        <th>Tagihan</th>
                        {{-- <th>Pembayaran</th> --}}
                        <th>Status Bayar</th>
                        <th>Cek Sj</th>
                        <th>Cek Batal</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">
   $('#Tanggal').datetimepicker({
      format: 'DD-MM-YYYY'
    });

      $('#tglExport_m').datetimepicker({
        format: 'YYYY-MM-DD'
      });

      $('#tglExport_a').datetimepicker({
        format: 'YYYY-MM-DD'
      });
    // console.log('uyeee');
    var datatableSj = $('#tbSuratJalan').DataTable({
        processing: true,
        serverSide: true,
        order: false,
        ajax: {
            type: 'GET',
            url: '{{ route('sjdefault.datatable') }}', 
        },
        columns: [
          { data: 'id', name: 'id' },
          { data: 'tgl', name: 'tgl' },
          { data: 'nama', name: 'nama' },
          { data: 'total', name: 'total' },
          // { data: 'pembayaran', name: 'pembayaran' },
          { data: 'status_bayar', name: 'status_bayar' },
          { data: 'cek_sj', name: 'cek_sj' },
          { data: 'batal', name: 'batal' },
          { data: 'opsi', name: 'opsi' }
        ]
    });

    function delete_suratjalan(id) {
      $.ajax({
        type    : 'GET',
        url     : '{{ route('sjdefault.delete') }}',
        data    : { '_idSj' : id },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableSj.draw(); 
        }
      });
    }

    $('#modal_detail_sj').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var form = $button.data('form');
     var id = $button.data('id');
     var tgl = $button.data('tgl');
     var pelanggan = $button.data('pelanggan');
     var catatan = $button.data('catatan');
     var pembayaran = $button.data('pmb');
     var pengiriman =  $button.data('png');
     var id_kendaraan = $button.data('id_kend');
     var id_karyawan = $button.data('id_kar');
     var total = $button.data('total');
     var ongkir = $button.data('ongkir');
     var grandtotal = $button.data('grandtotal');
     var bank = $button.data('bank');
     var cek_nota = $button.data('cek');
     
     console.log(form);
    //  if(form == 'cek_sj') {
      $('input[name=id]').val(id);
      $('input[name=tgl]').val(tgl);
      $('input[name=pelanggan]').val(pelanggan);
      $('input[name=catatan]').val(catatan);
      $('input[name=pembayaran]').val(pembayaran);
      $('input[name=pengiriman]').val(pengiriman);
      $('input[name=id_kendaraan]').val(id_kendaraan);
      $('input[name=id_karyawan]').val(id_karyawan);
      $('input[name=total]').val(total);
      $('input[name=ongkir]').val(ongkir);
      $('input[name=grandtotal]').val(grandtotal);  
      $('input[name=bank]').val(bank);    
      $('input[name=status]').val(cek_nota);     

      if(cek_nota == 'acc') {
        $('#btn-setuju').attr('hidden', true);
        $('#btn-batal').attr('hidden', true);
      } else {
        $('#btn-setuju').removeAttr('hidden');
        $('#btn-batal').removeAttr('hidden',);
      }

      datatable_detail_sj.draw();

    //  } else if (form == 'cek_nota') {
      // $('input[name=id]').val(id);
      // $('input[name=tgl]').val(tgl);
    //  }
     
   });

   var datatable_detail_sj = $('#tb_detail_sj').DataTable({
      processing : true,
      serverSide : true,
      scrollY: true,
      ajax : {
        type : 'POST',
        url : '{{ route('sjdefault.datatable_sj') }}',
        data : function (e) {
          e._id = $('input[name=id]').val();
        }
      },
      columns : [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'nama_brg', name:'nama_brg' },
            // { data: 'ketr', name:'ketr' },
            { data: 'satuan', name:'satuan' },
            { data: 'qty', name:'qty' },
            { data: 'harga', name:'harga' },
            { data: 'harga_new', name:'harga_new' },
            { data: 'potongan', name:'potongan' },
            { data: 'ketr_tambahan', name:'ketr_tambahan' },
            // { data: 'ketr_ganti_harga', name:'ketr_ganti_harga' },
            { data: 'subtotal', name:'subtotal' },
      ]
    }); 

    function export_excel() {
      var tgl_m = $('#tglExport_m').val();
      var tgl_a = $('#tglExport_a').val();

      var tgl_gabung = tgl_m+'&'+tgl_a;

      var url = '{{ route('sjdefault.excel_sj', ['.p']) }}';
      var url_fix = url.replaceAll('.p',tgl_gabung);
      // console.log(url_fix);
      window.location.href = url_fix;
    }

    function delete_tgl() {
      var tgl_m = $('#tglExport_m').val();
      var tgl_a = $('#tglExport_a').val();
      $.ajax({

        
        type    : 'POST',
        url     : '{{ route('sjdefault.delete_tgl') }}',
        data    : { '_tglM' : tgl_m,
                    '_tglA' : tgl_a,
                  },
        success : function (e) {
          notif(e.response.code, e.response.msg);
          datatableSj.draw(); 
        }
      })
    }

    $(document).on('click', '.print_nota', function() {
      var id_sj = $(this).attr('id');
      var jenis = 'print_nota';
      $.ajax({
        type    : 'POST',
        url     : '{{ route('sjdefault.print_sj_count') }}',
        data    : { '_idSj' : id_sj,
                    '_jenis' : jenis },
        success : function (e) {
          notif(e.code, e.msg);
          datatableSj.ajax.reload(); 
            window.location.href = 'sjdefault/nota/'+id_sj;
        }
      });
    });

    $('#modal_tanda_terima').on('shown.bs.modal', function (event) {
      var $button = $(event.relatedTarget);
      var id = $button.data('id');
      var total = $button.data('total');
      $('input[name=no_nota]').val(id);
      $('input[name=total_bayar]').val(total);
    });

    $(document).on('keyup', '#uangSebesar', function(){ 
      var grandtotal = $('#totalBayar').val();
      var bayar = $(this).val();

      var grandtotal_ = grandtotal.replaceAll('.','');
      var kembalian = bayar - grandtotal_;
      
      $('#Kembalian').val(kembalian)
      console.log(kembalian); 
    });


    function simpan_tanda_terima() {
      var pembayaran = $('#jenisPembayaran').val();
      var no_nota = $('#noNota').val();
      var tanggal = $('#Tanggal').val();
      var total_bayar = $('#totalBayar').val();
      var uang_sebesar = $('#uangSebesar').val();
      var kembalian = $('#Kembalian').val();

      $.ajax({
          type : 'POST',
          url : '{{ route('sjdefault.simpan_tanda_terima') }}',
          data : {
                  '_noNota' : no_nota,
                  '_tanggal' : tanggal,
                  '_totalBayar' : total_bayar,
                  '_pembayaran' : pembayaran,
                  '_uangSebesar' : uang_sebesar,
                  '_kembalian' : kembalian
                },
          success: function (e) {
            notif(e.code, e.msg);
            $('#modal_tanda_terima').modal('hide');
            datatableSj.draw();
            clear_tanda_terima();
            // window.location.href = '{{ route('sjdefault.index') }}';
            console.log(e);
          }
        });
    }

    function clear_tanda_terima() {
      $('#Tanggal').val('');
      $('#jenisPembayaran').val('');
      $('#uangSebesar').val('');
      $('#Kembalian').val('');
    }

  </script>
@endpush