@extends('master.ella')
    
@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    
    {{-- notif --}}
    @if (session('response'))
        @push('js')
          <script type="text/javascript">
            notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
          </script>
        @endpush
    @endif
    {{-- notif --}}

            {{-- Awal Modal Klaim Absen --}}
            <div id="modal_claim" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
        
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Detail Absensi</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="" action="#" method="#" data-parsley-validate class="form-verical form-label-left">
                    {{-- Kiri --}}
                    <div class="col-md-6 col-sm-6">
                      <div class="item form-group">
                        <label class="col-form-label col-md-5 col-sm-5 label-align" for="first-name">Tgl <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                          <input type="text" id="Tgl" name="tgl" class="form-control" readonly>
                        </div>
                      </div>
                    </div>
                    </form>
                    <div class="card-box table-responsive">
                      <table id="tbDetailAbsen" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th>No.</th>
                            <th>Tgl</th>
                            <th>Kodep</th>
                            <th>Jam Hadir</th>
                            <th>Jam Pulang</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="claim_absen()">Claim</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
        
                </div>
              </div>
            </div>
            {{-- Akhir Modal Klaim Absen --}}

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Klaim Absensi</h2>
            <ul class="nav navbar-right panel_toolbox">
              <a href="{{ route('claimabsen.form') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-12">
                  <table id="tb_absen" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tgl</th>
                        <th>Total Claim</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
        
@endsection

@push('js')
  <script type="text/javascript">  
    var tb_absen = '';

    tb_absen = $('#tb_absen').DataTable({
      processing    : true,
      serverSide    : true,
      ajax  : {
          type : 'GET',
          url : '{{ route('claimabsen.datatable') }}',

      },
      columns: [
        { data: 'DT_RowIndex', name: 'DT_RowIndex' },
        { data: 'tgl', name: 'tgl' },
        { data: 'total_claim', name:'total_claim' },
        { data: 'opsi', name: 'opsi' }
      ]
    });

    $('#modal_claim').on('shown.bs.modal', function (event) {
     var $button = $(event.relatedTarget);
     var tgl = $button.data('tgl');
     
     console.log(tgl);
      $('input[name=tgl]').val(tgl);
      tb_detail_absen.draw();
   });

    var tb_detail_absen = $('#tbDetailAbsen').DataTable({
      processing : true,
      serverSide : true,
      scrollY: true,
      ajax : {
        type : 'POST',
        url : '{{ route('claimabsen.datatable_detail_absen') }}',
        data : function (e) {
          e._tgl = $('#Tgl').val();
        }
      },
      columns : [
            { data: 'cekbox', name: 'cekbox' },
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'tgl', name: 'tgl' },
            { data: 'kodep', name: 'kodep' },
            { data: 'jamhadir', name: 'jamhadir' },
            { data: 'jampulang', name: 'jampulang' },            
      ]
    }); 

    function delete_absen(tgl) {
      var txt;
      var r =  confirm('Apakah Anda yakin ingin menghapus?');

      if (r == true) {
        $.ajax({
            type : 'POST',
            url : '{{ route('claimabsen.delete') }}',
            data : { '_tgl' : tgl },
            success : function (e) {
              notif(e.response.code, e.response.msg);
              tb_absen.draw();
            }
        });
      } else {
        txt = 'n';
      } 
    }

    function claim_absen() {
      var tgl = $('#Tgl').val();
      dtt = [];
      $('input:checkbox').each(function () {
        var $this = $(this);
        id = $this.val();

        if ($(this).prop('checked')) {
          inp = {
            'id' : id,
          }
          dtt.push(inp);
        }
      });
      console.log(dtt);
      $.ajax({
        type: 'POST',
        url: '{{ route('claimabsen.simpan_claim')}}',
        data : { 
                  '_kodep' : dtt,
                  '_tgl' : tgl
                },
        success: function (e) {
          notif(e.code, e.msg);
          $('#modal_claim').modal('hide');
          tb_absen.draw();
        }
      })
    }

  </script>
@endpush