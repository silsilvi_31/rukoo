<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3>General</h3>
      @php
      $username = Session::get('username');
      // dd($username);
      $fitur = 'hidden';
      if ($username == 'pimpinan' || $username == '6102' ||$username == 'admin1' || $username == 'admin2' || $username == 'admin3') {
        $fitur = '';
      }
      @endphp
      <ul class="nav side-menu">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-home"></i> General <span class="label label-success pull-right"></span></a></li>
        @php
            $username = Session::get('username');
            // dd($username);
            $fitur = 'hidden';
            if ($username == 'pimpinan' || $username == '6102' ||$username == 'admin1' || $username == 'admin2' || $username == 'admin3') {
              $fitur = '';
            }
        @endphp

        <li><a><i class="fa fa-table"></i> Penjualan <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{ route('sjdefault.index') }}"><i class="fa fa-align-justify"></i> Triplek <span class="label label-success pull-right"></span></a></li>
            <li><a href="{{ route('ceksj.index') }}"><i class="fa fa-check-square"></i> Cek Triplek <span class="label label-success pull-right"></span></a></li>
            <li><a href="{{ route('retur.list') }}"><i class="fa fa-table"></i> Retur <span class="label label-success pull-right"></span></a></li>
          </ul>
        </li> 
        
        <li><a><i class="fa fa-table"></i> Pembelian <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{ route('pembelian.index') }}"><i class="fa fa-list-ul"></i> Pembelian <span class="label label-success pull-right"></span></a></li>
            <li><a href="{{ route('cekbeli.index') }}"><i class="fa fa-check-square"></i> Cek Beli <span class="label label-success pull-right"></span></a></li>
          </ul>
        </li> 

        <li><a><i class="fa fa-sitemap"></i> Stok <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{ route('stok.index') }}"><i class="fa fa-file-o"></i> Stok <span class="label label-success pull-right"></span></a></li>
            <li><a href="{{ route('stokOpname.index') }}"><i class="fa fa-laptop"></i> Opname <span class="label label-success pull-right"></span></a></li>
            <li><a href="{{ route('cekopname.index') }}"><i class="fa fa-check-square"></i> Cek Opname <span class="label label-success pull-right"></span></a></li>
          </ul>
        </li>
        
        <li><a><i class="fa fa-table"></i> Gaji <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{ route('absen.index') }}"><i class="fa fa-list-ul"></i> Absen <span class="label label-success pull-right"></span></a></li>
            <li><a href="{{ route('claimabsen.index') }}"><i class="fa fa-check-square"></i> Klaim Absen <span class="label label-success pull-right"></span></a></li>
            <li><a href="{{ route('gaji.index') }}"><i class="fa fa-list-ul"></i> Gaji <span class="label label-success pull-right"></span></a></li>
          </ul>
        </li>

        <li><a><i class="fa fa-sitemap"></i> Kwitansi <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{ route('kwitansi.index') }}"><i class="fa fa-calculator"></i> Kwitansi <span class="label label-success pull-right"></span></a></li>
            <li><a href="{{ route('cekkwitansi.index') }}"><i class="fa fa-check-square"></i> Cek Kwitansi <span class="label label-success pull-right"></span></a></li>
          </ul>
        </li>

        {{-- <li><a href="{{ route('setor.index') }}"><i class=s"fa fa-money"></i> Setor <span class="label label-success pull-right"></span></a></li> --}}

        <li><a href="{{ route('jurnalUmum.index') }}"><i class="fa fa-list"></i> Jurnal Umum <span class="label label-success pull-right"></span></a></li>
        
        {{-- <li><a><i class="fa fa-road"></i> SJ <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{ route('sjdefault.index') }}">Triplek</a></li>
            <li><a href="{{ route('PrintSj.index') }}">SJ Triplek</a></li>
            <li {{ $fitur }}><a href="{{ route('ceksj.index') }}">Cek SJ</a></li>
          </ul>
        </li> --}}
        <li><a href="{{ route('jurnal.index') }}"><i class="fa fa-calculator"></i> Jurnal <span class="label label-success pull-right"></span></a></li>
        {{-- <li><a href="{{ route('neraca.index') }}"><i class="fa fa-calculator"></i> Neraca <span class="label label-success pull-right"></span></a></li> --}}
        <li><a href="{{ route('laporan.index') }}"><i class="fa fa-laptop"></i> Laporan <span class="label label-success pull-right"></span></a></li>
        <li><a href="{{ route('tutupBuku.index') }}"><i class="fa fa-file"></i> Tutup Buku <span class="label label-success pull-right"></span></a></li>
      </ul>
    </div>

    <div class="menu_section">
      <h3>Akuntansi</h3>
      <ul class="nav side-menu">
        <li><a href=" {{ route('akuntansi.index') }} "><i class="fa fa-windows"></i> List <span class="label label-success pull-right"></span></a></li>               
      </ul>
    </div>

    <div class="menu_section">
      <h3>Live On</h3>
      <ul class="nav side-menu">
        <li><a><i class="fa fa-sitemap"></i> Manage <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href=" {{ route('karyawan.index') }} ">Karyawan</a>
            </li>
          </ul>
        </li>   
        <li><a><i class="fa fa-bug"></i> Master <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href=" {{ route('akun.index') }} ">Daftar Akun</a></li>
            <li><a href=" {{ route('jabatan.index') }} ">Jabatan</a></li>
            <li><a href=" {{ route('satuan.index') }} ">Satuan</a></li>
            <li><a href=" {{ route('kendaraan.index') }} ">Kendaraan</a></li>
            <li><a href=" {{ route('jenisbarang.index') }} ">Jenis Barang</a></li>
            <li><a href=" {{ route('barang.index') }} ">Barang</a></li>
            <li><a href=" {{ route('peralatan.index') }} ">Peralatan</a></li>
            <li><a href=" {{ route('inventaris.index') }} ">Inventaris</a></li>
            <li><a href=" {{ route('bangunan.index') }} ">Bangunan</a></li>
            <li><a href=" {{ route('instalasi.index') }} ">Instalasi</a></li>
            <li><a href=" {{ route('pelanggan.index') }} ">Pelanggan</a></li>
            <li><a href=" {{ route('suplier.index') }} ">Suplier</a></li>
            <li><a href=" {{ route('bank.index') }} ">Bank</a></li>
          </ul>
        </li>
        <li><a href=" {{ route('account.index') }} "><i class="fa fa-windows"></i> Account <span class="label label-success pull-right"></span></a></li>               
        <li><a href=" {{ route('account_pimpinan.index') }} "><i class="fa fa-windows"></i> Account (Pimpinan)<span class="label label-success pull-right"></span></a></li>               
      </ul>
    </div>

  </div>
  <!-- /sidebar menu -->


{{-- 
  <li><a><i class="fa fa-sitemap"></i> Manage <span class="fa fa-chevron-down"></span></a>
    <ul class="nav child_menu">
        <li><a href="#level1_1">Level One</a>
        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li class="sub_menu"><a href="level2.html">Level Two</a>
            </li>
            <li><a href="#level2_1">Level Two</a>
            </li>
            <li><a href="#level2_2">Level Two</a>
            </li>
          </ul>
        </li>
        <li><a href="#level1_2">Level One</a>
        </li>
    </ul>
  </li> --}}