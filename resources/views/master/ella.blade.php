<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wijaya | Administrasi</title>

    <!-- Bootstrap -->
    <link href="{{ URL::asset('/') }}ella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ URL::asset('/') }}ella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ URL::asset('/') }}ella/vendors/nprogress/nprogress.css" rel="stylesheet">
     <!-- bootstrap-daterangepicker -->
     <link href="{{ URL::asset('/') }}ella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
     <!-- bootstrap-datetimepicker -->
    <link href="{{ URL::asset('/') }}ella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="{{ URL::asset('/') }}ella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- iCheck -->
    <link href="{{ URL::asset('/') }}ella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="{{ URL::asset('/') }}ella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="{{ URL::asset('/') }}ella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="{{ URL::asset('/') }}ella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="{{ URL::asset('/') }}ella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="{{ URL::asset('/') }}ella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- PNotify -->
    <link href="{{ URL::asset('/') }}ella/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="{{ URL::asset('/') }}ella/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="{{ URL::asset('/') }}ella/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ URL::asset('/') }}ella/build/css/custom.min.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
  @php
      $id_user = Session::get('id_user');
  @endphp
  @if (!$id_user)
    <script>window.location = "/";</script>
  @endif
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Wpi!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ URL::asset('/') }}img/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Selamat Datang,</span>
                <h2> {{ session::get('nama') }} </h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            {{-- Menu --}}
            @php
              $username = Session::get('username');
              // dd($username);
              $fitur = 'n';
              if ($username == 'pimpinan' || $username == '6107' || $username == '6102' ||$username == 'admin1' || $username == 'admin2' || $username == 'admin3') {
                $fitur = 'y';
              }
            @endphp

            @if ($fitur == 'y')
              @include('master.menu')
            @else
              @include('master.menu_v2')
            @endif
           
            {{-- /Menu --}}
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a {{ ($fitur == 'n') ? 'hidden' : '' }} data-toggle="tooltip" data-placement="top" title="Settings" href="{{ route('menu.index') }}">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a {{ ($fitur == 'n') ? 'hidden' : '' }} data-toggle="tooltip" data-placement="top" title="Access" href="{{ route('akses.index') }}">
                <span class="glyphicon glyphicon-leaf" aria-hidden="true"></span>
              </a>
              <a {{ ($fitur == 'n') ? 'hidden' : '' }} data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('logout') }}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <div class="nav toggle">
                  <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>
                <nav class="nav navbar-nav">
                <ul class=" navbar-right">
                  <li class="nav-item dropdown open" style="padding-left: 15px;">
                    <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                      <img src="{{ URL::asset('/') }}img/img.jpg" alt="">{{ session::get('nama') }}
                    </a>
                    <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="javascript:;"> Profile</a>
                        <a class="dropdown-item"  href="javascript:;">
                          <span class="badge bg-red pull-right">50%</span>
                          <span>Settings</span>
                        </a>
                    <a class="dropdown-item"  href="javascript:;">Help</a>
                      <a class="dropdown-item"  href="{{ route('logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </div>
                  </li>
  
                  <li role="presentation" class="nav-item dropdown open">
                    <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                      <i class="fa fa-envelope-o"></i>
                      <span class="badge bg-green">6</span>
                    </a>
                    <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="{{ URL::asset('/') }}img/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="{{ URL::asset('/') }}img/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="{{ URL::asset('/') }}img/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a class="dropdown-item">
                          <span class="image"><img src="{{ URL::asset('/') }}img/img.jpg" alt="Profile Image" /></span>
                          <span>
                            <span>John Smith</span>
                            <span class="time">3 mins ago</span>
                          </span>
                          <span class="message">
                            Film festivals used to be do-or-die moments for movie makers. They were where...
                          </span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <div class="text-center">
                          <a class="dropdown-item">
                            <strong>See All Alerts</strong>
                            <i class="fa fa-angle-right"></i>
                          </a>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        <!-- /top navigation -->

        <!-- page content -->
        {{-- notif --}}
        @if (session('response'))
            @push('js')
              <script type="text/javascript">
                notif({{ session('response')['code'] }}, "{{ session('response')['msg'] }}");
              </script>
            @endpush
        @endif
        {{-- notif --}}
        @yield('content')
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{ URL::asset('/') }}ella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="{{ URL::asset('/') }}ella/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="{{ URL::asset('/') }}ella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="{{ URL::asset('/') }}ella/vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ URL::asset('/') }}ella/vendors/moment/min/moment.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="{{ URL::asset('/') }}ella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="{{ URL::asset('/') }}ella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="{{ URL::asset('/') }}ella/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- iCheck -->
    <script src="{{ URL::asset('/') }}ella/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/jszip/dist/jszip.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- PNotify -->
    <script src="{{ URL::asset('/') }}ella/vendors/pnotify/dist/pnotify.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="{{ URL::asset('/') }}ella/vendors/pnotify/dist/pnotify.nonblock.js"></script>

    <!-- Chart -->
    {{-- <script src="{{ URL::asset('/') }}ella/vendors/echarts/dist/echarts.min.js"></script> --}}
    <script src="{{ URL::asset('/') }}ella/vendors/Chart.js/dist/Chart.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ URL::asset('/') }}ella/build/js/custom.js"></script>
    <script src="{{ URL::asset('/') }}ella/build/js/inputmask.js"></script>
  
    <script>
      $.ajaxSetup({
        headers:{
          'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        }
      });
    
      function notif(code, msg) {
      if (code == 200) {
      new PNotify({
          title: 'Success',
          text: msg,
          type: 'success',
          delay : 2500,
          styling: 'bootstrap3'
        });
      }else if (code == 201) {
        new PNotify({
            title: 'Info',
            text: msg,
            type: 'info',
            delay : 2500,
            styling: 'bootstrap3'
          });
      }else if (code == 300) {
        new PNotify({
            title: 'Warning',
            text: msg,
            delay : 2500,
            styling: 'bootstrap3'
          });
      }else if (code == 400) {
        new PNotify({
            title: 'Error',
            text: msg,
            type: 'error',
            delay : 2500,
            styling: 'bootstrap3'
          });
      }
    }

    </script>
    @stack('js')
  </body>
</html>