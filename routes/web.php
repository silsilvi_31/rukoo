<?php

Route::get('/tes', 'SampleController@menu_q');


Route::get('/lps', 'SjDefaultController@is_lps');

Route::get('/', 'LoginController@login')->name('login');
Route::post('login', 'LoginController@post_login')->name('login.post');
// Route::get('/add', 'LoginController@add_user')->name('l');

Route::get('logout', 'LoginController@logout')->name('logout');

Route::group(['prefix' => 'dashboard'], function () {
    Route::get('/', 'HomeController@index')->name('dashboard.index');
    Route::post('grafik', 'HomeController@grafik')->name('dashboard.grafik');
    
    //Manage
    Route::group(['prefix' => 'manage'], function () {
        //Karyawan
        Route::group(['prefix' => 'karyawan'], function () {
            Route::get('/', 'KaryawanController@index')->name('karyawan.index');
            Route::get('datatable', 'KaryawanController@datatable')->name('karyawan.datatable');
            Route::post('save', 'KaryawanController@save')->name('karyawan.save');
            Route::post('delete', 'KaryawanController@delete')->name('karyawan.delete');
        });
    });
    //Master
    Route::group(['prefix' => 'master'], function () {
        //Daftar akun
        Route::group(['prefix' => 'akun'], function () {
            Route::get('/', 'AkunController@index')->name('akun.index');
            Route::get('datatable', 'AkunController@datatable')->name('akun.datatable');
            Route::get('list', 'AkunController@akun_list')->name('akun.list');
            Route::get('form', 'AkunController@form')->name('akun.form');
            Route::post('save', 'AkunController@form')->name('akun.save');

            Route::get('formQ', 'AkunController@formQ')->name('akun.formQ');
            Route::post('saveQ', 'AkunController@saveQ')->name('akun.saveQ');
            Route::get('listQ', 'AkunController@akun_listQ')->name('akun.listQ');
        });

        //jenisbarang
        Route::group(['prefix' => 'jenisbarang'], function () {
            Route::get('/', 'JenisBarangController@index')->name('jenisbarang.index');
            Route::get('datatable', 'JenisBarangController@datatable')->name('jenisbarang.datatable');
            Route::get('form', 'JenisBarangController@form')->name('jenisbarang.form');
            Route::post('save', 'JenisBarangController@save')->name('jenisbarang.save');
            Route::get('form-edit/{id}', 'JenisBarangController@form_edit')->name('jenisbarang.form_edit');
            Route::post('update', 'JenisBarangController@update')->name('jenisbarang.update');
            Route::post('delete', 'JenisBarangController@delete')->name('jenisbarang.delete');
        });

        //barang
        Route::group(['prefix' => 'barang'], function () {
            Route::get('/', 'BarangController@index')->name('barang.index');
            Route::get('datatable', 'BarangController@datatable')->name('barang.datatable');
            Route::get('form', 'BarangController@form')->name('barang.form');
            Route::post('save', 'BarangController@save')->name('barang.save');
            Route::get('form-edit/{id}', 'BarangController@form_edit')->name('barang.form_edit');
            Route::post('update', 'BarangController@update')->name('barang.update');
            Route::post('delete', 'BarangController@delete')->name('barang.delete');
        });

       //bkkepada
        Route::group(['prefix' => 'bkkepada'], function () {
            Route::get('/', 'BkKepadaController@index')->name('bkkepada.index');
            Route::get('datatable', 'BkKepadaController@datatable')->name('bkkepada.datatable');
            Route::get('form', 'BkKepadaController@form')->name('bkkepada.form');
            Route::post('save', 'BkKepadaController@save')->name('bkkepada.save');
            Route::get('form-edit/{id}', 'BkKepadaController@form_edit')->name('bkkepada.form_edit');
            Route::post('update', 'BkKepadaController@update')->name('bkkepada.update');
            Route::post('delete', 'BkKepadaController@delete')->name('bkkepada.delete');
        });

        //satuan
        Route::group(['prefix' => 'satuan'], function () {
            Route::get('/', 'SatuanController@index')->name('satuan.index');
            Route::get('datatable', 'SatuanController@datatable')->name('satuan.datatable');
            Route::get('form', 'SatuanController@form')->name('satuan.form');
            Route::post('save', 'SatuanController@save')->name('satuan.save');
            Route::get('form-edit/{id}', 'SatuanController@form_edit')->name('satuan.form_edit');
            Route::post('update', 'SatuanController@update')->name('satuan.update');
            Route::post('delete', 'SatuanController@delete')->name('satuan.delete');
        });    

        //jabatan
        Route::group(['prefix' => 'jabatan'], function () {
            Route::get('/', 'JabatanController@index')->name('jabatan.index');
            Route::get('datatable', 'JabatanController@datatable')->name('jabatan.datatable');        
            Route::get('form', 'JabatanController@form')->name('jabatan.form');        
            Route::post('save', 'JabatanController@save')->name('jabatan.save');        
            Route::get('form-edit/{id}', 'JabatanController@form_edit')->name('jabatan.form_edit');        
            Route::post('update', 'JabatanController@update')->name('jabatan.update');        
            Route::post('delete', 'JabatanController@delete')->name('jabatan.delete');        
        });

        //kendaraan
        Route::group(['prefix' => 'kendaraan'], function ()  {
            Route::get('/', 'KendaraanController@index')->name('kendaraan.index');
            Route::get('datatable', 'KendaraanController@datatable')->name('kendaraan.datatable');
            Route::get('form', 'KendaraanController@form')->name('kendaraan.form');
            Route::post('save', 'KendaraanController@save')->name('kendaraan.save');
            Route::get('form-edit/{id}', 'KendaraanController@form_edit')->name('kendaraan.form_edit');
            Route::post('update', 'KendaraanController@update')->name('kendaraan.update');
            Route::post('delete', 'KendaraanController@delete')->name('kendaraan.delete');        
        });

        // Pelanggan
        Route::group(['prefix' => 'pelanggan'], function () {
            Route::get('/', 'PelangganController@index')->name('pelanggan.index');
            Route::get('datatable', 'PelangganController@datatable')->name('pelanggan.datatable');
            Route::get('form', 'PelangganController@form')->name('pelanggan.form');
            Route::post('save', 'PelangganController@save')->name('pelanggan.save');
            Route::get('form-edit/{id}', 'PelangganController@form_edit')->name('pelanggan.form_edit');
            Route::post('update', 'PelangganController@update')->name('pelanggan.update');
            Route::post('delete', 'PelangganController@delete')->name('pelanggan.delete');
        });

        // Bank
        Route::group(['prefix' => 'bank'], function (){
            Route::get('/', 'BankController@index')->name('bank.index');
            Route::get('datatable', 'BankController@datatable')->name('bank.datatable');
            Route::get('form', 'BankController@form')->name('bank.form');
            Route::post('save', 'BankController@save')->name('bank.save');
            Route::get('form_edit/{id}', 'BankController@form_edit')->name('bank.form_edit');
            Route::post('update', 'BankController@update')->name('bank.update');
            Route::post('delete', 'BankController@delete')->name('bank.delete');

        });
    });

    //Account
    Route::group(['prefix' => 'account'], function () {
        Route::get('/', 'AccountController@index')->name('account.index');  
        Route::get('datatable', 'AccountController@datatable')->name('account.datatable');
        Route::get('karyawan-list', 'AccountController@karyawan_list')->name('karyawan.list');
        Route::get('form', 'AccountController@form')->name('account.form');  
        Route::post('save', 'AccountController@save')->name('account.save');
        Route::post('update', 'AccountController@update')->name('account.update');
        Route::post('akses-update', 'AccountController@akses_update')->name('account_akses.update');
        Route::post('permis-update', 'AccountController@permis_update')->name('account_permis.update');
        Route::post('permis-get', 'AccountController@permis_get')->name('account_permis.get');

        Route::post('delete', 'AccountController@delete')->name('account.delete');  

        Route::get('/pimpinan', 'AccountController@index_pimpinan')->name('account_pimpinan.index');  
        Route::get('/data-pimpinan', 'AccountController@data_pimpinan')->name('account_pimpinan.datatable');  
        Route::get('/form-pimpinan/{id}', 'AccountController@create_account_pimpinan')->name('account_pimpinan.form');  
        Route::post('/pimpinan', 'AccountController@save_account_pimpinan')->name('account_pimpinan.save'); 
        Route::post('/create-pimpinan', 'AccountController@createv_account_pimpinan')->name('account_pimpinan.create');  
    });

    //Surat Jalan
    Route::group(['prefix' => 'sjdefault'], function () {
        Route::get('/', 'SjDefaultController@index')->name('sjdefault.index');
        Route::get('datatable', 'SjDefaultController@datatable')->name('sjdefault.datatable');
        Route::get('form', 'SjDefaultController@form')->name('sjdefault.form');
        Route::post('save', 'SjDefaultController@save')->name('sjdefault.save');
        Route::get('form_edit/{id}', 'SjDefaultController@form_edit')->name('sjdefault.form_edit');
        Route::get('delete', 'SjDefaultController@delete')->name('sjdefault.delete');
        Route::get('no','SjDefaultController@no_urut')->name('no_urut_sj');
        Route::get('datatable_brg','SjDefaultController@datatable_brg')->name('sjdefault.datatable_brg');
        Route::post('datatable_brg_detail','SjDefaultController@datatable_brg_detail')->name('sjdefault.datatable_brg_detail');
        Route::post('detele_item', 'SjDefaultController@delete_item')->name('sjdefault.delete_item');
        Route::get('nota/{id}', 'SjDefaultController@nota')->name('sjdefault.nota');
        Route::get('sj/{id}', 'SjDefaultController@sj')->name('sjdefault.sj');
        Route::post('barang_nota', 'SjDefaultController@barang_nota')->name('sjdefault.barang_nota');
        Route::post('total', 'SjDefaultController@total')->name('sjdefault.total');
        Route::post('add_pelanggan', 'SjDefaultController@add_pelanggan')->name('sjdefault.add_pelanggan');
        Route::post('select_pelanggan', 'SjDefaultController@select_pelanggan')->name('sjdefault.select_pelanggan');
        Route::get('excel_sj/{tgl}','SjDefaultController@excel_sj')->name('sjdefault.excel_sj');
        Route::post('delete_tgl', 'SjDefaultController@delete_tgl')->name('sjdefault.delete_tgl');
        Route::post('update_checkout', 'SjDefaultController@update_checkout')->name('sjdefault.update_checkout');
        Route::post('datatable_sj', 'SjDefaultController@datatable_sj')->name('sjdefault.datatable_sj');
        Route::post('print_sj_count', 'SjDefaultController@print_sj_count')->name('sjdefault.print_sj_count');
        Route::post('simpan_tanda_terima', 'SjDefaultController@simpan_tanda_terima')->name('sjdefault.simpan_tanda_terima');

        Route::get('retur/{id}', 'SjDefaultController@form_retur')->name('retur.form');
        Route::post('datatable_detail_retur', 'SjDefaultController@datatable_detail_retur')->name('retur.datatable_detail_retur');
        Route::post('save_retur', 'SjDefaultController@save_retur')->name('retur.save');
        Route::get('list_retur', 'SjDefaultController@list_retur')->name('retur.list');
        Route::get('datatable_retur', 'SjDefaultController@datatable_retur')->name('retur.datatable_retur');
    });

    // cek
    Route::group(['prefix' => 'cek-sj'], function () {
        Route::get('/', 'CekSjController@index')->name('ceksj.index');
        Route::get('datatable', 'CekSjController@datatable')->name('ceksj.datatable');
        Route::post('datatable_sj', 'CekSjController@datatable_sj')->name('ceksj.datatable_sj');
        Route::post('acc_sj', 'CekSjController@acc_sj')->name('ceksj.acc_sj');
        Route::get('nota/{id}', 'CekSjController@nota')->name('ceksj.nota');
        Route::get('sj/{id}', 'CekSjController@sj')->name('ceksj.sj');
        Route::get('delete', 'CekSjController@delete')->name('ceksj.delete');
        Route::post('batal_suratjalan', 'CekSjController@batal_suratjalan')->name('ceksj.batal_suratjalan');
        Route::post('print_sj_count', 'CekSjController@print_sj_count')->name('ceksj.print_sj_count');
        
    });

    //jurnal
    Route::group(['prefix' => 'jurnal'], function () {
      Route::get('/', 'JurnalController@index')->name('jurnal.index');
      Route::post('datatable', 'JurnalController@datatable')->name('jurnal.datatable');
      Route::get('excel/{tgl}','JurnalController@excel_jurnal')->name('jurnal.excel');
      Route::post('export-rmv', 'JurnalController@export_rmv')->name('jurnal_export.rmv');      
      Route::post('export_excel_count', 'JurnalController@export_excel_count')->name('jurnal.export_excel_count');
      Route::post('total', 'JurnalController@total')->name('jurnal.total');
    });

    Route::group(['prefix' => 'setting'], function () {
        Route::group(['prefix' => 'menu'], function () {
            Route::get('/', 'MenuController@index')->name('menu.index');
            Route::get('datatable', 'MenuController@datatable')->name('menu.datatable'); 
            Route::get('form', 'MenuController@form')->name('menu.form');
            Route::post('save', 'MenuController@save')->name('menu.save');
            Route::get('edit/{id}', 'MenuController@edit')->name('menu.edit');
            Route::post('delete', 'MenuController@delete')->name('menu.delete');
        });

        Route::group(['prefix' => 'akses'], function () {
            Route::get('/', 'MenuController@akses')->name('akses.index');
            Route::get('datatable', 'MenuController@datatable_akses')->name('akses.datatable');
            Route::get('form', 'MenuController@akses_form')->name('akses.form');
            Route::post('save', 'MenuController@akses_save')->name('akses.save');
            Route::post('delete', 'MenuController@akses_delete')->name('akses.delete');
            Route::get('list-akses', 'MenuController@list_akses')->name('list_akses.datatable');
            Route::get('list-fitur', 'MenuController@list_fitur')->name('list_fitur.datatable');
            Route::post('item-fitur', 'MenuController@item_fitur')->name('item_fitur.datatable');
            Route::post('delete-fitur', 'MenuController@delete_fitur')->name('item_fitur.delete');
        });
    });

    //Surat Jalan untuk admin
    Route::group(['prefix' => 'print_sj'], function () {
        Route::get('/', 'PrintSjController@index')->name('PrintSj.index');
        Route::get('datatable', 'PrintSjController@datatable')->name('PrintSj.datatable');
        Route::get('form', 'PrintSjController@form')->name('PrintSj.form');
        Route::post('save', 'PrintSjController@save')->name('PrintSj.save');
        Route::get('form_edit/{id}', 'PrintSjController@form_edit')->name('PrintSj.form_edit');
        Route::get('delete', 'PrintSjController@delete')->name('PrintSj.delete');
        Route::get('no','PrintSjController@no_urut')->name('no_urut_sj');
        Route::get('datatable_brg','PrintSjController@datatable_brg')->name('PrintSj.datatable_brg');
        Route::post('datatable_brg_detail','PrintSjController@datatable_brg_detail')->name('PrintSj.datatable_brg_detail');
        Route::post('detele_item', 'PrintSjController@delete_item')->name('PrintSj.delete_item');
        Route::get('nota/{id}', 'PrintSjController@nota')->name('PrintSj.nota');
        Route::get('sj/{id}', 'PrintSjController@sj')->name('PrintSj.sj');
        Route::post('barang_nota', 'PrintSjController@barang_nota')->name('PrintSj.barang_nota');
        Route::post('total', 'PrintSjController@total')->name('PrintSj.total');
        Route::post('add_pelanggan', 'PrintSjController@add_pelanggan')->name('PrintSj.add_pelanggan');
        Route::post('select_pelanggan', 'PrintSjController@select_pelanggan')->name('PrintSj.select_pelanggan');
        Route::get('excel_sj/{tgl}','PrintSjController@excel_sj')->name('PrintSj.excel_sj');
        Route::post('delete_tgl', 'PrintSjController@delete_tgl')->name('PrintSj.delete_tgl');
        Route::post('print_sj_count', 'PrintSjController@print_sj_count')->name('PrintSj.print_sj_count');
    });

    Route::group(['prefix' => 'akuntansi'], function () {
        Route::get('/', 'ListAkunController@index')->name('akuntansi.index');
        Route::post('list', 'ListAkunController@list_akun_json')->name('akuntansi_list.json');
    });

    Route::group(['prefix' => 'stok'], function () {
        Route::get('/', 'StokController@index')->name('stok.index');
        Route::get('datatable', 'StokController@datatable')->name('stok.datatable');
        Route::get('detail/{kode}', 'StokController@detail')->name('stok.detail');
        Route::post('datatable', 'StokController@datatable_detail')->name('stok.datatable_detail');
        Route::post('detail_stok_mk', 'StokController@detail_stok_mk')->name('stok.detail_stok_mk');
        Route::get('excel_sj/{tgl}','StokController@excel_sj')->name('stok.excel_sj');
    });

    Route::group(['prefix' => 'stok-opname'], function () {
        Route::get('/', 'StokOpnameController@index')->name('stokOpname.index');
        Route::get('datatable', 'StokOpnameController@datatable')->name('stokOpname.datatable');
        Route::get('form', 'StokOpnameController@form')->name('stokOpname.form');
        Route::post('save', 'StokOpnameController@save')->name('stokOpname.save');
        Route::get('form_edit/{id}', 'StokOpnameController@form_edit')->name('stokOpname.form_edit');
        Route::get('delete', 'StokOpnameController@delete')->name('stokOpname.delete');
        Route::get('datatable_brg','StokOpnameController@datatable_brg')->name('stokOpname.datatable_brg');
        Route::post('datatable_brg_detail','StokOpnameController@datatable_brg_detail')->name('stokOpname.datatable_brg_detail');
        Route::post('detele_item', 'StokOpnameController@delete_item')->name('stokOpname.delete_item');
        Route::get('nota/{id}', 'StokOpnameController@nota')->name('stokOpname.nota');
        Route::get('sj/{id}', 'StokOpnameController@sj')->name('stokOpname.sj');
        Route::post('barang_nota', 'StokOpnameController@barang_nota')->name('stokOpname.barang_nota');
        Route::post('total', 'StokOpnameController@total')->name('stokOpname.total');
        Route::post('add_pelanggan', 'StokOpnameController@add_pelanggan')->name('stokOpname.add_pelanggan');
        Route::post('select_pelanggan', 'StokOpnameController@select_pelanggan')->name('stokOpname.select_pelanggan');
        Route::get('excel_sj/{tgl}','StokOpnameController@excel_sj')->name('stokOpname.excel_sj');
        Route::post('delete_tgl', 'StokOpnameController@delete_tgl')->name('stokOpname.delete_tgl');
        Route::post('print_sj_count', 'StokOpnameController@print_sj_count')->name('stokOpname.print_sj_count');
        Route::post('datatable_detail', 'StokOpnameController@datatable_detail')->name('stokOpname.datatable_detail');
        Route::post('batal-opname', 'StokOpnameController@batal_opname')->name('stokOpname.batal_opname');
    });

    Route::group(['prefix' => 'jurnal-umum'], function () {
        Route::get('/', 'JurnalUmumController@index')->name('jurnalUmum.index');
        Route::get('form', 'JurnalUmumController@form')->name('jurnalUmum.form');
        Route::post('save', 'JurnalUmumController@save')->name('jurnalUmum.save');
        Route::get('datatable', 'JurnalUmumController@datatable')->name('jurnalUmum.datatable');
        Route::post('datatable_detail', 'JurnalUmumController@datatable_detail')->name('jurnalUmum.datatable_detail');
        Route::get('datatable_akun_debit', 'JurnalUmumController@datatable_akun_debit')->name('jurnalUmum.datatable_akun_debit');
        Route::get('datatable_akun_kredit', 'JurnalUmumController@datatable_akun_kredit')->name('jurnalUmum.datatable_akun_kredit');
        Route::get('no','JurnalUmumController@no_urut')->name('no_urut_jurnalUmum');
        Route::post('delete', 'JurnalUmumController@delete')->name('jurnalUmum.delete');
    });

    Route::group(['prefix' => 'pembelian'], function () {
        Route::get('/', 'PembelianController@index')->name('pembelian.index');
        Route::get('datatable', 'PembelianController@datatable')->name('pembelian.datatable');
        Route::get('form', 'PembelianController@form')->name('pembelian.form');
        Route::post('save', 'PembelianController@save')->name('pembelian.save');
        Route::get('form-edit/{id}', 'PembelianController@form_edit')->name('pembelian.form_edit');
        Route::post('update', 'PembelianController@update')->name('pembelian.update');
        Route::post('delete', 'PembelianController@delete')->name('pembelian.delete');
        Route::get('datatable_brg','PembelianController@datatable_brg')->name('pembelian.datatable_brg');
        Route::get('no','PembelianController@no_urut')->name('no_urut_beli');
        Route::post('datatable_detail','PembelianController@datatable_detail')->name('pembelian.datatable_detail');
        Route::post('detele_item', 'PembelianController@delete_item')->name('pembelian.delete_item');
        Route::post('add_brg', 'PembelianController@add_brg')->name('add_brg.save');
        Route::post('simpan_bayar', 'PembelianController@simpan_bayar')->name('pembelian.simpan_bayar');
        Route::get('datatable-akun', 'PembelianController@datatable_akun')->name('pembelian.datatable_akun');
    });

    Route::group(['prefix' => 'absen'], function () {
        Route::get('/', 'AbsenController@index')->name('absen.index');
        Route::get('datatable', 'AbsenController@datatable')->name('absen.datatable');
        Route::get('form', 'AbsenController@form')->name('absen.form');
        Route::post('save', 'AbsenController@save')->name('absen.save');
        Route::post('get-nama', 'AbsenController@get_nama')->name('get_nama');
        Route::post('add_absen', 'AbsenController@add_absen')->name('absen.add_absen');
        Route::post('tampil_absen', 'AbsenController@tampil_absen')->name('absen.tampil_absen');
        Route::get('form-edit/{id}', 'AbsenController@form_edit')->name('absen.form_edit');
        Route::post('get-absen', 'AbsenController@get_absen')->name('get_absen.json');
        Route::post('delete', 'AbsenController@delete')->name('absen.delete');
        Route::post('delete-item-absen', 'AbsenController@delete_item_absen')->name('absen.delete_item_absen');
    });

     // cek
     Route::group(['prefix' => 'cek-beli'], function () {
        Route::get('/', 'CekBeliController@index')->name('cekbeli.index');
        Route::get('datatable', 'CekBeliController@datatable')->name('cekbeli.datatable');
        Route::post('datatable_beli', 'CekBeliController@datatable_beli')->name('cekbeli.datatable_beli');
        Route::post('acc_beli', 'CekBeliController@acc_beli')->name('cekbeli.acc_beli');
        Route::get('nota/{id}', 'CekBeliController@nota')->name('cekbeli.nota');
        Route::get('sj/{id}', 'CekBeliController@sj')->name('cekbeli.sj');
        Route::get('delete', 'CekBeliController@delete')->name('cekbeli.delete');
        Route::post('batal_suratjalan', 'CekBeliController@batal_suratjalan')->name('cekbeli.batal_suratjalan');
        Route::post('print_sj_count', 'CekBeliController@print_sj_count')->name('cekbeli.print_sj_count');
    });

    //suplier
    Route::group(['prefix' => 'suplier'], function ()  {
        Route::get('/', 'SuplierController@index')->name('suplier.index');
        Route::get('datatable', 'SuplierController@datatable')->name('suplier.datatable');
        Route::get('form', 'SuplierController@form')->name('suplier.form');
        Route::post('save', 'SuplierController@save')->name('suplier.save');
        Route::get('form-edit/{id}', 'SuplierController@form_edit')->name('suplier.form_edit');
        Route::post('update', 'SuplierController@update')->name('suplier.update');
        Route::post('delete', 'SuplierController@delete')->name('suplier.delete');        
    });

    //Kwitansi
    Route::group(['prefix' => 'kwitansi'], function () {
        Route::get('/', 'KwitansiController@index')->name('kwitansi.index');
        Route::get('datatable', 'KwitansiController@datatable')->name('kwitansi.datatable');
        Route::get('form', 'KwitansiController@form')->name('kwitansi.form');
        Route::post('save', 'KwitansiController@save')->name('kwitansi.save');
        Route::get('form-edit/{id}', 'KwitansiController@form_edit')->name('kwitansi.form_edit');
        Route::post('update', 'KwitansiController@update')->name('kwitansi.update');
        Route::post('delete', 'KwitansiController@delete')->name('kwitansi.delete');
        Route::get('datatable_brg','KwitansiController@datatable_brg')->name('kwitansi.datatable_brg');
        Route::get('no','KwitansiController@no_urut')->name('no_urut_kwi');
        Route::post('datatable_detail','KwitansiController@datatable_detail')->name('kwitansi.datatable_detail');
        Route::post('detele_item', 'KwitansiController@delete_item')->name('kwitansi.delete_item');
        Route::post('add_brg', 'KwitansiController@add_brg')->name('add_barang.save');
        Route::post('simpan_bayar', 'KwitansiController@simpan_bayar')->name('kwitansi.simpan_bayar');
        Route::get('print/{id}', 'KwitansiController@print')->name('kwitansi.print');
        Route::post('print_kwi_count', 'KwitansiController@print_kwi_count')->name('kwitansi.print_kwi_count');
    });

    //peralatan
    Route::group(['prefix' => 'peralatan'], function ()  {
        Route::get('/', 'PeralatanController@index')->name('peralatan.index');
        Route::get('datatable', 'PeralatanController@datatable')->name('peralatan.datatable');
        Route::get('form', 'PeralatanController@form')->name('peralatan.form');
        Route::post('save', 'PeralatanController@save')->name('peralatan.save');
        Route::get('form-edit/{id}', 'PeralatanController@form_edit')->name('peralatan.form_edit');
        Route::post('update', 'PeralatanController@update')->name('peralatan.update');
        Route::post('delete', 'PeralatanController@delete')->name('peralatan.delete');        
    });

    //Inventaris
    Route::group(['prefix' => 'inventaris'], function ()  {
        Route::get('/', 'InventarisController@index')->name('inventaris.index');
        Route::get('datatable', 'InventarisController@datatable')->name('inventaris.datatable');
        Route::get('form', 'InventarisController@form')->name('inventaris.form');
        Route::post('save', 'InventarisController@save')->name('inventaris.save');
        Route::get('form-edit/{id}', 'InventarisController@form_edit')->name('inventaris.form_edit');
        Route::post('update', 'InventarisController@update')->name('inventaris.update');
        Route::post('delete', 'InventarisController@delete')->name('inventaris.delete');        
    });

    //Bangunan
    Route::group(['prefix' => 'bangunan'], function ()  {
        Route::get('/', 'BangunanController@index')->name('bangunan.index');
        Route::get('datatable', 'BangunanController@datatable')->name('bangunan.datatable');
        Route::get('form', 'BangunanController@form')->name('bangunan.form');
        Route::post('save', 'BangunanController@save')->name('bangunan.save');
        Route::get('form-edit/{id}', 'BangunanController@form_edit')->name('bangunan.form_edit');
        Route::post('update', 'BangunanController@update')->name('bangunan.update');
        Route::post('delete', 'BangunanController@delete')->name('bangunan.delete');        
    });

     //Instalasi
     Route::group(['prefix' => 'instalasi'], function ()  {
        Route::get('/', 'InstalasiController@index')->name('instalasi.index');
        Route::get('datatable', 'InstalasiController@datatable')->name('instalasi.datatable');
        Route::get('form', 'InstalasiController@form')->name('instalasi.form');
        Route::post('save', 'InstalasiController@save')->name('instalasi.save');
        Route::get('form-edit/{id}', 'InstalasiController@form_edit')->name('instalasi.form_edit');
        Route::post('update', 'InstalasiController@update')->name('instalasi.update');
        Route::post('delete', 'InstalasiController@delete')->name('instalasi.delete');        
    });

    // claimabsen
    Route::group(['prefix' => 'claim-absen'], function () {
        Route::get('/', 'claimAbsenController@index')->name('claimabsen.index');
        Route::get('datatable', 'claimAbsenController@datatable')->name('claimabsen.datatable');
        Route::post('datatable-detail-absen', 'claimAbsenController@datatable_detail_absen')->name('claimabsen.datatable_detail_absen');
        Route::get('form', 'claimAbsenController@form')->name('claimabsen.form');
        Route::post('save', 'claimAbsenController@save')->name('claimabsen.save');
        Route::post('get-nama', 'claimAbsenController@get_nama')->name('get_nama');
        Route::post('add_absen', 'claimAbsenController@add_absen')->name('claimabsen.add_absen');
        Route::post('tampil_absen', 'claimAbsenController@tampil_absen')->name('claimabsen.tampil_absen');
        Route::get('form-edit/{id}', 'claimAbsenController@form_edit')->name('claimabsen.form_edit');
        Route::post('get-absen', 'claimAbsenController@get_absen')->name('get_claimabsen.json');
        Route::post('delete', 'claimAbsenController@delete')->name('claimabsen.delete');
        Route::post('delete-item-absen', 'claimAbsenController@delete_item_absen')->name('claimabsen.delete_item_absen');
        Route::post('simpan-claim', 'claimAbsenController@simpan_claim')->name('claimabsen.simpan_claim');
    });

    // gaji
    Route::group(['prefix' => 'gaji'], function () {
        Route::get('/', 'GajiController@index')->name('gaji.index');
        Route::get('datatable', 'GajiController@datatable')->name('gaji.datatable');
        Route::post('datatable-detail-gaji', 'GajiController@datatable_detail_gaji')->name('gaji.datatable_detail_gaji');
        Route::post('save', 'GajiController@save')->name('gaji.save');
        Route::post('get-nama', 'GajiController@get_nama')->name('get_nama');
        Route::post('tampil_absen', 'GajiController@tampil_absen')->name('gaji.tampil_absen');
        Route::get('form-edit/{id}', 'GajiController@form_edit')->name('gaji.form_edit');
        Route::get('detail-gaji/{id}', 'GajiController@detail_gaji')->name('gaji.detail_gaji');
        Route::post('get-absen', 'GajiController@get_absen')->name('get_gaji.json');
        Route::post('delete', 'GajiController@delete')->name('gaji.delete');
        Route::post('delete-item-absen', 'GajiController@delete_item_absen')->name('gaji.delete_item_absen');
        Route::post('simpan-claim', 'GajiController@simpan_claim')->name('gaji.simpan_claim');
        Route::get('gaji_mingguan/{tgl}', 'GajiController@gaji_mingguan')->name('gaji.gaji_mingguan');
        Route::post('datatable-gaji_mingguan', 'GajiController@datatable_gaji_mingguan')->name('gaji.datatable_gaji_mingguan');
        Route::get('excel_gaji/{tgl}','GajiController@excel_gaji')->name('gaji.excel_gaji');
        Route::post('buat-jurnal', 'GajiController@buat_jurnal')->name('gaji.buat_jurnal');
    });

    // cek Opname
    Route::group(['prefix' => 'cek-opname'], function () {
        Route::get('/', 'CekOpnameController@index')->name('cekopname.index');
        Route::get('datatable', 'CekOpnameController@datatable')->name('cekopname.datatable');
        Route::get('cek-detail-opname/{tgl}', 'CekOpnameController@cek_detail_opname')->name('cekopname.cek_detail_opname');
        Route::post('datatable-detail-opname', 'CekOpnameController@datatable_detail_opname')->name('cekopname.datatable_detail_opname');
        Route::post('cek-opname', 'CekOpnameController@cek_opname')->name('cekopname.cek_opname');
    });

    // cek Kwitansi
    Route::group(['prefix' => 'cek-kwitansi'], function () {
        Route::get('/', 'CekKwitansiController@index')->name('cekkwitansi.index');
        Route::get('datatable', 'CekKwitansiController@datatable')->name('cekkwitansi.datatable');
        Route::post('datatable_kwitansi', 'CekKwitansiController@datatable_kwitansi')->name('cekkwitansi.datatable_kwitansi');
        Route::post('acc_kwitansi', 'CekKwitansiController@acc_kwitansi')->name('cekkwitansi.acc_kwitansi');
        Route::get('nota/{id}', 'CekKwitansiController@nota')->name('cekkwitansi.nota');
        Route::get('sj/{id}', 'CekKwitansiController@sj')->name('cekkwitansi.sj');
        Route::get('delete', 'CekKwitansiController@delete')->name('cekkwitansi.delete');
        Route::post('batal_suratjalan', 'CekKwitansiController@batal_suratjalan')->name('cekkwitansi.batal_suratjalan');
        Route::post('print_sj_count', 'CekKwitansiController@print_sj_count')->name('cekkwitansi.print_sj_count');
    });

    //neraca
    Route::group(['prefix' => 'neraca'], function () {
        Route::get('/', 'NeracaController@index')->name('neraca.index');
        Route::post('datatable-aktiva', 'NeracaController@datatable_aktiva')->name('neraca.datatable_aktiva');
        Route::post('datatable-pasiva', 'NeracaController@datatable_pasiva')->name('neraca.datatable_pasiva');
        Route::post('datatable-pendapatan', 'NeracaController@datatable_pendapatan')->name('neraca.datatable_pendapatan');
        Route::post('datatable-hpp', 'NeracaController@datatable_hpp')->name('neraca.datatable_hpp');
        Route::post('datatable-beban', 'NeracaController@datatable_beban')->name('neraca.datatable_beban');
        Route::get('excel_neraca/{tgl}','NeracaController@excel_neraca')->name('neraca.excel_neraca');
        Route::post('cek-tgl', 'NeracaController@cek_tgl')->name('neraca.cek_tgl');
    });

    //laba-rugi
    Route::group(['prefix' => 'laba-rugi'], function () {
        Route::get('/', 'LabarugiController@index')->name('labarugi.index');
        Route::post('datatable-pendapatan', 'LabarugiController@datatable_pendapatan')->name('labarugi.datatable_pendapatan');
        Route::post('datatable-hpp', 'LabarugiController@datatable_hpp')->name('labarugi.datatable_hpp');
        Route::post('datatable-beban', 'LabarugiController@datatable_beban')->name('labarugi.datatable_beban');
        Route::get('excel_labarugi/{tgl}','LabarugiController@excel_labarugi')->name('labarugi.excel_labarugi');
        Route::get('pdf_labarugi/{tgl}','LabarugiController@pdf_labarugi')->name('labarugi.pdf_labarugi');
    });

    //laporan 
    Route::group(['prefix' => 'laporan'], function () {
        Route::get('/', 'LaporanController@index')->name('laporan.index');

        Route::get('lap_jurnal', 'LaporanController@lap_jurnal')->name('laporan.lap_jurnal');
        Route::post('table-jurnal', 'NeracaController@table_jurnal')->name('laporan.table_jurnal');

        //penjualan
        Route::get('lap_penjualan', 'LaporanController@lap_penjualan')->name('laporan.lap_penjualan');
        Route::post('table-penjualan', 'LaporanController@table_penjualan')->name('laporan.table_penjualan');
        
        Route::get('lap-penjualan-barang', 'LaporanController@lap_penjualan_barang')->name('laporan.lap_penjualan_barang');
        Route::post('table-penjualan-barang', 'LaporanController@table_penjualan_barang')->name('laporan.table_penjualan_barang');

        Route::get('lap-penjualan-pelanggan', 'LaporanController@lap_penjualan_pelanggan')->name('laporan.lap_penjualan_pelanggan');
        Route::post('table-penjualan-pelanggan', 'LaporanController@table_penjualan_pelanggan')->name('laporan.table_penjualan_pelanggan');

        Route::get('lap-piutang-pelanggan', 'LaporanController@lap_piutang_pelanggan')->name('laporan.lap_piutang_pelanggan');
        Route::post('table-piutang-pelanggan', 'LaporanController@table_piutang_pelanggan')->name('laporan.table_piutang_pelanggan');

        //pembelian
        Route::get('lap_pembelian', 'LaporanController@lap_pembelian')->name('laporan.lap_pembelian');
        Route::post('table-pembelian', 'LaporanController@table_pembelian')->name('laporan.table_pembelian');
        
        Route::get('lap-pembelian-barang', 'LaporanController@lap_pembelian_barang')->name('laporan.lap_pembelian_barang');
        Route::post('table-pembelian-barang', 'LaporanController@table_pembelian_barang')->name('laporan.table_pembelian_barang');

        Route::get('lap-pembelian-suplier', 'LaporanController@lap_pembelian_suplier')->name('laporan.lap_pembelian_suplier');
        Route::post('table-pembelian-suplier', 'LaporanController@table_pembelian_suplier')->name('laporan.table_pembelian_suplier');

        Route::get('lap-pembelian-hutang', 'LaporanController@lap_pembelian_hutang')->name('laporan.lap_pembelian_hutang');
        Route::post('table-pembelian-hutang', 'LaporanController@table_pembelian_hutang')->name('laporan.table_pembelian_hutang');
    });

    // Tutup Buku
    Route::group(['prefix' => 'tutup-buku'], function () {
        Route::get('/', 'TutupBukuController@index')->name('tutupBuku.index');
        Route::post('simpan_tutupbuku', 'TutupBukuController@simpan_tutupbuku')->name('tutupBuku.simpan_tutupbuku');
        Route::post('datatable_tutupbuku', 'TutupBukuController@datatable_tutupbuku')->name('tutupBuku.datatable_tutupbuku');

        Route::get('bukabuku', 'TutupBukuController@bukabuku')->name('bukaBuku.index');
        Route::post('simpan_bukabuku', 'TutupBukuController@simpan_bukabuku')->name('bukaBuku.simpan_bukabuku');
        Route::post('datatable_bukabuku', 'TutupBukuController@datatable_bukabuku')->name('bukaBuku.datatable_bukabuku');

    });

    //Buku Besar
    Route::group(['prefix' => 'buku-besar'], function () {
        Route::get('/', 'BukubesarController@index')->name('bukubesar.index');
        Route::post('datatable', 'BukubesarController@datatable')->name('bukubesar.datatable');
        Route::get('excel_bukubesar/{tgl}','BukubesarController@excel_bukubesar')->name('bukubesar.excel_bukubesar');
    });

    //Transaksi
    Route::group(['prefix' => 'transaksi'], function () {
        Route::get('/', 'TransaksiController@index')->name('transaksi.index');
        Route::get('datatable', 'TransaksiController@datatable')->name('transaksi.datatable');
        Route::get('form', 'TransaksiController@form')->name('transaksi.form');
        Route::get('datatable_sj', 'TransaksiController@datatable_sj')->name('transaksi.datatable_sj');
        Route::post('save', 'TransaksiController@save')->name('transaksi.save');

    });

    //setor 
    Route::group(['prefix' => 'setor'], function () {
        Route::get('/', 'SetorController@index')->name('setor.index');
        Route::post('simpan_setor', 'SetorController@simpan_setor')->name('setor.simpan');
        Route::get('datatable', 'SetorController@datatable')->name('setor.datatable');
    });







































    // Route::group(['prefix' => 'suplier'], function () {
        //     Route::get('/', 'SuplierController@index')->name('suplier.index');
        //     Route::get('datatable', 'SuplierController@datatable')->name('suplier.datatable');
        //     Route::get('form', 'SuplierController@form')->name('suplier.form');
        //     Route::post('save', 'SuplierController@save')->name('suplier.save');
        //     // Route::get('delete/{id}', 'AdminController@delete_shift')->name('suplier.delete');
        // });
        
        // Route::group(['prefix' => 'permintaan'], function () {
            //     Route::get('/', 'PermintaanController@index')->name('permintaan.index');
            //     Route::get('datatable', 'PermintaanController@datatable')->name('permintaan.datatable');
            //     Route::get('form', 'PermintaanController@form')->name('permintaan.form');
            //     Route::post('save', 'PermintaanController@save')->name('permintaan.save');
            //     Route::post('detail', 'PermintaanController@permintaan_barang')->name('permintaan.detail');
            //     Route::post('delete-brg', 'PermintaanController@delete_barang')->name('permintaan_brg.delete');
            // });
            // Route::get('/welcome', function () {
                //     return view('welcome');
                // });        
                // Route::get('/', 'HomeController@index')->name('dashboard.index');
});
